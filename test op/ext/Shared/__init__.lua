class "TestShared"

local g_Logger = require "__shared/Logger"

function TestShared:__init()
  g_Logger:Write("TestShared initializing")
  
  self:RegisterVars()
  self:RegisterEvents()
end

function TestShared:RegisterVars()
  g_Logger.debug = true
end

function TestShared:RegisterEvents()
  Events:Subscribe('Partition:ReadInstance', self, self.OnReadInstance)
end

function TestShared:OnReadInstance(p_Instance, p_PartitionGuid, p_Guid)
  if p_Instance == nil then
    return
  end

  if p_Instance.typeName == "SoldierWeaponData" then
    local s_Instance = SoldierWeaponData(p_Instance)
    
    local s_Name = tostring(s_Instance.damageGiverName)

    if string.find(s_Name:lower(), "m16a4") ~= nil then
      g_Logger:Write("Found m16a4, modifying...")
      self:Start(s_Instance, p_Guid)
    end
  end
end

function TestShared:Start(p_Instance, p_Guid)
  local s_Count = p_Instance:GetWeaponModifierDataCount()

  if s_Count == 0 then
    return
  end

  s_Count = s_Count - 1

  g_Logger:Write(p_Guid:ToString("D"))
  g_Logger:Write("Got " .. tostring(s_Count) .. " modifiers.")

  for i = s_Count, 0, -1 do 
    local s_WeaponModifierData = p_Instance:GetWeaponModifierDataAt(i)

    local s_UnlockAssetBase = s_WeaponModifierData.unlockAsset
    g_Logger:Write(tostring(i))
    g_Logger:Write(s_UnlockAssetBase.typeName)
  end
end

local g_TestShared = TestShared()
return TestShared