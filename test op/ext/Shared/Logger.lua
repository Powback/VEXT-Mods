class "Logger"
Logger.debug = true
function Logger:Write(p_Message)
	if(self.debug == false) then
		return
	end
	-- only print when debug flag is set. probably have to split client and server logging.
	-- Future versions automatically add the [] tag, but let's keep it for now
	print("[RealityMod] " .. p_Message)
		
	--WebUI:ExecuteJS(string.format('Debug("%s")', p_Message))
end

return Logger