class "TestServer"

local g_Logger = require "__shared/Logger"

function TestServer:__init()
	g_Logger:Write("TestServer Initializing")
  g_Logger.debug = true
end


local g_TestServer = TestServer()
return TestServer