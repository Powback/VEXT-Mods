class 'animationrestrictionServer'


function animationrestrictionServer:__init()
	print("Initializing animationrestrictionServer")
	self:RegisterVars()
	self:RegisterEvents()
end


function animationrestrictionServer:RegisterVars()
	--self.m_this = that
end


function animationrestrictionServer:RegisterEvents()
	self.m_ChatEvent = Events:Subscribe("Player:Chat", self, self.OnChat)
	self.m_Whatever = Hooks:Install('ServerEntityFactory:Create',999, self, self.OnEntityCreate)
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)

end


function animationrestrictionServer:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeInfo.name == "MultipleActorrestrictionEntityData") then
			local s_Instance = MultipleActorrestrictionEntityData(l_Instance)
			print("IO")
			print(p_Partition.name)
		end
	end
end

function animationrestrictionServer:OnChat(p_Player, p_Mask, p_Message)
	--print("[kBot] " .. p_Player.name .. ": " .. p_Message)
	if p_Player == nil then
		return
	end

	local restriction = InputRestrictionEntityData()
    restriction.enabled = true
    restriction.overridePreviousInputRestriction = true
    restriction.applyRestrictionsToSpecificPlayer = true
    restriction.throttle = false
    restriction.strafe = false
    restriction.brake = false
    restriction.handBrake = false
    restriction.clutch = false
    restriction.yaw = false
    restriction.pitch = false
    restriction.roll = false
    restriction.fire = false
    restriction.fireCountermeasure = false
    restriction.altFire = false
    restriction.cycleRadioChannel = false
    restriction.selectMeleeWeapon = false
    restriction.zoom = false
    restriction.jump = false
    restriction.changeVehicle = false
    restriction.changeEntry = false
    restriction.changePose = false
    restriction.toggleParachute = false
    restriction.changeWeapon = false
    restriction.reload = false
    restriction.toggleCamera = false
    restriction.sprint = false
    restriction.scoreboardMenu = false
    restriction.mapZoom = false
    restriction.gearUp = false
    restriction.gearDown = false
    restriction.threeDimensionalMap = false
    restriction.giveOrder = false
    restriction.prone = false
    restriction.switchPrimaryInventory = false
    restriction.switchPrimaryWeapon = false
    restriction.grenadeLauncher = false
    restriction.staticGadget = false
    restriction.dynamicGadget1 = false
    restriction.dynamicGadget2 = false
    restriction.meleeAttack = false
    restriction.throwGrenade = false
    restriction.selectWeapon1 = false
    restriction.selectWeapon2 = false
    restriction.selectWeapon3 = false
    restriction.selectWeapon4 = false
    restriction.selectWeapon5 = false
    restriction.selectWeapon6 = false
    restriction.selectWeapon7 = false
    restriction.selectWeapon8 = false
    restriction.selectWeapon9 = false

    local objectEntity = EntityManager:CreateServerEntity(restriction, LinearTransform())
	objectEntity:Init(Realm.Realm_Server, true)
    objectEntity:FireEvent(ServerPlayerEvent("Activate", p_Player, false, false, false, false, false, TeamId.Team1))

end

function animationrestrictionServer:OnEntityCreate(p_Hook, p_Data, p_Transform)
	if p_Data == nil then
		print("Didnt get no data")
	else
		--print("spawned: " .. p_Data.typeInfo.name .. " - " .. tostring(p_Data.instanceGuid))
	end	 
end


function split(pString, pPattern)
   local Table = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pPattern
   local last_end = 1
   local s, e, cap = pString:find(fpat, 1)
   while s do
	  if s ~= 1 or cap ~= "" then
	 table.insert(Table,cap)
	  end
	  last_end = e+1
	  s, e, cap = pString:find(fpat, last_end)
   end
   if last_end <= #pString then
	  cap = pString:sub(last_end)
	  table.insert(Table, cap)
   end
   return Table
end
g_animationrestrictionServer = animationrestrictionServer()

