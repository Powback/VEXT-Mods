class 'animationscenarioClient'


function animationscenarioClient:__init()
	print("Initializing animationscenarioClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function animationscenarioClient:RegisterVars()
	--self.m_this = that
end


function animationscenarioClient:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
	self.m_Whatever = Hooks:Install('ClientEntityFactory:Create',999, self, self.OnEntityCreate)
	NetEvents:Subscribe('Spawn:Anim', self, self.SpawnAnim)

end

function animationscenarioClient:SpawnAnim()
	print("ohg")
	local scenario = MultipleActorScenarioEntityData()
	scenario.worldSpaceConnectTransform = LinearTransform()
	scenario.worldSpace = false
	scenario.scenarioAntRef.assetId = -1
	scenario.levelChoice = -1
	scenario.scenarioChoice = -1
	scenario.aligningEnabled = false
	scenario.useInputEventPlayerAsPlayer1 = true
	scenario.actor1 = 0
	scenario.actor1Part = 0
	scenario.actor1SecondPart = -1
	scenario.actor2 = 1
	scenario.actor2Part = 0
	scenario.actor2SecondPart = -1
	scenario.actor3 = -1
	scenario.actor3Part = -1
	scenario.actor3SecondPart = -1
	scenario.actor4 = -1
	scenario.actor4Part = -1
	scenario.actor4SecondPart = -1
	scenario.actor5 = -1
	scenario.actor5Part = -1
	scenario.actor5SecondPart = -1
	scenario.actor6 = -1
	scenario.actor6Part = -1
	scenario.actor6SecondPart = -1
	scenario.scenarioActive = true
	scenario.actor1Prepared = true
	scenario.actor2Prepared = true
	scenario.actor3Prepared = true
	scenario.actor4Prepared = true
	scenario.actor5Prepared = true
	scenario.actor6Prepared = true
	scenario.checkActor1Finished = true
	scenario.checkActor2Finished = true
	scenario.checkActor3Finished = true
	scenario.checkActor4Finished = true
	scenario.checkActor5Finished = true
	scenario.checkActor6Finished = true
	scenario.externalTime = -1.0
	scenario.useExternalTime = false
	scenario.realm = Realm.Realm_Client
	scenario.scenarioAntRef = AntRef()

	local params = EntityCreationParams()
	params.transform = LinearTransform()
	params.networked = false

    local objectEntity = EntityManager:CreateClientEntity(scenario, params)
	objectEntity:Init(Realm.Realm_Client, true)

	local s_LocalPlayer = PlayerManager:GetLocalPlayer()
	local s_Players = PlayerManager:GetPlayers()
	objectEntity:FireEvent(ClientPlayerEvent("ForceStart", s_LocalPlayer))

end

function animationscenarioClient:OnEntityCreate(p_Hook, p_Data, p_Transform)
	if p_Data == nil then
		print("Didnt get no data")
	else
		print("spawned: " .. p_Data.typeInfo.name .. " - " .. tostring(p_Data.instanceGuid))
	end	 
end

function animationscenarioClient:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeInfo.Name == "SomeType") then
			local s_Instance = SomeType(l_Instance:Clone(l_Instance.instanceGuid))
			s_Instance.someThing = 1
			p_Partiton:ReplaceInstance(l_Instance, s_Instance, true)
		end
		if(l_Instance.instanceGuid == Guid("SomeGuid")) then
			local s_Instance = SomeType(l_Instance:Clone(l_Instance.instanceGuid))
			s_Instance.someThing = 1
			p_Partiton:ReplaceInstance(l_Instance, s_Instance, true)
		end
	end
end


g_animationscenarioClient = animationscenarioClient()

