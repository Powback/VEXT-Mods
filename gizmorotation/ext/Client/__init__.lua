class 'gizmorotationClient'


function gizmorotationClient:__init()
	print("Initializing gizmorotationClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function gizmorotationClient:RegisterVars()
	--self.m_this = that
end


function gizmorotationClient:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
	self.m_ClientUpdateInputEvent = Events:Subscribe('Client:UpdateInput', self, self.OnUpdateInput)
	self.m_ExtensionLoadedEvent = Events:Subscribe('ExtensionLoaded', self, self.OnLoaded)
end
function gizmorotationClient:OnLoaded()
	WebUI:Init()
	WebUI:Show()
end

function gizmorotationClient:OnUpdateInput(p_Hook, p_Cache, p_DeltaTime)
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_V) then
	   	local transform = LinearTransform()
		local left = transform.left
		local up = transform.up
		local forward = transform.forward
		WebUI:ExecuteJS(string.format('editor.webGL.UpdateCameraPos(%s, %s, %s);', 0,0,0))
		WebUI:ExecuteJS(string.format('editor.webGL.UpdateCameraAngle(%s, %s, %s,%s, %s, %s,%s, %s, %s);', left.x, left.y, left.z,up.x, up.y, up.z,forward.x, forward.y, forward.z))
		WebUI:ExecuteJS('vext.SpawnedEntity(1, "F17C9834-6CE0-D963-6911-926921A3FF24", "1,0,0,0,1,0,0,0,1,0, 0, -5");')
    end

    if InputManager:WentKeyDown(InputDeviceKeys.IDK_F) then
		local transform = ClientUtils:GetCameraTransform()
		local s_Left = transform.left
		local s_Up = transform.up
		local s_Forward = transform.forward
		local s_Pos = Vec3(0,0,-5)

		local s_LinearTransformString = string.format('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s', 
			s_Left.x, s_Left.y, s_Left.z, s_Up.x, s_Up.y, s_Up.z, s_Forward.x, s_Forward.y, s_Forward.z, s_Pos.x, s_Pos.y, s_Pos.z )

        
		WebUI:ExecuteJS('console.log("' .. s_LinearTransformString .. '");')
		WebUI:ExecuteJS('editor.UpdateSelectedObject(\"' .. s_LinearTransformString .. '\");')
    end
end
g_gizmorotationClient = gizmorotationClient()

