class 'typeinfoShared'


event = nil

function OnInstance(instance, partitionGuid, guid)
    print('Partition type: ' .. instance.typeName)
    print('Reflection type: ' .. instance.typeInfo.name)
    print('Real reflection type: ' .. _G[instance.typeName].typeInfo.name)
    local typeInfo = AdsrNodeData.typeInfo

	print('Type Name: ' .. typeInfo.name)

	if typeInfo.super ~= nil then
	    print('Super Class: ' .. typeInfo.super.name)
	end

	print('Is Array: ' .. tostring(typeInfo.array))

	if typeInfo.array then
	    print('Array Element Type: ' .. typeInfo.elementType.name)
	end

	print('Is Enum: ' .. tostring(typeInfo.enum))

	for _, field in ipairs(typeInfo.fields) do
	    print('Field Name: ' .. field.name)   
	    if field.typeInfo ~= nil then
	        print('Field Type: ' .. field.typeInfo.name)
	    end
	end

    event:Unsubscribe()
end

event = Events:Subscribe('Partition:ReadInstance', OnInstance)

----




g_typeinfoShared = typeinfoShared()

return typeinfoShared
