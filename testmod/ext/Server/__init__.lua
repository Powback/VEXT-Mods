class 'testmodServer'


function testmodServer:__init()
	print("Initializing testmodServer")
	self:RegisterVars()
	self:RegisterEvents()
end


function testmodServer:RegisterVars()
	--self.m_this = that
end


function testmodServer:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
		Events:Subscribe("Player:Chat", self, self.OnChat)

end


function testmodServer:ReadInstance(p_Instance,p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	
	if p_Instance.typeName == "SomeType" then
		local s_Instance = SomeType(p_Instance)
		s_Instance.someThing = true
	end
	
	
	if p_Guid == Guid("Some-Guid", "D") then
		local s_Instance = SomeClass(p_Instance)
		s_Instance.someThing = true
	end
end

function testmodServer:OnChat(p_Player, p_Mask, p_Message)

  if p_Player == nil then
    return
  end

  if p_Message == "t" then
	  local soldier = p_Player.soldier
	  if soldier == nil then
	    return
	  end
	  print(tostring(soldier.transform))
	  soldier:SetPosition(soldier.transform.trans, false)

		soldier = p_Player.soldier
	  print(tostring(soldier.transform))
  end

end

g_testmodServer = testmodServer()

