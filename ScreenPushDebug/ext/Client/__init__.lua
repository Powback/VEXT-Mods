class 'ScreenPushDebugClient'


function ScreenPushDebugClient:__init()
	print("Initializing ScreenPushDebugClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function ScreenPushDebugClient:RegisterVars()
	--self.m_this = that
end


function ScreenPushDebugClient:RegisterEvents()
	Hooks:Install('UI:PushScreen', 999, self, self.OnPushScreen)
end


function ScreenPushDebugClient:OnPushScreen(p_Hook, p_Screen, p_GraphPriority, p_ParentGraph)
	local s_Screen = UIScreenAsset(p_Screen)

	print(string.format("Pushing screen '%s'", s_Screen.name))

	p_Hook:Next()
end


g_ScreenPushDebugClient = ScreenPushDebugClient()

