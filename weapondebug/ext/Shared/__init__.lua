class 'weapondebugShared'


function weapondebugShared:__init()
	print("Initializing weapondebugShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function weapondebugShared:RegisterVars()
	--self.m_this = that
end


function weapondebugShared:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end


function weapondebugShared:ReadInstance(p_Instance,p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	
	if p_Instance.typeName == "SoldierWeaponBlueprint" then
		local s_Instance = SoldierWeaponBlueprint(p_Instance)
		print(s_Instance.name)
	end

end


g_weapondebugShared = weapondebugShared()

