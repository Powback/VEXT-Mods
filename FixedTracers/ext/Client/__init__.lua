class 'FixedTracersClient'


function FixedTracersClient:__init()
	print("Initializing FixedTracersClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function FixedTracersClient:RegisterVars()
	--self.m_this = that
end


function FixedTracersClient:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end


function FixedTracersClient:ReadInstance(p_Instance, p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	if p_Instance.typeName == "EmitterEntityData" then
		local s_Instance = EmitterEntityData(p_Instance)
		if(s_Instance.startDelay == 0) then
			s_Instance.startDelay = 0.04
		end
	end
	
end


g_FixedTracersClient = FixedTracersClient()

