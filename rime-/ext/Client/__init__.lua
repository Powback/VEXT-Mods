class "RimeClient"

function RimeClient:__init()
	self.m_UpdateEvent = Events:Subscribe("Engine:Update", self, self.OnUpdate)
end

function RimeClient:OnUpdate(p_Delta, p_SimulationDelta)
	local s_Player = PlayerManager:GetLocalPlayer()
	if s_Player == nil then
		return
	end
	
	--print("Player: " .. s_Player.name .. " K: " .. s_Player:GetKills() .. " D: " .. s_Player:GetDeaths() .. "Time: " .. s_Player:GetTime())
	
	local s_Soldier = s_Player.soldier
	if s_Soldier == nil then
		return
	end
	
	local s_WeaponName = s_Soldier:GetCurrentWeaponName()
	local s_WeaponSlot = s_Soldier:GetCurrentWeaponSlot()
	local s_PriAmmoCount = s_Soldier:GetCurrentPrimaryAmmo()
	local s_SecAmmoCount = s_Soldier:GetCurrentSecondaryAmmo()
	
	print("Weapon " .. s_WeaponName .. " in slot " .. s_WeaponSlot .. " " .. s_PriAmmoCount .. " - " .. s_SecAmmoCount)
	
	s_WeaponName = s_Soldier:GetEquipmentName()
	s_WeaponSlot = 3
	s_PriAmmoCount = s_Soldier:GetEquipmentPrimaryAmmo()
	s_SecAmmoCount = s_Soldier:GetEquipmentSecondaryAmmo()
	
	print("Equipment " .. s_WeaponName .. " in slot " .. s_WeaponSlot .. " " .. s_PriAmmoCount .. " - " .. s_SecAmmoCount)
	
	s_WeaponName = s_Soldier:GetGadget1Name()
	s_WeaponSlot = 4
	s_PriAmmoCount = s_Soldier:GetGadget1PrimaryAmmo()
	s_SecAmmoCount = s_Soldier:GetGadget1SecondaryAmmo()
	
	print("Gadget1 " .. s_WeaponName .. " in slot " .. s_WeaponSlot .. " " .. s_PriAmmoCount .. " - " .. s_SecAmmoCount)
	
	s_WeaponName = s_Soldier:GetGadget2Name()
	s_WeaponSlot = 5
	s_PriAmmoCount = s_Soldier:GetGadget2PrimaryAmmo()
	s_SecAmmoCount = s_Soldier:GetGadget2SecondaryAmmo()
	
	print("Gadget2 " .. s_WeaponName .. " in slot " .. s_WeaponSlot .. " " .. s_PriAmmoCount .. " - " .. s_SecAmmoCount)

	
	s_PriAmmoCount = s_Soldier:GetGrenadePrimaryAmmo()
	s_SecAmmoCount = s_Soldier:GetGrenadeSecondaryAmmo()
	
	print("Grenade " .. s_PriAmmoCount .. " - " .. s_SecAmmoCount)
end

local g_AdminClient = RimeClient()