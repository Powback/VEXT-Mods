class 'letscrashShared'


function letscrashShared:__init()
	print("Initializing letscrashShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function letscrashShared:RegisterVars()
	--self.m_this = that
end


function letscrashShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end


function letscrashShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end

		
		if(l_Instance.typeInfo == RegistryContainer.typeInfo) then
			print("Touching registry")
			local s_Instance = RegistryContainer(l_Instance)
			print("Touched!")
		end


	end
end


g_letscrashShared = letscrashShared()

