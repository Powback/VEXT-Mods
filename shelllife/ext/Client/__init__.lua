class 'shelllifeClient'


function shelllifeClient:__init()
	print("Initializing shelllifeClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function shelllifeClient:RegisterVars()
	--self.m_this = that
end


function shelllifeClient:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end


function shelllifeClient:ReadInstance(p_Instance,p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	
	if p_Instance.typeName == "EmitterTemplateData" then
		local s_Instance = EmitterTemplateData (p_Instance)
		local s_Name = tostring(s_Instance.name:lower())
		if string.find(s_Name, "shells") then
			s_Instance.lifetime = s_Instance.lifetime * 5
			s_Instance.maxSpawnDistance = s_Instance.maxSpawnDistance * 5 
		end
	end
	if p_Instance.typeName == "AntAnimatableComponentData" then
		local s_Instance = AntAnimatableComponentData (p_Instance)
		s_Instance.animationControlledFromStart = true
		s_Instance.autoActivate = true
		s_Instance.forceDisableCulling = true  
		
	end

	if p_Instance.typeName == "CameraRecoilData" then
		local s_Instance = CameraRecoilData(p_Instance)
		s_Instance.springConstant = 0
		s_Instance.springDamping  = 0 
		s_Instance.springMinThresholdAngle  = 0 
	end
	if(p_Guid == Guid('F27FF9D5-A149-404D-992E-983BDE210312', 'D')) then
		local s_Instance = PhysicsDrivenAnimationEntityData(p_Instance)
		print(s_Instance.binding.jump.assetId)
		s_Instance.binding.jump.assetId = 357069682

		print(s_Instance.binding.crouch.assetId)
		s_Instance.binding.crouch.assetId = 357099611
	end
end


g_shelllifeClient = shelllifeClient()

