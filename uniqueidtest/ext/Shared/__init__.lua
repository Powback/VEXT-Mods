class 'uniqueidtestShared'


function uniqueidtestShared:__init()
	print("Initializing uniqueidtestShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function uniqueidtestShared:RegisterVars()
	--self.m_this = that
end


function uniqueidtestShared:RegisterEvents()
	Hooks:Install('ClientEntityFactory:Create',999, self, self.OnEntityCreate)
	Hooks:Install('ServerEntityFactory:Create',999, self, self.OnEntityCreate)
end

function uniqueidtestShared:OnEntityCreate(p_Hook, p_Data, p_Transform)
	if p_Data == nil then
		print("Didnt get no data")
	else
		local x = p_Hook:Call()
		if(x.uniqueID ~= 0) then
			print("unique: " .. x.uniqueID)
			print("instance: " .. x.instanceID)
		end
	end	 
end


g_uniqueidtestShared = uniqueidtestShared()

