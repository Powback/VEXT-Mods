class 'uniqueidShared'


function uniqueidShared:__init()
	print("Initializing uniqueidShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function uniqueidShared:RegisterVars()
	self.m_BlueprintInfo = {}
	self.m_Indention = 0
	self.m_ReferenceObjects = {}
	self.m_Lazy = {}
	self.m_Entities = {}
	self.m_Index = 0

	self.m_prefabs = {}
end


function uniqueidShared:RegisterEvents()
	--Hooks:Install('ClientEntityFactory:Create',999, self, self.OnEntityCreate)
	--Hooks:Install('ServerEntityFactory:Create',999, self, self.OnEntityCreate)
	--Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
	--Events:Subscribe('Server:LevelLoaded', self, self.OnLevelLoaded)
	Hooks:Install('ServerEntityFactory:CreateFromBlueprint', 999, self, self.OnEntityCreateFromBlueprint)
	Hooks:Install('ClientEntityFactory:CreateFromBlueprint', 999, self, self.OnEntityCreateFromBlueprint)
	self.m_LastPrefab = nil
end

function uniqueidShared:OnEntityCreateFromBlueprint(p_Hook, p_Blueprint, p_Transform, p_Variation, p_Parent )
	print("---------------")
	print(Blueprint(p_Blueprint).name)
	print(p_Transform)
	print(p_Variation)
	print(p_Parent)
	local x = p_Hook:Call(p_Blueprint, p_Transform, p_Variation, p_Parent)
	print(#x .. " | " .. tostring(p_Parent.instanceGuid))
	--for k,v in ipairs(x) do
	--	print(tostring(v.typeInfo.name))
	--end

		local a = {
		    x = 1,
		    y = 2,
		    z = 3,
		}

		local b = Vec3(a)
		print(b) -- (1.0, 2.0, 3.0
end


function uniqueidShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end

	local s_Instances = p_Partition.instances

    local s_PrimaryInstance = p_Partition.primaryInstance
    local s_Blueprint = false
    if(s_PrimaryInstance:Is("Blueprint") and s_PrimaryInstance.typeInfo.name ~= "WorldPartData" and s_PrimaryInstance.typeInfo.name ~= "SubWorldData") then
        s_Blueprint = true
    end


	for i, l_Instance in ipairs(s_Instances) do

        --if(s_Blueprint and l_Instance.typeInfo.name ~= "LevelData") then
            self.m_BlueprintInfo[tostring(l_Instance.instanceGuid)] = {
	            instanceGuid = p_Partition.primaryInstance.instanceGuid,
	            partitionGuid = p_Partition.guid
	        }
	   -- end

	    if(s_Blueprint == false and l_Instance.typeInfo.name == "ReferenceObjectData") then
	    	local s_Instance = ReferenceObjectData(l_Instance)
	    	table.insert(self.m_Lazy, {instanceGuid = tostring(s_Instance.instanceGuid), partitionGuid = tostring(p_Partition.guid)}) 
	    end
    end
end

function uniqueidShared:OnLevelLoaded()
	print("Level loading")
	for k,v in ipairs(self.m_Lazy) do
		local s_Blueprint = ReferenceObjectData(ResourceManager:FindInstanceByGUID(Guid(v.partitionGuid), Guid(v.instanceGuid)))
		if(s_Blueprint.blueprint.isLazyLoading) then
			print("fuckaroo")
		end
		print(Blueprint(s_Blueprint.blueprint).name)
		self.m_ReferenceObjects[tostring(s_Blueprint.blueprint.instanceGuid)] = self.m_Index
		self.m_Index = self.m_Index + 1
	end

	for k,v in ipairs(self.m_Entities) do
		if(self.m_ReferenceObjects[tostring(v.instanceGuid)] ~= nil) then
			local s_Blueprint = Blueprint(ResourceManager:FindInstanceByGUID(v.partitionGuid, v.instanceGuid))
			print(self.m_ReferenceObjects[tostring(v.instanceGuid)] .. " | " .. s_Blueprint.name)
		end
	end
end

function uniqueidShared:OnEntityCreate(p_Hook, p_Data, p_Transform)
    if p_Data == nil then
        print("Didnt get no data")
    else
    	if(p_Data:Is("ComponentData")) then
    		return
    	end
		local s_Data = _G[p_Data.typeInfo.name](p_Data)
        local s_BlueprintInfo = self.m_BlueprintInfo[tostring(p_Data.instanceGuid)]
        if(s_BlueprintInfo == nil) then
        	print("wtf" .. tostring(p_Data.instanceGuid))
        end
        local s_Blueprint = self:GetBlueprint(s_BlueprintInfo)
        local x = p_Hook:Call(p_Data, p_Transform)
        print(tostring(p_Data.instanceGuid) .. " | " .. s_Data.indexInBlueprint .. " | " .. p_Data.typeInfo.name .. " | " .. s_Blueprint.name)
    end
end

function uniqueidShared:GetBlueprint(p_BlueprintInfo)
   	local s_Instance = ResourceManager:FindInstanceByGUID(p_BlueprintInfo.partitionGuid, p_BlueprintInfo.instanceGuid)
   	if(s_Instance == nil) then
   		print("Failed to find partition: " .. tostring(p_BlueprintGuid))
   		return
   	end
   	local s_Blueprint = _G[s_Instance.typeInfo.name](s_Instance)
   	return s_Blueprint
end

function GenerateIndention( count )
	local s_Ret = ""
	for i= 0, count, 1 do
		--s_Ret = s_Ret .. "--"
	end
	return s_Ret
end

g_uniqueidShared = uniqueidShared()