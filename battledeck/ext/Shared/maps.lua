class 'Maps'

function Maps:__init()
	Hooks:Install('UI:PushScreen', self, self.OnPushasdScreen)
	Events:Subscribe('Partition:ReadInstance', self, self.OnReadInstance)
	Events:Subscribe('Level:LoadResources', self, self.OnLoadResources)
	Events:Subscribe('Player:Respawn', self, self.OnPlayerRespawn)

	self.m_SchematicGUID = Guid('E210FDBA-6024-A322-402D-9E2AABC07526', 'D')

	self.m_RegistryGUID = Guid('96EF016C-4246-27BF-E65F-D93E823EA96C', 'D')
	self.m_MeshDatabaseGUID = Guid('4F866DDE-8BF7-9C0C-B347-F9B2059AAD92', 'D')
	self.m_WorldPartDataGUID = Guid('0275C5F7-C49D-452C-B91E-32FDD9502D5D', 'D')

	self.m_Enable = false
	

end

function Maps:OnPushScreen(p_Hook, p_Screen, p_GraphPriority, p_ParentGraph)
	local s_Screen = UIScreenAsset(p_Screen)

	print(string.format("Pushing screen '%s'", s_Screen.name))

	if s_Screen.name == 'UI/Flow/Screen/CoopWaitingScreen' then
		return true
	end

	return p_Hook:CallOriginal(p_Screen, p_GraphPriority, p_ParentGraph)
end

function Maps:OnLoadResources(p_Dedicated)
	print("Loading level resources!")
	self.m_Enable = true
end

function Maps:OnReadInstance(p_Instance, p_GUID)
	if p_Instance == nil then
		return
	end

	if p_Instance.typeName == 'LevelData' then
		local s_Instance = LevelData(p_Instance)
		s_Instance.levelDescription.isCoop = false
		s_Instance.levelDescription.isMultiplayer = true
	end

	if p_Instance.typeName == 'LevelDescriptionAsset' then
		local s_Instance = LevelDescriptionAsset(p_Instance)

		
		s_Instance:ClearStartPoints()
		s_Instance.description.isMultiplayer = true
		s_Instance.description.isCoop = false

		local s_Category = LevelDescriptionInclusionCategory()
		s_Category.category = 'GameMode'
		s_Category:AddMode('TeamDeathMatch0')
		s_Category:AddMode('TeamDeathMatchC0')

		s_Instance:AddCategories(s_Category)

	end

	if self.m_Enable then
		--print(string.format("Created instance of type '%s' (%s)", p_Instance.typeName, p_GUID:ToString("D")))
	end
end

function Maps:OnPlayerRespawn(p_Player)
	print('Spawning sink')

	-- local s_Data = self.sink

	-- print(string.format('Sink! %s', s_Data.typeName))

	-- local s_Transform = LinearTransform(
	-- 	Vec3(5.0, 0.0, 0.0),
	-- 	Vec3(0.0, 5.0, 0.0),
	-- 	Vec3(0.0, 0.0, 5.0),
	-- 	Vec3(35.243832, 11.393118, 10.775893)
	-- )

	-- print('Creating entity')

	-- self.m_SinkEntity = EntityManager:CreateServerEntity(s_Data, s_Transform)

	-- if self.m_SinkEntity == nil then
	-- 	print('Could not create sink entity')
	-- 	return
	-- end

	-- print('Initializing entity')
	-- self.m_SinkEntity:Init(Realm.Realm_ClientAndServer, true)
	-- print('Initialized entity')

	-- NetEvents:SendTo('VU:Spawn', p_Player)
end

return Maps