class 'Maps'

function Maps:__init()
	Hooks:Install('UI:PushScreen', self, self.OnPushScreen)
	Events:Subscribe('Partition:ReadInstance', self, self.OnReadInstance)
	Events:Subscribe('Level:LoadResources', self, self.OnLoadResources)
	Events:Subscribe('Player:Respawn', self, self.OnPlayerRespawn)

	self.m_SchematicGUID = Guid('E210FDBA-6024-A322-402D-9E2AABC07526', 'D')

	self.m_RegistryGUID = Guid('96EF016C-4246-27BF-E65F-D93E823EA96C', 'D')
	self.m_MeshDatabaseGUID = Guid('4F866DDE-8BF7-9C0C-B347-F9B2059AAD92', 'D')
	self.m_WorldPartDataGUID = Guid('0275C5F7-C49D-452C-B91E-32FDD9502D5D', 'D')

	self.m_Enable = false
	
	self.ig01 = Guid('E7BCCB29-DD37-11E0-839A-B516FFC722DC', 'D')
	self.ii01 = nil
	
	self.ig02 = Guid('740454AC-A223-11E0-A054-9CA7FAEE751B', 'D')
	self.ii02 = nil
	
	self.ig03 = Guid('D531D9AC-6FB4-DF6E-26DE-32D3E2471779', 'D')
	self.ii03 = nil
	
	self.ig04 = Guid('0103772C-9B2C-C440-6138-D00F5826E4AB', 'D')
	self.ii04 = nil
	
	self.ig05 = Guid('16756E38-8EE9-7335-3B94-CAA9C6805141', 'D')
	self.ii05 = nil
	
	self.ig06 = Guid('74866776-D5AF-BD32-7964-CD234506235D', 'D')
	self.ii06 = nil
	
	self.ig07 = Guid('B2A83EFA-9763-4021-AC03-73DC0267D203', 'D')
	self.ii07 = nil
	
	self.ig08 = Guid('364EABE5-CAA5-B8D0-0474-CF0F00B6E270', 'D')
	self.ii08 = nil
	
	self.ig09 = Guid('02E48B1A-51D6-C08B-0114-A406E5FA42C4', 'D')
	self.ii09 = nil
	
	self.ig10 = Guid('3DBC7862-7365-5D61-C18B-5A9898072851', 'D')
	self.ii10 = nil
	
	self.ig11 = Guid('72C783DB-E1CD-C254-A48D-24FF005185D1', 'D')
	self.ii11 = nil

	self.ig12 = Guid('4EFA0316-9709-4264-BA08-A8BF341DC4BD', 'D')
	self.ii12 = nil

	self.sinkid = Guid('7F3545C9-481F-11E1-B792-88A15BCDD28C', 'D')
	self.sink = nil

	self.ah1zg0 = Guid('13637316-6B9C-4474-8A99-9D3DE64AE752', 'D')
	self.ah1zi0 = nil
	self.ah1zg1 = Guid('9D7A016A-C57A-44C1-A0AF-84C734B7C981', 'D')
	self.ah1zi1 = nil
	self.ah1zg2 = Guid('81D83886-888B-3962-61B1-4A1FC1AD49DB', 'D')
	self.ah1zi2 = nil
	self.ah1zg3 = Guid('F0B544BB-6743-11E0-AD5C-F0C60AC956FE', 'D')
	self.ah1zi3 = nil
	self.ah1zg4 = Guid('B54DE6CC-6743-11E0-AD5C-F0C60AC956FE', 'D')
	self.ah1zi4 = nil
	self.ah1zg5 = Guid('BBFFC4D0-E79D-44DC-AA7B-7819CDD13B74', 'D')
	self.ah1zi5 = nil
	self.ah1zg6 = Guid('F8F63D87-13E6-66B8-F060-AEFA03E4E45B', 'D')
	self.ah1zi6 = nil
	self.ah1zg7 = Guid('A676D498-A524-42AD-BE78-72B071D8CD6A', 'D')
	self.ah1zi7 = nil
	self.ah1zg8 = Guid('02D7C4C0-B0DE-19DD-8BC0-BB31BCDAB087', 'D')
	self.ah1zi8 = nil
	self.ah1zg9 = Guid('62BD4366-5A58-459A-9399-992B1E0CB274', 'D')
	self.ah1zi9 = nil
	self.ah1zg10 = Guid('02FBE4CC-9A0F-5838-9F70-1FF63FE8C3AD', 'D')
	self.ah1zi10 = nil
	self.ah1zg11 = Guid('A4F96074-6557-3EDA-C0AF-41D54A19F81E', 'D')
	self.ah1zi11 = nil
	self.ah1zg12 = Guid('E20C1DF6-B9CF-4668-5875-1DF144B98B9E', 'D')
	self.ah1zi12 = nil
	self.ah1zg13 = Guid('2DD8EB07-D0DC-4D85-F2EA-37877C494F59', 'D')
	self.ah1zi13 = nil
	self.ah1zg14 = Guid('B95EC765-F2D0-76BD-B213-9BD306D41245', 'D')
	self.ah1zi14 = nil
	self.ah1zg15 = Guid('C8D55C52-E697-7A82-C185-F02E78724AF4', 'D')
	self.ah1zi15 = nil

	self.ah1zg16 = Guid('81D83886-888B-3962-61B1-4A1FC1AD49DB', 'D')

	self.m_SinkEntity = nil
end

function Maps:OnPushScreen(p_Hook, p_Screen, p_GraphPriority, p_ParentGraph)
	local s_Screen = UIScreenAsset(p_Screen)

	print(string.format("Pushing screen '%s'", s_Screen.name))

	if s_Screen.name == 'UI/Flow/Screen/CoopWaitingScreen' then
		return true
	end

	return p_Hook:CallOriginal(p_Screen, p_GraphPriority, p_ParentGraph)
end

function Maps:OnLoadResources(p_Dedicated)
	print("Loading level resources!")
	SharedUtils:MountSuperBundle('Xp1Chunks')
	SharedUtils:MountSuperBundle('Levels/XP1_004/XP1_004')

	SharedUtils:PrecacheBundle('levels/xp1_004/xp1_004')
	SharedUtils:PrecacheBundle('levels/xp1_004/cq_l')

	self.m_Enable = true
end

function Maps:OnReadInstance(p_Instance, p_GUID)
	if p_Instance == nil then
		return
	end

	if p_GUID == self.ig01 then print('01') self.ii01 = p_Instance end
	if p_GUID == self.ig02 then print('02') self.ii02 = p_Instance end
	if p_GUID == self.ig03 then print('03') self.ii03 = p_Instance end
	if p_GUID == self.ig04 then print('04') self.ii04 = p_Instance end
	if p_GUID == self.ig05 then print('05') self.ii05 = p_Instance end
	if p_GUID == self.ig06 then print('06') self.ii06 = p_Instance end
	if p_GUID == self.ig07 then print('07') self.ii07 = p_Instance end
	if p_GUID == self.ig08 then print('08') self.ii08 = p_Instance end
	if p_GUID == self.ig09 then print('09') self.ii09 = p_Instance end
	if p_GUID == self.ig10 then print('10') self.ii10 = p_Instance end
	if p_GUID == self.ig11 then print('11') self.ii11 = p_Instance end
	if p_GUID == self.ig12 then print('12') self.ii12 = p_Instance end

	if p_GUID == self.ah1zg0 then print('a0') self.ah1zi0 = p_Instance end
	if p_GUID == self.ah1zg1 then print('a1') self.ah1zi1 = p_Instance end
	if p_GUID == self.ah1zg2 then print('a2') self.ah1zi2 = p_Instance end
	if p_GUID == self.ah1zg3 then print('a3') self.ah1zi3 = p_Instance end
	if p_GUID == self.ah1zg4 then print('a4') self.ah1zi4 = p_Instance end
	if p_GUID == self.ah1zg5 then print('a5') self.ah1zi5 = p_Instance end
	if p_GUID == self.ah1zg6 then print('a6') self.ah1zi6 = p_Instance end
	if p_GUID == self.ah1zg7 then print('a7') self.ah1zi7 = p_Instance end
	if p_GUID == self.ah1zg8 then print('a8') self.ah1zi8 = p_Instance end
	if p_GUID == self.ah1zg9 then print('a9') self.ah1zi9 = p_Instance end
	if p_GUID == self.ah1zg10 then print('a10') self.ah1zi10 = p_Instance end
	if p_GUID == self.ah1zg11 then print('a11') self.ah1zi11 = p_Instance end
	if p_GUID == self.ah1zg12 then print('a12') self.ah1zi12 = p_Instance end
	if p_GUID == self.ah1zg13 then print('a13') self.ah1zi13 = p_Instance end
	if p_GUID == self.ah1zg14 then print('a14') self.ah1zi14 = p_Instance end
	if p_GUID == self.ah1zg15 then print('a15') self.ah1zi15 = p_Instance end

	if p_GUID == self.ah1zg16 then
		local s_Instance = VehicleEntityData(p_Instance)
		s_Instance.transform.right = Vec3(0.2, 0.0, 0.0)
		s_Instance.transform.up = Vec3(0.0, 0.2, 0.0)
		s_Instance.transform.forward = Vec3(0.0, 0.0, 0.2)
	end

	if p_GUID == self.sinkid then
		print('sink')
		self.sink = p_Instance
	end

	if p_Instance.typeName == 'LevelData' then
		local s_Instance = LevelData(p_Instance)
		s_Instance.levelDescription.isCoop = false
		s_Instance.levelDescription.isMultiplayer = true
	end

	if p_Instance.typeName == 'LevelDescriptionAsset' then
		local s_Instance = LevelDescriptionAsset(p_Instance)

		if s_Instance.levelName == 'Levels/COOP_002/COOP_002' then
			s_Instance:ClearStartPoints()
			s_Instance.description.isMultiplayer = true
			s_Instance.description.isCoop = false

			local s_Category = LevelDescriptionInclusionCategory()
			s_Category.category = 'GameMode'
			s_Category:AddMode('TeamDeathMatch0')
			s_Category:AddMode('TeamDeathMatchC0')

			s_Instance:AddCategories(s_Category)
		end

	end

	if p_GUID == self.m_RegistryGUID then
		local s_Instance = RegistryContainer(p_Instance)

		print('Adding instances to registry!')

		s_Instance:AddEntityRegistry(self.ii01)
		s_Instance:AddEntityRegistry(self.ii02)
		s_Instance:AddEntityRegistry(self.ii03)
		s_Instance:AddEntityRegistry(self.ah1zi0)
		s_Instance:AddEntityRegistry(self.ah1zi1)
		s_Instance:AddEntityRegistry(self.ah1zi2)
		s_Instance:AddEntityRegistry(self.ah1zi3)
		s_Instance:AddEntityRegistry(self.ah1zi4)
		s_Instance:AddEntityRegistry(self.ah1zi5)

		s_Instance:AddBlueprintRegistry(self.ii04)
		s_Instance:AddBlueprintRegistry(self.ii05)
		s_Instance:AddBlueprintRegistry(self.ii06)
		s_Instance:AddBlueprintRegistry(self.ah1zi6)
		s_Instance:AddBlueprintRegistry(self.ah1zi7)
		s_Instance:AddBlueprintRegistry(self.ah1zi8)

		s_Instance:AddReferenceObjectRegistry(self.ii12)
		s_Instance:AddReferenceObjectRegistry(self.ah1zi9)
	end

	if p_GUID == self.m_WorldPartDataGUID then
		local s_Instance = WorldPartData(p_Instance)

		print('Adding vehicle spawner!')

		local s_Data = ReferenceObjectData(self.ii12)
		s_Data.blueprintTransform.trans = Vec3(35.243832, 12.393118, 10.775893)
		s_Data.indexInBlueprint = 4
		s_Data.blueprint = Blueprint(self.ii06)
	
		s_Instance:AddObjects(GameObjectData(self.ii12))

		print('Added spawner!')
	end

	if p_GUID == self.m_MeshDatabaseGUID then
		local s_Instance = MeshVariationDatabase(p_Instance)

		print('Adding meshes to database!')

		s_Instance:AddEntries(MeshVariationDatabaseEntry(self.ii08))
		s_Instance:AddEntries(MeshVariationDatabaseEntry(self.ii09))
		s_Instance:AddEntries(MeshVariationDatabaseEntry(self.ii10))
		s_Instance:AddEntries(MeshVariationDatabaseEntry(self.ii11))

		s_Instance:AddEntries(MeshVariationDatabaseEntry(self.ah1zi10))
		s_Instance:AddEntries(MeshVariationDatabaseEntry(self.ah1zi11))
		s_Instance:AddEntries(MeshVariationDatabaseEntry(self.ah1zi12))
		s_Instance:AddEntries(MeshVariationDatabaseEntry(self.ah1zi13))
		s_Instance:AddEntries(MeshVariationDatabaseEntry(self.ah1zi14))
		s_Instance:AddEntries(MeshVariationDatabaseEntry(self.ah1zi15))
	end

	if self.m_Enable then
		--print(string.format("Created instance of type '%s' (%s)", p_Instance.typeName, p_GUID:ToString("D")))
	end
end

function Maps:OnPlayerRespawn(p_Player)
	print('Spawning sink')

	-- local s_Data = self.sink

	-- print(string.format('Sink! %s', s_Data.typeName))

	-- local s_Transform = LinearTransform(
	-- 	Vec3(5.0, 0.0, 0.0),
	-- 	Vec3(0.0, 5.0, 0.0),
	-- 	Vec3(0.0, 0.0, 5.0),
	-- 	Vec3(35.243832, 11.393118, 10.775893)
	-- )

	-- print('Creating entity')

	-- self.m_SinkEntity = EntityManager:CreateServerEntity(s_Data, s_Transform)

	-- if self.m_SinkEntity == nil then
	-- 	print('Could not create sink entity')
	-- 	return
	-- end

	-- print('Initializing entity')
	-- self.m_SinkEntity:Init(Realm.Realm_ClientAndServer, true)
	-- print('Initialized entity')

	-- NetEvents:SendTo('VU:Spawn', p_Player)
end

return Maps