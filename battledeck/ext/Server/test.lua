class 'TestClass'

function TestClass:__init()
	self.m_Swag = 1337
end

function TestClass:Swagger()
	print(string.format('Got swag: %d', self.m_Swag))
end

return TestClass