class 'RimeShared'

function RimeShared:__init()
	Hooks:Install('ResourceManager:LoadBundle', self, self.OnLoadBundle)

	Events:Subscribe('Partition:ReadInstance', self, self.OnReadInstance)
	Events:Subscribe('Level:LoadResources', self, self.OnLoadResources)

	--self.m_ObjectsGuid = Guid('01193075-ACBC-421A-B29E-1EE5E22FE27D', 'D')
	self.m_RegistryGuid = Guid('CEDC393F-3DDC-A9C1-5CD1-E186790CE053', 'D')

	self.m_DeathMatchSWGuid = Guid('6D9ACF85-14A3-4815-B33B-44BDE0D7CAFE', 'D')
	self.m_DeathMatchSW = nil
	
	self.m_MapName = "sp_valley/sp_valley"
end

function RimeShared:OnLoadBundle(p_Hook, p_Bundle)
	print(string.format("Loading bundle '%s'", p_Bundle))

	local s_Name = p_Bundle:lower()
	
	if string.find(s_Name, "uiplaying") ~= nil then
		return p_Hook:CallOriginal('Levels/XP1_002/XP1_002_UiPlaying')
	end

	return p_Hook:CallOriginal(p_Bundle)
end

function RimeShared:OnLoadResources(p_Dedicated)
	print("Loading level resources!")

	SharedUtils:MountSuperBundle('MpChunks')
	SharedUtils:MountSuperBundle('Xp1Chunks')
	SharedUtils:MountSuperBundle('Levels/XP1_002/XP1_002')

	SharedUtils:PrecacheBundle('levels/xp1_002/xp1_002')
	--SharedUtils:PrecacheBundle('levels/xp1_002/cq_l')
end

function RimeShared:OnReadInstance(p_Instance, p_Guid)
	if p_Instance == nil then
		return
	end

	if p_Guid == self.m_DeathMatchSWGuid then
		print('Got DeathMatch Bundle SubWorld!')
		self.m_DeathMatchSW = p_Instance
	end
	
	if p_Instance.typeName == 'LevelDescriptionAsset' then
		local s_Instance = LevelDescriptionAsset(p_Instance)
		local s_LevelName = s_Instance.levelName:lower()
		
		if string.find(s_LevelName, self.m_MapName) ~= nil then
			-- Remove start points and set map to multiplayer.
			--s_Instance:ClearStartPoints()
			s_Instance.description.isMultiplayer = true
			s_Instance.description.isCoop = false

			-- Add TeamDeathMatch0 as a supported gamemode.
			local s_Category = LevelDescriptionInclusionCategory()
			s_Category.category = 'GameMode'
			s_Category:AddMode('TeamDeathMatch0')

			s_Instance:AddCategories(s_Category)
			print("[Rime] Changed map to multiplayer.")
		end

	end

	if p_Guid == self.m_RegistryGuid then
		local s_Instance = RegistryContainer(p_Instance)

		print('Got registry container!')

		-- Remove the Gameplay/Logic/SP_FAIL_Effect
		--s_Instance:RemoveBlueprintRegistryAt(23)
		
		-- Add a reference to the DeathMatch bundle.
		if self.m_DeathMatchSW ~= nil then
			s_Instance:AddReferenceObjectRegistry(self.m_DeathMatchSW)
		end
		
	end
	
	if p_Instance.typeName == "LevelData" then
		local s_Instance = LevelData(p_Instance)
		local s_LevelName = s_Instance.name:lower()
		
		print('Got level data!')

		if string.find(s_LevelName, self.m_MapName) ~= nil then
			-- Set map to multiplayer.
			s_Instance.levelDescription.isCoop = false
			s_Instance.levelDescription.isMultiplayer = true
			
			print("[Rime] Converted map to multiplayer.")
			
			local s_ObjectCount = s_Instance:GetObjectsCount() - 1
			
			print("[Rime] LevelData Object Count: " .. s_ObjectCount)
			
			
			if string.find(s_LevelName, self.m_MapName) ~= nil then		
				local s_SafeIndex = { 0, 1, 3, 7 } 
				
				local s_ObjectCount = s_Instance:GetObjectsCount() - 1
				
				print("[Rime] LevelData Object Count: " .. s_ObjectCount)
				for i = s_ObjectCount, 1, -1 do
					local s_Exists = s_SafeIndex[i] ~= nil
					if s_Exists == false then
						s_Instance:RemoveObjectsAt(i)
						print("[Rime] Removed: " .. i)
					end
				end
				
				-- Add the DeathMatch bundle.
				s_Instance:AddObjects(GameObjectData(self.m_DeathMatchSW))
			end
			
			print("[Rime] Converted map to multiplayer and removed crashing assets.")
		end
	end
	
	if p_Instance.typeName == "WorldPartData" then
		local s_Instance = WorldPartData(p_Instance)
		local s_ObjectCount = s_Instance:GetObjectsCount()
		local s_Name = s_Instance.name:lower()
		


		
		if string.find(s_Name, "xp1") ~= nil then
			print("[Rime] skipping " .. s_Instance.name)
			return
		end
		
		if string.find(s_Name, "ui") ~= nil then
			print("[Rime] skipping " .. s_Instance.name)
			return
		end
		
		if string.find(s_Name, "light") ~= nil then
			print("[Rime] Removing light " .. s_Instance.name)
			s_Instance:ClearObjects()
		
			s_Instance.enabled = false
			return
		end
		
		-- if string.find(s_Name, "ve") ~= nil then
			-- print("[Rime] Removing VE " .. s_Instance.name)
			-- s_Instance:ClearObjects()
		
			-- s_Instance.enabled = false
			-- return
		-- end
		
		--print("[Rime] Removing " .. s_ObjectCount .. " objects from " .. s_Name);
		
		--s_Instance:ClearObjects()
		
		--s_Instance.enabled = false
	end
end

local g_RimeShared = RimeShared()