class 'catchnullShared'


function catchnullShared:__init()
	print("Initializing catchnullShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function catchnullShared:RegisterVars()
	self.m_Record = false
	self.m_Parsed = {}
end


function catchnullShared:RegisterEvents()
	
	Hooks:Install('ServerEntityFactory:CreateFromBlueprint', 1000, self, self.OnBlueprintCreate)
	Hooks:Install('ClientEntityFactory:CreateFromBlueprint', 1000, self, self.OnBlueprintCreate)

	Hooks:Install('ClientEntityFactory:Create',1000, self, self.OnEntityCreate)
	Hooks:Install('ServerEntityFactory:Create',1000, self, self.OnEntityCreate)


	Hooks:Install('ResourceManager:LoadBundles',999, self, self.OnLoadBundles)
	Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)

end



function catchnullShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeInfo == LevelData.typeInfo) then
			self.m_AISystem = LevelData(l_Instance)
			if(self.m_AISystem.aiSystem ~= nil) then
				self:VerifyInstance(self.m_AISystem, self.m_AISystem.typeInfo)
			end
		end
	end
end

function catchnullShared:OnLoadBundles(p_Hook, p_Bundles, p_Compartment)
	if(p_Bundles[1] == "UI/Flow/Bundle/InGameBundleMp") then
		self.m_Record = true
	end
end
function catchnullShared:OnEntityCreate( p_Hook, p_Data, p_Transform )
	print(p_Data.typeInfo.name)
	if (p_Data.typeInfo.name == "SoldierEntityData") then
		print("Soldier spawned")
		self:VerifyInstance(p_Data)
	end

	if (p_Data.typeInfo.name == "CharacterSpawnReferenceObjectData") then
		self:VerifyInstance(p_Data)
	end

	if(self.m_Record == true) then
		print(p_Data.typeInfo.name)
	end
end

function catchnullShared:OnBlueprintCreate( p_Hook, blueprint, transform, variation, parent)
	if(blueprint.typeInfo.name == "SoldierBlueprint") then
		self:VerifyInstance(parent)
		self:VerifyInstance(blueprint)
	end
end

function catchnullShared:VerifyInstance( p_Instance )
	if(p_Instance == nil) then
		return
	end
	local s_Instance = _G[p_Instance.typeInfo.name](p_Instance)
	print("Instance: " .. tostring(s_Instance.instanceGuid) .." | " .. p_Instance.typeInfo.name ..":")
	self:PrintFields(s_Instance, s_Instance.typeInfo)
	print("Instance end")
	
end
-- Prints all members and child members of a given instance. Useful for debugging.
function catchnullShared:PrintFields( instance, typeInfo, padding, currentDepth, maxDepth, parentField)
	if(instance.instanceGuid == nil) then
		print("FUCKING WHAT")
		return
	end
	if(instance.isLazyLoaded) then
		print("LazyLoaded sorri")
		return
	end
	
	if(self.m_Parsed[tostring(instance.instanceGuid)] ~= nil) then
		return
	else
		self.m_Parsed[tostring(instance.instanceGuid)] = true
	end
	if(currentDepth == nil) then
		currentDepth = 0
	end
	if(string.match(typeInfo.name:lower(), "voice") or string.match(typeInfo.name:lower(), "sound") or typeInfo == MaterialContainerPair.typeInfo or typeInfo == MaterialContainerAsset.typeInfo) then
		return
	end

	if(maxDepth == nil) then
		maxDepth = -1
	else
		if(maxDepth ~= -1 and currentDepth > maxDepth) then
			return
		end
	end

	if(typeInfo == nil) then
		print("No typeInfo???")
	end



	currentDepth = currentDepth + 1

	if(padding == nil) then
		padding = ""
	end 
	if(parentField ~= nil) then
		print(padding ..parentField.name..' (object - '..parentField.typeInfo.name..') '.. typeInfo.name .. ' {')

	else
		print(padding .. typeInfo.name .. ' {' .. tostring(instance.instanceGuid))
	end

	padding = padding .. "|"

	local s_Fields = getFields(typeInfo)
	for _, field in pairs(s_Fields) do
		if field.typeInfo ~= nil and field.name ~= "MaterialPairs" then

			local s_Name = FirstToLower(field.name)

			--Value that can be printed 
			--NOTE: these arent all possible types
			if isPrintable(field.typeInfo.name) then

				local s_Value = instance[s_Name]

				print(padding ..field.name..' ('..field.typeInfo.name..') : '.. tostring(s_Value))

			--Array
			elseif field.typeInfo.array and field.name ~= "MaterialPairs" then
				-- So UIBundlesAsset[uIBundleAssetStateList] returns nil even though it's not??
				if(instance[FirstToLower(field.name)] ~= nil) then
					local s_Count = #instance[FirstToLower(field.name)]
					print(padding ..field.name..' (Array), '..tostring(s_Count)..' Members {')

					if s_Count ~= 0 then
						s_Count = s_Count

						for i=1,s_Count,1 do 
							if(field == nil) then
								print("What the fuck")
								print(field.name .. " | " .. instance.typeInfo.name .. " | " .. tostring(instance.instanceGuid))
							else
								if isPrintable(field.typeInfo.elementType.name) or field.typeInfo.elementType.enum then
									print(padding .. "[" .. i .. "] " ..tostring(instance[FirstToLower(field.name)]:get(i)))
								else
									if(instance[FirstToLower(field.name)]:get(i).instanceGuid == nil) then
										print(padding .. "[" .. i .. "] " .."nil")
									elseif(instance[FirstToLower(field.name)]:get(i).isLazyLoaded) then
										print(padding .. "[" .. i .. "] " .."isLazyLoaded")
									else
										local s_MemberInstance = instance[FirstToLower(field.name)]:get(i)
										local s_Member = _G[s_MemberInstance.typeInfo.name](s_MemberInstance)

										if s_Member ~= nil then
											self:PrintFields(s_Member, s_Member.typeInfo, padding, currentDepth, maxDepth)
										end
									end
								end
							end
						end
					end
					print(padding .. "}")
				end
			--Enum
			elseif field.typeInfo.enum then
				local s_Value = instance[s_Name]
				print(padding..field.name..' (Enum) : ' .. tostring(s_Value))
			elseif field.typeInfo.name == "Guid" then
				local s_Value = instance[s_Name]
				print(padding..field.name..' (Guid) : ' .. tostring(s_Value))
			--Object
			else
				print(padding..s_Name .. " | " .. tostring(instance.instanceGuid))
				if instance[s_Name] ~= nil then 
					if(instance[s_Name].isLazyLoaded) then
						print(padding ..field.name..' (object - '..field.typeInfo.name..') '.. 'isLazyLoaded')
					else
						-- local s_Value = instance[s_Name]
						print(instance[s_Name].typeInfo.name)
						local i = _G[instance[s_Name].typeInfo.name](instance[s_Name])
						if i ~= nil then
							self:PrintFields( i, i.typeInfo, padding, currentDepth, maxDepth, field) 
						else
							print(padding ..field.name..' (object - '..field.typeInfo.name..') '.. 'nil')
						end
					end
				else
					print(padding ..field.name..' (Object - '..field.typeInfo.name..') ' .."nil")
				end
			end
		end
	end
	print (padding:sub(1, -2) .. "}")
end


-- Prints all members and child members of a given instance. Useful for debugging.
function catchnullShared:IsNull( instance, typeInfo, padding, currentDepth, maxDepth, parentField, path)
	if(currentDepth == nil) then
		currentDepth = 0
	end

	if(maxDepth == nil) then
		maxDepth = -1
	else
		if(maxDepth ~= -1 and currentDepth > maxDepth) then
			return
		end
	end

	if(path == nil) then
		path = instance.typeInfo.name
	else
		path = path .. "->"..instance.typeInfo.name
	end




	currentDepth = currentDepth + 1

	if(padding == nil) then
		padding = ""
	end 	
	if(parentField ~= nil) then
		--print(padding ..parentField.name..' (object - '..parentField.typeInfo.name..') '.. typeInfo.name .. ' {')

	else
		--print(padding .. typeInfo.name .. ' {')
	end

	padding = padding .. "|"

	local s_Fields = getFields(typeInfo)
	for _, field in pairs(s_Fields) do
		if field.typeInfo ~= nil then

			local s_Name = FirstToLower(field.name)

			--Value that can be printed 
			--NOTE: these arent all possible types
			if isPrintable(field.typeInfo.name) then
				local s_Value = instance[s_Name]
				if(s_Value == nil) then
					print(tostring(instance.instanceGuid).." " .. path.."."..field.name..' ('..field.typeInfo.name..') : '.. "nil" .. " | ")
				end
			--Array
			elseif field.typeInfo.array then
				-- So UIBundlesAsset[uIBundleAssetStateList] returns nil even though it's not??
				if(instance[FirstToLower(field.name)] ~= nil) then
					local s_Count = #instance[FirstToLower(field.name)]
					--print(padding ..field.name..' (Array), '..tostring(s_Count)..' Members {')

					if s_Count ~= 0 then
						s_Count = s_Count

						for i=1,s_Count,1 do 
							if isPrintable(field.typeInfo.elementType.name) then
								if(instance[FirstToLower(field.name)] == nil) then
									print(tostring(instance.instanceGuid).." " .. path.."."..field.name .. " [" .. i .. "] " .. "nil")
								end
							else
								local s_MemberInstance = instance[FirstToLower(field.name)]:get(i)
								if(s_MemberInstance == nil) then
									print(tostring(instance.instanceGuid).." " .. path.."."..field.name .. "[" .. i .. "] " ..field.name .. " nil" )
								end
								if s_Member ~= nil then
									self:IsNull(s_Member, s_Member.typeInfo, padding, currentDepth, maxDepth, path.."."..field.name)
								end
							end
						end
					end
					--print(padding .. "}")
				end
			--Enum
			elseif field.typeInfo.enum then
				local s_Value = instance[s_Name]
				if(s_Value == nil) then
					print(padding..field.name..' (Enum) : ' .. tostring(s_Value))
				end
			else
				if instance[s_Name] ~= nil then 
					-- local s_Value = instance[s_Name]
					local i = _G[instance[s_Name].typeInfo.name](instance[s_Name])
					if i ~= nil then
						self:IsNull( i, i.typeInfo, padding, currentDepth, maxDepth, field, path.."."..field.name) 
					else
						print(tostring(instance.instanceGuid).." " .. path.."." ..field.name..' (object - '..field.typeInfo.name..') '.. 'nil')
					end
				else
					print(tostring(instance.instanceGuid).." " .. path.."." ..field.name..' (Object - '..field.typeInfo.name..') ' .."nil")
				end
			end
		end
	end
	--print (padding:sub(1, -2) .. "}")
end


function isPrintable( typ )
	if typ == "CString" or
	typ == "Float8" or
	typ == "Float16" or
	typ == "Float32" or
	typ == "Float64" or
	typ == "Int8" or
	typ == "Int16" or
	typ == "Int32" or
	typ == "Int64" or
	typ == "Uint8" or
	typ == "Uint16" or
	typ == "Uint32" or
	typ == "Uint64" or
	typ == "LinearTransform" or
	typ == "Vec2" or
	typ == "Vec3" or
	typ == "Vec4" or
	typ == "Boolean" or 
	typ == "Guid" then
		return true
	end
	return false
end
function getFields( typeInfo )
	local s_Super = {}
	if typeInfo.super ~= nil then
		if typeInfo.super.name ~= "DataContainer" then
			for k,superv in pairs(getFields(typeInfo.super)) do
				table.insert(s_Super, superv)
			end
		end
	end
	for k,v in pairs(typeInfo.fields) do
		table.insert(s_Super, v)
	end
	return s_Super
end

function FirstToLower(str)
	return (str:gsub("^%L", string.lower))
end
g_catchnullShared = catchnullShared()

