class 'bundleCrashShared'

function bundleCrashShared:__init()
	self:RegisterEvents()
	self:RegisterVariables()
end

function bundleCrashShared:RegisterVariables()
end

function bundleCrashShared:RegisterEvents()
	self.m_LevelLoadEvent = Events:Subscribe("Level:LoadResources", self, self.OnLoadResources)
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end

function bundleCrashShared:OnLoadResources(p_Dedicated)
	print("OnLoadResources")
	self:RegisterVariables()
  
	ResourceManager:MountSuperBundle("Levels/COOP_003/COOP_003")
	self.m_loadHandle = ResourceManager:BeginLoadData(3, 
						{ "Levels/COOP_003/COOP_003", 
						"Levels/COOP_003/AB00_Parent" })
end

function bundleCrashShared:OnEngineUpdate(p_Delta, p_SimDelta)
	if self.m_loadHandle == nil then 
		return 
	end
	
	if not ResourceManager:PollBundleOperation(self.m_loadHandle) then 
		return 
	end
	
	self.m_loadHandle = nil
	
	if not ResourceManager:EndLoadData(self.m_loadHandle) then
		print("Bundles failed to load")
		return
	end
end

function bundleCrashShared:ReadInstance(p_Instance, p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	if p_Guid == Guid('B092514F-F93F-4D6D-B675-B3ED56929920', 'D') then
		local s_Instance = LevelData(p_Instance)
		print(s_Instance.name .. " is indeed loaded")
	end
end	

g_bundleCrashShared = bundleCrashShared()

return bundleCrashShared