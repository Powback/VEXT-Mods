class 'MapEditorClient'

local m_vuExtensions = require "__shared/Util/VUExtensions"


function MapEditorClient:__init()
	print("Initializing MapEditorClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function MapEditorClient:RegisterVars()
	self.m_ReferenceObjectData = {}
	self.entities = {}
	self.entityInfo = {}
	self.current = 1
	self.types = {}
end


function MapEditorClient:RegisterEvents()
	Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	Hooks:Install('ClientEntityFactory:Create', 9, self, self.OnEntityCreate)
	self.m_EngineUpdateEvent = Events:Subscribe('Engine:Update', self, self.OnEngineUpdate)

end

function MapEditorClient:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		error('p_Partition was nil')
		return
	end
 
	local instances = p_Partition.instances
   
	for _, instance in ipairs(instances) do
		if instance == nil then
			print("Instance is null wtf")
			break
		end

		

		if instance.typeName == "ReferenceObjectData" then
			--local s_Instance = m_vuExtensions:PrepareInstanceForEdit(p_Partition, instance)
			local s_Instance = ReferenceObjectData(instance)
			self.m_ReferenceObjectData[tostring(s_Instance.blueprintTransform.trans)] = s_Instance
		end
	end
end

function MapEditorClient:OnEntityCreate(p_Hook, p_Data, p_Transform)
	if p_Data == nil then
		print("Didnt get no data")
	else
		if(self.types[p_Data.typeName] == nil) then
			print(p_Data.typeName)
			self.types[p_Data.typeName] = true
		end
		if p_Data.typeName == "StaticModelEntityData" or
		p_Data.typeName == "MeshProxyEntityData" or 
		p_Data.typeName == "GroupPhysicsEntityData" or
		p_Data.typeName == "BangerEntityData" or
		p_Data.typeName == "BreakableModelEntityData" or
		p_Data.typeName == "StaticModelGroupEntityData" or
		p_Data.typeName == "StaticModelEntityData"
		then
			local s_Data = _G[p_Data.typeName](p_Data)
			
			local x = p_Hook:Call()
			p_Hook:Return(x)

			local s_Spatial = SpatialEntity(x)


			table.insert(self.entities, s_Spatial)
			table.insert(self.entityInfo, tostring(s_Data.instanceGuid))
		end
	end

	 
end

function MapEditorClient:OnEngineUpdate(p_Delta, p_SimDelta)
	local s_LocalPlayer = PlayerManager:GetLocalPlayer()
    if(s_LocalPlayer == nil) then
    	return
    end
	if s_LocalPlayer.soldier == nil then
		return
	end
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F1) then
		-- WebUI:ExecuteJS('document.location.reload()')
		self:MoveDyn(s_LocalPlayer.soldier.transform)
	end
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F2) then
		self.current = 1
	end
end



function MapEditorClient:MoveDyn( p_Transform )
	local s_Iterator = EntityManager:GetClientIterator('DynamicPhysicsEntity')
			print("yo")

	if s_Iterator == nil then
		print('Failed to get camera iterator')
	else
		local s_Entity = s_Iterator:Next()
		while s_Entity ~= nil do
			print("yo")

			local s_spatial = SpatialEntity(s_Entity)
			print(s_spatial.transform)
			s_spatial.transform = p_Transform
			s_Entity = s_Iterator:Next()
		end
	end
end

g_MapEditorClient = MapEditorClient()

