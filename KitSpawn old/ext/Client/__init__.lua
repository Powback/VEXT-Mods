class 'KitSpawnClient'


function KitSpawnClient:__init()
	print("Initializing KitSpawnClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function KitSpawnClient:RegisterVars()
	--self.m_this = that
end


function KitSpawnClient:RegisterEvents()
	self.m_ClientUpdateInputEvent = Events:Subscribe('Client:UpdateInput', self, self.OnUpdateInput)

end

	
function KitSpawnClient:OnUpdateInput(p_Delta)
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F1) then
	   	print("Fire")
		local s_Player = PlayerManager:GetLocalPlayer()
		if s_Player == nil then
			return
		end
			
			--print("Player: " .. s_Player.name .. " K: " .. s_Player:GetKills() .. " D: " .. s_Player:GetDeaths() .. "Time: " .. s_Player:GetTime())
		
		local s_Soldier = s_Player.soldier
		if s_Soldier == nil then
			return
		end

		NetEvents:SendLocal('kit:spawn')
		print("Sent")
	end
end



g_KitSpawnClient = KitSpawnClient()

