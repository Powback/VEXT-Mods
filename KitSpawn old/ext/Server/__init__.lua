class 'KitSpawnServer'


function KitSpawnServer:__init()
	print("Initializing KitSpawnServer")
	self:RegisterVars()
	self:RegisterEvents()
end


function KitSpawnServer:RegisterVars()
	self.weapon = nil
end


function KitSpawnServer:RegisterEvents()
	self.m_SaveEvent = NetEvents:Subscribe('kit:spawn', self, self.OnSpawnKit)
    Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end


function KitSpawnServer:OnPartitionLoaded(p_Partition)
 	 local instances = p_Partition.instances
   
    for _, instance in ipairs(instances) do
        if instance == nil then
            break
        end
        if(instance.typeName == "ObjectBlueprint") then
        	instance = ObjectBlueprint(instance)
        	--print(instance.name .. " | " .. tostring(instance.instanceGuid) .. " | " .. tostring(p_Partition.guid))
        end
    end
end

function KitSpawnServer:OnSpawnKit(p_Player)
	print("Spawn kit yo")
	if p_Player == nil then
		return
	end
		
		--print("Player: " .. s_Player.name .. " K: " .. s_Player:GetKills() .. " D: " .. s_Player:GetDeaths() .. "Time: " .. s_Player:GetTime())
	
	local s_Soldier = p_Player.soldier
	if s_Soldier == nil then
		return
	end
	local s_Blueprint = SpatialPrefabBlueprint()
	s_Blueprint.name = "Gameplay/Prefabs/CoOp/CoOp_PickUp_DAO-12_Flashlight"
	s_Blueprint.needNetworkId = true

	local s_Pickup = WeaponUnlockPickupEntityData()

	s_Pickup.transform = LinearTransform(
		Vec3(1.0, 0.0, 0.0),
		Vec3(0.0, 1.0, 0.0),
		Vec3(0.0, 0.0, 1.0),
		Vec3(0, 0, 0)
	)
	s_Pickup.randomizeAmmoOnDropForPlayer = PickupPlayerEnum.PickupPlayerEnum_AIOnly 
	s_Pickup.enabled = true
	s_Pickup.physicsBlueprint = ResourceManager:FindInstanceByGUID(Guid('625C2806-0CE7-11E0-915B-91EB202EAE87', 'D'), Guid('B9E3B4B8-062A-1DA8-E13D-8D7095F2A610','D'))
	s_Pickup.allowPickup = true
	s_Pickup.useWeaponMesh = true
	s_Pickup.positionIsStatic = false
	s_Pickup.allowPickup = true
	s_Pickup.forceWeaponSlotSelection = true
	s_Pickup.displayInMiniMap = false
	s_Pickup.hasAutomaticAmmoPickup = true
	s_Pickup.interactionRadius = 10
	s_Pickup.sendPlayerInEventOnPickup = true
	s_Pickup.isEventConnectionTarget = 0
	s_Pickup.isPropertyConnectionTarget = 0
	
	print("Getting wep")
	local wep = ResourceManager:FindInstanceByGUID(Guid('1556281A-0F0B-4EB3-B280-661018F8D52F', 'D'), Guid('3BA55147-6619-4697-8E2B-AC6B1D183C0E', 'D'))
	print(wep.typeName)
	print("member1")
	local member = WeaponUnlockPickupData()
		local slot2 = UnlockWeaponAndSlot() 
	    slot2.weapon = SoldierWeaponUnlockAsset(wep)
	    slot2.slot = WeaponSlot.WeaponSlot_0
	    slot2.unlockAssets:add(UnlockAsset(ResourceManager:FindInstanceByGUID(Guid('053617DF-44E0-11E0-B23E-A6B4F072D07D', 'D'), Guid('74102E70-4166-6A1F-D54A-FA67E688D1F5', 'D'))))
		    
		member.unlockWeaponAndSlot = UnlockWeaponAndSlot(slot2)

		member.altWeaponSlot = 1
		member.linkedToWeaponSlot  = -1
		member.minAmmo = 0	
		member.maxAmmo = 0
		member.defaultToFullAmmo = false
		s_Pickup.weapons:add(member)
	print("member2")

	s_Blueprint.objects:set(1, GameObjectData(s_Pickup))
	print("member3")
	print(#s_Blueprint.objects)

		print("member3")

	local spawnedExplosion = EntityManager:CreateServerEntity(PickupEntityData(s_Pickup), s_Soldier.transform)
	print("so far so good")
	

	if spawnedExplosion == nil then
		print("Could not spawn explosion")
		return
	end
	spawnedExplosion:Init(Realm.Realm_Server, true)
	print(tostring(spawnedExplosion.typeName))
	
	--[[

	for i, entity in ipairs(spawnedExplosion) do
		print(tostring(entity))
		entity:Init(Realm.Realm_ClientAndServer, true)
	end
	]]
	

	print("spawned explosion")
end



g_KitSpawnServer = KitSpawnServer()

