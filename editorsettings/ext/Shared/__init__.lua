class 'editorsettingsShared'


function editorsettingsShared:__init()
	print("Initializing editorsettingsShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function editorsettingsShared:RegisterVars()
	--self.m_this = that
end


function editorsettingsShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end


function editorsettingsShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Settings = ResourceManager:GetSettings("EntitySettings")

	if s_Settings ~= nil then
		local s_EntitySettings = EntitySettings(s_Settings)

		s_EntitySettings.editorGameViewEnable = true
		s_EntitySettings.executionMode = 1
	else
		print("Could not get OnlineSettings")
	end
end


g_editorsettingsShared = editorsettingsShared()

