class 'shadergraphShared'


function shadergraphShared:__init()
	print("Initializing shadergraphShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function shadergraphShared:RegisterVars()
	self.m_Shader = nil
	self.m_DebugTexture = nil
end


function shadergraphShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end


function shadergraphShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.instanceGuid == Guid("7F34BC19-F926-11DD-84F6-C77413F0C9F1")) then
			local s_Instance = TextureAsset (l_Instance)
			self.m_DebugTexture = s_Instance
			print("Oooh")
		end

		
		if(l_Instance.typeInfo == RoadData.typeInfo) then
			local s_Instance = RoadData(l_Instance)
			local s_Out = VisualVectorShapeData()
			s_Out.points = s_Instance.points
			s_Out.normals = s_Instance.normals
			s_Out.tension = s_Instance.tension
			s_Out.isClosed = false
			s_Out.allowRoll = true
			s_Out.errorTolerance = 1
			s_Out.shader3d = s_Instance.shader3d
			s_Out.drawOrderIndex = s_Instance.drawOrderIndex
			s_Out.tessellationTriangleSize = s_Instance.tessellationTriangleSize
			s_Instance:MakeWritable()

		end
		if(l_Instance.typeInfo == MeshVariationDatabaseEntry.typeInfo and self.m_DebugTexture ~= nil) then
			print("oos")
			local s_Instance = _G[l_Instance.typeInfo.name](l_Instance)
			s_Instance:MakeWritable()
			for k, l_Mat in pairs(s_Instance.materials) do
				for k2, l_TP in pairs(l_Mat.textureParameters) do
					l_TP.value = self.m_DebugTexture
				end
			end
		end
	end
end


g_shadergraphShared = shadergraphShared()

