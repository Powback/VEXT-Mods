class 'audiClient'


function audiClient:__init()
	print("Initializing audiClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function audiClient:RegisterVars()
	--self.m_this = that
end


function audiClient:RegisterEvents()
	WebUI:Init()
	WebUI:Show()
	WebUI:BringToFront()
end


g_audiClient = audiClient()

