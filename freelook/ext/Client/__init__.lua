class 'Freelook'


function Freelook:__init()
	print("Initializing Freelook")
	self:RegisterVars()
	self:RegisterEvents()
end


function Freelook:RegisterVars()
	self.m_Camera = nil
	self.m_CameraData = nil

end


function Freelook:RegisterEvents()
    self.m_UpdateEvent = Events:Subscribe("UpdateManager:Update", self, self.OnUpdateInput)
end

function Freelook:ClearCam()

end

function Freelook:OnUpdateInput(p_Hook, p_Cache, p_DeltaTime)
	local p_Player = PlayerManager:GetLocalPlayer()

	if (p_Player == nil) then
		return
	end
	if( p_Player.soldier ~= nil) then

		self.data = CameraEntityData()
		self.data.fov = 90
		local s_Entity = EntityManager:CreateClientEntity(self.data, LinearTransform())

		if s_Entity == nil then
			print("Could not spawn camera")
			return
		end
		s_Entity:Init(Realm.Realm_Client, true)


		--s_Entity:FireEvent("TakeControl") -- Takes control over the camera
		
		self.data.transform = ClientUtils:GetCameraTransform()
		self.data.fov = 90

		-- Get the head bone transform
		local transformLocal = p_Player.soldier.ragdollComponent:GetLocalTransform(1)
		print(transformLocal)
		transformLocal.transAndScale.w = 0
		p_Player.soldier.ragdollComponent:SetLocalTransform(1, transformLocal)


	end
end

-- Never ever fucking modify this, holy shit.
-- I spent 9 hours trying to figure out why my camera rotation wasn't working.
-- I swear to god, I will personally hunt you down.
function multiply(in1, in2)
	local Q1 = in1
	local Q2 = in2
	return Quat( (Q2.w * Q1.x) + (Q2.x * Q1.w) + (Q2.y * Q1.z) - (Q2.z * Q1.y),
	   (Q2.w * Q1.y) - (Q2.x * Q1.z) + (Q2.y * Q1.w) + (Q2.z * Q1.x),
	   (Q2.w * Q1.z) + (Q2.x * Q1.y) - (Q2.y * Q1.x) + (Q2.z * Q1.w),
	   (Q2.w * Q1.w) - (Q2.x * Q1.x) - (Q2.y * Q1.y) - (Q2.z * Q1.z) )
end

-- Regular normalizing function for quats.
function Normalize(quat)
	local n = quat.x * quat.x + quat.y * quat.y + quat.z * quat.z + quat.w * quat.w
	
	if n ~= 1 and n > 0 then
		n = 1 / math.sqrt(n)
		quat.x = quat.x * n
		quat.y = quat.y * n
		quat.z = quat.z * n
		quat.w = quat.w * n		
	end
	return quat
end

g_Freelook = Freelook()

