local InstanceFinder = require('__shared/util/instancefinder')

class('SP_Villa')

function SP_Villa:__init()
	self.m_Finder = InstanceFinder()

	self:InstallHooks()

    self.m_InclusionCategory = LevelDescriptionInclusionCategory()
    self.m_InclusionCategory.category = 'GameMode'
    self.m_InclusionCategory:AddMode('TeamDeathMatch0')
    self.m_InclusionCategory:AddMode('TeamDeathMatchC0')

	self.m_Finder:RegisterInstance('uidesc', Guid('4166F3A8-DAF2-7338-EA99-B1E05948E8F9', 'D'), self, self.OnUIDescription)
	self.m_Finder:RegisterInstance('desc', Guid('2AC0E2CB-C837-9FAB-A8D2-B1A03936DA97', 'D'), self, self.OnDescription)
	self.m_Finder:RegisterInstance('defwrld', Guid('6DE684C3-5D43-462C-8529-A2166214E85F', 'D'), self, self.OnDefaultWorld)
    self.m_Finder:RegisterInstance('level', Guid('03CEBAF4-C8BB-82A0-5F53-2FAD34CA84A1', 'D'), self, self.OnLevel)
    self.m_Finder:RegisterInstance('tdmsw', Guid('85413BF3-3775-4DF9-8B95-FAEE25CD1B31', 'D'), self, self.OnTDMSubWorld)
    self.m_Finder:RegisterInstance('tdmlog', Guid('7CF6DE9C-7BD3-406D-AFCC-BDF890A85462', 'D'), self, self.OnTDMLogic)
    self.m_Finder:RegisterInstance('reg', Guid('383F2AC9-94BF-286D-587A-27594550D561', 'D'), self, self.OnRegistry)
    self.m_Finder:RegisterInstance('dterr', Guid('24E08707-8D33-7BBC-1436-38E9C631C6C8', 'D'), self, self.OnDummyTerrain)
    self.m_Finder:RegisterInstance('terr', Guid('DCC6F05A-E6CB-45A5-B381-B96D1FFEFBCB', 'D'))
    self.m_Finder:RegisterInstance('grnd', Guid('6DE684C3-5D43-462C-8529-A2166214E85F', 'D'))


	--self.m_Finder:RegisterInstance('dmreg', Guid('5BA45AFD-07CD-09DE-94B5-2BE176AA80A1', 'D'))
	--self.m_Finder:RegisterInstance('dmsw', Guid('6D9ACF85-14A3-4815-B33B-44BDE0D7CAFE', 'D'))
	--self.m_Finder:RegisterInstance('cqlsw', Guid('AEC98074-2DAD-4D25-9DC0-A1E9C2355155', 'D'))
end

function SP_Villa:InstallHooks()
	self.m_LoadBundleHook = Hooks:Install('ResourceManager:LoadBundle', self, self.OnLoadBundle)
	self.m_ReadInstanceHook = Hooks:Install('Partition:ReadInstance', self, self.OnReadInstanceHook)
end

function SP_Villa:OnLoadBundle(p_Hook, p_Bundle)
	print(string.format("Loading bundle '%s'", p_Bundle))

	if p_Bundle == 'Levels/SP_Villa/SP_Villa_UiPlaying' then
		return p_Hook:CallOriginal('Levels/XP1_002/XP1_002_UiPlaying')
    end

	return p_Hook:CallOriginal(p_Bundle)
end

function SP_Villa:OnLoadResources(p_Dedicated)
	print("Loading level resources!")

	SharedUtils:MountSuperBundle('MpChunks')
	SharedUtils:MountSuperBundle('Xp1Chunks')
	SharedUtils:MountSuperBundle('Levels/XP1_002/XP1_002')

    SharedUtils:PrecacheBundle('levels/xp1_002/xp1_002')
    --SharedUtils:PrecacheBundle('levels/xp1_002/xp1_002_uiplaying')
    SharedUtils:PrecacheBundle('levels/xp1_002/xp1_002_uiloadingmp')
    SharedUtils:PrecacheBundle('levels/xp1_002/xp1_002_loading_music')
    SharedUtils:PrecacheBundle('levels/sp_villa/sp_villa')
    SharedUtils:PrecacheBundle('levels/sp_villa/sp_villa_settings_win32')
    SharedUtils:PrecacheBundle('levels/sp_villa/sp_villa_gameconfiglight_win32')
    SharedUtils:PrecacheBundle('levels/sp_villa/background')
    SharedUtils:PrecacheBundle('levels/sp_villa/poolhouse')
    SharedUtils:PrecacheBundle('levels/sp_villa/poolhouse_extra')
    SharedUtils:PrecacheBundle('levels/sp_villa/drive')
    SharedUtils:PrecacheBundle('levels/sp_villa/drive_2')
    SharedUtils:PrecacheBundle('levels/sp_villa/drive_pc')
    SharedUtils:PrecacheBundle('levels/sp_villa/basement')
    SharedUtils:PrecacheBundle('levels/sp_villa/garden')
    SharedUtils:PrecacheBundle('levels/sp_villa/garden_pc')
    SharedUtils:PrecacheBundle('levels/sp_villa/rail')
    SharedUtils:PrecacheBundle('levels/sp_villa/railsuv')
    SharedUtils:PrecacheBundle('levels/sp_villa/landing')
    SharedUtils:PrecacheBundle('levels/sp_villa/villa')
    SharedUtils:PrecacheBundle('levels/sp_villa/villa_extra')
    SharedUtils:PrecacheBundle('levels/sp_villa/villa_pc')
    SharedUtils:PrecacheBundle('levels/sp_villa/blackburn')
    SharedUtils:PrecacheBundle('levels/sp_villa/halo')
    SharedUtils:PrecacheBundle('levels/sp_villa/halo_backdrop')
    SharedUtils:PrecacheBundle('levels/sp_villa/chopper')
    SharedUtils:PrecacheBundle('levels/sp_villa/lightmap_01')
    SharedUtils:PrecacheBundle('levels/sp_villa/lightmap_02')
    SharedUtils:PrecacheBundle('levels/sp_villa/lightmap_03')
    SharedUtils:PrecacheBundle('levels/sp_villa/lightmap_cutscene01')
    SharedUtils:PrecacheBundle('levels/sp_villa/lightmap_cutscene02')
    SharedUtils:PrecacheBundle('levels/sp_villa/gatehouse')
    SharedUtils:PrecacheBundle('levels/sp_villa/sp_villa_uiloadingsp')
    SharedUtils:PrecacheBundle('levels/sp_villa/sp_villa_uiloadingmp')
    SharedUtils:PrecacheBundle('levels/sp_villa/sp_villa_loading_music')
    --SharedUtils:PrecacheBundle('levels/sp_villa/sp_villa_uiplaying')
    SharedUtils:PrecacheBundle('levels/sp_villa/sp_villa_uipreendofround')
    SharedUtils:PrecacheBundle('levels/sp_villa/sp_villa_uiendofround')
end

function SP_Villa:OnReadInstance(p_Instance, p_Guid)
	self.m_Finder:OnReadInstance(p_Instance, p_Guid)

	--[[if p_Instance.typeName == 'GeometryTriggerEntityData' or
		p_Instance.typeName == 'LookAtTriggerEntityData' then
		--p_Instance.typeName == 'MultipleTriggerEntityData' or
		--p_Instance.typeName == 'WaypointTriggerEntityData' or
		--p_Instance.typeName == 'PlayerInputTriggerEntityData' then
		print(string.format('Disabling "%s" trigger (%s).', p_Instance.typeName, p_Guid:ToString('D')))

		local s_Instance = TriggerEntityData(p_Instance)
		s_Instance.enabled = false
	end]]
end

function SP_Villa:OnReadInstanceHook(p_Hook, p_Instance, p_Guid)
    if p_Guid == Guid('24E08707-8D33-7BBC-1436-38E9C631C6C8', 'D') or
        p_Guid == Guid('8D6CF61E-0474-9484-2017-359D7DBC07BF', 'D') or
        p_Guid == Guid('1F1713FC-A988-F7AA-62D8-47D90F7A830F', 'D') or
        p_Guid == Guid('7C5B7C7B-7EA9-725C-53F3-E74B9EB02C06', 'D') then
        print('Skipping terrain')
        return false
    end

    return true

	--return p_Hook:CallOriginal(p_Instance, p_Guid)
end

function SP_Villa:OnUIDescription(p_Instance)
    local s_Instance = UILevelDescriptionComponent(p_Instance)

    --local s_OtherDescription = UILevelDescriptionComponent(EntityManager:SearchForInstanceByGUID(Guid('6CA15126-A0A1-9ABB-F473-5A9BF82A3444', 'D')))

    s_Instance.sPLoadingAssetPath = 'UI/Assets/LoadingScreen'
    s_Instance.mPLoadingAssetPath = 'UI/Assets/LoadingScreen'
    --s_Instance.hintAsset = s_OtherDescription.hintAsset
end

function SP_Villa:OnDescription(p_Instance)
	local s_Instance = LevelDescriptionAsset(p_Instance)

    --local s_OtherDescription = LevelDescriptionAsset(EntityManager:SearchForInstanceByGUID(Guid('861C6301-33DE-C4D2-568B-7F3EBC6DBC5D', 'D')))
    s_Instance:AddCategories(self.m_InclusionCategory)

    s_Instance:ClearStartPoints()

	s_Instance.description.name = 'KAFFAROV\'S VILLA'
    s_Instance.description.isMultiplayer = true
end

function SP_Villa:OnDefaultWorld(p_Instance)
    local s_Instance = WorldPartData(p_Instance)

    print('Default world')

    local s_Terrain = s_Instance:GetObjectsAt(0)
    local s_Ground = s_Instance:GetObjectsAt(6)

    s_Instance:ClearObjects()
    s_Instance:AddObjects(s_Terrain)
    --s_Instance:AddObjects(s_Ground)
end

function SP_Villa:OnLevelXP1_002(p_Instance)
    local s_Instance = LevelData(p_Instance)

    print('Level thing')

    s_Instance:RemoveObjectsAt(34) -- StaticModelGroupEntityData
    s_Instance:RemoveObjectsAt(33) -- Levels/XP1_002/Container_03_Open/84aebb2b53af754b9c4fd9c6ddadcf36
    s_Instance:RemoveObjectsAt(32) -- Levels/XP1_002/FX_Props/3d29079326bd144da6bd7c673cdb1af1
    s_Instance:RemoveObjectsAt(31) -- Levels/XP1_002/FX_Cables/76d2f1b745866e4596cc3565ee141a1a
    s_Instance:RemoveObjectsAt(30) -- Levels/XP1_002/MEC_HotelKarkand_Prefab2/696cc7c57eb90e49bc97e49a00717223
    s_Instance:RemoveObjectsAt(29) -- Levels/XP1_002/MEC_HotelKarkand_Prefab/c57b0d3382bc684a8394a9ffc9ba0d71
    s_Instance:RemoveObjectsAt(28) -- Levels/XP1_002/FX_Schematic_Schematic/de1e2918b6cf7f4086c0b9643181166c
    s_Instance:RemoveObjectsAt(27) -- Levels/XP1_002/Sound/81bfc4ad839c0a4e9d970d27e6cf5a1f
    s_Instance:RemoveObjectsAt(26) -- Levels/XP1_002/MEC_House_Double_02_Prefab/937715b09d3a8d4e8044d40c8a2f629d
    s_Instance:RemoveObjectsAt(25) -- Levels/XP1_002/FX_Schematic/3a7a98c6620587479e4040b993cf1abc
    s_Instance:RemoveObjectsAt(24) -- Levels/XP1_002/Water/233e662367110f4ab289a29f7de530bb
    s_Instance:RemoveObjectsAt(22) -- Levels/XP1_002/Boulders/2a76c6858867d34d9322d2e1f0e47586
    s_Instance:RemoveObjectsAt(21) -- Levels/XP1_002/Backdrop/81b58b0ea61eb64d8c68f10f977f84ec
    s_Instance:RemoveObjectsAt(20) -- Levels/XP1_002/Lighting/Oman_Static_Enlighten/9e8c785a8ebff4d1ac75787822d148cf
    s_Instance:RemoveObjectsAt(19) -- Levels/XP1_002/Lighting/Oman_Enlighten/81296146d66809c782e46709ac319ed7
    s_Instance:RemoveObjectsAt(18) -- Levels/XP1_002/Lighting/VE_XP1_002_02/e82943d7ac3d58498a40a3b495e9bf02
    s_Instance:RemoveObjectsAt(17) -- Levels/XP1_002/Sound_Schematic/63f9b1fc62d6564c873b1585c78a5ca2
    s_Instance:RemoveObjectsAt(16) -- Levels/XP1_002/Lights/6464966712d4db42b900be8b42735de3
    s_Instance:RemoveObjectsAt(15) -- Levels/XP1_002/Construction/07916c7ed5f5d7449f0e58831112329c
    s_Instance:RemoveObjectsAt(14) -- Levels/XP1_002/Vegetation/e10eafb8ab66d84f9decf7ecc958ab4a
    s_Instance:RemoveObjectsAt(13) -- Levels/XP1_002/Buildings/96dfc3b2c170f84286d61e59b1cdff6b
    s_Instance:RemoveObjectsAt(12) -- Levels/XP1_002/Objects/47cd2e95eecc5849929280297ce9123a
    s_Instance:RemoveObjectsAt(5) -- Levels/XP1_002/FX/f82edad6d979954a8feb39ddad2a65fe
    s_Instance:RemoveObjectsAt(4) -- Levels/XP1_002/Roads/c777466fb2e879448956ee16125f12bd
    s_Instance:RemoveObjectsAt(3) -- Levels/XP1_002/GameLogic2/426dfdb84431214293f090b7a0595ea5
    s_Instance:RemoveObjectsAt(2) -- Levels/XP1_002/GulfOfOmanSchematics/08b6d2bdb19dff48aad4b86e6e9e8f0e
    s_Instance:RemoveObjectsAt(0) -- Levels/XP1_002/Terrain_2/GulfTerrain_03.Water.Mesh/eb2da1bb7abcf7e75e0d43dfaf4afff4

    s_Instance.maxVehicleHeight = 9999.0
    --s_Instance.freeStreamingEnable = false

    s_Instance.levelDescription:ClearComponents()

    s_Instance:ClearEventConnections()
    s_Instance:ClearLinkConnections()
    s_Instance:ClearPropertyConnections()
end

function SP_Villa:OnTDMSubWorld(p_Instance)
    local s_Instance = SubWorldData(p_Instance)

    print('TDM data')

    local s_Spawn01 = s_Instance:GetLinkConnectionsAt(0)
    local s_Spawn02 = s_Instance:GetLinkConnectionsAt(1)
    local s_Spawn03 = s_Instance:GetLinkConnectionsAt(19)
    local s_Spawn04 = s_Instance:GetLinkConnectionsAt(20)
    local s_Spawn05 = s_Instance:GetLinkConnectionsAt(38)
    local s_Spawn06 = s_Instance:GetLinkConnectionsAt(39)
    local s_Spawn07 = s_Instance:GetLinkConnectionsAt(40)
    local s_Spawn08 = s_Instance:GetLinkConnectionsAt(41)
    local s_Spawn09 = s_Instance:GetLinkConnectionsAt(42)
    local s_Spawn10 = s_Instance:GetLinkConnectionsAt(79)
    local s_Spawn11 = s_Instance:GetLinkConnectionsAt(80)

    --[[s_Instance:ClearLinkConnections()
    s_Instance:AddLinkConnections(s_Spawn01)
    s_Instance:AddLinkConnections(s_Spawn02)
    s_Instance:AddLinkConnections(s_Spawn03)
    s_Instance:AddLinkConnections(s_Spawn04)
    s_Instance:AddLinkConnections(s_Spawn05)
    s_Instance:AddLinkConnections(s_Spawn06)
    s_Instance:AddLinkConnections(s_Spawn07)
    s_Instance:AddLinkConnections(s_Spawn08)
    s_Instance:AddLinkConnections(s_Spawn09)
    s_Instance:AddLinkConnections(s_Spawn10)
    s_Instance:AddLinkConnections(s_Spawn11)]]

    for i = 84, 81, -1 do
        s_Instance:RemoveLinkConnectionsAt(i)
    end

    for i = 78, 43, -1 do
        s_Instance:RemoveLinkConnectionsAt(i)
    end

    for i = 37, 1, -1 do
        s_Instance:RemoveLinkConnectionsAt(i)
    end

    s_Instance:ClearPropertyConnections()

    InterfaceDescriptorData(s_Instance.descriptor):ClearFields()

    s_Instance:RemoveObjectsAt(4) -- StaticModelGroupEntityData
    s_Instance:RemoveObjectsAt(3) -- Levels/XP1_002/TDM_Objects/3b53ac177f1e8a4a90814c199b999b91
    --s_Instance:RemoveObjectsAt(2) -- Levels/XP1_002/Full_TeamDeathmatch_Breakable/21e8025f3c8f8d49b204b3e5ae8ff98a
    --s_Instance:RemoveObjectsAt(1) -- Levels/XP1_002/TDM_CZ/f8e3c94c078285438fff3ce977997976

    print('boom!')
end

function SP_Villa:OnTDMLogic(p_Instance)
    local s_Instance = WorldPartData(p_Instance)

    print('TDM logic')

    local s_Setup = s_Instance:GetObjectsAt(19)

    s_Instance:ClearObjects()
    s_Instance:AddObjects(s_Setup)
end

function SP_Villa:OnRegistry(p_Instance)
    local s_Instance = RegistryContainer(p_Instance)

    s_Instance:ClearReferenceObjectRegistry()
    s_Instance:ClearBlueprintRegistry()
    s_Instance:ClearAssetRegistry()
    s_Instance:ClearEntityRegistry()
end


function SP_Villa:OnLevel(p_Instance)
	local s_Instance = LevelData(p_Instance)

	-- Set map to multiplayer.
	s_Instance.levelDescription.isCoop = false
	s_Instance.levelDescription.isMultiplayer = true

	--[[SubWorldReferenceObjectData(s_Instance:GetObjectsAt(10)).autoLoad = true -- Rail
	SubWorldReferenceObjectData(s_Instance:GetObjectsAt(11)).autoLoad = true -- GateHouse
	SubWorldReferenceObjectData(s_Instance:GetObjectsAt(12)).autoLoad = true -- Drive
	SubWorldReferenceObjectData(s_Instance:GetObjectsAt(13)).autoLoad = true -- Garden
	--SubWorldReferenceObjectData(s_Instance:GetObjectsAt(14)).autoLoad = true -- Villa
	--SubWorldReferenceObjectData(s_Instance:GetObjectsAt(15)).autoLoad = true -- Chopper
	SubWorldReferenceObjectData(s_Instance:GetObjectsAt(16)).autoLoad = true -- Basement
	SubWorldReferenceObjectData(s_Instance:GetObjectsAt(17)).autoLoad = true -- Background
	SubWorldReferenceObjectData(s_Instance:GetObjectsAt(21)).autoLoad = false -- Halo
	SubWorldReferenceObjectData(s_Instance:GetObjectsAt(28)).autoLoad = true -- Landing
	SubWorldReferenceObjectData(s_Instance:GetObjectsAt(29)).autoLoad = true -- Poolhouse
	--SubWorldReferenceObjectData(s_Instance:GetObjectsAt(32)).autoLoad = true -- Blackburn
	SubWorldReferenceObjectData(s_Instance:GetObjectsAt(34)).autoLoad = true -- Villa_Extra
	--SubWorldReferenceObjectData(s_Instance:GetObjectsAt(43)).autoLoad = true -- Halo_Backdrop
	--SubWorldReferenceObjectData(s_Instance:GetObjectsAt(44)).autoLoad = true -- RailSUV
	--SubWorldReferenceObjectData(s_Instance:GetObjectsAt(51)).autoLoad = false -- Lightmap_01
	--SubWorldReferenceObjectData(s_Instance:GetObjectsAt(52)).autoLoad = false -- Lightmap_02
	--SubWorldReferenceObjectData(s_Instance:GetObjectsAt(53)).autoLoad = false -- Lightmap_03
	SubWorldReferenceObjectData(s_Instance:GetObjectsAt(55)).autoLoad = true -- Poolhouse_Extra

	-- Line 8421
	s_Instance:RemoveObjectsAt(57) -- Pathfinding
	s_Instance:RemoveObjectsAt(56) -- Sensing

	--s_Instance:RemoveObjectsAt(54) -- VE

	s_Instance:RemoveObjectsAt(48) -- Levels/SP_Villa/Lightmap_Cutscene02
	s_Instance:RemoveObjectsAt(47) -- Levels/SP_Villa/Lightmap_Cutscene01

	--s_Instance:RemoveObjectsAt(46) -- VE
	--s_Instance:RemoveObjectsAt(45) -- VE
	s_Instance:RemoveObjectsAt(42) -- VE
	s_Instance:RemoveObjectsAt(37) -- VE

	-- Remove VE Schematic
	s_Instance:RemoveObjectsAt(31)
	s_Instance:RemoveObjectsAt(30)

	-- Remove objective logic.
	s_Instance:RemoveObjectsAt(27)
	s_Instance:RemoveObjectsAt(26)

	s_Instance:RemoveObjectsAt(25) -- VE
	s_Instance:RemoveObjectsAt(24) -- VE
	s_Instance:RemoveObjectsAt(23) -- VE
	s_Instance:RemoveObjectsAt(22) -- VE

	--s_Instance:RemoveObjectsAt(20)
	--s_Instance:RemoveObjectsAt(19)
	--s_Instance:RemoveObjectsAt(18)

	-- Remove unneeded bundles.
	s_Instance:RemoveObjectsAt(21) -- Levels/SP_Villa/Halo

	s_Instance:RemoveObjectsAt(9) -- VE
	s_Instance:RemoveObjectsAt(5) -- VE

	-- Remove setup.
	s_Instance:RemoveObjectsAt(4)
	s_Instance:RemoveObjectsAt(3)

	-- Add the DeathMatch bundle.
	--s_Instance:AddObjects(GameObjectData(self.m_Finder:GetInstance('dmsw')))

	-- Add the Conquest Large bundle.
	s_Instance:AddObjects(GameObjectData(self.m_Finder:GetInstance('cqlsw')))]]

    s_Instance.maxVehicleHeight = 9999.0
    --s_Instance.freeStreamingEnable = false

    local s_Thing = EntityManager:SearchForInstanceByGUID(Guid('F01CC389-A9A3-40E1-97CD-0BAE80C82D5E', 'D'))

    print(string.format('%s', s_Thing))

    --s_Instance.levelDescription:SetComponentsAt(0, LevelDescriptionComponent(s_Thing)) -- Minimap

    print('boom')

    s_Instance.levelDescription:RemoveComponentsAt(1)
    s_Instance.levelDescription:RemoveComponentsAt(1)

    print('boom')

    s_Instance:ClearEventConnections()
    s_Instance:ClearLinkConnections()
    s_Instance:ClearPropertyConnections()

    local s_Default = s_Instance:GetObjectsAt(1)

    s_Instance:ClearObjects()
    s_Instance:AddObjects(s_Default) -- Default Blueprint

    local s_TDMBundle = EntityManager:SearchForInstanceByGUID(Guid('21758A6B-2563-4CF4-AE5E-6F515AADEE6D', 'D'))
    SubWorldReferenceObjectData(s_TDMBundle).bundleName = 'Levels/XP1_002/TDM'

    s_Instance:AddObjects(GameObjectData(s_TDMBundle)) -- XP1_002 TDM Bundle
end

function SP_Villa:OnDummyTerrain(p_Instance)
    local s_Instance = TerrainData(p_Instance)

    print('Got dummy terrain')

    --s_Instance.name = 'Levels/SP_Villa/Terrain/SP_Villa_Terrain_01'
end

return SP_Villa