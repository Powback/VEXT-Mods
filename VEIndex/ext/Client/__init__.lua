class 'VEIndexClient'


function VEIndexClient:__init()
	print("Initializing VEIndexClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function VEIndexClient:RegisterVars()
	--self.m_this = that
end


function VEIndexClient:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end


function VEIndexClient:ReadInstance(p_Instance,p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	
	if p_Instance.typeName == "VisualEnvironmentBlueprint" then
		local s_Instance = VisualEnvironmentBlueprint(p_Instance)
		print(s_Instance.name)
	end
	
	if p_Guid == Guid('B2FF66A5-494A-B9C4-429E-2E94E03B9CA3', 'D') then
        local s_Instance = VisualEnvironmentBlueprint(ResourceManager:FindInstanceByGUID(Guid("3A3E5533-4B2A-11E0-A20D-FE03F1AD0E2F", 'D'), Guid('B2FF66A5-494A-B9C4-429E-2E94E03B9CA3', 'D')))
        
		local s_Object = VisualEnvironmentEntityData(s_Instance.object)
		local s_Dof = DofComponentData (s_Object:GetComponentsAt(2))
		local s_CC = ColorCorrectionComponentData(s_Object:GetComponentsAt(1))
		local s_Vig = VignetteComponentData (s_Object:GetComponentsAt(0))
		
		s_Object:AddComponents(s_FilmGrain)

		s_CC.enable = true
		s_CC.colorGradingEnable = true
		s_CC.brightness = Vec3(0.6, 0.6, 0.6)
		s_CC.contrast = Vec3(1.8, 1.8, 1.8)
		s_CC.saturation = Vec3(0.3, 0.3, 0.3)

		s_Dof.enable = true
		s_Dof.blurFilter = 6;
		s_Dof.blurFilterDeviation = 3.12

		s_Vig.enable = true
		s_Vig.scale = Vec2(1.4, 1.65)
		s_Vig.exponent = 0.66
		s_Vig.opacity = 0.86

	end
	
end

local table = [[
{
    "ColorCorrection": {
        "Enable": true,
        "ColorGradingEnable": true,
        "Saturation": "0.3:0.3:0.3:",
        "Contrast": "1.2:1.2:1.2:",
        "Brightness": "0.79:0.79:0.79:"
    },
    "Dof": {
        "Enable": true,
        "BlurFilter": "6",
        "BlurFilterDeviation": "3.12",
        "NearDistanceScale": "0.57",
        "FarDistanceScale": "3.59",
        "BlurAdd": "0",
        "DiffusionDofEnable": true,
        "DiffusionDofAperture": "5.16",
        "FocusDistance": "4.45",
        "Scale": "1"
    },
    "Vignette": {
        "Enable": true,
        "Scale": "1.78:1.42:",
        "Exponent": "0.66",
        "Opacity": "0.84"
    },
    "Tonemap": {
        "MiddleGray": "0.38"
    },
    "FilmGrain": {
        "Enable": true,
        "TextureScale": "10:2:",
        "ColorScale": "0.5:0.5:0.5:",
        "LinearFilteringEnable": true,
        "RandomEnable": true
    },
    "Name": "CustomPreset",
    "Priority": "0"
}
]]


g_VEIndexClient = VEIndexClient()

