class 'OppressorShared'


function OppressorShared:__init()
	print("Initializing OppressorShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function OppressorShared:RegisterVars()
end


function OppressorShared:RegisterEvents()
	Events:Subscribe('ExtensionLoaded', self, self.OnModify)
	Events:Subscribe('Level:Destory', self, self.OnCleanup)
	Events:Subscribe('ExtensionUnloading', self, self.OnCleanup)
   	Events:Subscribe('Engine:Message', self, self.OnEngineMessage)
   	Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end



function OppressorShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end

	local s_Instances = p_Partition.instances

	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeInfo.name == "VehicleSpawnReferenceObjectData") then
			local s_Instance = VehicleSpawnReferenceObjectData(l_Instance)
			s_Instance:MakeWritable()
			s_Instance.enabled = false
		end
	end

	

end

function OppressorShared:OnEngineMessage(p_Message)
	if p_Message.type == MessageType.ClientLevelFinalizedMessage or p_Message.type == MessageType.ServerLevelLoadedMessage then
		self:OnModify()
	end
end


function OppressorShared:OnModify()
	if(ResourceManager:FindInstanceByGUID(Guid('38FA36CC-22C8-4624-89BF-1D95C7CE7815'), Guid('A401FE88-C8AE-43D1-BD60-13F9464060B6')) == nil) then
		print("Failed to get AC130, probably loading for the first time...")
		return
	end
	print("Modifying")

	local s_BikeBlueprint = VehicleBlueprint(ResourceManager:FindInstanceByGUID(Guid('38FA36CC-22C8-4624-89BF-1D95C7CE7815'), Guid('A401FE88-C8AE-43D1-BD60-13F9464060B6')))
	local s_Bike = VehicleEntityData(s_BikeBlueprint.object)
	s_Bike:MakeWritable()
	local s_BikeChassis = ChassisComponentData(s_Bike.components:get(1))
	s_BikeChassis:MakeWritable()
	print("Component count clean: " .. #s_BikeChassis.components)

	local s_BikeConfig = VehicleConfigData(s_BikeChassis.vehicleConfig)
	s_BikeConfig:MakeWritable()

	s_Bike.runtimeComponentCount = s_Bike.runtimeComponentCount + 99
	s_BikeChassis.components:add(self:CreateWing(Vec3(-1,0,0)))
	s_BikeChassis.components:add(self:CreateWing(Vec3(1,0,0)))

	s_BikeChassis.components:add(self:CreateWing(Vec3(-1,0,0), RotationAxisEnum.RALeft, -1))
	s_BikeChassis.components:add(self:CreateWing(Vec3(1,0,0), RotationAxisEnum.RALeft, 1))

	local s_Engine = EngineComponentData(s_BikeChassis.components:get(7))
	local s_EngineConfig = CombustionEngineConfigData(s_Engine.config)
	s_EngineConfig:MakeWritable()
	--s_EngineConfig.boost.forwardStrength = 1
	s_BikeChassis.components:add(self:CreateEngine())
	print("Modified")
	print("Component count modified: " .. #s_BikeChassis.components)



end
function OppressorShared:OnCleanup()
	local s_BikeBlueprint = VehicleBlueprint(ResourceManager:FindInstanceByGUID(Guid('38FA36CC-22C8-4624-89BF-1D95C7CE7815'), Guid('A401FE88-C8AE-43D1-BD60-13F9464060B6')))
	local s_Bike = VehicleEntityData(s_BikeBlueprint.object)
	local s_BikeChassis = ChassisComponentData(s_Bike.components:get(1))
	print(#s_BikeChassis.components)
	for k = #s_BikeChassis.components, 1, -1 do
		print(k)
		local s_Component = s_BikeChassis.components:get(k)
		if(s_Component.instanceGuid == nil) then
			print("Erasing " .. s_Component.typeInfo.name)
			s_BikeChassis.components:erase(k)
		end
	end
	print("Component count cleaned: " .. #s_BikeChassis.components)

	print("Cleaned")
	s_Bike.runtimeComponentCount = s_Bike.runtimeComponentCount - 99
end
function OppressorShared:CreateWing(p_Trans, p_RotationAxis, p_RotationScale)
	local s_WingBase = WingComponentData()
	s_WingBase.indexInBlueprint = 65535
	s_WingBase.isEventConnectionTarget = 3
	s_WingBase.isPropertyConnectionTarget = 3
	s_WingBase.isNetworkable = false

	local s_WingConfig = WingPhysicsData()
	s_WingConfig.lift = 100
	s_WingConfig.flapLift = 2.5
	s_WingConfig.liftCoefficient = Curve2D()
	s_WingConfig.liftCoefficient.curve:add(Vec2(-60, -50))
	s_WingConfig.liftCoefficient.curve:add(Vec2(-40, -20))
	s_WingConfig.liftCoefficient.curve:add(Vec2(-28, -10))
	s_WingConfig.liftCoefficient.curve:add(Vec2(-24, -6))
	s_WingConfig.liftCoefficient.curve:add(Vec2(-10, -2))
	s_WingConfig.liftCoefficient.curve:add(Vec2(-0.75, 0))
	s_WingConfig.liftCoefficient.curve:add(Vec2(0.75, 0))
	s_WingConfig.liftCoefficient.curve:add(Vec2(10, 2))
	s_WingConfig.liftCoefficient.curve:add(Vec2(24, 6))
	s_WingConfig.liftCoefficient.curve:add(Vec2(28, 10))
	s_WingConfig.liftCoefficient.curve:add(Vec2(40, 20))
	s_WingConfig.liftCoefficient.curve:add(Vec2(60, 50))

	s_WingConfig.drag = 0
	s_WingConfig.flapDrag = 0
	s_WingConfig.dragRotationModifier = 0
	s_WingConfig.flapTurnSpeed = 1
	s_WingConfig.visualFlapTurnSpeed = 1
	s_WingConfig.visualFlapAngleLimit = 90

	s_WingBase.config = s_WingConfig

	s_WingBase.transform = LinearTransform()
	s_WingBase.transform.trans = p_Trans
	if(p_RotationAxis == nil) then
		return s_WingBase
	end

	local s_FlapComponent = FlapComponentData()
	s_FlapComponent.rotationAxis = p_RotationAxis
	s_FlapComponent.rotationScale = p_RotationScalet
	s_FlapComponent.isNetworkable = false
	s_FlapComponent.transform = LinearTransform()
	s_FlapComponent.transform.trans = p_Trans
	s_WingBase.components:add(s_FlapComponent)

	return s_WingBase
end

function OppressorShared:CreateEngine()
	local s_Engine = EngineComponentData()
	s_Engine.indexInBlueprint = -1
	s_Engine.isEventConnectionTarget = 3
	s_Engine.isPropertyConnectionTarget = 3
	s_Engine.transform = LinearTransform()
	s_Engine.transform.trans.z = -1


	local s_EngineConfig = JetEngineConfigData()
	s_EngineConfig.position = Vec3(0,0,-1)


	s_EngineConfig.rpmCurvePoints:add(0.0)
	s_EngineConfig.rpmCurvePoints:add(250.0)
	s_EngineConfig.rpmCurvePoints:add(500.0)

	s_EngineConfig.torqueCurvePoints:add(0.0)
	s_EngineConfig.torqueCurvePoints:add(250.0)
	s_EngineConfig.torqueCurvePoints:add(500.0)

	s_EngineConfig.rpmMin = 0
	s_EngineConfig.rpmMax = 500
	s_EngineConfig.rpmCut = 950
	s_EngineConfig.enginePowerMultiplier = 1.4
	s_EngineConfig.internalAccelerationFactor = 0.8
	s_EngineConfig.internalDeaccelerationFactor = 0.6

	s_EngineConfig.boost.forwardStrength = 2
	s_EngineConfig.boost.reverseStrength = 1.0
	s_EngineConfig.boost.dissipationTime = 1.0
	s_EngineConfig.boost.recoveryTime = 1.0
	s_EngineConfig.boost.crawlStrength = 1.0
	s_EngineConfig.boost.accelerationScale = 1.0

	s_EngineConfig.directionVectorIndex = 2
	s_EngineConfig.forceMagnitudeMultiplier = 1
	s_EngineConfig.angleInputYMultiplier = 1
	s_EngineConfig.angleInputPitchMultiplier = 1
	s_EngineConfig.maxVelocity = 3200
	s_EngineConfig.isTurnable = true
	s_EngineConfig.isWaterJetEngine = false

	s_EngineConfig.powerFadeOutRange = Vec2(1000, 1500)


	s_Engine.config = s_EngineConfig
	return s_Engine
end

function ClearPartsFromComponents( p_Instance )
	local s_TypeInfo = p_Instance.typeInfo
	local s_Instance = _G[s_TypeInfo.name](p_Instance)
	if(s_Instance.healthStates ~= nil) then
		for k,v in ipairs(s_Instance.healthStates) do
			local s_HealthState = HealthStateData(v)
			s_HealthState:MakeWritable()
			print("Removing healthstate" .. s_HealthState.partIndex)
			s_HealthState.partIndex = 4294967295
		end
	end
	if(s_Instance.components ~= nil) then
		for k,v in ipairs(s_Instance.components) do
			s_Instance.components[k] = ClearPartsFromComponents(v:Clone())
		end
	end


	return s_Instance
end

g_OppressorShared = OppressorShared()

