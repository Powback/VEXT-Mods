class 'OppressorServer'


function OppressorServer:__init()
	print("Initializing OppressorServer")
	self:RegisterVars()
	self:RegisterEvents()
end


function OppressorServer:RegisterVars()
	--self.m_this = that
end


function OppressorServer:RegisterEvents()
	self.m_PlayerChat = Events:Subscribe('Player:Chat', self, self.PlayerChat)
end

function OppressorServer:PlayerChat(p_Player, p_RecipientMask, p_Message)
	if message == '' then
		return
	end
	self:SpawnAC(p_Player)
end

function OppressorServer:SpawnAC( p_Player )
	local trans = p_Player.soldier.transform.trans
	print('Spawning')

	local transform = LinearTransform(
		Vec3(1, 0, 0),
		Vec3(0, 1, 0),
		Vec3(0, 0, 1),
		Vec3(trans.x, trans.y, trans.z)
	)
	local params = EntityCreationParams()
	params.transform = transform
	params.networked = true
	print("lego")
	local s_Blueprint = ResourceManager:FindInstanceByGUID(Guid('38FA36CC-22C8-4624-89BF-1D95C7CE7815'), Guid('A401FE88-C8AE-43D1-BD60-13F9464060B6'))

	local vehicles = EntityManager:CreateEntitiesFromBlueprint(s_Blueprint, params)
	for i, entity in ipairs(vehicles) do
		print(i)
		entity:Init(Realm.Realm_ClientAndServer, true)
	end
end

O_oppressorServer = OppressorServer()

