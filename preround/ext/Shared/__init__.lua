class 'preroundShared'


function preroundShared:__init()
	print("Initializing preroundShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function preroundShared:RegisterVars()
	--self.m_this = that
end


function preroundShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end


function preroundShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeInfo.name == "PreRoundEntityData") then
			local s_Instance = PreRoundEntityData(l_Instance:Clone(l_Instance.instanceGuid))
				s_Instance.roundMaxPlayerCount = 0
				s_Instance.roundMinPlayerCount = -1
				s_Instance.roundRestartCountdown = 10
				s_Instance.enabled = true

			p_Partition:ReplaceInstance(l_Instance, s_Instance, true)
		end
	end
	local settings = ResourceManager:GetSettings("BFServerSettings")
	if(settings ~= nil) then
		print("settings")
		setting = BFServerSettings(settings)
		setting.roundMaxPlayerCount = 0
		setting.roundMinPlayerCount = -1
		setting.roundLockdownCountdown = 0
		setting.roundRestartCountdown = 0
		setting.roundWarmupTimeout = 0
	end
end


g_preroundShared = preroundShared()

