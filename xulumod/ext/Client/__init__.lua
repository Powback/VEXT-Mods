class 'xulumodClient'


function xulumodClient:__init()
	print("Initializing xulumodClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function xulumodClient:RegisterVars()
	self.objectBlueprints = {}
	self.spawnedObjects = {}
end


function xulumodClient:RegisterEvents()
	-- self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)

  self.m_SpawnObjectEvent = NetEvents:Subscribe('spawnObject', self, self.SpawnObject)
  self.m_SpawnObjectEvent = NetEvents:Subscribe('spawnWall', self, self.SpawnWall)
end

function xulumodClient:SpawnWall(p_X, p_Y, p_Z)
  local wall = ResourceManager:FindInstanceByGUID("e407b506-a4d3-11e1-8f7b-a4270eaee571", "be93df81-5212-2de3-4852-c72148004f15")

  if wall == nil then
    print("Cound't find wall")
  end

  local obj = _G[wall.typeInfo.name](wall)
  
  local transform = LinearTransform(
    Vec3(1.0, 0.0, 0.0),
    Vec3(0.0, 1.0, 0.0),
    Vec3(0.0, 0.0, 1.0),
    Vec3(p_X, p_Y , p_Z)
  )

  local objectEntities = EntityManager:CreateEntitiesFromBlueprint(obj, transform)
  
  table.insert(self.spawnedObjects, objectEntities)
  for i, entity in ipairs(objectEntities) do
    entity:Init(Realm.Realm_Client, true)
  end
end

function xulumodClient:SpawnObject(p_ObjectName, p_X, p_Y, p_Z)
	local objectMatch = string.lower(p_ObjectName)
	local objectBlueprint = nil

	-- Find object that matches
	for i, blueprint in spairs(self.objectBlueprints) do
		local lowerName = string.lower(blueprint.name)

		if string.find(lowerName, objectMatch) ~= nil then
			objectBlueprint = blueprint
			break
		end
	end

	if objectBlueprint == nil then
		print('Could not find matching object ['.. p_ObjectName .. ']')
		return
	end

	print('Got spawn object event for '.. objectBlueprint.name)
	
	local transform = LinearTransform(
		Vec3(1.0, 0.0, 0.0),
		Vec3(0.0, 1.0, 0.0),
		Vec3(0.0, 0.0, 1.0),
		Vec3(p_X, p_Y , p_Z)
	)

	local params = EntityCreationParams()
	params.transform = transform
	params.networked = false
	params.variationNameHash = 0
	
	print("Creating entity")
	local objectEntities = EntityManager:CreateEntitiesFromBlueprint(objectBlueprint, params)
	print("Tried to create entity")
	table.insert(self.spawnedObjects, objectEntities)
	print("Spawning entities")

	for i, entity in ipairs(objectEntities) do
		print(i)
		entity:Init(Realm.Realm_Client, false)
	end
end


-- function xulumodClient:ReadInstance(p_Instance,p_PartitionGuid, p_Guid)
-- 	if p_Instance == nil then
-- 		return
-- 	end
	
-- 	if p_Instance.typeInfo.name == "EmitterEntityData" then
--         local s_Instance = EmitterEntityData(p_Instance)

--         if(s_Instance.startDelay == 0) then
-- 			s_Instance.startDelay = 0.04
--         end
-- 	end

-- 	if p_Instance.typeInfo.name == 'ObjectBlueprint' then
-- 		local blueprint = ObjectBlueprint(p_Instance)
-- 		table.insert(self.objectBlueprints, blueprint)
-- 		print('Found object blueprint ' .. blueprint.name)
-- 	end
-- end

function xulumodClient:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			goto continue
		end


		if l_Instance.typeInfo.name == 'ObjectBlueprint' then
			local blueprint = ObjectBlueprint(l_Instance)
			table.insert(self.objectBlueprints, blueprint)
			print('Found object blueprint ' .. blueprint.name)
		end

		::continue::
	end
end

function spairs(t, order)
    -- collect the keys
    local keys = {}
    for k in pairs(t) do keys[#keys+1] = k end

    -- if order function given, sort by it by passing the table and keys a, b,
    -- otherwise just sort the keys 
    if order then
        table.sort(keys, function(a,b) return order(t, a, b) end)
    else
        table.sort(keys)
    end

    -- return the iterator function
    local i = 0
    return function()
        i = i + 1
        if keys[i] then
            return keys[i], t[keys[i]]
        end
    end
end

g_xulumodClient = xulumodClient()

