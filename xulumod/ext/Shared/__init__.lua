class 'xulumodShared'


function xulumodShared:__init()
	print("Initializing xulumodShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function xulumodShared:RegisterVars()
	--self.m_this = that
end


function xulumodShared:RegisterEvents()
	-- self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	--self.m_LoadResourcesEvent = Events:Subscribe('Level:LoadResources', self, self.LoadResources)
end


function xulumodShared:ReadInstance(p_Instance,p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	
	if p_Instance.typeInfo.name == "SomeType" then
		local s_Instance = SomeType(p_Instance)
		s_Instance.someThing = true
	end
	
	
	if p_Guid == Guid("Some-Guid", "D") then
		local s_Instance = SomeClass(p_Instance)
		s_Instance.someThing = true
	end
end

function xulumodShared:LoadResources(p_blub)
	print("OnLoadResources Event")
end


g_xulumodShared = xulumodShared()

