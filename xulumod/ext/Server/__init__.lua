function string:split(sep)
	local sep, fields = sep or ":", {}
	local pattern = string.format("([^%s]+)", sep)
	self:gsub(pattern, function(c) fields[#fields+1] = c end)
	return fields
 end


class 'xulumodServer'

function xulumodServer:__init()
	print("Initializing xulumodServer")
	self:RegisterVars()
	self:RegisterEvents()
end


function xulumodServer:RegisterVars()
	self.vehicleBlueprints = {}
	self.objectBlueprints = {}
	self.objectBlueprintNetworked = {}

	self.spawnedObjects = {}
end


function xulumodServer:RegisterEvents()
	-- self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)

	self.m_LoadResourcesEvent = Events:Subscribe('Level:LoadResources', self, self.LoadResources)
	self.m_PlayerChat = Events:Subscribe('Player:Chat', self, self.PlayerChat)
end


function xulumodServer:ReadInstance(p_Instance,p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end


		
	if p_Instance.typeInfo.name == 'VehicleBlueprint' then
		local blueprint = VehicleBlueprint(p_Instance)
		table.insert(self.vehicleBlueprints, blueprint)
		-- print('Found vehicle blueprint ' .. blueprint.name)
	end

	if p_Instance.typeInfo.name == 'ObjectBlueprint' then
		local blueprint = ObjectBlueprint(p_Instance)
		table.insert(self.objectBlueprints, blueprint)

		self.objectBlueprintNetworked[blueprint.name] = blueprint.needNetworkId

		-- print(tostring( blueprint.needNetworkId  ) .. ' | Found object blueprint ' .. blueprint.name)
	end
end

function xulumodServer:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			goto continue
		end

		if l_Instance.typeInfo.name == 'VehicleBlueprint' then
			local blueprint = VehicleBlueprint(l_Instance)
			table.insert(self.vehicleBlueprints, blueprint)
			print('Found vehicle blueprint ' .. blueprint.name)
		end

		if l_Instance.typeInfo.name == 'ObjectBlueprint' then
			local blueprint = ObjectBlueprint(l_Instance)
			table.insert(self.objectBlueprints, blueprint)

			self.objectBlueprintNetworked[blueprint.name] = blueprint.needNetworkId

			print(tostring( blueprint.needNetworkId  ) .. ' | Found object blueprint ' .. blueprint.name)
		end

		if l_Instance.typeInfo.name == 'SpatialPrefabBlueprint' then
			local blueprint = SpatialPrefabBlueprint(l_Instance)
			table.insert(self.objectBlueprints, blueprint)

			self.objectBlueprintNetworked[blueprint.name] = blueprint.needNetworkId

			print(tostring( blueprint.needNetworkId  ) .. ' | Found object blueprint ' .. blueprint.name)
		end

		::continue::
	end
end

function xulumodServer:PlayerChat(p_Player, p_RecipientMask, p_Message)
	if message == '' then
		return
	end

	local parts = p_Message:split(' ')
	print('Chat: ' .. p_Message)
	if parts[1] == 'vehicle' then
		local num = 1
		if(parts[3] ~= nil) then 
			num = parts[3]
		end
		self:SpawnVehicle(p_Player.soldier, parts[2], num)
		return
	end

	if parts[1] == 'object' then
		self:SpawnObject(p_Player.soldier, parts[2])
		return
	end
	
	if parts[1] == 'objects' then
		local sortFunction = function(a , b) return a.name < b.name end
		table.sort(self.objectBlueprints, sortFunction)

		local counter = 1
		for k,v in spairs(self.objectBlueprints) do
			print(v.name)
			counter = counter + 1
		end
		
		print("object count: " .. counter)
	end

	if parts[1] == 'pos' then
		local trans = p_Player.soldier.transform.trans
		print('Position of player ' .. p_Player.name .. ' is X:' .. trans.x .. ' Y:'.. trans.y ..' Z:' .. trans.z)
	end


	if parts[1] == 'w' then
		self:SpawnWall(p_Player.soldier)
	end
end


function xulumodServer:SpawnVehicle(p_Soldier, p_vehicle, p_Num)
	if p_Soldier == nil then
		print('Soldier is nil')
		return
	end
	print('Vehicle: '.. p_vehicle)
	local vehicleMatch = string.lower(p_vehicle)
	local vehicleBlueprint = nil

	-- Find vehicle that matches
	for i, blueprint in spairs(self.vehicleBlueprints) do
		local lowerName = string.lower(blueprint.name)

		if string.find(lowerName, vehicleMatch) ~= nil then
			vehicleBlueprint = blueprint
			break
		end
	end

	if vehicleBlueprint == nil then
		print('Could not find matching vehicle')
		return
	end
	local trans = p_Soldier.transform.trans
	print('Spawning '.. p_Num .. ' ' .. vehicleBlueprint.name .. ' at X:' .. trans.x .. ' Y:'.. trans.y ..' Z:' .. trans.z)
	for i=1,p_Num do
		local transform = LinearTransform(
			Vec3(1, 0, 0),
			Vec3(0, 1, 0),
			Vec3(0, 0, 1),
			Vec3(trans.x+10, trans.y + (2*i), trans.z)
		)
		local params = EntityCreationParams()
		params.transform = transform
		params.networked = true
		print("lego")
		local vehicles = EntityManager:CreateEntitiesFromBlueprint(vehicleBlueprint, params)
		for i, entity in ipairs(vehicles) do
			print(i)
			entity:Init(Realm.Realm_ClientAndServer, true)
		end
		-- createdVehicle:Init(Realm.Realm_ClientAndServer, true)
	end
end

function xulumodServer:SpawnWall(p_Soldier)
	if p_Soldier == nil then
		print('Soldier is nil')
		return
	end

	local wall = ResourceManager:FindInstanceByGUID("e407b506-a4d3-11e1-8f7b-a4270eaee571", "be93df81-5212-2de3-4852-c72148004f15")

	if wall == nil then
		print("Cound't find wall")
	end

	local obj = _G[wall.typeInfo.name](wall)

	local trans = p_Soldier.transform.trans
	local transform = LinearTransform(
		Vec3(1.0, 0.0, 0.0),
		Vec3(0.0, 1.0, 0.0),
		Vec3(0.0, 0.0, 1.0),
		Vec3(trans.x, trans.y, trans.z)
	)

	if obj.needNetworkId == false then
		NetEvents:BroadcastLocal('spawnWall', transform.trans.x, transform.trans.y, transform.trans.z)
	end
	

	local objectEntities = EntityManager:CreateEntitiesFromBlueprint(obj, transform, obj.needNetworkId)
	-- table.insert(self.spawnedObjects, objectEntities)
	for i, entity in ipairs(objectEntities) do
		entity:Init(Realm.Realm_ClientAndServer, true)
	end


end


function xulumodServer:SpawnObject(p_Soldier, p_ObjectName)
	if p_Soldier == nil then
		print('Soldier is nil')
		return
	end

	local objectMatch = string.lower(p_ObjectName)
	local objectBlueprint = nil

	-- Find object that matches
	for i, blueprint in spairs(self.objectBlueprints) do
		local lowerName = string.lower(blueprint.name)

		if string.find(lowerName, objectMatch) ~= nil then
			objectBlueprint = blueprint
			break
		end
	end

	if objectBlueprint == nil then
		print('Could not find matching object')
		return
	end

	print('Sending spawn object event for '.. objectBlueprint.name)
	local trans = p_Soldier.transform.trans
	local transform = LinearTransform(
		Vec3(1.0, 0.0, 0.0),
		Vec3(0.0, 1.0, 0.0),
		Vec3(0.0, 0.0, 1.0),
		Vec3(trans.x, trans.y, trans.z)
	)
	--if(self.objectBlueprintNetworked[objectBlueprint.name] == false) then
		NetEvents:BroadcastLocal('spawnObject', objectBlueprint.name, transform.trans.x, transform.trans.y, transform.trans.z)
	--end
	print("we should get here at least")
	local params = EntityCreationParams()
	params.transform = transform
	params.networked = false
	params.variationNameHash = 0


	print("Creating entity")
	local objectEntities = EntityManager:CreateEntitiesFromBlueprint(objectBlueprint, params)
	print("Tried to create entity")
	table.insert(self.spawnedObjects, objectEntities)
	print("Spawning entities")
	for i, entity in ipairs(objectEntities) do
		print(i)
		entity:Init(Realm.Realm_ClientAndServer, false)
	end


end

function xulumodServer:LoadResources(p_blub)
	print("OnLoadResources Event")
	self.vehicleBlueprints = {}
	self.objectBlueprints = {}
end

function spairs(t, order)
    -- collect the keys
    local keys = {}
    for k in pairs(t) do keys[#keys+1] = k end

    -- if order function given, sort by it by passing the table and keys a, b,
    -- otherwise just sort the keys 
    if order then
        table.sort(keys, function(a,b) return order(t, a, b) end)
    else
        table.sort(keys)
    end

    -- return the iterator function
    local i = 0
    return function()
        i = i + 1
        if keys[i] then
            return keys[i], t[keys[i]]
        end
    end
end

g_xulumodServer = xulumodServer()

