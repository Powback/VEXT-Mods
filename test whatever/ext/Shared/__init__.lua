class "TestShared"

local m_OnGroundStateData_Guid = Guid("18B95C2B-5D12-4D1F-BA8C-0266A5240C04", "D")
local m_InAirStateData_Guid = Guid("584D7B54-FBFE-4755-8AD4-89065EEB45C3", "D")


function TestShared:__init()
	print("TestShared initializing")
	
	self:RegisterVars()
	self:RegisterEvents()
end

function TestShared:RegisterVars()

end

function TestShared:RegisterEvents()
	Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)

end

function TestShared:OnPartitionLoaded(p_Partition)

	local instances = p_Partition.instances

	for _, instance in pairs(instances) do
		if instance == nil then
			goto continue
		end

		if instance.instanceGuid == m_OnGroundStateData_Guid then
			print("found m_OnGroundStateData_Guid")
		end

		if instance.instanceGuid == m_InAirStateData_Guid then
			print("found m_InAirStateData_Guid")
		end

		if instance.typeInfo == CharacterPhysicsData.typeInfo then
			print("Found cpd")
			local s_Instance = CharacterPhysicsData(instance)
			print(s_Instance.states:get(1).instanceGuid)
			print(s_Instance.states:get(1).isLazyLoaded)
		end
		if instance.typeInfo == OnGroundStateData.typeInfo then
			print("Found groundstate")
			print(instance.instanceGuid)
			print(instance.instanceGuid == m_OnGroundStateData_Guid)

		end

		::continue::
	end
end

return TestShared()