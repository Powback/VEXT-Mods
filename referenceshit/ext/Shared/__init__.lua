class 'referenceshitShared'


function referenceshitShared:__init()
	print("Initializing referenceshitShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function referenceshitShared:RegisterVars()
	self.m_references = {}
	self.m_RefCount = 0
	self.m_EntityCount = 0
	self.m_Matches = 0

end


function referenceshitShared:RegisterEvents()
	Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
	Hooks:Install('ServerEntityFactory:Create',999, self, self.OnEntityCreate)
	Hooks:Install('ClientEntityFactory:Create', 9, self, self.OnEntityCreate)
	Events:Subscribe('ExtensionLoaded', self, self.OnLoaded)
end


function referenceshitShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeInfo.name == "ReferenceObjectData") then
			local s_Instance = ReferenceObjectData(l_Instance)
			self:AddReference(s_Instance)
		end
	end
end
function referenceshitShared:BuildTable( trans )
	if(self.m_references[trans.x] == nil) then
		self.m_references[trans.x] = {}
	end
	if(self.m_references[trans.x][trans.y] == nil) then
		self.m_references[trans.x][trans.y] = {}
	end
	if(self.m_references[trans.x][trans.y][trans.z] == nil) then
		self.m_references[trans.x][trans.y][trans.z] = {
		references = {},
		entities = {}
	}
	end
end
function referenceshitShared:AddReference( instance )
	self.m_RefCount = self.m_RefCount + 1
	local trans = instance.blueprintTransform.trans
	self:BuildTable(trans)
	table.insert(self.m_references[trans.x][trans.y][trans.z]["references"], tostring(instance.instanceGuid))
end

function referenceshitShared:PathExists( trans )
	if(self.m_references[trans.x] == nil) then
		return false
	end
	if(self.m_references[trans.x][trans.y] == nil) then
		return false
	end
	if(self.m_references[trans.x][trans.y][trans.z] == nil) then
		return false
	end
	return true
end

function referenceshitShared:AddEntity( entity, transform )

	local trans = transform.trans
	self.m_EntityCount = self.m_EntityCount + 1

	if(self:PathExists(trans)) then
		self.m_Matches = self.m_Matches + 1
		print(self.m_Matches .. " - " .. self.m_EntityCount .. " / " .. self.m_RefCount)
	else
		print(self.m_Matches .. " - " .. self.m_EntityCount .. " / " .. self.m_RefCount)
	end

	--self:BuildTable(trans)
	--table.insert(self.m_references[trans.x][trans.y][trans.z]["entities"], tostring(entity))

end

function referenceshitShared:OnEntityCreate(p_Hook, p_Data, p_Transform)
	if p_Data == nil then
		print("Didnt get no data")
	end
	local entity = p_Hook:Call()
	self:AddEntity(p_Data.instanceGuid, p_Transform) 
	if(p_Data.typeInfo.name == "SoldierEntityData") then
		local response = json.encode(self.m_references)
		WebUI:ExecuteJS(string.format("SetText('%s');", response))
	end
end

function referenceshitShared:OnLoaded()
	-- Initialize our custom WebUI package.
	WebUI:Init()

	-- Show our custom WebUI package.
	WebUI:Show()

end

g_referenceshitShared = referenceshitShared()

