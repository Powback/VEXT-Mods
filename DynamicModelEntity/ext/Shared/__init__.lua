class 'DynamicModelEntityShared'


function DynamicModelEntityShared:__init()
	print("Initializing DynamicModelEntityShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function DynamicModelEntityShared:RegisterVars()
	--self.m_this = that
end


function DynamicModelEntityShared:RegisterEvents()
	Hooks:Install('Partition:ReadInstance', 100, self, self.ReadInstance)
end


function DynamicModelEntityShared:ReadInstance(p_Hook, p_Instance, p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	if p_Instance.typeName == "ChassisComponentData" then
		
		local s_Instance = ComponentData (p_Instance)
		
		local s_Clone = s_Instance:Clone(p_Guid)
		
		s_Clone = ComponentData(s_Clone)

		s_Clone.transform = LinearTransform(
	 	Vec3(5.0, 0.0, 0.0),
	 	Vec3(0.0, 5.0, 0.0),
	 	Vec3(0.0, 0.0, 5.0),
	 	s_Clone.transform.trans
		)
		
		p_Hook:Pass(s_Clone, p_PartitionGuid, p_Guid)
	end
end


g_DynamicModelEntityShared = DynamicModelEntityShared()

