class 'bundles'


function bundles:__init()
	print("Initializing bundles")
	self:RegisterVars()
	self:RegisterEvents()
end


function bundles:RegisterVars()
	print("registering vars")
	self.g_nuke = Guid('64AA8847-BB88-171F-1DA8-7CC3825E0D63', 'D')
	self.g_c4effect = Guid('F88F469B-E075-4770-AC03-42D9320CF000', 'D')
	self.bundlesLoaded = false

	self.m_heli = nil
end

function bundles:RegisterEvents()
	Hooks:Install('ResourceManager:LoadBundles',999, self, self.OnLoadBundles)
	--Hooks:Install('ClientEntityFactory:Create',999, self, self.OnEntityCreate)
	--Hooks:Install("ServerEntityFactory:Create", 999, self, self.OnEntityCreate)
end


function bundles:OnLoadBundles(p_Hook, p_Bundles, p_Compartment)
	print(p_Bundles)
	if(p_Bundles[1] == "gameconfigurations/game" or p_Bundles[1] == "UI/Flow/Bundle/LoadingBundleMp") then

		Events:Dispatch('BundleMounter:LoadBundle', 'levels/sp_paris/sp_paris', {
			'levels/sp_paris/sp_paris',
			'levels/sp_paris/heat_pc_only',
			'levels/sp_paris/chase'
		})
		Events:Dispatch('BundleMounter:LoadBundle', 'spchunks', {
		})
		print("Sent!")
		
	end
end



g_bundles = bundles()

