class 'MP_001'

function MP_001:__init()
	self.m_StateAddedEvent = Events:Subscribe('VE:StateAdded', self, self.OnStateAdded)
end

function MP_001:OnStateAdded(p_State)
	-- Fix a newly added state (if needed).
	if p_State.entityName ~= 'EffectEntity' then
		self:FixEnvironmentState(p_State)
	end
end

function MP_001:OnLoaded()
	-- Make sure all our VE states are fixed.
	local s_States = VisualEnvironmentManager:GetStates()

	for i, s_State in ipairs(s_States) do
		if s_State.entityName ~= 'EffectEntity' then
			self:FixEnvironmentState(s_State)			
		end
	end
end

function MP_001:FixEnvironmentState(p_State)
	print('Fixing visual environment state ' .. p_State.entityName)

	if p_State.entityName == 'Levels/MP_001/Lighting/VE_MP_001_Bazaar' then
		local s_ColorCorrection = p_State.colorCorrection

		if s_ColorCorrection ~= nil then
			s_ColorCorrection.brightness = Vec3(1.15, 1.12, 1.08)
			--s_ColorCorrection.brightness = Vec3(1.0, 1.0, 1.0)
			s_ColorCorrection.contrast = Vec3(1.0, 1.0, 1.0)
			s_ColorCorrection.saturation = Vec3(0.8, 0.8, 0.84)
			s_ColorCorrection.colorGradingEnable = false
			s_ColorCorrection.enable = false
		end

		local s_Sky = p_State.sky

		if s_Sky ~= nil then
			s_Sky.sunSize = 0.013
			s_Sky.brightnessScale = 1.15
			s_Sky.skyVisibilityExponent = 0.75
			s_Sky.cloudLayer1SunLightIntensity = 0
			s_Sky.cloudLayer2SunLightIntensity = 0
			s_sky.enable = false
		end

		local s_Fog = p_State.fog

		if s_Fog ~= nil then
			s_Fog.enable = false
		end

		local s_OutdoorLight = p_State.outdoorLight

		if s_OutdoorLight ~= nil then
			s_OutdoorLight.translucencyPower = 0
			s_OutdoorLight.sunColor = Vec3(0.2, 0.2, 0.1)
		end

		local s_TonemapData = p_State.tonemap

		if s_TonemapData ~= nil then
			s_TonemapData.bloomScale = Vec3(0, 0, 0)
			s_TonemapData.minExposure = 8
			s_TonemapData.middleGray = 0.3
			s_TonemapData.maxExposure = 8
		end

		local s_Vignette = p_State.vignette

		if s_Vignette ~= nil then
			s_Vignette.enable = false
		end

		local s_CameraParams = p_State.cameraParams

		if s_CameraParams ~= nil then
			s_CameraParams.viewDistance = 500
			s_CameraParams.sunShadowmapViewDistance = 100
		end

		VisualEnvironmentManager.dirty = true
		return
	end
end


function MP_001:OnReadInstance(p_Instance, p_Guid)

end


return MP_001