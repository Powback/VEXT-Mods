class 'SharedPatches'

local MP_001 = require '__shared/maps/mp_001'
local MP_011 = require '__shared/maps/mp_011'

function SharedPatches:__init()
	self:RegisterEvents()
	self:InitComponents()
end

function SharedPatches:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	self.m_StateAddedEvent = Events:Subscribe('VE:StateAdded', self, self.OnStateAdded)
end

function SharedPatches:InitComponents()
	self.m_MP_001 = MP_001()
	self.m_MP_011 = MP_011()
end

function SharedPatches:OnLoaded()
	self.m_MP_001:OnLoaded()
	self.m_MP_011:OnLoaded()
end
local set = {}

function SharedPatches:ReadInstance(p_Instance, p_Guid)
	self.m_MP_011:OnReadInstance(p_Instance, p_Guid)


		
end


return SharedPatches