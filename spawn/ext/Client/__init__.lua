local lightEntity = nil
local dinoEntity = nil
local dinos = nil

local data = SpotLightEntityData()
data.color = Vec3(0.0, 0.0, 1.0)
data.radius = 30.0
data.intensity = 50.0
data.visible = true
data.attenuationOffset = 1.0
data.specularEnable = true
data.enlightenColorMode = EnlightenColorMode.EnlightenColorMode_Multiply
data.enlightenEnable = true
data.enlightenColorScale = Vec3(2.0, 2.0, 2.0)
data.particleColorScale = Vec3(1.0, 1.0, 1.0)
data.castShadowsEnable = true
data.castShadowsMinLevel = QualityLevel.QualityLevel_Low

function SpawnLight(x, y, z)
	print('Spawning light')

	local transform = LinearTransform(
		Vec3(1.0, 0.0, 0.0),
		Vec3(0.0, 1.0, 0.0),
		Vec3(0.0, 0.0, 1.0),
		Vec3(x, y, z)
	)

	lightEntity = EntityManager:CreateClientEntity(data, transform)
	lightEntity:Init(Realm.Realm_Client, true)

	print(transform)
end

function SpawnDino(x, y, z)
	print('Got dino spawn event')

	local transform = LinearTransform(
		Vec3(1.0, 0.0, 0.0),
		Vec3(0.0, 1.0, 0.0),
		Vec3(0.0, 0.0, 1.0),
		Vec3(x, y, z)
	)

	dinoEntity = EntityManager:CreateClientEntitiesFromBlueprint(dinos, transform)

	for i, entity in ipairs(dinoEntity) do
		entity:Init(Realm.Realm_Client, true)
	end
end

function OnInstance(instance, partitionGuid, instanceGuid)
	if dinos == nil and instance.typeName == 'ObjectBlueprint' then
		local blueprint = ObjectBlueprint(instance)

		--if blueprint.name == 'Levels/XP1_004/Props/FindDinos_01/FindDinos_01' then
		if blueprint.name == 'Levels/XP1_004/Props/MemorialPlate_01/MemorialPlate_01' then
			dinos = blueprint
			print('Found dino blueprint ' .. blueprint.name)
		end
	end
end

function OnLoadLevel(dedicated)
	print('Loading level')
	dinos = nil
end

function dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end

function OnUpdateInput(p_Hook, p_Cache, p_DeltaTime)
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_Q) then
       local p_Player = PlayerManager:GetLocalPlayer()

		if p_Player == nil then
			return
		end
		if p_Player.soldier == nil then
			return
		end
		local s_Soldier = p_Player.soldier
		
		
		local s_Iterator = EntityManager:GetClientIterator('ClientVehicleEntity')


		if s_Iterator == nil then
			print('Failed to get camera iterator')
		else
			local s_Entity = s_Iterator:Next()
			while s_Entity ~= nil do
				print(SoldierEntity)
				s_Entity = s_Iterator:Next()
			end
		end

    end
end

NetEvents:Subscribe('spawnLight', SpawnLight)
NetEvents:Subscribe('spawnDino', SpawnDino)
Events:Subscribe('Partition:ReadInstance', OnInstance)
Events:Subscribe('Level:LoadResources', OnLoadLevel)
Events:Subscribe('Client:UpdateInput', OnUpdateInput)