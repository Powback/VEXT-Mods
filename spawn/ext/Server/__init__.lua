function string:split(sep)
   local sep, fields = sep or ":", {}
   local pattern = string.format("([^%s]+)", sep)
   self:gsub(pattern, function(c) fields[#fields+1] = c end)
   return fields
end

--

local vehicleBlueprints = {}
local soldierBlueprint = nil
local createdVehicle = nil
local createdSoldier = nil
local dinoEntity = nil
local dinos = nil

function OnChat(player, recipientMask, message)
	if message == '' then
		return
	end

	print('Chat: ' .. message)

	local parts = message:split(' ')

	if parts[1] == 'light' then
		SpawnLight(player)
	end

	if parts[1] == 'dino' then
		SpawnDino(player)
	end

	if parts[1] == 'vehicle' then
		SpawnVehicle(player, parts[2])
	end

	if parts[1] == 'soldier' then
		SpawnSoldier(player)
	end
end

function OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances

	for _, instance in ipairs(s_Instances) do
		if instance == nil then
			print('Instance is null?')
			break
		end

		if instance.typeInfo.name == 'VehicleBlueprint' then
			local blueprint = VehicleBlueprint(instance)
			table.insert(vehicleBlueprints, blueprint)
			print('Found vehicle blueprint ' .. blueprint.name)
		end

		if instance.typeInfo.name == 'SoldierBlueprint' then
			soldierBlueprint = SoldierBlueprint(instance)
			print('Found soldier blueprint ' .. soldierBlueprint.name)
		end

		if dinos == nil and instance.typeInfo.name == 'ObjectBlueprint' then
			local blueprint = ObjectBlueprint(instance)

			--if blueprint.name == 'Levels/XP1_004/Props/FindDinos_01/FindDinos_01' then
			if blueprint.name == 'Levels/XP1_004/Props/MemorialPlate_01/MemorialPlate_01' then
				dinos = blueprint
				print('Found dino blueprint ' .. blueprint.name)
			end
		end
	end
end

function OnLoadLevel(dedicated)
	print('Loading level')
	vehicleBlueprints = {}
	soldierBlueprint = nil
end

function SpawnLight(player)
	if player == nil or player.soldier == nil then
		print('Player must be alive to spawn light')
		return
	end

	print('Sending spawn light event')
	local trans = player.soldier.transform.trans
	NetEvents:BroadcastLocal('spawnLight', trans.x, trans.y, trans.z)
end

function SpawnDino(player)
	if player == nil or player.soldier == nil then
		print('Player must be alive to spawn dino')
		return
	end

	print('Sending spawn dino event')
	local trans = player.soldier.transform.trans
	NetEvents:BroadcastLocal('spawnDino', trans.x, trans.y, trans.z)

	local transform = LinearTransform(
		Vec3(1.0, 0.0, 0.0),
		Vec3(0.0, 1.0, 0.0),
		Vec3(0.0, 0.0, 1.0),
		trans
	)

	dinoEntity = EntityManager:CreateServerEntitiesFromBlueprint(dinos, transform)

	for i, entity in ipairs(dinoEntity) do
		entity:Init(Realm.Realm_ClientAndServer, true)
	end
end

function SpawnVehicle(player, vehicle)
	if player == nil or player.soldier == nil then
		print('Player must be alive to spawn vehicle')
		return
	end

	if createdVehicles ~= nil then
		for _, vehicle in ipairs(createdVehicles) do
			vehicle:Destroy()
		end

		createdVehicles = nil
	end

	local vehicleMatch = string.lower(vehicle)
	local vehicleBlueprint = nil

	-- Find vehicle that matches
	for i, blueprint in ipairs(vehicleBlueprints) do
		local lowerName = string.lower(blueprint.name)

		if string.find(lowerName, vehicleMatch) ~= nil then
			vehicleBlueprint = blueprint
			break
		end
	end

	if vehicleBlueprint == nil then
		print('Could not find matching vehicle')
		return
	end

	--local transform = player.soldier.transform
	local transform = LinearTransform(
		Vec3(1, 0, 0),
		Vec3(0, 1, 0),
		Vec3(0, 0, 1),
		Vec3(player.soldier.transform.trans.x, player.soldier.transform.trans.y + 20, player.soldier.transform.trans.z)
	)

	print('Spawning vehicle ' .. vehicleBlueprint.name)
	createdVehicle = EntityManager:CreateServerEntitiesFromBlueprint(vehicleBlueprint, transform, true)

	for i, entity in ipairs(createdVehicle) do
		entity:Init(Realm.Realm_ClientAndServer, true)
	end
end

function SpawnSoldier(player)
	if player == nil or player.soldier == nil then
		print('Player must be alive to spawn soldier')
		return
	end

	if soldierBlueprint == nil then
		print('No soldier blueprint loaded')
		return
	end

	print('Spawning soldier ' .. soldierBlueprint.name)

	local transform = LinearTransform(
		Vec3(1, 0, 0),
		Vec3(0, 1, 0),
		Vec3(0, 0, 1),
		player.soldier.transform.trans
	)
	local params = EntityCreationParams()
	params.transform = transform
	params.networked = true

	createdSoldier = EntityManager:CreateServerEntitiesFromBlueprint(soldierBlueprint, params)

	for i, entity in ipairs(createdSoldier) do
		entity:Init(Realm.Realm_ClientAndServer, true)
		entity:FireEvent("Enable")
	end
end

Events:Subscribe('Player:Chat', OnChat)
Events:Subscribe('Partition:ReadInstance', OnInstance)
Events:Subscribe('Partition:Loaded', OnPartitionLoaded)
Events:Subscribe('Level:LoadResources', OnLoadLevel)