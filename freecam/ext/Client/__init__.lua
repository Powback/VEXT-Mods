class 'freecamClient'


function freecamClient:__init()
	print("Initializing freecamClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function freecamClient:RegisterVars()
	self.camera = nil
	self.data = nil
	self.yaw = 0.0
	self.pitch = 0.0
	self.roll = 0.0
end


function freecamClient:RegisterEvents()
	self.m_OnUpdateInputEvent = Events:Subscribe('Client:UpdateInput', self, self.OnUpdateInput)
	self.m_InputPreUpdateHook = Hooks:Install("Input:PreUpdate", 999, self, self.OnUpdateInputHook)
end

function freecamClient:OnUpdateInputHook(p_Hook, p_Cache, p_DeltaTime)
	if (self.camera ~= nil) then

		self.yaw   = self.yaw   - p_Cache[InputConceptIdentifiers.ConceptYaw]
		self.pitch = self.pitch - p_Cache[InputConceptIdentifiers.ConceptPitch]
		self.roll  = self.roll  - p_Cache[InputConceptIdentifiers.ConceptRoll]
	end
end

function freecamClient:OnUpdateInput(p_Delta, p_SimulationDelta)

	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F) then
		if (self.camera ~= nil) then
			self.camera:FireEvent("TakeControl")
			print("Took control over the camera again.")
			return
		end

		self.data = CameraEntityData()
		self.data.fov = 90

		local s_Entity = EntityManager:CreateEntity(self.data, LinearTransform())

		if s_Entity == nil then
			print("Could not spawn camera")
			return
		end
		s_Entity:Init(Realm.Realm_Client, true)

		print(s_Entity.typeName) -- ClientCameraEntity

		s_Entity:FireEvent("TakeControl") -- Takes control over the camera
		
		local s_Spatial = s_Entity
		self.data.transform = ClientUtils:GetCameraTransform()
		self.data.fov = 90
		self.camera = s_Spatial
	end

	

	if InputManager:WentKeyDown(InputDeviceKeys.IDK_Numpad4) then
		self.yaw = self.yaw + ( math.pi/180 )
	end

	if InputManager:WentKeyDown(InputDeviceKeys.IDK_Numpad6) then
		self.yaw = self.yaw - ( math.pi/180 )
	end

	if InputManager:WentKeyDown(InputDeviceKeys.IDK_Numpad8) then
		self.pitch = self.pitch - ( math.pi/180 )
	end

	if InputManager:WentKeyDown(InputDeviceKeys.IDK_Numpad5) then
		self.pitch = self.pitch + ( math.pi/180 )
	end


	if InputManager:WentKeyDown(InputDeviceKeys.IDK_O) then
		if (self.camera ~= nil) then
			self.data.fov = self.data.fov + 1
			print(tostring(self.data.fov))
		end
	end

	if (self.camera ~= nil) then
		local forward = Vec3( math.sin(self.yaw)*math.cos(self.pitch), 
                          	      math.sin(self.pitch),
                            	      math.cos(self.yaw)*math.cos(self.pitch))



		local up = Vec3( -(math.sin(self.yaw)*math.sin(self.pitch)*math.cos(self.roll) + math.cos(self.yaw)*math.sin(self.roll)), 
				 math.cos(self.pitch)*math.cos(self.roll), 
				 -(math.cos(self.yaw)*math.sin(self.pitch)*math.cos(self.roll) - math.sin(self.yaw)*math.sin(self.roll)) )

		local left = forward:Cross(Vec3(up.x * -1, up.y * -1, up.z * -1))

		self.data.transform.left = left
		self.data.transform.up = up
		self.data.transform.forward = forward
	end


	if InputManager:WentKeyDown(InputDeviceKeys.IDK_V) then
		if (self.camera ~= nil) then
			print(tostring(self.data.transform))
			print(tostring(self.data.fov))
			self.camera:FireEvent("ReleaseControl")
		end
	end


	if InputManager:WentKeyDown(InputDeviceKeys.IDK_I) then
		if (self.camera ~= nil) then
			self.data.fov = self.data.fov - 1
			print(tostring(self.data.fov))
		end
	end

	if InputManager:WentKeyDown(InputDeviceKeys.IDK_O) then
		if (self.camera ~= nil) then
			self.data.fov = self.data.fov + 1
			print(tostring(self.data.fov))
		end
	end

	if InputManager:WentKeyDown(InputDeviceKeys.IDK_ArrowUp) then
		if (self.camera ~= nil) then
			self.data.transform.trans.x = self.data.transform.trans.x - self.data.transform.forward.x
			self.data.transform.trans.y = self.data.transform.trans.y - self.data.transform.forward.y
			self.data.transform.trans.z = self.data.transform.trans.z - self.data.transform.forward.z
			print(tostring(self.data.transform))
			print(tostring(ClientUtils:GetCameraTransform()))
		end
	end

	if InputManager:WentKeyDown(InputDeviceKeys.IDK_ArrowDown) then
		if (self.camera ~= nil) then
			self.data.transform.trans.x = self.data.transform.trans.x + self.data.transform.forward.x
			self.data.transform.trans.y = self.data.transform.trans.y + self.data.transform.forward.y
			self.data.transform.trans.z = self.data.transform.trans.z + self.data.transform.forward.z
			print(tostring(self.data.transform))
		end
	end

	if InputManager:WentKeyDown(InputDeviceKeys.IDK_ArrowLeft) then
		if (self.camera ~= nil) then
			self.data.transform.trans.x = self.data.transform.trans.x - self.data.transform.left.x
			self.data.transform.trans.y = self.data.transform.trans.y - self.data.transform.left.y
			self.data.transform.trans.z = self.data.transform.trans.z - self.data.transform.left.z
			print(tostring(self.data.transform))
		end
	end

	if InputManager:WentKeyDown(InputDeviceKeys.IDK_ArrowRight) then
		if (self.camera ~= nil) then
			self.data.transform.trans.x = self.data.transform.trans.x + self.data.transform.left.x
			self.data.transform.trans.y = self.data.transform.trans.y + self.data.transform.left.y
			self.data.transform.trans.z = self.data.transform.trans.z + self.data.transform.left.z
			print(tostring(self.data.transform))
		end
	end



	
end

g_freecamClient = freecamClient()

