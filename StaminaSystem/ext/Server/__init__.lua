class 'StaminaSystemServer'
local lume = require '__shared/Util/lume'


function StaminaSystemServer:__init()
	print("Initializing StaminaSystemServer")
	self:RegisterVars()
	self:RegisterEvents()
end


function StaminaSystemServer:RegisterVars()
	--self.m_this = that
	self.m_Soldiers = {}
	self.m_PlayerSpawning = false
	self.m_TempSoldier = nil
end


function StaminaSystemServer:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	Hooks:Install('ServerEntityFactory:Create', 9, self, self.OnEntityCreate)
	Events:Subscribe('Player:Respawn', self, self.OnPlayer)
	Events:Subscribe('Player:SpawnOnPlayer', self, self.OnPlayer)
	Events:Subscribe('Player:SpawnAtVehicle', self, self.OnPlayer)
	Events:Subscribe('Player:Deleted', self, self.OnPlayerDeleted)
	Events:Subscribe('Player:Destroyed', self, self.OnPlayerDeleted)

	NetEvents:Subscribe('StaminaSystem:SlowDown', self, self.OnSlowDown)
	NetEvents:Subscribe('StaminaSystem:SpeedUp', self, self.OnSpeedUp)
end
function StaminaSystemServer:OnSlowDown(p_Player)
	print("SlowDown called")
	self:SetSpeed(p_Player, -0.1)
end
function StaminaSystemServer:OnSpeedUp(p_Player)
	print("SpeedUp called")
	self:SetSpeed(p_Player, 0.1)
end

function StaminaSystemServer:SetSpeed(p_Player, p_Speed)
	if(self:HasSoldier(p_Player)) then
		if(self.m_Soldiers[p_Player.name] ~= nil) then
			local s_SoldierEntity = self.m_Soldiers[p_Player.name]

			local s_CharacterPhysics = CharacterPhysicsData(s_SoldierEntity.characterPhysics)
			local s_GroundState = OnGroundStateData(s_CharacterPhysics:GetStatesAt(0))
			local s_PoseInfo = CharacterStatePoseInfo(s_GroundState:GetPoseInfoAt(0))

			print(tostring(s_PoseInfo.sprintMultiplier))
			s_PoseInfo.sprintMultiplier = lume.clamp(s_PoseInfo.sprintMultiplier + p_Speed, 1, 1.625)
			print(tostring(s_PoseInfo.sprintMultiplier))
			ChatManager:SendMessage(p_Player.name .."'s new sprintMultiplier is " .. s_PoseInfo.sprintMultiplier)
		end
	else 
		print("Tried to enable noclip for a player that\'s not alive: ' " .. p_Player)
	end
end

function StaminaSystemServer:ReadInstance(p_Instance,p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
end

function StaminaSystemServer:HasSoldier(p_Player)
	if p_Player == nil then
		return false
	elseif p_Player.soldier == nil then
		return false
	else
		return true
	end
end

function StaminaSystemServer:OnEntityCreate(p_Hook, p_Data, p_Transform)
	if (p_Data.typeName == "SoldierEntityData" ) then
		self.m_TempSoldier = SoldierEntityData(p_Data)
		-- NetEvents:SendTo('StaminaSystem:Soldier', self.m_TempSoldier)
		-- print(tostring(p_Transform))
		-- p_Hook:Pass(p_Data,p_Transform)

	end
	p_Hook:Next()
end

function StaminaSystemServer:OnPlayerDeleted(p_Player)
	self.m_Soldiers[p_Player.name] = nil
end

function StaminaSystemServer:OnPlayer(p_Player)
	self.m_Soldiers[p_Player.name] = self.m_TempSoldier


end

g_StaminaSystemServer = StaminaSystemServer()

