class 'StaminaSystemClient'

local lume = require '__shared/Util/lume'


function StaminaSystemClient:__init()
	print("Initializing StaminaSystemClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function StaminaSystemClient:RegisterVars()
  self.m_LocalSoldier = nil
end


function StaminaSystemClient:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	self.m_ClientUpdateInputEvent = Events:Subscribe('Client:UpdateInput', self, self.OnUpdateInput)
  -- self.m_InputPreUpdateHook = Hooks:Install("Input:PreUpdate", 999, self, self.OnUpdateInputHook)

  -- Events:Subscribe('Player:Respawn', self, self.OnPlayer)
  -- Events:Subscribe('Player:SpawnOnPlayer', self, self.OnPlayer)
  -- Events:Subscribe('Player:SpawnAtVehicle', self, self.OnPlayer)
  -- Events:Subscribe('Player:Deleted', self, self.OnPlayerDeleted)
  -- Events:Subscribe('Player:Destroyed', self, self.OnPlayerDeleted)

end


function StaminaSystemClient:OnEntityCreate(p_Hook, p_Data, p_Transform)
  -- print(tostring(p_Data.typeName))
  if (p_Data.typeName == "SoldierEntityData" ) then
    print("found SoldierEntityData")
    self.m_LocalSoldier = SoldierEntityData(p_Data)
    print(tostring(p_Transform))
    -- p_Hook:Pass(p_Data, p_Transform)
  end
  -- p_Hook:Next()
end


function StaminaSystemClient:ReadInstance(p_Instance,p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	
end

function StaminaSystemClient:OnUpdateInputHook(p_Hook, p_Cache, p_DeltaTime)
  local s_Jump = p_Cache[math.floor(InputConceptIdentifiers.ConceptJump ) + 1]

  if s_Jump > 0.0 then
    print("jumped")
    -- NetEvents:SendLocal('StaminaSystem:SlowDown')

    -- p_Cache[math.floor(InputConceptIdentifiers.ConceptJump ) + 1] = math.floor(0)
    p_Hook:Pass(p_Cache, p_DeltaTime)
  end
end

function StaminaSystemClient:OnUpdateInput(p_Delta)
  if InputManager:WentKeyDown(InputDeviceKeys.IDK_F1) then
    NetEvents:SendLocal('StaminaSystem:SlowDown')
    -- self:SetSpeed(-0.1)
  end

  if InputManager:WentKeyDown(InputDeviceKeys.IDK_F2) then
    NetEvents:SendLocal('StaminaSystem:SpeedUp')
    -- self:SetSpeed(0.1)
  end

  if InputManager:WentKeyDown(InputDeviceKeys.IDK_F3) then
      Hooks:Install('ClientEntityFactory:Create', 999, self, self.OnEntityCreate)

    print("Hooked ClientEntityFactory:Create")
  end

end

function StaminaSystemClient:SetSpeed(p_Speed)
  if(self.m_LocalSoldier ~= nil) then

    local s_CharacterPhysics = CharacterPhysicsData(self.m_LocalSoldier.characterPhysics)
    local s_GroundState = OnGroundStateData(s_CharacterPhysics:GetStatesAt(0))
    local s_PoseInfo = CharacterStatePoseInfo(s_GroundState:GetPoseInfoAt(0))

    print(tostring(s_PoseInfo.sprintMultiplier))
    s_PoseInfo.sprintMultiplier = lume.clamp(s_PoseInfo.sprintMultiplier + p_Speed, 1, 1.625)
    print(tostring(s_PoseInfo.sprintMultiplier))
    -- ChatManager:SendMessage("your new sprintMultiplier is " .. s_PoseInfo.sprintMultiplier)
  end
 
end 

g_StaminaSystemClient = StaminaSystemClient()

