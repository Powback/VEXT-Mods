class 'threetestClient'

local rotationHelper = require "__shared/RotationHelper"

function threetestClient:__init()
	print("Initializing threetestClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function threetestClient:RegisterVars()
	--self.m_this = that
end


function threetestClient:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
	self.m_ExtensionLoadedEvent = Events:Subscribe('ExtensionLoaded', self, self.OnLoaded)
	Events:Subscribe("Engine:Update", self, self.OnUpdate)

end

function threetestClient:OnUpdate(p_Delta, p_SimulationDelta)
	local s_Player = PlayerManager:GetLocalPlayer()
	if s_Player == nil or s_Player.soldier == nil then
		m_Soldier = nil
	else
		m_Soldier = s_Player.soldier
	end
	if(m_Soldier == nil) then
		return
	end

	local s_Transform = ClientUtils:GetCameraTransform()
	local pos = s_Transform.trans
	print(pos)

	local yaw, pitch, roll = rotationHelper:GetYPRfromLT(s_Transform)

	WebUI:ExecuteJS(string.format('UpdateCamera(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);', pos.x, pos.y, pos.z, s_Transform.left.x,s_Transform.left.y,s_Transform.left.z,s_Transform.up.x,s_Transform.up.y,s_Transform.up.z,s_Transform.forward.x,s_Transform.forward.y,s_Transform.forward.z))
		-- WebUI:ExecuteJS(string.format('UpdateCamera(%s, %s, %s, %s, %s, %s);', pos.x, pos.y, pos.z, yaw, pitch, roll ))

end

function threetestClient:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeName == "SomeType") then
			local s_Instance = SomeType(l_Instance)
			s_Instance.someThing = 1
		end
		if(l_Instance.instanceGuid == Guid("SomeGuid")) then
			local s_Instance = SomeType(l_Instance)
			s_Instance.someThing = 1
		end
	end
end

function threetestClient:OnLoaded()
	print("init")
	-- Initialize our custom WebUI package.
	WebUI:Init()

	-- Show our custom WebUI package.
	WebUI:Show()
end


g_threetestClient = threetestClient()

