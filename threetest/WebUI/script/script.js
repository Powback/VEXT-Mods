function Debug() {
	$('body').css("background", 'url(\"img/bf3bg.png\")');
	$('body').css("background-size", 'cover');

}


var camera, scene, renderer, control;
init();
render();

function init() {

	renderer = new THREE.WebGLRenderer({
		alpha: true
	});
	// You can leave the clear color at the defaultvalue.
	renderer.setClearColor(0x000000, 0); //default
	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(window.innerWidth, window.innerHeight);
	document.body.appendChild(renderer.domElement);
	//
	camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 1, 3000);
	camera.position.set(538, 114, 330);
	camera.lookAt(new THREE.Vector3(518, 116, 241));
	scene = new THREE.Scene();
	var grid = new THREE.GridHelper(1000, 10)
	scene.add(grid);
	grid.position.set(518, 116, 241)
	// var light = new THREE.DirectionalLight( 0xffffff, 2 );
	// light.position.set( 1, 1, 1 );
	// scene.add( light );
	var texture = new THREE.TextureLoader().load('textures/crate.gif', render);
	texture.mapping = THREE.UVMapping;
	texture.anisotropy = renderer.capabilities.getMaxAnisotropy();
	var geometry = new THREE.BoxBufferGeometry(200, 200, 200);

	// console.log(geometry.matrixWorld);
	// geometry.translate(518.13958740234, 116.07349395752, 241.20928955078);


	var material = new THREE.MeshLambertMaterial({
		map: texture
	});
	control = new THREE.TransformControls(camera, renderer.domElement);
	control.addEventListener('change', render);
	var mesh = new THREE.Mesh(geometry, material);
	scene.add(mesh);
	control.attach(mesh);
	scene.add(control);

	mesh.position.set(518, 116, 241);
	// console.log(geometry.matrixWorld);


	window.addEventListener('resize', onWindowResize, false);
	window.addEventListener('keydown', function(event) {
		switch (event.keyCode) {
			case 81: // Q
				let pos = camera.position;
				console.log(camera.position.x);
				console.log(camera.position.y);
				console.log(camera.position.y);
				camera.position.set(pos.x, pos.y + 1, pos.z);

				// control.setSpace( control.space === "local" ? "world" : "local" );
				break;
				// case 17: // Ctrl
				// 	control.setTranslationSnap( 100 );
				// 	control.setRotationSnap( THREE.Math.degToRad( 15 ) );
				// 	break;
				// case 87: // W
				// 	control.setMode( "translate" );
				// 	break;
				// case 69: // E
				// 	control.setMode( "rotate" );
				// 	break;
				// case 82: // R
				// 	control.setMode( "scale" );
				// 	break;
				// case 187:
				// case 107: // +, =, num+
				// 	control.setSize( control.size + 0.1 );
				// 	break;
				// case 189:
				// case 109: // -, _, num-
				// 	control.setSize( Math.max( control.size - 0.1, 0.1 ) );
				// 	break;
		}
	});
	window.addEventListener('keyup', function(event) {
		switch (event.keyCode) {
			case 17: // Ctrl
				control.setTranslationSnap(null);
				control.setRotationSnap(null);
				break;
		}
	});
}

function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize(window.innerWidth, window.innerHeight);
	render();
}

function render() {
	control.update();
	renderer.render(scene, camera);
	camera.updateProjectionMatrix()
}

function UpdateCamera(x, y, z, m00,m01,m02,m10,m11,m12,m20,m21,m22) {
    camera.matrix.set(
        m00,m01,m02,0,
        m10,m11,m12,0,
        m20,m21,m22,1,
		x,y,z);
    camera.updateMatrixWorld( true );

	render();
}

document.onkeydown = getKey;

function getKey(e) {
	switch (e.keyCode) {
		case 81: // Q

			let pos = camera.position;
			// console.log(camera.position.x);
			// console.log(camera.position.y);
			// console.log(camera.position.y);
			camera.position.set(pos.x, pos.y + 1, pos.z);
			render();

			// control.setSpace( control.space === "local" ? "world" : "local" );
			break;
		case 17: // Ctrl
			// control.setTranslationSnap( 100 );
			// control.setRotationSnap( THREE.Math.degToRad( 15 ) );
			break;
		case 87: // W
			// control.setMode( "translate" );
			break;
		case 69: // E
			// control.setMode( "rotate" );
			break;
		case 82: // R
			// control.setMode( "scale" );
			break;
		case 187:
		case 107: // +, =, num+
			// control.setSize( control.size + 0.1 );
			break;
		case 189:
		case 109: // -, _, num-
			// control.setSize( Math.max( control.size - 0.1, 0.1 ) );
			break;
	}
	// if(e.keyCode == 77) {// 'M'
	// 	ToggleMap();
	// }
	// if(e.keyCode == 65) { // 'A'
	// 	DisableHud();
	// 	ShowSpawnUI(1);
	// }
	// if(e.keyCode == 83) { // 'S'
	// 	EnableHud();
	// 	HideSpawnUI();
	// }
	// if(e.keyCode == 49) {
	// 	UpdatePlayerMarkerIcon(1, false, 1, false);
	// }
	// if(e.keyCode == 50) {
	// 	UpdatePlayerMarkerIcon(1, false, 1, true);
	// }
	// if(e.keyCode == 51) {
	// 	UpdatePlayerMarkerIcon(1, true, 1, false);
	// }
	// if(e.keyCode == 52) {
	// 	UpdatePlayerMarkerIcon(1, true, 1, true);
	// }
}