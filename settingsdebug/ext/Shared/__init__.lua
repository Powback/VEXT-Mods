class 'settingsdebugShared'


function settingsdebugShared:__init()
	print("Initializing settingsdebugShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function settingsdebugShared:RegisterVars()
	self.m_CurrentViewMode = 0
end


function settingsdebugShared:RegisterEvents()
		self.m_EngineMessageEvent = Events:Subscribe('Engine:Message', self, self.OnEngineMessage)
		self.m_OnUpdateInputEvent = Events:Subscribe('Client:UpdateInput', self, self.OnUpdateInput)
end


function settingsdebugShared:OnEngineMessage(p_Message) 

	if p_Message.type == MessageType.ClientConnectedMessage or
		p_Message.type == MessageType.ServerLevelLoadedMessage then 

		local m_ClientSettings = ResourceManager:GetSettings("ClientSettings")
		if m_ClientSettings ~= nil then
			print("Got ClientSettings ")
			local s_ClientSettings = ClientSettings (m_ClientSettings)
			s_ClientSettings.showBuildId = true
			s_ClientSettings.debrisClusterEnabled = true
			s_ClientSettings.ingameTimeout = 10000

		else 
			print("Failed to get ClientSettings")
		end		 
	end
end

function settingsdebugShared:OnUpdateInput(p_Delta, p_SimulationDelta)

	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F) then
		SpectatorManager:SetCameraMode(SpectatorCameraMode.FreeCamera)

		local m_WorldRenderSettings = ResourceManager:GetSettings("ClientSettings")
		if m_WorldRenderSettings ~= nil then
			local s_WorldRenderSettings = ClientSettings(m_WorldRenderSettings)
			s_WorldRenderSettings.isSpectator = true
		else 
			print("Failed to get WorldRenderSettings")
		end

	end

	if InputManager:WentKeyDown(InputDeviceKeys.IDK_V) then
		self.m_CurrentViewMode = self.m_CurrentViewMode -1
		if(self.m_CurrentViewMode >= 25) then
			self.m_CurrentViewMode = 0
		end

		print(self.m_CurrentViewMode)
		local m_WorldRenderSettings = ResourceManager:GetSettings("WorldRenderSettings")
		if m_WorldRenderSettings ~= nil then
			local s_WorldRenderSettings = WorldRenderSettings(m_WorldRenderSettings)
			s_WorldRenderSettings.viewMode = self.m_CurrentViewMode
		else 
			print("Failed to get WorldRenderSettings")
		end
	end


end
g_settingsdebugShared = settingsdebugShared()

