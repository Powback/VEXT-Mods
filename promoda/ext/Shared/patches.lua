class 'SharedPatches'

local MP_001 = require '__shared/maps/mp_001'

function SharedPatches:__init()
	self:RegisterEvents()
	self:InitComponents()
end

function SharedPatches:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end

function SharedPatches:InitComponents()
	self.m_MP_001 = MP_001()
end

function SharedPatches:OnLoaded()
	self.m_MP_001:OnLoaded()
end

function SharedPatches:ReadInstance(p_Instance, p_Guid)
	self.m_MP_001:OnReadInstance(p_Instance, p_Guid)
end


return SharedPatches