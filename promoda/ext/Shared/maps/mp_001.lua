class 'MP_001'

function MP_001:__init()
	self.m_FXGuid = Guid('AC4C1198-CB9A-4F84-A74F-F3EB724A5DBB', 'D')
	self.m_VERainGuid = Guid('5E4510C1-94BE-4D47-9825-6EB768C60C1B', 'D')
	self.m_WindGuid = Guid('20C10935-801E-45F2-865F-8A17CA96986C', 'D')
end

function MP_001:OnLoaded()
	
end

function MP_001:OnReadInstance(p_Instance, p_Guid)
	if p_Instance == nil then
		return 
	end

	-- Disable all rain, smoke, and paper trail emitters.
	if p_Guid == self.m_FXGuid then
		local s_Instance = WorldPartData(p_Instance)
		s_Instance:ClearObjects()
	end

	-- Disable the raindrop screen overlay.
	if p_Guid == self.m_VERainGuid then
		local s_Instance = VisualEnvironmentEntityData(p_Instance)
		s_Instance:ClearComponents()
	end

	-- Remove wind to make trees less distracting.
	if p_Guid == self.m_WindGuid then
		local s_Instance = WindComponentData(p_Instance)
		s_Instance.windStrength = 0.0
	end
end

return MP_001