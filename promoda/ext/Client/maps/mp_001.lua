require 'socket'
class 'MP_001'

function MP_001:__init()
	self.m_StateAddedEvent = Events:Subscribe('VE:StateAdded', self, self.OnStateAdded)
end

function MP_001:OnStateAdded(p_State)
	-- Fix a newly added state (if needed).
	if p_State.entityName ~= 'EffectEntity' then
		self:FixEnvironmentState(p_State)
	end
end

function MP_001:OnLoaded()
	-- Make sure all our VE states are fixed.
	local s_States = VisualEnvironmentManager:GetStates()

	for i, s_State in ipairs(s_States) do
		if s_State.entityName ~= 'EffectEntity' then
			self:FixEnvironmentState(s_State)			
		end
	end
end

function MP_001:FixEnvironmentState(p_State)
	print('Fixing visual environment state ' .. p_State.entityName)
	
	local s_ColorCorrection = p_State.colorCorrection

	if s_ColorCorrection ~= nil then
		s_ColorCorrection.brightness = Vec3(1.1,1.1,1.1)
		s_ColorCorrection.contrast = Vec3(1.1,1.1,1.1)
		s_ColorCorrection.saturation = Vec3(1,1,1)
		s_ColorCorrection.hue = 0.0
		s_ColorCorrection.colorGradingEnable = true
	end




	local s_Fog = p_State.fog

	if s_Fog ~= nil then
		s_Fog.fogDistanceMultiplier = 1.0
		s_Fog.fogGradientENable = true
		s_Fog.start = 300.0
		--s_Fog.end = 3000.0
		s_Fog.curve = Vec4(-0.2277586, -0.2494643, 1.475728, 0.00467146)
		s_Fog.fogColorEnable = false
		s_Fog.fogColor = Vec3(0.01, 0.012, 0.017)
		s_Fog.fogColorStart = 200
		s_Fog.fogColorEnd = 500
		s_Fog.enable = false

	end


	local s_OutdoorLight = p_State.outdoorLight

	if s_OutdoorLight ~= nil then
		s_OutdoorLight.sunRotationX = 100.5
		s_OutdoorLight.sunRotationY = 50.5
		s_OutdoorLight.sunColor = Vec3(1,1,1)
		s_OutdoorLight.skyColor = Vec3(0.023, 0.027, 0.0325)
		s_OutdoorLight.groundColor = Vec3(0.223, 0.227, 0.232)
		s_OutdoorLight.skyLightAngleFactor = 10.134
		s_OutdoorLight.sunSpecularScale = 0.1
		s_OutdoorLight.skyEnvmapShadowScale = 2.0
		s_OutdoorLight.sunShadowHeightScale = 1.298
		s_OutdoorLight.cloudShadowEnable = true
		s_OutdoorLight.cloudShadowSpeed = Vec2(30,30)
		s_OutdoorLight.cloudShadowSize = 3000
		s_OutdoorLight.cloudShadowExponent = 3.0
		s_OutdoorLight.translucencyAmbient = 0
		s_OutdoorLight.translucencyScale = 0
		s_OutdoorLight.translucencyPower = 8
		s_OutdoorLight.translucencyDistortion = 0.1
		s_OutdoorLight.enable = true

	end


	local s_Sky = p_State.sky

	if s_Sky ~= nil then
		s_Sky.brightnessScale = 2
		s_Sky.sunSize = 0.001
		s_Sky.sunScale = 100
		s_Sky.enable = true
	end

	local s_TonemapData = p_State.tonemap

	if s_TonemapData ~= nil then
		s_TonemapData.middleGray = 0.2
		s_TonemapData.minExposure = 1
		s_TonemapData.maxExposure = 8
		s_TonemapData.exposureAdjustTime = 0.25

	end

	local s_Vignette = p_State.vignette

	if s_Vignette ~= nil then
		s_Vignette.enable = false
	end

	local s_CameraParams = p_State.cameraParams

	if s_CameraParams ~= nil then

	end


	local s_Wind = p_State.wind

	if s_Wind ~= nil then
		s_Wind.windStrength = 0
	end

	VisualEnvironmentManager.dirty = true

end




return MP_001