class 'Patches'

local MP_001 = require 'maps/mp_001'

function Patches:__init()
	self:RegisterEvents()
	self:InitComponents()
end

function Patches:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	self.m_VEStateAddedEvent = Events:Subscribe('VisualEnvironment:StateAdded', self, self.StateAdded)
end

function Patches:InitComponents()
	self.m_MP_001 = MP_001()
end

function Patches:OnLoaded()
	self.m_MP_001:OnLoaded()
end

function Patches:ReadInstance(p_Instance, p_Guid)
	self.m_MP_001:OnReadInstance(p_Instance, p_Guid)
end

function Patches:StateAdded(p_State)
	self.m_MP_001:OnStateAdded(p_State)
end


return Patches