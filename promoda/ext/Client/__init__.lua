class 'PromodClient'

local SharedPatches = require '__shared/patches'
local Patches = require 'patches'

function PromodClient:__init()
	self:InitComponents()
	self:RegisterEvents()
	self:InstallHooks()
end

function PromodClient:RegisterEvents()
	self.m_LoadedEvent = Events:Subscribe('ExtensionLoaded', self, self.Loaded)
end

function PromodClient:InstallHooks()

end

function PromodClient:InitComponents()
	self.m_SharedPatches = SharedPatches()
	self.m_Patches = Patches()
end

function PromodClient:Loaded()
	self.m_SharedPatches:OnLoaded()
	self.m_Patches:OnLoaded()
end
g_PromodClient = PromodClient()