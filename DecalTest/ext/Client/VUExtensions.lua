class "VUExtensions"

function VUExtensions:PrepareInstanceForEdit(p_Partition, p_Instance)
	if p_Partition == nil then
		error('VUExtensions - ERROR: parameter p_Partition was nil')
		return
	end

	if p_Instance == nil then
		error('VUExtensions - ERROR: parameter p_Instance was nil')
		return
	end

	if not p_Instance.isReadOnly then
		return _G[p_Instance.typeName](p_Instance)
	end

	if p_Instance.isLazyLoading then 
		print(p_Instance)
		error('VUExtensions - ERROR: The instance is being lazy loaded, thus it cant be prepared for editing. Instance type: "' .. p_Instance.typeName)-- maybe add callstack
		return p_Instance
	end

	if p_Instance.instanceGuid == nil then
		print(p_Instance)
		return
	end

	local clone = p_Instance:Clone(p_Instance.instanceGuid)

	p_Partition:ReplaceInstance(p_Instance, clone, true)

	local castedClone = _G[clone.typeName](clone)

	if castedClone ~= nil and castedClone.typeName ~= clone.typeName then
		error('VUExtensions - ERROR: VUExtensions:PrepareInstanceForEdit() - Failed to prepare instance of type ' .. clone.typeName)
		return nil
	end

	return castedClone
end

g_VUExtensions = VUExtensions()

return VUExtensions