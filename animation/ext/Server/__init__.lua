class 'animationServer'


function animationServer:__init()
	print("Initializing animationServer")
	self:RegisterVars()
	self:RegisterEvents()
end


function animationServer:RegisterVars()
	self.soldiers = {}
	self.soldierEntities ={}
	self.template = nil
end


function animationServer:RegisterEvents()
	self.m_SaveEvent = NetEvents:Subscribe('animation:fire', self, self.OnFireAnimation)
	self.m_SaveEvent = NetEvents:Subscribe('animation:soldier', self, self.OnSpawnTestSoldier)
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)

    self.m_Whatever = Hooks:Install('ServerEntityFactory:Create',999, self, self.OnEntityCreate)
end

function animationServer:OnSpawnTestSoldier(player)
	print("Received event")

	for _, l_Soldier in ipairs(self.soldiers) do
		if l_Soldier == nil then
			print('l_Soldier is null?')
			break
		end
		print(tostring(l_Soldier))
		local spawnedExplosion = EntityManager:CreateServerEntity(l_Soldier, player.soldier.transform)
		print("so far so good")

		if spawnedExplosion == nil then
			print("Could not spawn explosion")
			return
		end
		spawnedExplosion:Init(Realm.Realm_ClientAndServer, true)
	end

end

function animationServer:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances

	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end

		if l_Instance.typeInfo.name == "SoldierSpawnTemplateData" then
			print("Template")
			self.template = SoldierSpawnTemplateData(l_Instance)
		end

		if l_Instance.instanceGuid == Guid('1B48BC45-614B-F708-3ADC-935CDD8209AC') then
			print("Testrange!")
		end
	end
end

function animationServer:OnFireAnimation(player, p_EntityName, p_Event, p_Prefix)
	print("Received event")
	local s_EntityName = p_EntityName

	if(p_Prefix) then
		s_EntityName = 'Server' .. s_EntityName
	end

	local s_Iterator = EntityManager:GetServerIterator(s_EntityName)
	print(tostring(s_Iterator))
	if s_Iterator == nil then
		print('Failed to get iterator')
	else
		local s_Entity = s_Iterator:Next()
		local sent = 0

		while s_Entity ~= nil do
			sent = sent + 1
			s_Entity:FireEvent(p_Event)
			s_Entity = s_Iterator:Next()
		end
		print(p_Event)
		print("Sent " .. sent .. " events.")
	end
end


function animationServer:OnEntityCreate(p_Hook, p_Data, p_Transform)
	if p_Data == nil then
		print("Didnt get no data")
	else
		print(p_Data.typeInfo.name)
		if(p_Data.typeInfo.name == "CharacterSpawnReferenceObjectDataa") then
			local s_Data = CharacterSpawnReferenceObjectData(p_Data:Clone())
			s_Data.template = self.template
			s_Data.playerType = 2
			print("Spawned :  " .. p_Data.typeInfo.name)
			p_Hook:Pass(s_Data, p_Transform)
			local x = p_Hook:Call()
			print("entity: " ..x.typeName)
			print(tostring(p_Data.instanceGuid))
			table.insert(self.soldierEntities, x)
			p_Hook:Return(x)
		end
	end	 
end

g_animationServer = animationServer()

