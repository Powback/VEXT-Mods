class 'animationClient'


function animationClient:__init()
	print("Initializing animationClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function animationClient:RegisterVars()
	self.m_customizations = {}

	self.iterator = nil;
	self.castDistance = 10
end


function animationClient:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
	self.m_ClientUpdateInputEvent = Events:Subscribe('Client:UpdateInput', self, self.OnUpdateInput)
    --self.m_Whatever = Hooks:Install('ClientEntityFactory:Create',999, self, self.OnEntityCreate)
end

function animationClient:OnUpdateInput(p_Delta)
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_C) then
		self:PlayAnimation()
	end
	
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F) then
		NetEvents:SendLocal('animation:soldier', "go")
	end

	if InputManager:WentKeyDown(InputDeviceKeys.IDK_V) then
		self:PlayNext()
	end
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_B) then

		local s_Transform = ClientUtils:GetCameraTransform()

		if(s_Transform.trans == Vec3(0,0,0)) then -- Camera is below the ground. Creating an entity here would be useless.
			return
		end

		-- The freecam transform is inverted. Invert it back

		local s_CameraForward = Vec3(s_Transform.forward.x * -1, s_Transform.forward.y * -1, s_Transform.forward.z * -1)

		local s_CastPosition = Vec3(s_Transform.trans.x + (s_CameraForward.x*self.castDistance),
									s_Transform.trans.y + (s_CameraForward.y*self.castDistance),
									s_Transform.trans.z + (s_CameraForward.z*self.castDistance))

		local s_Raycast = RaycastManager:Raycast(s_Transform.trans, s_CastPosition, 2)

		local s_Transform = LinearTransform(
			Vec3(1,0,0),
			Vec3(0,1,0),
			Vec3(0,0,1),
			s_Transform.trans
		)

		if(s_Raycast == nil) then
			return
		end
		if(s_Raycast.rigidBody ~= nil) then
			print(tostring(s_Raycast.rigidBody.typeName))--.data.instanceGuid))
			--s_Raycast.rigidBody:FireEvent("Destroy")
			--if(s_Raycast.rigidBody.data ~= nil) then
				--print(tostring(s_Raycast.rigidBody.data.instanceGuid))
			--else 
				print("No data")
			--end
		end
	end
end

function animationClient:PlayNext() 
	if(self.iterator == nil) then
		print("no bueno")
		return
	end
	local s_Iterator = self.iterator
	local s_Entity = s_Iterator:Next()
	if s_Entity == nil then
		print("No entity")
		return
	end

	local s_Guid = tostring(s_Entity.data.instanceGuid)
	if s_Guid == '1FFE39E6-5A0D-4819-9535-B1200964C5CE' or
	s_Guid == 'D193C7DB-CFE9-41FA-8112-6FE0BD08D59C' or
	s_Guid == '88B82033-4E34-46B3-AE14-2E50CF1C8300' or
	s_Guid == '3D06D784-744C-4405-B271-E1544E248F4D' or
	s_Guid == '5C57D80A-DC4F-485B-9EF8-E71CCFE71494' or
	s_Guid == '4F8FB7D8-8ED9-4D00-8E97-B17D66DF497B' or
	s_Guid == '2B77D0BE-3E8C-498B-910F-0769BBDEAF45' or

	s_Guid == 'E134EA13-7312-4ACC-B535-93C8A41BCE99' or
	s_Guid == '745363B8-5AE4-4CEC-AE39-76C6311E7AD0'
	then
		print("skipped")
		return
		print(s_Guid)
	end
	print("sent")
	s_Entity:FireEvent("Start")
		
end
function animationClient:PlayAnimation()


	local s_EntityName = "InputRestrictionEntity"
	local s_EventName = "Deactivate"
	local s_Prefix = true

	NetEvents:SendLocal('animation:fire', s_EntityName, s_EventName, s_Prefix)

	if(s_Prefix) then
		s_EntityName = 'Client' .. s_EntityName
	end
	
	local s_Iterator = EntityManager:GetClientIterator(s_EntityName)
	
	
	if s_Iterator == nil then
		print('Failed to get iterator')
	else
		print("Sending events: " .. s_EventName .. " to " .. s_EntityName)
		local sent = 0
		local s_Entity = s_Iterator:Next()
		while s_Entity ~= nil do
			::Start::
			local s_Guid = tostring(s_Entity.data.instanceGuid)
			print(s_Guid)
			if s_Guid == '1FFE39E6-5A0D-4819-9535-B1200964C5CE' or
			s_Guid == 'D193C7DB-CFE9-41FA-8112-6FE0BD08D59C' or
			s_Guid == '88B82033-4E34-46B3-AE14-2E50CF1C8300' or
			s_Guid == '3D06D784-744C-4405-B271-E1544E248F4D' or
			s_Guid == '5C57D80A-DC4F-485B-9EF8-E71CCFE71494' or
			s_Guid == '4F8FB7D8-8ED9-4D00-8E97-B17D66DF497B' or
			s_Guid == '2B77D0BE-3E8C-498B-910F-0769BBDEAF45' or

			s_Guid == 'E134EA13-7312-4ACC-B535-93C8A41BCE99' or
			s_Guid == '745363B8-5AE4-4CEC-AE39-76C6311E7AD0'
			then
				print("skipped")
				s_Entity = s_Iterator:Next()
				goto Start
			end
			sent = sent + 1
			s_Entity:FireEvent(s_EventName)
			s_Entity = s_Iterator:Next()
		end
		print("Sent " .. sent .. " events.")
		self.iterator = EntityManager:GetClientIterator(s_EntityName)
	end
end
function animationClient:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances

	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
	end
end

function animationClient:OnEntityCreate(p_Hook, p_Data, p_Transform)
	if p_Data == nil then
		print("Didnt get no data")
	else
		if(p_Data.typeInfo.name == "LogicReferenceObjectData") then
			print("data :  " .. p_Data.typeInfo.name)
			--local x = p_Hook:Call()
			print("entity: " ..x.typeName)
			print(tostring(p_Data.instanceGuid))
		end
	end	 
end



g_animationClient = animationClient()

