class 'RegisterShared'
local m_vuExtensions = require "__shared/VUExtensions"
local m_Cloner = require "__shared/InstanceCloner"



function RegisterShared:__init()
	print("Initializing RegisterShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function RegisterShared:RegisterVars()
	self.m_MVDE = nil
	self.m_MVD = nil
	self.m_Mesh = {}
end


function RegisterShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end


function RegisterShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances

	local s_Clone = nil
	local s_Original = nil
	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if l_Instance.typeInfo.name == "WorldPartData" then
			s_Original = l_Instance
			s_Clone = WorldPartData(l_Instance:Clone(l_Instance.instanceGuid))
			print(s_Clone.name)
		end
	end

	if(s_Clone ~= nil) then
		print("Modified and replaced LevelData")
		p_Partition:ReplaceInstance(s_Original, s_Clone, true)
		print("LEGO")
	end
end
g_RegisterShared = RegisterShared()

