class 'VEManagerClient'


function VEManagerClient:__init()
	print("Initializing VEManagerClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function VEManagerClient:RegisterVars()
	--self.m_this = that
end


function VEManagerClient:RegisterEvents()
	self.m_OnLoadedEvent = Events:Subscribe('ExtensionLoaded', self, self.OnLoaded)
	self.m_StateAddedEvent = Events:Subscribe('VE:StateAdded', self, self.OnStateAdded)
	self.m_StateRemovedEvent = Events:Subscribe('VE:StateRemoved', self, self.OnStateRemoved)
	self.m_OnUpdateInputEvent = Events:Subscribe('Client:UpdateInput', self, self.OnUpdateInput)
end


function CinematicToolsClient:OnLoaded()
	-- Make sure all our VE states are fixed.
	local s_States = VisualEnvironmentManager:GetStates()

	for i, s_State in ipairs(s_States) do
		if s_State.entityName ~= 'EffectEntity' then
			self:FixEnvironmentState(s_State)
		end
	end
end

function CinematicToolsClient:OnStateAdded(p_State)
	-- Fix a newly added state (if needed).
	if p_State.entityName ~= 'EffectEntity' then
		self:FixEnvironmentState(p_State)
	end
end
function CinematicToolsClient:OnStateRemoved(p_State)
	-- Fix a newly added state (if needed).
	if p_State.entityName ~= 'EffectEntity' then
		self:FixEnvironmentState(p_State)
	end
end

function CinematicToolsClient:FixEnvironmentState(p_State)
	--Ignore the Loading VE
	if(p_State.entityName == "Levels/Web_Loading/Lighting/Web_Loading_VE") then
	    return
	end

	print('Fixing visual environment state ' .. p_State.entityName)
	for _,s_Class in pairs(self.m_SupportedClasses) do
		s_Class_Lower = firstToLower(s_Class)

		local s_State = p_State[s_Class_Lower]

		if(_G[s_Class.."ComponentData"] == nil) then
			print(tostring(s_Class) .. " | " .. tostring(s_State))
			return
		end

		local typeInfo = _G[s_Class.."ComponentData"].typeInfo


		--placeholder while the creation thing is fucked
		if(s_State ~= nil) then
			self:SendState(s_Class, s_State , _G[s_Class.."ComponentData"].typeInfo)	
		end
	end
end

function CinematicToolsClient:OnUpdateInput(p_Delta, p_SimulationDelta)

	if(self.m_LerpUpdate['time'] ~= nil ) then
		
		local TimeSinceStarted = SharedUtils:GetTimeMS() - self.m_LerpUpdate['startTime']
		local PercentageComplete = TimeSinceStarted / self.m_LerpUpdate['time']
		self:UpdateLerp(PercentageComplete)
		if(PercentageComplete >= 1) then
			self.m_LerpUpdate = {}
			self.m_LerpStart = {}
			return
		end
	end
end



g_VEManagerClient = VEManagerClient()

