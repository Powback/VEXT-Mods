class 'typeinfodumpShared'


function typeinfodumpShared:__init()
	print("Initializing typeinfodumpShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function typeinfodumpShared:RegisterVars()
	--self.m_this = that
end


function typeinfodumpShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Extension:Loaded', self, self.OnExtensionLoaded)
end


function typeinfodumpShared:OnExtensionLoaded()
	local a = "StaticModelEntityData"
	local s_TypeInfo = _G[a].typeInfo
	print(s_TypeInfo.name)
end

function getFields( typeInfo )
	local s_Super = {}
	if typeInfo.super ~= nil then
		if typeInfo.super.name ~= "DataContainer" then
			for k,superv in pairs(getFields(typeInfo.super)) do
				table.insert(s_Super, superv)
			end
		end
	end
	for k,v in pairs(typeInfo.fields) do
		table.insert(s_Super, v)
	end
	return s_Super
end

g_typeinfodumpShared = typeinfodumpShared()

