class 'FixedSettingsClient'


function FixedSettingsClient:__init()
	print("Initializing FixedSettingsClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function FixedSettingsClient:RegisterVars()
	--local m_this = that
end


function FixedSettingsClient:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end


function FixedSettingsClient:ReadInstance(p_Instance, p_Guid)
	if p_Instance == nil then
		return
	end
	
	if p_Instance.typeName == "SomeType" then
		local s_Instance = SomeType(p_Instance)
		s_Instance.someThing = true
	end
	
	
	if p_Guid == Guid("0D8063D3-840E-6FF8-4390-467D38E7863F", "D") then --profile/OptionCarRadio.txt
		local s_Instance = ProfileOptionDataBool (p_Instance)
		s_Instance.value = true
	end
	if p_Guid == Guid("DF1D2AFF-FCD9-5BA5-FD53-DBA4D19F5527", "D") then -- profile/OptionFieldOfView.txt
		local s_Instance = ProfileOptionDataFloat(p_Instance)
		s_Instance.max = 140
	end
	if p_Guid == Guid("0D8063D3-840E-6FF8-4390-467D38E7863F", "D") then
		local s_Instance = SomeClass(p_Instance)
		s_Instance.someThing = true
	end
	if p_Guid == Guid("0D8063D3-840E-6FF8-4390-467D38E7863F", "D") then
		local s_Instance = SomeClass(p_Instance)
		s_Instance.someThing = true
	end

end


g_FixedSettingsClient = FixedSettingsClient()

return FixedSettingsClient
