class 'firingfunctionvisualClient'


function firingfunctionvisualClient:__init()
	print("Initializing firingfunctionvisualClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function firingfunctionvisualClient:RegisterVars()
	--self.m_this = that
end


function firingfunctionvisualClient:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end


function firingfunctionvisualClient:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeName == "SomeType") then
			local s_Instance = SomeType(l_Instance)
			s_Instance.someThing = 1
		end
		if(l_Instance.instanceGuid == Guid("SomeGuid") then
			local s_Instance = SomeType(l_Instance)
			s_Instance.someThing = 1
		end
	end
end


g_firingfunctionvisualClient = firingfunctionvisualClient()

