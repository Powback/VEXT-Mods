class 'DynamicEnlightenClient'


function DynamicEnlightenClient:__init()
	print("Initializing DynamicEnlightenClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function DynamicEnlightenClient:RegisterVars()
	--self.m_this = that
end


function DynamicEnlightenClient:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	self.m_VEStateAddedEvent = Events:Subscribe('VE:StateAdded', self, self.OnStateAdded)
end


function DynamicEnlightenClient:ReadInstance(p_Instance,p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	if p_Instance.typeName == "EnlightenDataAsset" then
		local s_Instance = EnlightenDataAsset(p_Instance)
		s_Instance.dynamicEnable = true 


	end
	if(p_Instance.typeName == "CameraComponentData") then
		local s_Instance = CameraComponentData(p_Instance)
		s_Instance.ignoreOwnerOrientation = true
		s_Instance.enableCameraMesh = true
		print("modified mesh for some reason")
	end
	
end

function DynamicEnlightenClient:OnStateAdded(p_State)
	local s_States = VisualEnvironmentManager:GetStates()

	for i, s_State in ipairs(s_States) do
		if s_State.entityName ~= 'EffectEntity' then
			self:FixEnvironmentState(s_State)			
		end
	end
	
end

function DynamicEnlightenClient:FixEnvironmentState(p_State)
	print('Fixing visual environment state ' .. p_State.entityName)
	

	if instance ~= nil then
		print("Got EnlightenRuntimeSettings")
		local settings = EnlightenRuntimeSettings(instance)
		settings.forceDynamic = true
		settings.shadowsEnable = false
		settings.lightMapsEnable = true
		settings.enable = true
		settings.lightProbeForceUpdate = true



	else
		print("Could not get EnlightenRuntimeSettings.")
	end

	local instance = ResourceManager:GetSettings("GameAnimationSettings")
	if instance ~= nil then
		print("Got GameAnimationSettings")
		local settings = GameAnimationSettings(instance)
		settings.useAnimationDrivenCharacter = false




	else
		print("Could not get EnlightenRuntimeSettings.")
	end

	local instance = ResourceManager:GetSettings("ClientSettings")
	if instance ~= nil then
		print("Got ClientSettings ")
		local settings = ClientSettings(instance)
		settings.havokVisualDebugger = true
		settings.havokVDBShowsEffectsWorld = true
		settings.havokCaptureToFile = true

	end
	 
	local s_ColorCorrection = p_State.colorCorrection

	if s_ColorCorrection ~= nil then
		s_ColorCorrection.brightness = Vec3(1.15, 1.12, 1.08)
		--s_ColorCorrection.brightness = Vec3(1.0, 1.0, 1.0)
		s_ColorCorrection.contrast = Vec3(1.0, 1.0, 1.0)
		s_ColorCorrection.saturation = Vec3(0.8, 0.8, 0.84)
		s_ColorCorrection.colorGradingEnable = false
		s_ColorCorrection.enable = false
	end

	local s_Sky = p_State.sky

	if s_Sky ~= nil then
		s_Sky.sunSize = 0.013
		s_Sky.brightnessScale = 0.15
		s_Sky.skyVisibilityExponent = 0.75
		s_Sky.cloudLayer1SunLightIntensity = 0
		s_Sky.cloudLayer2SunLightIntensity = 0
		s_Sky.enable = true
	end

	local s_Fog = p_State.fog

	if s_Fog ~= nil then
		s_Fog.enable = true
		s_Fog.heightFogEnable = true
	end

	local s_OutdoorLight = p_State.outdoorLight

	if s_OutdoorLight ~= nil then
		s_OutdoorLight.translucencyPower = 0.4
		s_OutdoorLight.sunColor = Vec3(0.1, 0.1, 0.1)
	end

	local s_TonemapData = p_State.tonemap

	if s_TonemapData ~= nil then
		s_TonemapData.bloomScale = Vec3(0, 0, 0)
		s_TonemapData.minExposure = 8
		s_TonemapData.middleGray = 0.3
		s_TonemapData.maxExposure = 8
	end

	local s_Vignette = p_State.vignette

	if s_Vignette ~= nil then
		s_Vignette.enable = false
	end

	local s_CameraParams = p_State.cameraParams

	if s_CameraParams ~= nil then
		s_CameraParams.viewDistance = 50000
		s_CameraParams.sunShadowmapViewDistance = 100
	end

	VisualEnvironmentManager.dirty = true
	return

end


g_DynamicEnlightenClient = DynamicEnlightenClient()

