class 'BotModShared'


function BotModShared:__init()
	print("Initializing BotModShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function BotModShared:RegisterVars()
	self.m_Soldier = nil
	self.m_Spawned = false
	self.m_Spawns = {}
	self.m_SpawnCount = 0

	self.m_AISystem = nil
	self.m_DebugConstantData = nil

	self.m_LevelData = nil
	self.m_MPSoldier = nil
	self.BFAISettingsData = nil
end



function BotModShared:RegisterEvents()
	Hooks:Install('ServerEntityFactory:CreateFromBlueprint', 999, self, self.OnBlueprintCreate)
	Hooks:Install('ClientEntityFactory:CreateFromBlueprint', 999, self, self.OnBlueprintCreate)
	--Hooks:Install('ResourceManager:LoadBundles',999, self, self.OnLoadBundles)

	Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)

	Hooks:Install('ClientEntityFactory:Create',999, self, self.OnEntityCreate)
	Hooks:Install('ServerEntityFactory:Create',999, self, self.OnEntityCreate)

end



function BotModShared:OnEntityCreate( p_Hook, p_Data, p_Transform )
	if (p_Data.typeInfo.name == "CharacterSpawnReferenceObjectData") then
		local s_Data = self:Modify(CharacterSpawnReferenceObjectData(p_Data))
		p_Hook:Pass(s_Data, p_Transform)
		print("Modified spawn")
	end
end


function BotModShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.instanceGuid == Guid("BC1FBB1E-8C60-5EC8-79BD-1E809A07EEAB")) then
			self.m_AISystem = BFAISystem(l_Instance)
			self.m_AISystem:MakeWritable()
			print("AI System")
		end

		if(l_Instance.instanceGuid == Guid("962D4475-9E4F-4989-B8EC-AC32EDB901A7")) then
			self.m_DebugConstantData = DebugConstantData(l_Instance)
			self.BFAISettingsData.debugConstants = self.m_DebugConstantData
			print("AI Debug Data")

		end
		if(l_Instance.instanceGuid == Guid("4B199773-C475-4F87-8A2C-115BD606E5C5")) then
			self.BFAISettingsData = BFAISettingsData(l_Instance)
			self.BFAISettingsData:MakeWritable()
			self.BFAISettingsData.debugConstants = DebugConstantData()
		end
		if(l_Instance.typeInfo.name == "LevelData") then
			local s_Instance = LevelData(l_Instance)
			s_Instance:MakeWritable()
			if(self.m_MPSoldier ~= nil) then
				self.m_LevelData = s_Instance
				print("LevelData")
				self.m_LevelData.aiSystem = self.m_AISystem
			else
				--s_Instance.aiSystem = nil
				--s_Instance.propertyConnections:clear()
				--s_Instance.linkConnections:clear()
			end
		end

		if(l_Instance.typeInfo.name == "ServerSettings") then
			local s_Instance = ServerSettings(l_Instance)
			s_Instance:MakeWritable()
			--s_Instance.isAiEnabled = true
		end

		if(l_Instance.typeInfo.name == "SoldierSpawnTemplateData") then
			local s_Instance = SoldierSpawnTemplateData(l_Instance)
			print("--------------".. s_Instance.name)
			s_Instance:MakeWritable()
			s_Instance.voiceOverLabels:clear()
			self.m_Template = s_Instance
		end
		if(l_Instance.typeInfo.name == "SoldierBlueprint") then
			local s_Instance = SoldierBlueprint(l_Instance)
			if(s_Instance.name == "Characters/Soldiers/CannedScenarioCivilian" and self.m_Soldier == nil) then
				self.m_Soldier = s_Instance
			elseif(s_Instance.name == "Characters/Soldiers/MpSoldier" and self.m_MPSoldier == nil) then
				self.m_MPSoldier = s_Instance
			end
			print(s_Instance.name)
		end

		if(l_Instance:Is("DataBusData")) then
			local s_Instance = DataBusData(l_Instance)
			for _,v in pairs(s_Instance.propertyConnections) do
				local a = v.source
				local b = v.target
			end
		end
	end
end

function BotModShared:Modify( p_Instance )
	return p_Instance
end



function BotModShared:OnBlueprintCreate( p_Hook, blueprint, transform, variation, parent)
	if(blueprint.typeInfo.name == "SoldierBlueprint") then
		local s_Parent = CharacterSpawnReferenceObjectData(parent)
		if(s_Parent.blueprint == nil) then
			s_Parent.enabled = false
		end
		print(tostring(parent.instanceGuid))
		local s_Parent = CharacterSpawnReferenceObjectData(parent)
		print(s_Parent.playerType)
		local s_Soldiers = p_Hook:Call()
		for k,v in pairs(s_Soldiers) do
			print(v.typeInfo.name)
			local s_Soldier = SoldierEntity(v)
			print(s_Soldier.isAlive)
			print(s_Soldier.worldTransform)
			print(s_Soldier.health)
			print(s_Soldier.player)
		end
	end
end
function BotModShared:OnLoadBundles(p_Hook, p_Bundles, p_Compartment)
	print(p_Bundles)
	if(p_Bundles[1] == "gameconfigurations/game" or p_Bundles[1] == "UI/Flow/Bundle/LoadingBundleMp") then

		Events:Dispatch('BundleMounter:LoadBundle', 'levels/sp_paris/sp_paris', {
			"levels/sp_paris/sp_paris",
			"levels/sp_paris/heat_pc_only",
			"levels/sp_paris/sp_paris",
			"levels/sp_paris/chase",
			"levels/sp_paris/loweroffice",
			"levels/sp_paris/loweroffice_pc"
		})
		Events:Dispatch('BundleMounter:LoadBundle', 'spchunks', {
		})
		print("Sent!")
	end
end

g_BotModShared = BotModShared()

