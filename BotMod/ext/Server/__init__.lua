class 'BotModServer'


function BotModServer:__init()
	print("Initializing BotModServer")
	self:RegisterVars()
	self:RegisterEvents()
end


function BotModServer:RegisterVars()
	self.m_Soldier = nil
	self.m_Spawned = false
	self.m_Spawns = {}
	self.m_SpawnCount = 0

	self.m_AISystem = nil
end


function BotModServer:RegisterEvents()
	Events:Subscribe('Engine:Message', self, self.OnEngineMessage)
	--Events:Subscribe('ExtensionLoaded', self, self.SpawnBot)

end

function BotModServer:OnEngineMessage(p_Message)
	if p_Message.type == MessageType.ClientLevelFinalizedMessage or p_Message.type == MessageType.ServerLevelLoadedMessage then
		--self:Modify()
	end
end


g_BotModServer = BotModServer()

