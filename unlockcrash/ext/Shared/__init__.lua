class 'unlockcrashShared'


function unlockcrashShared:__init()
	print("Initializing unlockcrashShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function unlockcrashShared:RegisterVars()
	self.originalAsset = nil
	self.clonedAsset = nil
	self.modified = false
end


function unlockcrashShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end


function unlockcrashShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end

		if l_Instance.instanceGuid == Guid('E628556D-6CA5-4B9D-851A-CCE10BF6A59B') then --UI/Art/Persistence/Camo/U_CAMO_DrPepper
			print("Found U_CAMO_DrPepper")
			self.originalAsset = l_Instance
			self.clonedAsset = UnlockAsset(l_Instance:Clone(l_Instance.instanceGuid))

		end
	end
	if(self.originalAsset ~= nil and self.clonedAsset ~= nil and self.modified == false) then
		print("We have both assets, we're at the end of the partition, and we're replacing 'yo. ")
		p_Partition:ReplaceInstance(self.originalAsset, self.clonedAsset, true)
		self.modified = true
	end
end


g_unlockcrashShared = unlockcrashShared()

