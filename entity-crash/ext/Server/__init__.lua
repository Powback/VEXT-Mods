function SpawnNetworked(trans)
    local data = ResourceManager:SearchForInstanceByGUID(Guid('1B43FD78-C623-7ACE-E409-6CC781C848B3'))

    if data == nil then
        print("Could not find data")
        return
    end

    print("Found data " .. tostring(data))

    local transform = LinearTransform()
    transform.trans = trans
    print(ObjectBlueprint(data).name)
    print("Spawning entity at " .. tostring(transform))

    local entity = EntityManager:CreateServerEntity(data, transform)

    if entity == nil then
        print("Failed to spawn entity")
        return
    end

    print("Spawned entity, initializing")

    -- Client and Server
    entity:Init(2, true)

    print("Initialized!")
end

Events:Subscribe("Player:Chat", function(player, mask, message)
    if player == nil or player.soldier == nil then
        return
    end

    if message == "spawn" then
        SpawnNetworked(player.soldier.transform.trans)
        --NetEvents:BroadcastLocal("EntityCrash:Spawn", player.soldier.transform.trans)
    elseif message == "destroy" then
        --NetEvents:BroadcastLocal("EntityCrash:Destroy")
    end
end)


Events:Subscribe('Engine:Message', function(p_Message)
    if p_Message.type == MessageType.ClientConnectedMessage or
        p_Message.type == MessageType.ServerLevelLoadedMessage then 
        SpawnNetworked(LinearTransform())
    end
end)


--[[Hooks:Install('ServerEntityFactory:Create', 999, function(hook, data, transform)
    --print('Spawning ' .. data.typeInfo.name .. " (" .. tostring(data.instanceGuid) .. ")")
    x = hook:Call()
    print(x.typeName)
    hook:Return(x)
end)]]

