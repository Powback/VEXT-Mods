local entity = nil

NetEvents:Subscribe("EntityCrash:Spawn", function(trans)
    if entity ~= nil then
        print("Entity is already spawned.")
        return
    end

    --local bp = ResourceManager:LookupDataContainer(3, 'Props/Vehicles/DeliveryTruck/DeliveryTruck')
    local bp = ResourceManager:LookupDataContainer(3, 'Objects/Tires_Stack/Tires01')

    if bp == nil then
        print("Could not find blueprint")
        return
    end

    print("Found blueprint " .. tostring(bp.instanceGuid))

    local transform = LinearTransform()
    transform.trans = trans

    print("Spawning entity at " .. tostring(transform))

    local entities = EntityManager:CreateClientEntitiesFromBlueprint(bp, transform)

    print("Spawned " .. tostring(#entities) .. " entities")

    if #entities ~= 1 then
        print("Didn't get expected number of entities")
        return
    end

    entity = entities[1]
    entity:Init(0, true)

    print("Entity initialized")
end)

NetEvents:Subscribe("EntityCrash:Destroy", function()
    if entity == nil then
        print("Entity has not been spawned.")
        return
    end

    print("Destroying entity")
    entity:Destroy()

    print("Destroyed entity")
    entity = nil
    
    print("We're done here")
end)
