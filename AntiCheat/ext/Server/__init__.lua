class 'AntiCheatServer'

local SharedPatches = require '__shared/patches'

function AntiCheatServer:__init()
	self:InitComponents()
	self:RegisterEvents()
	self:InstallHooks()
	print("kekkkek")
end

function AntiCheatServer:RegisterEvents()
	self.m_LoadedEvent = Events:Subscribe('ExtensionLoaded', self, self.Loaded)
	self.m_SaveEvent = NetEvents:Subscribe('dude:save', self, self.OnSave)
end

function AntiCheatServer:OnSave(p_somedata)
	print("Debug: ".. tostring(p_somedata))
end

function AntiCheatServer:InstallHooks()

end

function AntiCheatServer:InitComponents()
	self.m_SharedPatches = SharedPatches()
end

function AntiCheatServer:Loaded()
	self.m_SharedPatches:OnLoaded()
end

g_AntiCheatServer = AntiCheatServer()

return AntiCheatServer()