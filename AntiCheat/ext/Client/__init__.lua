class 'AntiCheatClient'

local SharedPatches = require '__shared/patches'

local s_Projectile = {}
local s_projectileNum = 0
local s_curProjectileNum = 0

local s_ffd = {}
local s_ffdNum = 0
local s_curffdNum = 0

local s_zld = {}
local s_zldNum = 0
local s_curzldNum = 0

local im_cheating = false

function AntiCheatClient:__init()
	self:InitComponents()
	self:RegisterEvents()
	self:InstallHooks()
	self.cooldown = 100

	self.s_Player = PlayerManager:GetLocalPlayer()
	

end

function AntiCheatClient:RegisterEvents()
	self.m_LoadedEvent = Events:Subscribe('ExtensionLoaded', self, self.Loaded)
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	self.m_UpdateEvent = Events:Subscribe("Engine:Update", self, self.OnUpdate)
	self.m_ClientUpdateInputEvent = Events:Subscribe('Client:UpdateInput', self, self.OnUpdateInput)
end

	
function AntiCheatClient:OnUpdateInput(p_Delta)
	
	self.cooldown = self.cooldown - 1

	if self.cooldown < 0 then 
		self.cooldown = 10
		local s_Player = PlayerManager:GetLocalPlayer()
		if s_Player == nil then
			return
		end
		
		--print("Player: " .. s_Player.name .. " K: " .. s_Player:GetKills() .. " D: " .. s_Player:GetDeaths() .. "Time: " .. s_Player:GetTime())
		
		local s_Soldier = s_Player.soldier
		   if s_Soldier == nil then
			return
		end



		local s_proj = s_Projectile[s_curProjectileNum]

		if s_proj["Instance"].startDamage ~= s_proj["startDamage"] then
			print("[Anticheat] Using modified startDamage. Org: " .. tostring(s_proj["startDamage"]).. " | Current: " .. tostring(s_proj["Instance"].startDamage))
			im_cheating = true
		end

		if s_proj["Instance"].endDamage ~= s_proj["endDamage"] then
			print("[Anticheat] Using modified endDamage. Org: " .. tostring(s_proj["endDamage"]).. " | Current: " .. tostring(s_proj["Instance"].endDamage))
			im_cheating = true
		end

		if s_proj["Instance"].damageFalloffStartDistance ~= s_proj["damageFalloffStartDistance"] then
			print("[Anticheat] Using modified damageFalloffStartDistance. Org: " .. tostring(s_proj["damageFalloffStartDistance"]).. " | Current: " .. tostring(s_proj["Instance"].damageFalloffStartDistance))
			im_cheating = true
		end

		if s_proj["Instance"].damageFalloffEndDistance ~= s_proj["damageFalloffEndDistance"] then
			print("[Anticheat] Using modified damageFalloffEndDistance. Org: " .. tostring(s_proj["damageFalloffEndDistance"]).. " | Current: " .. tostring(s_proj["Instance"].damageFalloffEndDistance))
			im_cheating = true
		end

		if s_proj["Instance"].instantHit ~= s_proj["instantHit"] then
			print("[Anticheat] Using modified instantHit. Org: " .. tostring(s_proj["instantHit"]).. " | Current: " .. tostring(s_proj["Instance"].instantHit))
			im_cheating = true
		end

		if s_proj["Instance"].timeToLive ~= s_proj["timeToLive"] then
			print("[Anticheat] Using modified timeToLive. Org: " .. tostring(s_proj["timeToLive"]).. " | Current: " .. tostring(s_proj["Instance"].timeToLive))
			im_cheating = true
		end

		if s_proj["Instance"].gravity ~= s_proj["gravity"] then
			print("[Anticheat] Using modified gravity. Org: " .. tostring(s_proj["gravity"]).. " | Current: " .. tostring(s_proj["Instance"].gravity))
			im_cheating = true
		end

		s_curProjectileNum = s_curProjectileNum + 1
		if s_curProjectileNum == s_projectileNum then
			s_curProjectileNum = 0
		end



--[[
		local s_curffd = s_ffd[s_curffdNum]
		if s_curffd["initialSpeed"] ~= s_curffd["Instance"].initialSpeed.z then
			print("[Anticheat] Using modified initialSpeed. Org: " .. tostring(s_curffd["initialSpeed"]).. " | Current: " .. tostring(s_curffd["Instance"].initialSpeed.z))
		end

		s_curffdNum = s_curffdNum + 1
		if s_curffdNum == s_ffdNum then
			s_curffdNum = 0
		end
--]]



		local s_curzld = s_zld[s_curzldNum]
		if s_curzld["fieldOfView"] ~= s_curzld["Instance"].fieldOfView then
		    print("[Anticheat] Using modified fieldOfView. Org: " .. tostring(s_curzld["fieldOfView"]).. " | Current: " .. tostring(s_curzld["Instance"].fieldOfView))
		    im_cheating = true
		end
		if s_curzld["swayPitchMultiplier"] ~= s_curzld["Instance"].swayPitchMultiplier then
		    print("[Anticheat] Using modified swayPitchMultiplier. Org: " .. tostring(s_curzld["swayPitchMultiplier"]).. " | Current: " .. tostring(s_curzld["Instance"].swayPitchMultiplier))
		    im_cheating = true
		end
		if s_curzld["swayYawMultiplier"] ~= s_curzld["Instance"].swayYawMultiplier then
		    print("[Anticheat] Using modified swayYawMultiplier. Org: " .. tostring(s_curzld["swayYawMultiplier"]).. " | Current: " .. tostring(s_curzld["Instance"].swayYawMultiplier))
		    im_cheating = true
		end
		if s_curzld["dispersionMultiplier"] ~= s_curzld["Instance"].dispersionMultiplier then
		    print("[Anticheat] Using modified dispersionMultiplier. Org: " .. tostring(s_curzld["dispersionMultiplier"]).. " | Current: " .. tostring(s_curzld["Instance"].dispersionMultiplier))
		    im_cheating = true
		end
		if s_curzld["recoilMultiplier"] ~= s_curzld["Instance"].recoilMultiplier then
		    print("[Anticheat] Using modified recoilMultiplier. Org: " .. tostring(s_curzld["recoilMultiplier"]).. " | Current: " .. tostring(s_curzld["Instance"].recoilMultiplier))
		    im_cheating = true
		end
		if s_curzld["recoilFovMultiplier"] ~= s_curzld["Instance"].recoilFovMultiplier then
		    print("[Anticheat] Using modified recoilFovMultiplier. Org: " .. tostring(s_curzld["recoilFovMultiplier"]).. " | Current: " .. tostring(s_curzld["Instance"].recoilFovMultiplier))
		    im_cheating = true
		end
		if s_curzld["cameraImpulseMultiplier"] ~= s_curzld["Instance"].cameraImpulseMultiplier then
		    print("[Anticheat] Using modified cameraImpulseMultiplier. Org: " .. tostring(s_curzld["cameraImpulseMultiplier"]).. " | Current: " .. tostring(s_curzld["Instance"].cameraImpulseMultiplier))
		    im_cheating = true
		end
--		if s_curzld["supportedSwayPitchMultiplier"] ~= s_curzld["Instance"].supportedSwayPitchMultiplier then
--		    print("[Anticheat] Using modified supportedSwayPitchMultiplier. Org: " .. tostring(s_curzld["supportedSwayPitchMultiplier"]).. " | Current: " .. tostring(s_curzld["Instance"].supportedSwayPitchMultiplier))
--		end
--		if s_curzld["supportedSwayYawMultiplier"] ~= s_curzld["Instance"].supportedSwayYawMultiplier then
--		    print("[Anticheat] Using modified supportedSwayYawMultiplier. Org: " .. tostring(s_curzld["supportedSwayYawMultiplier"]).. " | Current: " .. tostring(s_curzld["Instance"].supportedSwayYawMultiplier))
--		end
		if s_curzld["cameraImpulseMultiplier"] ~= s_curzld["Instance"].cameraImpulseMultiplier then
		    print("[Anticheat] Using modified cameraImpulseMultiplier. Org: " .. tostring(s_curzld["cameraImpulseMultiplier"]).. " | Current: " .. tostring(s_curzld["Instance"].cameraImpulseMultiplier))
		    im_cheating = true
		end



		s_curzldNum = s_curzldNum + 1
		if s_curzldNum == s_zldNum then
			s_curzldNum = 0
			print("Reset s_curProjectileNum")
		end

		if im_cheating == true then
			ChatManager:SendMessage("I'm cheating. Fuck me.")
		end
	end

end


function AntiCheatClient:OnUpdate(p_Delta, p_SimulationDelta)

end

function AntiCheatClient:InstallHooks()

end

function AntiCheatClient:RequestSave(p_somedata)
	--NetEvents:SendLocal('dude:save', p_somedata)
end


function AntiCheatClient:InitComponents()
	--self.m_SharedPatches = SharedPatches()
end

function AntiCheatClient:Loaded()
	--self.m_SharedPatches:OnLoaded()
end

function AntiCheatClient:ReadInstance(p_Instance, p_Guid)

	if p_Instance.typeName == "BulletEntityData" then
		local s_Instance = BulletEntityData(p_Instance)
		s_Projectile[s_projectileNum] = {}

		s_Projectile[s_projectileNum]["Instance"] = s_Instance
		s_Projectile[s_projectileNum]["startDamage"] = s_Instance.startDamage
		s_Projectile[s_projectileNum]["endDamage"] = s_Instance.endDamage
		s_Projectile[s_projectileNum]["damageFalloffStartDistance"] = s_Instance.damageFalloffStartDistance
		s_Projectile[s_projectileNum]["damageFalloffEndDistance"] = s_Instance.damageFalloffEndDistance
		s_Projectile[s_projectileNum]["instantHit"] = s_Instance.instantHit
		s_Projectile[s_projectileNum]["timeToLive"] = s_Instance.timeToLive
		s_Projectile[s_projectileNum]["gravity"] = s_Instance.gravity

		
		s_projectileNum = s_projectileNum + 1

	end


	if p_Instance.typeName == "BulletEntityData" then
		local s_Instance = BulletEntityData(p_Instance)
		s_ffd[s_ffdNum] = {}

		s_ffd[s_ffdNum]["Instance"] = s_Instance
		s_ffd[s_ffdNum]["initialSpeed"] = s_Instance.initialSpeed.z
		
		s_ffdNum = s_ffdNum + 1

	end

	if p_Instance.typeName == "ZoomLevelData" then
		local s_Instance = ZoomLevelData(p_Instance)
		s_zld[s_zldNum] = {}

		s_zld[s_zldNum]["Instance"] = s_Instance
		s_zld[s_zldNum]["fieldOfView"] = s_Instance.fieldOfView
		s_zld[s_zldNum]["swayPitchMultiplier"] = s_Instance.swayPitchMultiplier
		s_zld[s_zldNum]["swayYawMultiplier"] = s_Instance.swayYawMultiplier
		s_zld[s_zldNum]["dispersionMultiplier"] = s_Instance.dispersionMultiplier
		s_zld[s_zldNum]["recoilMultiplier"] = s_Instance.recoilMultiplier
		s_zld[s_zldNum]["recoilFovMultiplier"] = s_Instance.recoilFovMultiplier
		s_zld[s_zldNum]["cameraImpulseMultiplier"] = s_Instance.cameraImpulseMultiplier
		--s_zld[s_zldNum]["supportedSwayPitchMultiplier"] = s_Instance.cameraImpulseMultiplier
		--s_zld[s_zldNum]["supportedSwayYawMultiplier"] = s_Instance.cameraImpulseMultiplier
		s_zld[s_zldNum]["cameraImpulseMultiplier"] = s_Instance.cameraImpulseMultiplier
		print("zld")
		s_zldNum = s_zldNum + 1

	end



end

g_AntiCheatClient = AntiCheatClient()

return AntiCheatClient()

--[[
https://gist.github.com/ExtReMLapin/4091e9ca34a404b624a9

Todo:
OnReadInstance: if type = weapons then
weapons[weaponname][weaponbullet][damage] = damage
if [weaponname][weaponbullet][damage] != damage then HACK
should solve everything
--]]

