class "TestShared"

local nadeWeaponUnlockAsset =  nil

function TestShared:__init()
	print("TestShared initializing")
	
	self:RegisterVars()
	self:RegisterEvents()
end

function TestShared:RegisterVars()

end

function TestShared:RegisterEvents()
	Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)

end

function TestShared:OnPartitionLoaded(p_Partition)

	local instances = p_Partition.instances

	for _, instance in pairs(instances) do
		if instance == nil then
			goto continue
		end

		if instance.typeInfo.name == 'SoldierWeaponUnlockAsset' then
			local asset = SoldierWeaponUnlockAsset(instance)

			if asset.name == 'Weapons/M67/U_M67' then
				print('Found soldier weapon unlock asset ' .. asset.name)
				nadeWeaponUnlockAsset = asset
			end
		end

		-- SoldierWeaponBlueprint -  Last thing that is loaded, we proceed to clone
		if instance.instanceGuid == Guid("C569F719-AB04-7BB0-2C08-B906DBC9FD3A", "D") then
			print("Ah shit, here we go again..")
			local s_WeaponUnlockAsset = nadeWeaponUnlockAsset:Clone()
		end

		::continue::
	end
end

return TestShared()