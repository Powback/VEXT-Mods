class 'ShadedPatches'

local SharedPatches = require '__shared/patches'

function ShadedPatches:__init()
	self:InitComponents()
	self:RegisterEvents()
end

function ShadedPatches:RegisterEvents()
	self.m_LoadedEvent = Events:Subscribe('ExtensionLoaded', self, self.Loaded)
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end

function ShadedPatches:InitComponents()
	self.m_SharedPatches = SharedPatches()
end

function ShadedPatches:Loaded()
	self.m_SharedPatches:OnLoaded()
end

function ShadedPatches:ReadInstance(p_Instance, p_Guid)

end

g_ShadedPatches = ShadedPatches()

return ShadedPatches