class 'PluginTemplateClient'

local SharedPatches = require '__shared/patches'

function PluginTemplateClient:__init()
	self:InitComponents()
	self:RegisterEvents()
end

function PluginTemplateClient:RegisterEvents()
	self.m_LoadedEvent = Events:Subscribe('ExtensionLoaded', self, self.Loaded)
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end


function PluginTemplateClient:InitComponents()
	self.m_SharedPatches = SharedPatches()
end

function PluginTemplateClient:Loaded()
	self.m_SharedPatches:OnLoaded()
end

function PluginTemplateClient:ReadInstance(p_Instance, p_Guid)

end

g_PluginTemplateClient = PluginTemplateClient()

return PluginTemplateClient