class 'bulletphysicsShared'


function bulletphysicsShared:__init()
	print("Initializing bulletphysicsShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function bulletphysicsShared:RegisterVars()
	self.m_ActiveBullets = {}
	self.m_Delta = 0
end


function bulletphysicsShared:RegisterEvents()
	Events:Subscribe("Engine:Update", self, self.OnUpdate)
	Hooks:Install('ClientEntityFactory:Create',999, self, self.OnEntityCreate)
	Hooks:Install('ServerEntityFactory:Create',999, self, self.OnEntityCreate)
end

function bulletphysicsShared:OnEntityCreate(p_Hook, p_Data, p_Transform)
	if p_Data == nil then
		print("Didnt get no data")
	else
		local s_Call = p_Hook:Call()
		if(s_Call == nil) then
			return
		end

		if(s_Call.typeInfo.name == "ClientBulletEntity" or s_Call.typeInfo.name == "ServerBulletEntity") then

			local s_Entity = s_Call
			if(s_Entity ~= nil) then
				local s_Data = _G[p_Data.typeInfo.name](p_Data)
				print(s_Data.timeToLive )
				print(tostring(s_Entity.instanceID))
				self.m_ActiveBullets[s_Entity.instanceID] = {
					timeToLive = s_Data.timeToLive,
					time = 0,
					entity = s_Entity
				}
			end
		end
	end	 
end
function bulletphysicsShared:OnUpdate(p_Delta, p_SimulationDelta)
	self.m_Delta = p_Delta
	self:ApplyPhysics(p_Delta)
end

function bulletphysicsShared:ApplyPhysics(p_Delta)
	for k,l_EntityData in pairs(self.m_ActiveBullets) do
		if(l_EntityData.entity ~= nil and l_EntityData.entity:Is("SpatialEntity")) then
			print(tostring(l_EntityData.entity))
			l_EntityData.time = l_EntityData.time + p_Delta
			if(l_EntityData.time > l_EntityData.timeToLive) then
				self.m_ActiveBullets[k] = nil
			else
				local s_Entity = PhysicsEntity(l_EntityData.entity)
				print(s_Entity.velocity)
			end
		end
	end
end
g_bulletphysicsShared = bulletphysicsShared()


