class 'MP_001'

function MP_001:__init()
	self.m_StateAddedEvent = Events:Subscribe('VE:StateAdded', self, self.OnStateAdded)
	--NetEvents:Subscribe('promod:GetRandom', self, self.OnRandom)
	--self.CaspianSkyComponent = Guid('CA86D374-2F13-4F8B-B86A-7D1F0809971D', 'D')

end

function MP_001:OnStateAdded(p_State)
	-- Fix a newly added state (if needed).
	if p_State.entityName ~= 'EffectEntity' then
		self:FixEnvironmentState(p_State)
	end
end


function MP_001:OnLoaded()
	-- Make sure all our VE states are fixed.
	local s_States = VisualEnvironmentManager:GetStates()

	for i, s_State in ipairs(s_States) do
		if s_State.entityName ~= 'EffectEntity' then
			self:FixEnvironmentState(s_State)			
		end
	end
end


function MP_001:ReadInstance(p_Instance, p_Guid)
end


function MP_001:FixEnvironmentState(p_State)
	print('Fixing visual environment state ' .. p_State.entityName .. " for ffs")
	

	local s_ColorCorrection = p_State.colorCorrection

	if s_ColorCorrection ~= nil then
		s_ColorCorrection.brightness = Vec3(1, 1, 1)
		--s_ColorCorrection.brightness = Vec3(1.0, 1.0, 1.0)
		s_ColorCorrection.contrast = Vec3(1.0, 1.0, 1.0)
		s_ColorCorrection.saturation = Vec3(0.8, 0.8, 0.8)
		s_ColorCorrection.colorGradingEnable = true
		s_ColorCorrection.enable = true
	end

	local s_Sky = p_State.sky

	if s_Sky ~= nil then

	end

	local s_OutdoorLight = p_State.outdoorLight

	if s_OutdoorLight ~= nil then
		s_OutdoorLight.translucencyPower = 0
		s_OutdoorLight.sunColor = Vec3(
										SharedUtils:GetRandom(0.4,1), 
										SharedUtils:GetRandom(0.4,0.7),
										SharedUtils:GetRandom(0.4,0.7)
										)

		s_OutdoorLight.sunRotationX = SharedUtils:GetRandom(0,360)
		s_OutdoorLight.sunRotationY = SharedUtils:GetRandom(0,180)
		s_OutdoorLight.sunShadowHeightScale = SharedUtils:GetRandom(0.2,5)
		s_OutdoorLight.enable = true
	end

	local s_CharacterLighting = p_State.characterLighting

	if s_CharacterLighting ~= nil then
		s_CharacterLighting.characterLightEnable = true
		s_CharacterLighting.firstPersonEnable = true
		s_CharacterLighting.lockToCameraDirection = false

	end


	local s_TonemapData = p_State.tonemap

	if s_TonemapData ~= nil then
		s_TonemapData.bloomScale = Vec3(0, 0, 0)
		s_TonemapData.minExposure = SharedUtils:GetRandom(0,10)
		s_TonemapData.middleGray = SharedUtils:GetRandom(0,0.7)
		s_TonemapData.maxExposure = SharedUtils:GetRandom(7,10)
	end


	local s_Enlighten = p_State.enlighten

	if s_Enlighten ~= nil then
		s_Enlighten.skyBoxEnable = true
		s_Enlighten.skyBoxSkyColor = Vec3(
										SharedUtils:GetRandom(0.1,1), 
										SharedUtils:GetRandom(0.1,1),
										SharedUtils:GetRandom(0.1,1)
										)

	end

	local s_Vignette = p_State.vignette

	if s_Vignette ~= nil then
		s_Vignette.enable = false
	end

	local s_Fog = p_State.fog

	if s_Fog ~= nil then
		s_Fog.enable = false
		s_Fog.fogGradientEnable = true
		s_Fog.start = 10000
		s_Fog.fogColor = Vec3(
								SharedUtils:GetRandom(0,1), 
								SharedUtils:GetRandom(0,1),
								SharedUtils:GetRandom(0,1)
								)
		s_Fog.fogDistanceMultiplier = 1
	end

	local s_Wind = p_State.wind

	if s_Wind ~= nil then
		s_Wind.windStrength = SharedUtils:GetRandom(0,10)
	end


	local s_CameraParams = p_State.cameraParams

	if s_CameraParams ~= nil then
		s_CameraParams.viewDistance = 5000
		s_CameraParams.sunShadowmapViewDistance = 100
	end

	VisualEnvironmentManager.dirty = true
	
end


function MP_001:OnReadInstance(p_Instance, p_Guid)

end


return MP_001