class 'botServer'


function botServer:__init()
	print("Initializing botServer")
	self:RegisterVars()
	self:RegisterEvents()
end


function botServer:RegisterVars()
	self.m_Soldier = nil
	self.m_SoldierCust = {}
end


function botServer:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
	self.m_ChatEvent = Events:Subscribe("Player:Chat", self, self.OnChat)
end



function botServer:OnChat(p_Player, p_Mask, p_Message)
	--print("[kBot] " .. p_Player.name .. ": " .. p_Message)
	if p_Player == nil then
		return
	end


	NetEvents:BroadcastLocal('Spawn:Anim')


	local s_Commands = split(p_Message, " ")
	
	local s_Command = s_Commands[1]
	if s_Command == nil then
		return
	end
	print(p_Player)
	local params = EntityCreationParams()
	params.transform = p_Player.soldier.transform
	params.networked = true

    local objectEntity = EntityManager:CreateServerEntity(self.m_Soldier, p_Player.soldier.transform)
    if(objectEntity == nil) then
    	print("fucking nothing spawned")
    	return
    end
    --print(tostring(objectEntity))
    --print(objectEntity)
	--objectEntity:Init(Realm.Realm_Server, true)
	--print("did done")
	--local s_Soldier = SoldierEntity(objectEntity)
	print(CustomizeSoldierData(self.m_SoldierCust[1]))
	p_Player.soldier:ApplyCustomization(CustomizeSoldierData(self.m_SoldierCust[1]))

end
function botServer:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeInfo.name == "SoldierEntityData") then
			self.m_Soldier = SoldierEntityData(l_Instance)
			print("Soldier")
		end
		if(l_Instance.typeInfo.name == "CustomizeSoldierData") then
			self.m_SoldierCust[#self.m_SoldierCust + 1] = l_Instance
			print(self.m_SoldierCust[#self.m_SoldierCust].name)
		end
		
	end
end

function split(pString, pPattern)
   local Table = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pPattern
   local last_end = 1
   local s, e, cap = pString:find(fpat, 1)
   while s do
	  if s ~= 1 or cap ~= "" then
	 table.insert(Table,cap)
	  end
	  last_end = e+1
	  s, e, cap = pString:find(fpat, last_end)
   end
   if last_end <= #pString then
	  cap = pString:sub(last_end)
	  table.insert(Table, cap)
   end
   return Table
end
g_botServer = botServer()

