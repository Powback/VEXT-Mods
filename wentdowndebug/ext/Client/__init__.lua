class 'wentdowndebugClient'


function wentdowndebugClient:__init()
	print("Initializing wentdowndebugClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function wentdowndebugClient:RegisterVars()
	--self.m_this = that
end


function wentdowndebugClient:RegisterEvents()
		Events:Subscribe('Client:UpdateInput', self, self.OnUpdate)

end

function wentdowndebugClient:OnUpdate(p_Delta, p_SimulationDelta)	
		local s_Player = PlayerManager:GetLocalPlayer()
		if s_Player == nil then
			return 
		elseif s_Player.soldier == nil then
			return 
		end

		self:CheckInput(p_Delta, p_SimulationDelta)

end
function wentdowndebugClient:CheckInput(p_Delta, p_SimulationDelta)
	if InputManager:WentDown(InputConceptIdentifiers.ConceptMoveForward) then
		print("WentDown: ConceptMoveForward")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptMoveBackward) then
		print("WentDown: ConceptMoveBackward")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptMoveLeft) then
		print("WentDown: ConceptMoveLeft")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptMoveRight) then
		print("WentDown: ConceptMoveRight")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptRecenterCamera) then
		print("WentDown: ConceptRecenterCamera")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptFire) then
		print("WentDown: ConceptFire")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptAltFire) then
		print("WentDown: ConceptAltFire")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptFireCountermeasure) then
		print("WentDown: ConceptFireCountermeasure")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptReload) then
		print("WentDown: ConceptReload")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptZoom) then
		print("WentDown: ConceptZoom")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptToggleCamera) then
		print("WentDown: ConceptToggleCamera")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSprint) then
		print("WentDown: ConceptSprint")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptCrawl) then
		print("WentDown: ConceptCrawl")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptToggleWeaponLight) then
		print("WentDown: ConceptToggleWeaponLight")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptJump) then
		print("WentDown: ConceptJump")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptCrouch) then
		print("WentDown: ConceptCrouch")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptCrouchOnHold) then
		print("WentDown: ConceptCrouchOnHold")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptProne) then
		print("WentDown: ConceptProne")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptInteract) then
		print("WentDown: ConceptInteract")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptPickUp) then
		print("WentDown: ConceptPickUp")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptDrop) then
		print("WentDown: ConceptDrop")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptBreathControl) then
		print("WentDown: ConceptBreathControl")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptParachute) then
		print("WentDown: ConceptParachute")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSwitchInventoryItem) then
		print("WentDown: ConceptSwitchInventoryItem")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSelectInventoryItem1) then
		print("WentDown: ConceptSelectInventoryItem1")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSelectInventoryItem2) then
		print("WentDown: ConceptSelectInventoryItem2")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSelectInventoryItem3) then
		print("WentDown: ConceptSelectInventoryItem3")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSelectInventoryItem4) then
		print("WentDown: ConceptSelectInventoryItem4")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSelectInventoryItem5) then
		print("WentDown: ConceptSelectInventoryItem5")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSelectInventoryItem6) then
		print("WentDown: ConceptSelectInventoryItem6")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSelectInventoryItem7) then
		print("WentDown: ConceptSelectInventoryItem7")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSelectInventoryItem8) then
		print("WentDown: ConceptSelectInventoryItem8")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSelectInventoryItem9) then
		print("WentDown: ConceptSelectInventoryItem9")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSwitchToPrimaryWeapon) then
		print("WentDown: ConceptSwitchToPrimaryWeapon")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSwitchToGrenadeLauncher) then
		print("WentDown: ConceptSwitchToGrenadeLauncher")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSwitchToStaticGadget) then
		print("WentDown: ConceptSwitchToStaticGadget")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSwitchToDynamicGadget1) then
		print("WentDown: ConceptSwitchToDynamicGadget1")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSwitchToDynamicGadget2) then
		print("WentDown: ConceptSwitchToDynamicGadget2")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptMeleeAttack) then
		print("WentDown: ConceptMeleeAttack")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptThrowGrenade) then
		print("WentDown: ConceptThrowGrenade")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptCycleFireMode) then
		print("WentDown: ConceptCycleFireMode")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptChangeVehicle) then
		print("WentDown: ConceptChangeVehicle")
	end

	if InputManager:WentDown(InputConceptIdentifiers.ConceptBrake) then
		print("WentDown: ConceptBrake")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptHandBrake) then
		print("WentDown: ConceptHandBrake")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptClutch) then
		print("WentDown: ConceptClutch")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptGearUp) then
		print("WentDown: ConceptGearUp")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptGearDown) then
		print("WentDown: ConceptGearDown")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptGearSwitch) then
		print("WentDown: ConceptGearSwitch")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptNextPosition) then
		print("WentDown: ConceptNextPosition")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSelectPosition1) then
		print("WentDown: ConceptSelectPosition1")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSelectPosition2) then
		print("WentDown: ConceptSelectPosition2")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSelectPosition3) then
		print("WentDown: ConceptSelectPosition3")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSelectPosition4) then
		print("WentDown: ConceptSelectPosition4")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSelectPosition5) then
		print("WentDown: ConceptSelectPosition5")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSelectPosition6) then
		print("WentDown: ConceptSelectPosition6")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSelectPosition7) then
		print("WentDown: ConceptSelectPosition7")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSelectPosition8) then
		print("WentDown: ConceptSelectPosition8")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptCameraPitch) then
		print("WentDown: ConceptCameraPitch")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptCameraYaw) then
		print("WentDown: ConceptCameraYaw")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptMapZoom) then
		print("WentDown: ConceptMapZoom")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptMapInnerZoom) then
		print("WentDown: ConceptMapInnerZoom")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptMapSize) then
		print("WentDown: ConceptMapSize")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptMapThreeDimensional) then
		print("WentDown: ConceptMapThreeDimensional")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptScoreboard) then
		print("WentDown: ConceptScoreboard")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptMenu) then
		print("WentDown: ConceptMenu")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSpawnMenu) then
		print("WentDown: ConceptSpawnMenu")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptCancel) then
		print("WentDown: ConceptCancel")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptCommMenu1) then
		print("WentDown: ConceptCommMenu1")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptCommMenu2) then
		print("WentDown: ConceptCommMenu2")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptCommMenu3) then
		print("WentDown: ConceptCommMenu3")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptAccept) then
		print("WentDown: ConceptAccept")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptDecline) then
		print("WentDown: ConceptDecline")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSelect) then
		print("WentDown: ConceptSelect")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptBack) then
		print("WentDown: ConceptBack")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptActivate) then
		print("WentDown: ConceptActivate")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptDeactivate) then
		print("WentDown: ConceptDeactivate")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptEdit) then
		print("WentDown: ConceptEdit")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptView) then
		print("WentDown: ConceptView")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptParentNavigateLeft) then
		print("WentDown: ConceptParentNavigateLeft")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptParentNavigateRight) then
		print("WentDown: ConceptParentNavigateRight")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptMenuZoomIn) then
		print("WentDown: ConceptMenuZoomIn")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptMenuZoomOut) then
		print("WentDown: ConceptMenuZoomOut")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptVoiceFunction1) then
		print("WentDown: ConceptVoiceFunction1")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSayAllChat) then
		print("WentDown: ConceptSayAllChat")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptTeamChat) then
		print("WentDown: ConceptTeamChat")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSquadChat) then
		print("WentDown: ConceptSquadChat")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptSquadLeaderChat) then
		print("WentDown: ConceptSquadLeaderChat")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptToggleChat) then
		print("WentDown: ConceptToggleChat")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptQuicktimeInteractDrag) then
		print("WentDown: ConceptQuicktimeInteractDrag")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptQuicktimeFire) then
		print("WentDown: ConceptQuicktimeFire")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptQuicktimeBlock) then
		print("WentDown: ConceptQuicktimeBlock")
	end
	if InputManager:WentDown(InputConceptIdentifiers.ConceptQuicktimeFastMelee) then
		print("WentDown: ConceptQuicktimeFastMelee")
	end

end


g_wentdowndebugClient = wentdowndebugClient()

