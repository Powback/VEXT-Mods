class "TestShared"

local m_OnGroundStateData_Guid = Guid("18B95C2B-5D12-4D1F-BA8C-0266A5240C04", "D")
local m_InAirStateData_Guid = Guid("584D7B54-FBFE-4755-8AD4-89065EEB45C3", "D")


function TestShared:__init()
	print("TestShared initializing")
	
	self:RegisterVars()
	self:RegisterEvents()
end

function TestShared:RegisterVars()

end

function TestShared:RegisterEvents()
	Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)

end

function TestShared:OnPartitionLoaded(p_Partition)

	local instances = p_Partition.instances

	for _, instance in pairs(instances) do
		if instance == nil then
			goto continue
		end

		if instance.instanceGuid == m_CharacterPhysicsData_Guid then
			print("found m_CharacterPhysicsData_Guid")
			goto continue
		end

		if instance.instanceGuid == m_InAirStateData_Guid then
			print("found m_InAirStateData_Guid")
			goto continue
		end
		::continue::
	end
end

return TestShared()