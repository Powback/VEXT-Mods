class 'lookaroundClient'


function lookaroundClient:__init()
	print("Initializing lookaroundClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function lookaroundClient:RegisterVars()
	self.camera = nil
	self.cameraData = nil
end


function lookaroundClient:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
	self.m_ClientUpdateInputEvent = Events:Subscribe('Client:UpdateInput', self, self.OnUpdateInput)
	Hooks:Install('ClientEntityFactory:Create',999, self, self.OnEntityCreate)

end



function lookaroundClient:OnUpdateInput(p_Hook, p_Cache, p_DeltaTime)
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F3) then
		local s_OriginalLocal = self.cameraData.localPose:get(127)
		s_OriginalLocal.trans.y = 20
		local s_OriginalModel = self.cameraData.modelPose:get(127)
		s_OriginalModel.trans.y = 20

		self.cameraData.localPose:set(127,s_OriginalLocal)
		self.cameraData.modelPose:set(127, s_OriginalModel)
	end
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F4) then
		if InputManager:WentKeyDown(InputDeviceKeys.IDK_F4) then
		    print(self.cameraData.isReadOnly) -- true
		end
	end
end

function lookaroundClient:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end


        if(l_Instance.instanceGuid == Guid("A9FFE6B4-257F-4FE8-A950-B323B50D2112")) then
            local s_Instance = l_Instance:Clone()
            self.cameraData = s_Instance
            print(self.cameraData.isReadOnly) -- false
            p_Partition:ReplaceInstance(l_Instance, s_Instance, true)
        end
	end
end

			

function lookaroundClient:OnEntityCreate(p_Hook, p_Data, p_Transform)
	if p_Data == nil then
		print("Didnt get no data")
		return
	end
	--print(p_Data.typeInfo.name)
	if(p_Data.typeInfo.name == "SoldierCameraComponentData") then
		--self.cameraData = SoldierCameraComponentData(p_Data)
		--self.camera = p_Hook:Call()
	end
end


g_lookaroundClient = lookaroundClient()

