class 'MECShared'


function MECShared:__init()
	print("Initializing MECShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function MECShared:RegisterVars()
	--self.m_this = that
end


function MECShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end


function MECShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeInfo.name == "TeamData") then
			local s_Instance = TeamData(l_Instance:Clone(l_Instance.instanceGuid))
			if(s_Instance.faction == 2) then
				print("Replaced faction: " .. tostring(s_Instance.faction))
				s_Instance.faction = 3
				p_Partition:ReplaceInstance(l_Instance, s_Instance, true)
			end
		end
	end
end


g_MECShared = MECShared()

