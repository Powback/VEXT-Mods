class 'RandomWeatherClient'

local SharedPatches = require '__shared/patches'
local Patches = require 'patches'

function RandomWeatherClient:__init()
	self:InitComponents()
	self:RegisterEvents()
	self:InstallHooks()


	self.outdoorLightComponentData =	  nil
	self.enlightenComponentData = 		  nil
	self.skyComponentData =				  nil
	self.characterLightingComponentData = nil
	self.colorCorrectionComponentData =   nil
	self.fogComponentData =				  nil
	self.tonemapComponentData =			  nil
	self.windComponentData = 			  nil

	self.outdoorLightComponentDataModified =	  false
	self.enlightenComponentDataModified = 		  false
	self.skyComponentDataModified =				  false
	self.characterLightingComponentDataModified = false
	self.colorCorrectionComponentDataModified =   false
	self.fogComponentDataModified =				  false
	self.tonemapComponentDataModified =			  false
	self.windComponentDataModified = 			  false

end

function RandomWeatherClient:RegisterEvents()
	self.m_LoadedEvent = Events:Subscribe('ExtensionLoaded', self, self.Loaded)
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end

function RandomWeatherClient:InstallHooks()

end
function RandomWeatherClient:OnReadInstance(p_Instance, p_Guid)

	if p_Instance.typeName == "OutdoorLightComponentData" then
		self.outdoorLightComponentData = OutdoorLightComponentData(p_Instance)
	end
	if p_Instance.typeName == "EnlightenComponentData" then
		self.enlightenComponentData = EnlightenComponentData(p_Instance)
	end


	if p_Instance.typeName == "SkyComponentData" then
		self.skyComponentData = SkyComponentData(p_Instance)
	end	


	if p_Instance.typeName == "CharacterLightingComponentData" then
		self.characterLightingComponentData = CharacterLightingComponentData(p_Instance)
	end

	if p_Instance.typeName == "ColorCorrectionComponentData" then
		self.colorCorrectionComponentData = ColorCorrectionComponentData(p_Instance)
	end

	if p_Instance.typeName == "FogComponentData" then
		self.fogComponentData = FogComponentData(p_Instance)
	end

	if p_Instance.typeName == "TonemapComponentData" then
		self.tonemapComponentData = TonemapComponentData(p_Instance)
	end
	if p_Instance.typeName == "WindComponentData" then
		self.windComponentData = WindComponentData(p_Instance)
	end

	if self.outdoorLightComponentData ~= nil and self.outdoorLightComponentDataModified == false then
		self.outdoorLightComponentData.enabled = false
		self.outdoorLightComponentDataModified = true
	end
end


function RandomWeatherClient:InitComponents()
	self.m_SharedPatches = SharedPatches()
	self.m_Patches = Patches()
end

function RandomWeatherClient:Loaded()
	self.m_SharedPatches:OnLoaded()
	self.m_Patches:OnLoaded()
end


g_RandomWeatherClient = RandomWeatherClient()

return RandomWeatherClient