class 'RandomWeatherServer'

local SharedPatches = require '__shared/patches'

function RandomWeatherServer:__init()
	self:InitComponents()
	self:RegisterEvents()
	self:InstallHooks()

end

function RandomWeatherServer:RegisterEvents()
	self.m_LoadedEvent = Events:Subscribe('ExtensionLoaded', self, self.Loaded)
end

function RandomWeatherServer:InstallHooks()

end

function RandomWeatherServer:InitComponents()
	self.m_SharedPatches = SharedPatches()
end

function RandomWeatherServer:Loaded()
	self.m_SharedPatches:OnLoaded()
end


g_RandomWeatherServer = RandomWeatherServer()