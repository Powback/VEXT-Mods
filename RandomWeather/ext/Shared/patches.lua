class 'SharedPatches'

local RandomWeather = require '__shared/RandomWeather'

function SharedPatches:__init()
	self:RegisterEvents()
	self:InitComponents()

	self.m_InvCol = Guid('8B71D173-8518-11E0-BB7A-DDA550615CAF', 'D')


	self.outdoorLightComponentData =	  nil
	self.enlightenComponentData = 		  nil
	self.skyComponentData =				  nil
	self.characterLightingComponentData = nil
	self.colorCorrectionComponentData =   nil
	self.fogComponentData =				  nil
	self.tonemapComponentData =			  nil
	self.windComponentData = 			  nil

	self.outdoorLightComponentDataModified =	  false
	self.enlightenComponentDataModified = 		  false
	self.skyComponentDataModified =				  false
	self.characterLightingComponentDataModified = false
	self.colorCorrectionComponentDataModified =   false
	self.fogComponentDataModified =				  false
	self.tonemapComponentDataModified =			  false
	self.windComponentDataModified = 			  false


end

function SharedPatches:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	self.m_VEStateAddedEvent = Events:Subscribe('VisualEnvironment:StateAdded', self, self.StateAdded)
end

function SharedPatches:InitComponents()
	self.m_RandomWeather = RandomWeather()
end

function SharedPatches:StateAdded(p_State)
	if p_State.entityName ~= 'EffectEntity' then
		self:FixEnvironmentState(p_State)
	end
end

function SharedPatches:ReadInstance(p_Instance, p_Guid)
	if p_Instance == nil then
		return 
	end



end



function SharedPatches:OnLoaded()
--	self.m_RandomWeather:OnLoaded()
-- Make sure all our VE states are fixed.


end

function FixEnvironmentState(p_State) 
	print(tostring(p_State))
end


return SharedPatches