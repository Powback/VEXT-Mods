class 'PromodServer'

local SharedPatches = require '__shared/patches'

function PromodServer:__init()
	self:InitComponents()
	self:RegisterEvents()
	self:InstallHooks()
	
end

function PromodServer:RegisterEvents()
	self.m_LoadedEvent = Events:Subscribe('ExtensionLoaded', self, self.Loaded)
end

function PromodServer:InstallHooks()

end

function PromodServer:InitComponents()
	self.m_SharedPatches = SharedPatches()
end

function PromodServer:Loaded()
	self.m_SharedPatches:OnLoaded()
	--NetEvents:Broadcast('promod:GetRandom', self.Random(min, max))
end

function PromodServer:Random(min,max)
	local rand = SharedUtils:GetRandom(min,max)
	--NetEvents:Broadcast('promod:GetRandom', rand)
end


g_PromodServer = PromodServer()

return PromodServer