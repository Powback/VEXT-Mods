class 'MP_001'
local nightmode = true
function MP_001:__init()
	self.m_StateAddedEvent = Events:Subscribe('VE:StateAdded', self, self.OnStateAdded)
	--NetEvents:Subscribe('promod:GetRandom', self, self.OnRandom)
	--self.CaspianSkyComponent = Guid('CA86D374-2F13-4F8B-B86A-7D1F0809971D', 'D')

end

function MP_001:OnStateAdded(p_State)
	-- Fix a newly added state (if needed).
	if p_State.entityName ~= 'EffectEntity' then
		self:FixEnvironmentState(p_State)
	end
end


function MP_001:OnLoaded()
	-- Make sure all our VE states are fixed.
	local s_States = VisualEnvironmentManager:GetStates()

	for i, s_State in ipairs(s_States) do
		if s_State.entityName ~= 'EffectEntity' then
			self:FixEnvironmentState(s_State)			
		end
	end
end


function MP_001:ReadInstance(p_Instance, p_Guid)
end


function MP_001:FixEnvironmentState(p_State)
	print('Fixing visual environment state ' .. p_State.entityName .. " for ffs")
	
	if nightmode == true then 
		local s_ColorCorrection = p_State.colorCorrection

		if s_ColorCorrection ~= nil then
			s_ColorCorrection.brightness = Vec3(1, 1, 1)
			--s_ColorCorrection.brightness = Vec3(1.0, 1.0, 1.0)
			s_ColorCorrection.contrast = Vec3(1.0, 1.0, 1.0)
			s_ColorCorrection.saturation = Vec3(0.8, 0.8, 0.8)
			s_ColorCorrection.colorGradingEnable = true
			s_ColorCorrection.enable = false
		end

		local s_Sky = p_State.sky

		if s_Sky ~= nil then
		    s_Sky.enable = true
		    s_Sky.brightnessScale = 0.03
		    s_Sky.sunSize = 0.004
		    s_Sky.sunScale = 5.0
		    s_Sky.panoramicUVMinX = 0.0
		    s_Sky.panoramicUVMaxX = 1.0
		    s_Sky.panoramicUVMinY = 0.0
		    s_Sky.panoramicUVMaxY = 0.7
		    s_Sky.panoramicTileFactor = 1.0
		    s_Sky.panoramicRotation = 0.578
		    s_Sky.cloudLayerMaskTexture = nil
		    s_Sky.cloudLayerSunColor = Vec3(1.0, 0.917, 0.53)
		    s_Sky.cloudLayer1Altitude = 400000.0
		    s_Sky.cloudLayer1TileFactor = 0.18
		    s_Sky.cloudLayer1Rotation = 30.194
		    s_Sky.cloudLayer1Speed = 0.003
		    s_Sky.cloudLayer1SunLightIntensity = -100
		    s_Sky.cloudLayer1SunLightPower = 0
		    s_Sky.cloudLayer1AmbientLightIntensity = 1
		    s_Sky.cloudLayer1Color = Vec3(1.0, 1.0, 1.0)
		    s_Sky.cloudLayer1AlphaMul = 0
		    s_Sky.cloudLayer1Texture = nil
		    s_Sky.cloudLayer2Altitude = 10000.0
		    s_Sky.cloudLayer2TileFactor = 0.25
		    s_Sky.cloudLayer2Rotation = 0.0
		    s_Sky.cloudLayer2Speed = 0.01
		    s_Sky.cloudLayer2SunLightIntensity = 4.0
		    s_Sky.cloudLayer2SunLightPower = 50.0
		    s_Sky.cloudLayer2AmbientLightIntensity = 0.2
		    s_Sky.cloudLayer2Color = Vec3(1.0, 1.0, 1.0)
		    s_Sky.cloudLayer2AlphaMul = 0
		    s_Sky.cloudLayer2Texture = nil
		    s_Sky.staticEnvmapScale = 0.2
		    s_Sky.skyEnvmap8BitTexScale = 0.25
		    s_Sky.customEnvmapScale = 1.0
		    s_Sky.customEnvmapAmbient = 0.0
		    s_Sky.skyVisibilityExponent = 1.0
		end

		local s_OutdoorLight = p_State.outdoorLight

		if s_OutdoorLight ~= nil then
		    s_OutdoorLight.enable = true
		    s_OutdoorLight.sunRotationX = 255.484
		    s_OutdoorLight.sunRotationY = 18.581
		    s_OutdoorLight.sunColor = Vec3(0.0, 0.0, 0.0)
		    s_OutdoorLight.skyColor = Vec3(0.0, 0.0, 0.0)
		    s_OutdoorLight.groundColor = Vec3(0.023, 0.027, 0.032)
		    s_OutdoorLight.skyLightAngleFactor = -0.134
		    s_OutdoorLight.sunSpecularScale = 0.0
		    s_OutdoorLight.skyEnvmapShadowScale = 1.0
		    s_OutdoorLight.sunShadowHeightScale = 0.298
		    s_OutdoorLight.cloudShadowEnable = false
		    s_OutdoorLight.cloudShadowSpeed = Vec2(30,30)
		    s_OutdoorLight.cloudShadowSize = 3000.0
		    s_OutdoorLight.cloudShadowCoverage = 0.446
		    s_OutdoorLight.cloudShadowExponent = 3.0
		    s_OutdoorLight.translucencyAmbient = 0.0
		    s_OutdoorLight.translucencyScale = 0.0
		    s_OutdoorLight.translucencyPower = 8.0
		    s_OutdoorLight.translucencyDistortion = 0.1
		end

		local s_CharacterLighting = p_State.characterLighting

		if s_CharacterLighting ~= nil then
			s_CharacterLighting.characterLightEnable = true
			s_CharacterLighting.firstPersonEnable = true
			s_CharacterLighting.lockToCameraDirection = true
			s_CharacterLighting.right = Vec3(-1,0,0)
			s_CharacterLighting.up = Vec3(-1,0,0)
			s_CharacterLighting.forward = Vec3(-1,0,0)
			s_CharacterLighting.transform = Vec3(-1,0,0)
		end


		local s_TonemapData = p_State.tonemap

		if s_TonemapData ~= nil then
			s_TonemapData.bloomScale = Vec3(0, 0, 0)
			s_TonemapData.minExposure =0.1
			s_TonemapData.middleGray = 0.4
			s_TonemapData.maxExposure = 1.5
		end


		local s_Enlighten = p_State.enlighten

		if s_Enlighten ~= nil then
		    s_Enlighten.enable = true
		    s_Enlighten.bounceScale = 1.0
		    s_Enlighten.sunScale = 0.75
		    s_Enlighten.terrainColor = Vec3(0.1, 0.1, 0.1)
		    s_Enlighten.cullDistance = -1.0
		    s_Enlighten.skyBoxEnable = true
		    s_Enlighten.skyBoxSkyColor = Vec3(0.01, 0.019, 0.032)
		    s_Enlighten.skyBoxGroundColor = Vec3(0.0, 0.0, 0.0)
		    s_Enlighten.skyBoxSunLightColor = Vec3(1.0, 0.759, 0.476)
		    s_Enlighten.skyBoxSunLightColorSize = 1.0
		    s_Enlighten.skyBoxBackLightColor = Vec3(0.0, 0.0, 0.0)
		    s_Enlighten.skyBoxBackLightColorSize = 0.0
		    s_Enlighten.skyBoxBackLightRotationX = 0.0
		    s_Enlighten.skyBoxBackLightRotationY = 60.0

		end

		local s_Vignette = p_State.vignette

		if s_Vignette ~= nil then
			s_Vignette.enable = false
		end

		local s_Fog = p_State.fog

		if s_Fog ~= nil then
		    s_Fog.enable = true
		    s_Fog.fogDistanceMultiplier = 1.0
		    s_Fog.fogGradientEnable = true
		    s_Fog.start = -10.0
		    s_Fog['end'] = 200.0
		    s_Fog.curve = Vec4(3.4986055, -4.841408, 3.4623053, -0.1758487)
		    s_Fog.fogColorEnable = false
		    s_Fog.fogColor = Vec3(0.036, 0.264, 0.374)
		    s_Fog.fogColorStart = -20.0
		    s_Fog.fogColorEnd = 200.0
		    s_Fog.fogColorCurve = Vec4(1.90640783, -2.1295185, 1.0955018, -0.083194144)
		    s_Fog.transparencyFadeStart = 100.0
		    s_Fog.transparencyFadeEnd = 200.0
		    s_Fog.transparencyFadeClamp = 0.5
		    s_Fog.heightFogEnable = true
		    s_Fog.heightFogFollowCamera = 0.0
		    s_Fog.heightFogAltitude = -10.0
		    s_Fog.heightFogDepth = 100.0
		    s_Fog.heightFogVisibilityRange = 100.0
		end

		local s_Wind = p_State.wind

		if s_Wind ~= nil then
			s_Wind.windStrength = SharedUtils:GetRandom(0,10)
		end


		local s_CameraParams = p_State.cameraParams

		if s_CameraParams ~= nil then
			s_CameraParams.viewDistance = 5000
			s_CameraParams.sunShadowmapViewDistance = 100
		end

		VisualEnvironmentManager.dirty = true
	end
	
end


function MP_001:OnReadInstance(p_Instance, p_Guid)
		if p_Instance.typeName == "SpotLightEntityData" then
			local s_Instance = SpotLightEntityData(p_Instance)
			s_Instance.visible = true
			--s_Instance.castShadowsEnable = true
			--s_Instance.specularEnable = true
		end	

		if p_Instance.typeName == "PointLightEntityData" then
			local s_Instance = PointLightEntityData(p_Instance)
			s_Instance.visible = true
			--s_Instance.castShadowsEnable = true
			--s_Instance.specularEnable = true
		end	

		if p_Guid == Guid('5FBA51D6-059F-4284-B5BB-6E20F145C064', 'D') then
			local s_Instance = SpotLightEntityData(p_Instance)
			s_Instance.radius = 100
			s_Instance.intensity = 100
			s_Instance.castShadowsEnable = true
		end
		if p_Guid == Guid('995E49EE-8914-4AFD-8EF5-59125CA8F9CD', 'D') then
			local s_Instance = SpotLightEntityData(p_Instance)
			s_Instance.radius = 100
			s_Instance.intensity = 20
			s_Instance.castShadowsEnable = true
			s_Instance.coneOuterAngle = 90
			s_Instance.frustumFov = 50
		end

		if p_Instance.typeName == "EmitterTemplateData" then
			local s_Instance = EmitterTemplateData(p_Instance)
			--s_Instance.actAsPointLight = true
			s_Instance.pointLightRadius = s_Instance.pointLightRadius * 5
			s_Instance.pointLightRandomIntensityMin = s_Instance.pointLightRandomIntensityMin * 2
			s_Instance.pointLightRandomIntensityMax = s_Instance.pointLightRandomIntensityMax * 5
		end
		if p_Instance.typeName == "PointLightEntityData" then
			local s_Instance = PointLightEntityData(p_Instance)
			--s_Instance.actAsPointLight = true
			s_Instance.radius = s_Instance.radius * 2
			s_Instance.intensity = s_Instance.intensity * 2
			s_Instance.intensity = s_Instance.width * 2
			s_Instance.visible = true
		end

		if p_Instance.typeName == "SoldierCameraComponentData" then
			local s_Instance = SoldierCameraComponentData(p_Instance)
			--s_Instance.actAsPointLight = true
			s_Instance.physicsControlled = false
		end



		

end


return MP_001