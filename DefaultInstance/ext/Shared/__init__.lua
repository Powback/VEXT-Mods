class 'DefaultInstanceShared'


function DefaultInstanceShared:__init()
	print("Initializing DefaultInstanceShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function DefaultInstanceShared:RegisterVars()
	--self.m_this = that
end


function DefaultInstanceShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end


function DefaultInstanceShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instance = ChassisComponentData()
	print(s_Instance.indexInBlueprint)
end


g_DefaultInstanceShared = DefaultInstanceShared()

