class 'roadtestClient'
local VUExtensions = require "VUExtensions"
local RotationHelper = require "RotationHelper"

function roadtestClient:__init()
	print("Initializing roadtestClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function roadtestClient:RegisterVars()
	self.m_Roads = {}
	self.m_Soldier = nil
	self.m_SoldierEntity = nil
	self.sizeMultiplier = 1
    self.m_RotationSpeedMultiplier = 200
    self.m_BoneIndex = 46
    self.m_multiplier = 1
    self.radian = 57.2958
end


function roadtestClient:RegisterEvents()
    self.m_UpdateEvent = Events:Subscribe("UpdateManager:Update", self, self.OnUpdateInput)

end

function roadtestClient:OnUpdateInput(delta, updatePass)
	if(updatePass ~= UpdatePass.UpdatePass_PreFrame) then
		return
	end
	print("a")
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F1) then
		print("b")

		self.cameraData = CameraEntityData()
		self.cameraData.fov = 90
		self.cameraData.transform = ClientUtils:GetCameraTransform()
		print("creating camera")
		local s_Entity = EntityManager:CreateClientEntity(self.cameraData, LinearTransform())

		if s_Entity == nil then
			print("Could not spawn camera")
			return
		end
		s_Entity:Init(Realm.Realm_Client, true)
		
		self.cameraData.fov = 90
		self.camera = s_Entity
	end
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F1) then
		self.m_multiplier = self.m_multiplier + 0.01
		print(self.m_multiplier)
	end
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F2) then
		self.m_multiplier = self.m_multiplier - 0.01
		print(self.m_multiplier)

	end
	if(self.camera ~= nil) then

		local p_Player = PlayerManager:GetLocalPlayer()

		if (p_Player == nil) then
			return
		end
		if( p_Player.soldier == nil) then
			return
		end
		
		if(self.camera ~= nil) then
			local s_Corpse = p_Player.soldier

			self.camera:FireEvent("TakeControl")
			
			local transformLocal = p_Player.soldier.ragdollComponent:GetLocalTransform(45)
			transformLocal.transAndScale.w = 0
			p_Player.soldier.ragdollComponent:SetLocalTransform(45, transformLocal)

			local transformQuat = p_Player.soldier.ragdollComponent:GetInterpolatedWorldTransform(46)
			transformQuat.transAndScale.w = 1

			local newQuat = Vec4(  0.500,0.500, -0.500, 0.500)
			transformQuat.rotation = Normalize(multiply(newQuat,transformQuat.rotation))
			local LT = transformQuat:ToLinearTransform()
			LT.trans.y = LT.trans.y + 0.05

			self.cameraData.transform = LT

			
			
		end
	end
end

function multiply(in1, in2)
	local Q1 = in1
	local Q2 = in2
	return Vec4( (Q2.w * Q1.x) + (Q2.x * Q1.w) + (Q2.y * Q1.z) - (Q2.z * Q1.y),
	   (Q2.w * Q1.y) - (Q2.x * Q1.z) + (Q2.y * Q1.w) + (Q2.z * Q1.x),
	   (Q2.w * Q1.z) + (Q2.x * Q1.y) - (Q2.y * Q1.x) + (Q2.z * Q1.w),
	   (Q2.w * Q1.w) - (Q2.x * Q1.x) - (Q2.y * Q1.y) - (Q2.z * Q1.z) )
end

function Normalize(quat)
	local n = quat.x * quat.x + quat.y * quat.y + quat.z * quat.z + quat.w * quat.w
	
	if n ~= 1 and n > 0 then
		n = 1 / math.sqrt(n)
		quat.x = quat.x * n
		quat.y = quat.y * n
		quat.z = quat.z * n
		quat.w = quat.w * n		
	end
	return quat
end
--[[

class QuatTransform
    {
    public:
        fb::Vec4 transAndScale;
        fb::Quat rotation;
    };

    ]]

g_roadtestClient = roadtestClient()

