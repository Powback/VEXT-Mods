function QuatToEuler(q1) 
    local heading, attitude, bank
    local test = q1.x*q1.y + q1.z*q1.w

    local sqx = q1.x*q1.x    
    local sqy = q1.y*q1.y
    local sqz = q1.z*q1.z
    heading = math.atan(2*q1.y*q1.w-2*q1.x*q1.z , 1 - 2*sqy - 2*sqz)
    attitude = math.asin(2*test)
    bank = math.atan(2*q1.x*q1.w-2*q1.y*q1.z , 1 - 2*sqx - 2*sqz)
    return Vec3(heading, attitude, bank)
end 

-- Conversion Euler to Quaternion
-- @param heading(yaw), attitude(roll), bank(pitch)
-- @returns: x,y,z,w
function EulerToQuat(heading, attitude, bank) 
    -- Assuming the angles are in radians.    
    local c1 = math.cos(heading/2)    
    local s1 = math.sin(heading/2)    
    local c2 = math.cos(attitude/2)    
    local s2 = math.sin(attitude/2)    
    local c3 = math.cos(bank/2)    
    local s3 = math.sin(bank/2)
    local c1c2 = c1*c2    
    local s1s2 = s1*s2
    w =c1c2*c3 - s1s2*s3
    x =c1c2*s3 + s1s2*c3
    y =s1*c2*c3 + c1*s2*s3
    z =c1*s2*c3 - s1*c2*s3
    return Vec4(x,y,z,w)
end