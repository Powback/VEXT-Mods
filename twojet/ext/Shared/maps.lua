class 'Maps'

function Maps:__init()
	Events:Subscribe('Partition:ReadInstance', self, self.OnReadInstance)
	Events:Subscribe('Level:LoadResources', self, self.OnLoadResources)

	self.ah1zg0 = Guid('5C397417-5A61-4FD6-BAFE-BF812448FCDC', 'D')
	self.ah1zi0 = nil
	self.ah1zg1 = Guid('51ABAB48-D535-42FB-9CEA-77A0AED9FDAE', 'D')
	self.ah1zi1 = nil
	self.ah1zg2 = Guid('A7282B58-C9F0-46BA-BA1D-E3A9D3CC2A53', 'D')
	self.ah1zi2 = nil
	self.ah1zg3 = Guid('3CEA855C-9DDA-4A42-B67B-6863CC471F95', 'D')
	self.ah1zi3 = nil
	self.ah1zg4 = Guid('7C9906C4-567F-49CE-8BF7-1B18AAEDA8E8', 'D')
	self.ah1zi4 = nil
	self.ah1zg5 = Guid('144853C9-B99B-447C-A416-E3436667ECED', 'D')
	self.ah1zi5 = nil
	self.ah1zg6 = Guid('11CF75DA-A978-40EA-88AF-EDCAE86BB128', 'D')
	self.ah1zi6 = nil
	self.ah1zg7 = Guid('BEE29FDD-EF22-4E3D-80B7-CA547CA18BCE', 'D')
	self.ah1zi7 = nil
	self.ah1zg8 = Guid('306C0EFF-170A-458A-A8F7-D5D2AC49E293', 'D')
	self.ah1zi8 = nil
	self.ah1zg9 = Guid('7232741F-6ECE-42EB-9ED6-E37A39F13A70', 'D')
	self.ah1zi9 = nil
	self.ah1zg10 = Guid('5BA6A269-D33F-4C5C-9F39-394AAB207404', 'D')
	self.ah1zi10 = nil
	self.ah1zg11 = Guid('DCA680A9-6D79-4F55-AE14-1A9350E1198F', 'D')
	self.ah1zi11 = nil
	self.ah1zg12 = Guid('839D67AC-F0AD-4318-819B-280E31899474', 'D')
	self.ah1zi12 = nil
	self.ah1zg13 = Guid('9A5E48B1-3D1E-487E-9E59-2389AD8A1416', 'D')
	self.ah1zi13 = nil
	self.ah1zg14 = Guid('970C84DA-939A-4A6F-BAA1-3E11650CD0C7', 'D')
	self.ah1zi14 = nil
	self.ah1zg15 = Guid('D8228CFE-58F5-4BAC-BD56-79ABF837C714', 'D')
	self.ah1zi15 = nil

	self.m_DriverEntryGuid = Guid('A2D67929-A6D3-4915-9629-A4388FFAD716', 'D')

	self.m_CameraGuid = Guid('674E02BF-381D-4B5F-B7F1-C2E83A2EDB40', 'D')

	self.m_Light01Guid = Guid('41E79FC3-587F-4AA7-A53C-D18C0126287D', 'D')
	self.m_Light02Guid = Guid('998485B8-867A-4D71-A5EF-8881E1F27CAF', 'D')
	self.m_Light03Guid = Guid('32F66B7D-5690-4D5F-BBC1-076C50A8F810', 'D')

	self.m_VehicleSpawnerGuid = Guid('CD4E0585-F433-4892-A378-028687D7D622', 'D')

	self.m_RegistryGUID = Guid('DE1B0E42-2FA7-AF09-04B7-263495B28631', 'D')
	self.m_MeshDatabaseGUID = Guid('4F866DDE-8BF7-9C0C-B347-F9B2059AAD92', 'D')

end

function Maps:OnLoadResources(p_Dedicated)
	SharedUtils:MountSuperBundle('SpChunks')
	SharedUtils:MountSuperBundle('Levels/SP_Jet/SP_Jet')

	SharedUtils:PrecacheBundle('levels/sp_jet/sp_jet')

	self.m_Enable = true
end

function Maps:OnReadInstance(p_Instance, p_GUID)
	if p_Instance == nil then
		return
	end

	if p_GUID == self.ah1zg0 then print('0') self.ah1zi0 = p_Instance end
	if p_GUID == self.ah1zg1 then print('1') self.ah1zi1 = p_Instance end
	if p_GUID == self.ah1zg2 then print('2') self.ah1zi2 = p_Instance end
	if p_GUID == self.ah1zg3 then print('3') self.ah1zi3 = p_Instance end
	if p_GUID == self.ah1zg4 then print('4') self.ah1zi4 = p_Instance end
	if p_GUID == self.ah1zg5 then print('5') self.ah1zi5 = p_Instance end
	if p_GUID == self.ah1zg6 then print('6') self.ah1zi6 = p_Instance end
	if p_GUID == self.ah1zg7 then print('7') self.ah1zi7 = p_Instance end
	if p_GUID == self.ah1zg8 then print('8') self.ah1zi8 = p_Instance end
	if p_GUID == self.ah1zg9 then print('9') self.ah1zi9 = p_Instance end
	if p_GUID == self.ah1zg10 then print('10') self.ah1zi10 = p_Instance end
	if p_GUID == self.ah1zg11 then print('11') self.ah1zi11 = p_Instance end
	if p_GUID == self.ah1zg12 then print('12') self.ah1zi12 = p_Instance end
	if p_GUID == self.ah1zg13 then print('13') self.ah1zi13 = p_Instance end
	if p_GUID == self.ah1zg14 then print('14') self.ah1zi14 = p_Instance end
	if p_GUID == self.ah1zg15 then print('15') self.ah1zi15 = p_Instance end

	if p_GUID == self.m_RegistryGUID then
		local s_Instance = RegistryContainer(p_Instance)

		s_Instance:AddEntityRegistry(self.ah1zi0)
		s_Instance:AddEntityRegistry(self.ah1zi1)
		s_Instance:AddEntityRegistry(self.ah1zi2)
		s_Instance:AddEntityRegistry(self.ah1zi3)
		s_Instance:AddEntityRegistry(self.ah1zi4)
		s_Instance:AddEntityRegistry(self.ah1zi5)
		s_Instance:AddEntityRegistry(self.ah1zi6)
		s_Instance:AddEntityRegistry(self.ah1zi7)
		s_Instance:AddEntityRegistry(self.ah1zi8)

		s_Instance:AddBlueprintRegistry(self.ah1zi9)
		s_Instance:AddBlueprintRegistry(self.ah1zi10)
		s_Instance:AddBlueprintRegistry(self.ah1zi11)
		s_Instance:AddBlueprintRegistry(self.ah1zi12)
		s_Instance:AddBlueprintRegistry(self.ah1zi13)
		s_Instance:AddBlueprintRegistry(self.ah1zi14)
		s_Instance:AddBlueprintRegistry(self.ah1zi15)
	end

	if p_GUID == self.m_DriverEntryGuid then
		local s_Instance = PlayerEntryComponentData(p_Instance)

		s_Instance.entryRadius = 5.0
		s_Instance.showSoldierInEntry = true
	end

	if p_GUID == self.m_CameraGuid then
		print('Got camera')
		local s_Instance = CameraComponentData(p_Instance)
		print('upda camera')

		-- s_Instance.transform = LinearTransform(
		-- 	Vec3(1, 0, 0),
		-- 	Vec3(0, 0.994, 0.108),
		-- 	Vec3(0, -0.108, 0.994),
		-- 	Vec3(0.0, 1.13, 0.96)
		-- )

		print('123 camera')
	end

	if p_GUID == self.m_VehicleSpawnerGuid then
		local s_Instance = VehicleSpawnReferenceObjectData(p_Instance)

		s_Instance.blueprint = Blueprint(self.ah1zi11)
	end

	if p_GUID == self.m_Light01Guid or
		p_GUID == self.m_Light02Guid or
		p_GUID == self.m_Light03Guid then
		local s_Instance = PointLightEntityData(p_Instance)

		s_Instance.visible = false
		s_Instance.intensity = 0
	end
end

return Maps