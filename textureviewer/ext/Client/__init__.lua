class 'textureviewerClient'


function textureviewerClient:__init()
	print("Initializing textureviewerClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function textureviewerClient:RegisterVars()
	self.m_Textures = {}
end


function textureviewerClient:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	self.m_OnLoadedEvent = Events:Subscribe('Extension:Loaded', self, self.OnLoaded)
	self.m_OnUpdateInputEvent = Events:Subscribe('Client:UpdateInput', self, self.OnUpdateInput)
	Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)

end

function textureviewerClient:OnUpdateInput(p_Delta)

	WebUI:EnableMouse()
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F) then
		print("faaack")
		for _,l_Texture in ipairs(self.m_Textures) do
			WebUI:ExecuteJS("AddImage(\""..l_Texture.."\")")
		end
	end
end
function textureviewerClient:OnLoaded()
	WebUI:Init()
	WebUI:Show()
	WebUI:BringToFront()
end

function textureviewerClient:OnPartitionLoaded(p_Partition)
    if p_Partition == nil then
        return
    end

    local s_Instances = p_Partition.instances

    for _, l_Instance in ipairs(s_Instances) do
        if l_Instance == nil then
            m_Logger:Write('Instance is null?')
            goto continue
        end
        if l_Instance.typeInfo.name == "TextureAsset" then
			local s_Instance = TextureAsset(l_Instance)
			print(s_Instance.name)
			table.insert(self.m_Textures, tostring(s_Instance.name))
		end
        ::continue::
    end
end

function textureviewerClient:ReadInstance(p_Instance,p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	
	if p_Instance.typeName == "TextureAsset" then
		local s_Instance = TextureAsset(p_Instance)

		table.insert(self.m_Textures, tostring(s_Instance.name))
	end
end



g_textureviewerClient = textureviewerClient()

