class 'DebugSettingsShared'


function DebugSettingsShared:__init()
	local s_Settings = ResourceManager:GetSettings("BFServerSettings")

	if s_Settings ~= nil then
		print("Got BFServerSettings")
		local s_ServerSettings = BFServerSettings(s_Settings)

		print(s_ServerSettings.roundMinPlayerCount)
		s_ServerSettings.roundMinPlayerCount = 1
		s_ServerSettings.gameSize = 100
		s_ServerSettings.gameMod = "RealityMod"
	else
		print("Could not get OnlineSettings")
	end

	print("Initializing DebugSettingsShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function DebugSettingsShared:RegisterVars()
	--self.m_this = that
end


function DebugSettingsShared:RegisterEvents()
	self.m_EngineMessageEvent = Events:Subscribe('Engine:Message', self, self.OnEngineMessage)
end



function DebugSettingsShared:OnEngineMessage(p_Message) 
	if p_Message.type == MessageType.ClientLevelLoadedMessage or
		p_Message.type == MessageType.ServerLevelLoadedMessage then 
		self.ApplySettings()
	end
end


function DebugSettingsShared:ApplySettings()
	local s_Settings = ResourceManager:GetSettings("ServerSettings")

	if s_Settings ~= nil then
		print("Got ServerSettings")
		local s_ServerSettings = ServerSettings(s_Settings)

		print(s_ServerSettings.playerCountNeededForMultiplayer)
		s_ServerSettings.playerCountNeededForMultiplayer = 1
	else
		print("Could not get OnlineSettings")
	end


end
g_DebugSettingsShared = DebugSettingsShared()

