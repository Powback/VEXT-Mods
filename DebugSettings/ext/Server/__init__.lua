class 'DebugSettingsServer'


function DebugSettingsServer:__init()
	print("Initializing DebugSettingsServer")
	self:RegisterVars()
	self:RegisterEvents()
end


function DebugSettingsServer:RegisterVars()
	--self.m_this = that
end


function DebugSettingsServer:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end


function DebugSettingsServer:ReadInstance(p_Instance,p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	
	if p_Instance.typeName == "SomeType" then
		local s_Instance = SomeType(p_Instance)
		s_Instance.someThing = true
	end
	
	
	if p_Guid == Guid("Some-Guid", "D") then
		local s_Instance = SomeClass(p_Instance)
		s_Instance.someThing = true
	end
end


g_DebugSettingsServer = DebugSettingsServer()

