class 'patches'


function patches:__init()
	self:RegisterEvents()
	self.sizeMultiplier = 0.5
	self.PickupEntity = nil
	self.pda = nil
	self.pdawd = nil
	self.pdacustomization = nil
	self.pdacustomizationTable = nil
	self.pdacustomizationunlockparts = nil
	self:ConfigEntities()
	self.vehicleList = {}
	self.vehicleCount = 0
end

function patches:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	self.m_Whatever = Hooks:Install('ClientEntityFactory:Create', self, self.OnEntityCreate)
end

function patches:ConfigEntities()
	self.PickupEntity = ExplosionPackEntityData()

	self.PickupEntity.enabled = true
	self.PickupEntity.maxCount = 3
	self.PickupEntity.isAttachable = true
	self.PickupEntity.maxAttachableInclination = 45
	self.PickupEntity.health = 10
	self.PickupEntity.defuseRadius = 2.5

end


function patches:OnEntityCreate(p_Hook, p_Data, p_Transform)
	print(string.format('Creating entity type "%s" (%f, %f, %f)', p_Data.typeName, p_Transform.trans.x, p_Transform.trans.y, p_Transform.trans.z))

	return p_Hook:CallOriginal(p_Data, p_Transform)
end

function patches:ReadInstance(p_Instance, p_Guid)

	if p_Instance == nil then
		return
	end
	

end

g_patches = patches()

return patches