class 'removehavokShared'


function removehavokShared:__init()
	print("Initializing removehavokShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function removehavokShared:RegisterVars()
	--self.m_this = that
end


function removehavokShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end


function removehavokShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeInfo.name == "StaticModelGroupEntityData") then
			local s_Instance = StaticModelGroupEntityData(l_Instance)
			s_Instance:MakeWritable()
			s_Instance.memberDatas:clear()
		end
	end
end


g_removehavokShared = removehavokShared()

