class 'LoadingInfoShared'


function LoadingInfoShared:__init()
	print("Initializing LoadingInfoShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function LoadingInfoShared:RegisterVars()
	--self.m_this = that
end


function LoadingInfoShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Level:LoadingInfo', self, self.OnLoadingInfo)
end


function LoadingInfoShared:OnLoadingInfo(p_Info)
	print(p_Info)
end


g_LoadingInfoShared = LoadingInfoShared()

