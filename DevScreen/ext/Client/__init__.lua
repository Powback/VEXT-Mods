class 'DevScreenClient'


function DevScreenClient:__init()
	print("Initializing DevScreenClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function DevScreenClient:RegisterVars()
	self.m_Screens = {}
end


function DevScreenClient:RegisterEvents()
	self.m_LevelLoadEvent = Events:Subscribe("Level:LoadResources", self, self.OnLoadResources)

	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	self.m_UpdateEvent = Events:Subscribe("Engine:Update", self, self.OnEngineUpdate)
	Hooks:Install('UI:PushScreen', 999, self, self.OnPushScreen)
	Hooks:Install('ResourceManager:LoadBundle',999, self, self.OnLoadBundle)
end


function DevScreenClient:OnLoadBundle(p_Hook, p_Bundle)
	print(string.format("Loading bundle '%s'", p_Bundle))
end

function DevScreenClient:OnLoadResources(p_Dedicated)
	print("OnLoadResources")

	ResourceManager:MountSuperBundle("Levels/SP_Tank/SP_Tank")
	self.m_loadHandle = ResourceManager:BeginLoadData(3, 
						{
							'levels/sp_tank/sp_tank_uiplaying',
						})
end

function DevScreenClient:OnEngineUpdate(p_Delta, p_SimDelta)
	if self.m_loadHandle == nil then 
		return 
	end
	
	if not ResourceManager:PollBundleOperation(self.m_loadHandle) then 
		return 
	end
	
	if not ResourceManager:EndLoadData(self.m_loadHandle) then
		print("Bundles failed to load")
		return
	end
	
	self.m_loadHandle = nil
	
end

function DevScreenClient:OnPushScreen(p_Hook, p_Screen, p_GraphPriority, p_ParentGraph)
	if p_Screen == nil then
	    return
	end
	local s_Screen = UIGraphAsset(p_Screen)
	print("Pushed: " .. s_Screen.name)
	if(s_Screen.name == "UI/Flow/Screen/Scoreboards/ScoreboardTwoTeamsHUD64Screen") then
		p_Hook:Pass(self.m_Screens['UI/Flow/Screen/SpectatorScreen'], p_GraphPriority, p_ParentGraph)
	end
	if(s_Screen.name == "UI/Flow/Screen/Weapon/CrosshairDefault") then
		p_Hook:Pass(self.m_Screens['UI/Flow/Screen/Weapon/DebugScreenCrosshairs'], p_GraphPriority, p_ParentGraph)
	end




	-- Debug settings:
 	local p_GameRenderSettings = ResourceManager:GetSettings("GameRenderSettings")
	if p_GameRenderSettings ~= nil then
		print("Got GameRenderSettings")
		local s_GameRenderSettings = GameRenderSettings(p_GameRenderSettings)
		s_GameRenderSettings.debugRenderServiceEnable = true
		s_GameRenderSettings.debugRendererEnable = true
	end
	  

	  self:SetSetting("DecalSettings", "DebugMemUsageEnable", true)
	  self:SetSetting("DecalSettings", "DebugWarningsEnable", true)




end

function DevScreenClient:SetSetting(p_SettingName, p_Field, p_Value)
	local p_Setting = ResourceManager:GetSettings(p_SettingName)
	if p_Setting ~= nil then
		print(p_SettingName)
		local s_Setting = _G[p_SettingName](p_Setting)
		s_Setting[firstToLower(p_Field)] = p_Value
	end
end
function DevScreenClient:ReadInstance(p_Instance, p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	if p_Instance.typeName == "UIScreenAsset" then
		local s_Instance = UIGraphAsset(p_Instance)
		print("Found: " .. s_Instance.name)
		self.m_Screens[s_Instance.name] = UIGraphAsset(p_Instance)

	end
	if p_Guid == Guid('12A8040C-01D8-495A-B3C1-66CE2F53567E', 'D') then
		print("Found devscreen")
		self.m_DevScreen = UIGraphAsset(p_Instance)
		print("Found Devscreen!")
	end

	if p_Instance.typeName == "LensFlareEntityData" then
		local s_Instance = LensFlareEntityData(p_Instance)
		s_Instance.debugDrawOccluder = true
	end 
	if p_Instance.typeName == "DamageEffectComponentData" then
		local s_Instance = DamageEffectComponentData(p_Instance)
		s_Instance.debugDamage = true
	end 
	--[[
	if p_Instance.typeName == "UIDynamicDataBinding" then
		local s_Instance = UIDynamicDataBinding(p_Instance)
		local s_NodeCount = s_Instance:GetBindingsCount()
 
	    for i = s_NodeCount - 1, 0, -1 do
	      local s_Binding = s_Instance:GetBindingsAt(i)
	      s_Binding.useDirectAccess = true
		end 
	end
		]]

end

function firstToLower(str)
	return (str:gsub("^%L", string.lower))
end

g_DevScreenClient = DevScreenClient()

return DevScreenClient
