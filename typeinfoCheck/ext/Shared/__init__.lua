class 'typeinfoCheckShared'


function typeinfoCheckShared:__init()
	print("Initializing typeinfoCheckShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function typeinfoCheckShared:RegisterVars()
	self.m_MisMatch = {}
end


function typeinfoCheckShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end


function typeinfoCheckShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeName ~= l_Instance.typeInfo.name) then
			if(self.m_MisMatch[l_Instance.typeName] == nil) then
				print("Mismatch: " .. l_Instance.typeName .. " - " ..  l_Instance.typeInfo.name)
				self.m_MisMatch[l_Instance.typeName] = true
			end
		end
	end
end


g_typeinfoCheckShared = typeinfoCheckShared()

