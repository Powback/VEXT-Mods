class 'SpMapsShared'


function SpMapsShared:__init()
	print("Initializing SpMapsShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function SpMapsShared:RegisterVars()
	self.toggle = 1
	self.m_DeathMatchSWGuid = Guid('6D9ACF85-14A3-4815-B33B-44BDE0D7CAFE', 'D')
	self.m_DeathMatchSW = nil
	self.m_TDMLogicGuid = Guid('7CF6DE9C-7BD3-406D-AFCC-BDF890A85462', 'D')
	self.m_TDMLogic = nil
end


function SpMapsShared:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	self.m_LevelLoadEvent = Events:Subscribe("Level:LoadResources", self, self.OnLoadResources)
	Hooks:Install('UI:PushScreen', 999, self, self.OnPushScreen)
	Hooks:Install('ResourceManager:LoadBundle',999, self, self.OnLoadBundle)
end

function SpMapsShared:OnPushScreen(p_Hook, p_Screen, p_GraphPriority, p_ParentGraph)
	if p_Screen == nil then
	    return
	end
	local s_Screen = UIGraphAsset(p_Screen)
	print("Pushed: " .. s_Screen.name)
	if string.find(s_Screen.name, "uiplaying") ~= nil then
		p_Hook:Pass(self.m_Screens['Levels/XP1_002/XP1_002_UiPlaying'], p_GraphPriority, p_ParentGraph)
	end
end

function SpMapsShared:OnLoadBundle(p_Hook, p_Bundle)
	print(string.format("Loading bundle '%s'", p_Bundle))
end

function SpMapsShared:OnLoadResources(p_Dedicated)
	print("OnLoadResources")

	ResourceManager:MountSuperBundle("MPChunks")
	ResourceManager:MountSuperBundle('Xp1Chunks')
	ResourceManager:MountSuperBundle('Levels/XP1_002/XP1_002')
	ResourceManager:BeginLoadData(3, 
						{ "Levels/XP1_002/XP1_002" })
end

function SpMapsShared:ReadInstance(p_Instance,p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	
	if p_Instance.typeName == "LevelDescriptionAsset" then
		local s_Instance = LevelDescriptionAsset(p_Instance)
		s_Instance.description.isMultiplayer = true
        s_Instance.description.isCoop = false
        s_Instance.description.isMenu = false

        local s_Category = LevelDescriptionInclusionCategory()
			s_Category.category = 'GameMode'
			s_Category:AddMode('TeamDeathMatch0')
			s_Category:AddMode('TeamDeathMatchC0')

			s_Instance:AddCategories(s_Category)

		s_Instance:ClearStartPoints()
	end

	if p_Instance.typeName == "LevelData" then
		local s_Instance = LevelData(p_Instance)
		if(s_Instance.name ~= "Levels/COOP_002/COOP_002") then
			return
		end
		s_Instance.levelDescription.isMultiplayer = true
        s_Instance.levelDescription.isCoop = false
        s_Instance.levelDescription.isMenu = false
        s_Instance:AddObjects(self.m_DeathMatchSW)
        print(s_Instance.name)
	end
	if p_Instance.typeName == "AlternateSpawnEntityData" then
		local s_Instance = AlternateSpawnEntityData(p_Instance)

		s_Instance.team = self.toggle

		if(self.toggle == 1) then
			self.toggle = 2
		else 
			self.toggle = 1
		end
	end


	if p_Instance.typeName == "CharacterSpawnReferenceObjectData" then
		local s_Instance = CharacterSpawnReferenceObjectData(p_Instance)
		s_Instance.playerType = 0
		s_Instance.team = self.toggle
		if(self.toggle == 1) then
			self.toggle = 2
		else 
			self.toggle = 1
		end
	end
 

	if p_Guid == Guid('71BBC497-0AAF-0F71-EDBA-BCFB40132769', 'D') then
		local s_Instance = LogicPrefabBlueprint(p_Instance)
		s_Instance:ClearEventConnections()
		s_Instance.descriptor = nil
        s_Instance.needNetworkId = false
        s_Instance.interfaceHasConnections = false
        s_Instance.alwaysCreateEntityBusClient = false
        s_Instance.alwaysCreateEntityBusServer = false
		s_Instance:ClearObjects() 
		print("fixed waiting")
	end

	if p_Guid == Guid('DEA8103E-0E5D-7423-8173-16D8CA63ABD3', 'D') then
		local s_Instance = SpatialPrefabBlueprint(p_Instance)
		s_Instance:ClearLinkConnections()
		s_Instance:ClearEventConnections()
		s_Instance.descriptor = nil
        s_Instance.needNetworkId = false
        s_Instance.interfaceHasConnections = false
        s_Instance.alwaysCreateEntityBusClient = false
        s_Instance.alwaysCreateEntityBusServer = false
		s_Instance:ClearObjects() 
		print("fixed tooltip")
	end

	if p_Guid == Guid('03669886-6735-42DE-1628-2F48A79F2197', 'D') then --Gamemode/coop
		local s_Instance = LogicPrefabBlueprint(p_Instance)
		print(s_Instance.name)
	end
	if p_Guid == Guid('5FA66B8C-BE0E-3758-7DE9-533EA42F5364', 'D') then -- Gamemode/Deathmatch
		local s_Instance = LogicPrefabBlueprint(p_Instance)
		self.DeathMatch = s_Instance
		print(s_Instance.name)
	end

	if p_Guid == Guid('953C9A17-F7D3-4A61-84E6-31745E66420F ', 'D') then-- Reference to Gamemode/coop
		local s_Instance = LogicReferenceObjectData(p_Instance)
		s_Instance.blueprint = self.DeathMatch
	end
end


g_SpMapsShared = SpMapsShared()

