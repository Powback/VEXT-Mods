class 'shoothouseClient'


function shoothouseClient:__init()
	print("Initializing shoothouseClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function shoothouseClient:RegisterVars()
	self.m_Screens = {}
end


function shoothouseClient:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
	self.m_LoadBundleHook = Hooks:Install('ResourceManager:LoadBundle', 99, self, self.OnLoadBundle)
	Events:Subscribe('Level:LoadResources', self, self.OnLoadResources)
	Hooks:Install('UI:PushScreen', 999, self, self.OnPushScreen)
end

function shoothouseClient:OnLoadBundle(p_Hook, p_Bundle)
	print(string.format("Loading bundle '%s'", p_Bundle))
end
function shoothouseClient:OnLoadResources(p_Hook)
	local s_Settings = GameSettings(ResourceManager:GetSettings("GameSettings"))
	print(s_Settings.level)
	s_Settings.defaultLayerInclusion = "GameMode=TutorialMP"
	self.m_loadHandle = ResourceManager:BeginLoadData(3, 
						{ 
"ui/flow/bundle/eorbundle",
"ui/flow/bundle/ingamebundlemp",
"ui/flow/bundle/ingamebundlesp",
"ui/flow/bundle/loadingbundlemp",
"ui/flow/bundle/loadingbundlesp",
"ui/flow/bundle/mainmenubundle",
"ui/flow/bundle/preeorbundle",
"ui/flow/bundle/staticbundle",
"ui/uicamometadata_bundle",
"ui/uigamemodemetadata_bundle",
"ui/uikititemmetadata_bundle",
"ui/uilevelmetadata_bundle",
"ui/uilicensemetadata_bundle",
"ui/uimetadata_bundle",
"ui/uimodmetadata_bundle",
"ui/uispecializationmetadata_bundle",
"ui/uivehicleaccessorymetadata_bundle",
"ui/uivehiclemetadata_bundle",
"ui/uivehicleweaponmetadata_bundle",
"ui/uiweaponaccessorymetadata_bundle",
"ui/uiweaponmetadata_bundle"


						})
end

function shoothouseClient:OnEngineUpdate(p_Delta, p_SimDelta)
	if self.m_loadHandle == nil then 
		return 
	end
	
	if not ResourceManager:PollBundleOperation(self.m_loadHandle) then 
		return 
	end
	
	self.m_loadHandle = nil
	
	if not ResourceManager:EndLoadData(self.m_loadHandle) then
		print("Bundles failed to load")
		return
	end
end

function shoothouseClient:OnPushScreen(p_Hook, p_Screen, p_GraphPriority, p_ParentGraph)
	if p_Screen == nil then
	    return
	end
	local s_Screen = UIGraphAsset(p_Screen)
	print("Pushed: " .. s_Screen.name)
	if(s_Screen.name == "UI/Flow/Screen/Scoreboards/ScoreboardTwoTeamsHUD64Screen") then
		p_Hook:Pass(self.m_Screens['UI/Flow/Screen/SpectatorScreen'], p_GraphPriority, p_ParentGraph)
	end
	if(s_Screen.name == "UI/Flow/Screen/Weapon/CrosshairDefault") then
		p_Hook:Pass(self.m_Screens['UI/Flow/Screen/Weapon/DebugScreenCrosshairs'], p_GraphPriority, p_ParentGraph)
	end	  
end

function shoothouseClient:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end

		if l_Instance.typeInfo.name == "UIScreenAsset" then
			local s_Instance = UIGraphAsset(l_Instance)
			print("Found: " .. s_Instance.name)
			self.m_Screens[s_Instance.name] = UIGraphAsset(l_Instance)
		end
	end
end


g_shoothouseClient = shoothouseClient()

