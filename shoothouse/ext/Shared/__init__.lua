class 'shoothouseShared'
local m_vuExtensions = require "__shared/VUExtensions"


function shoothouseShared:__init()
	print("Initializing shoothouseShared")
	self:RegisterVars()
	self:RegisterEvents()
	m_vuExtensions:Init()
end


function shoothouseShared:RegisterVars()

end


function shoothouseShared:RegisterEvents()

    Hooks:Install('ClientEntityFactory:Create', 99, self, self.OnEntityCreate)
    Hooks:Install('ServerEntityFactory:Create', 99, self, self.OnEntityCreate)

end



function shoothouseShared:OnEntityCreate(p_Hook, p_Data, p_Transform)
	if(p_Data == nil) then
		return
	end
	if(p_Data.typeInfo.name ~= nil) then
		local dataName = p_Data.typeInfo.name 
		local entity = p_Hook:Call()
		local entityName = entity.typeName
		print(dataName .. " |" .. entityName .. " | " .. tostring(p_Data.instanceGuid))
	else
		print(tostring(p_Data.instanceGuid))
	end
	
end
g_shoothouseShared = shoothouseShared()

