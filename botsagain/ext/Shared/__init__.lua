class 'botsagainShared'


function botsagainShared:__init()
	print("Initializing botsagainShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function botsagainShared:RegisterVars()
	self.m_AISystem = nil
	self.m_AITemplate = nil
	self.m_AIBlueprint = nil
end


function botsagainShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end


function botsagainShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.instanceGuid == Guid("BC1FBB1E-8C60-5EC8-79BD-1E809A07EEAB")) then
			self.m_AISystem = BFAISystem(l_Instance)
			print("AI System")
		end

		if(l_Instance.instanceGuid == Guid("0CCC3DE2-4188-42E4-B829-559588B34122")) then
			self.m_AITemplate = SoldierSpawnTemplateData(l_Instance)
			print("Spawn template")
		end

		if(l_Instance.instanceGuid == Guid("926C79D1-A70D-49CE-8F85-A790B887F817")) then
			self.m_AIBlueprint = SoldierBlueprint(l_Instance)
			print("AI blueprint")
		end

		if(l_Instance.typeInfo.name == "LevelData" and self.m_AISystem ~= nil) then
			local s_Instance = LevelData(l_Instance)
			s_Instance:MakeWritable()
			s_Instance.aiSystem = self.m_AISystem
			print("LevelData")
		end
		
		if(l_Instance.typeInfo.name == "AutoTeamEntityData") then
			local s_Instance = AutoTeamEntityData(l_Instance)
			s_Instance:MakeWritable()
			s_Instance.teamAssignMode = TeamAssignMode.TamFullTeams 
			s_Instance.teamDifferenceToAutoBalance = 32
			s_Instance.playerCountNeededToAutoBalance = 30
			s_Instance.forceIntoSquad = false 
			print("AutoTeam")
		end
		if(l_Instance.typeInfo.name == "CharacterSpawnReferenceObjectData" and self.m_AITemplate ~= nil and self.m_AIBlueprint ~= nil) then
			local s_Instance = CharacterSpawnReferenceObjectData(l_Instance)
			s_Instance:MakeWritable()
			s_Instance.playerType = PlayerSpawnType.PlayerSpawnType_Actor
			s_Instance.onlySendEventForHumanPlayers = false
			s_Instance.useAsSpawnPoint = true
			s_Instance.maxCount = 2
			s_Instance.maxCountSimultaneously = 2
			s_Instance.tryToSpawnOutOfSight = false
			s_Instance.initialAutoSpawn = true
			s_Instance.blueprint = self.m_AIBlueprint
			s_Instance.template = self.m_AITemplate 
			print("CharacterSpawn")
		end

	end
end


g_botsagainShared = botsagainShared()

