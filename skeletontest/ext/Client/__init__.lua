class 'skeletontestClient'


function skeletontestClient:__init()
	print("Initializing skeletontestClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function skeletontestClient:RegisterVars()
	self.skeleton = nil
	self.lastTransform = {}
	self.lastTransform[0] = "nil";
	self.lastTransform[1] = "nil";
	self.animations = {}
	self.animations['AimLeftRight'] = 357098665
	self.animations['AimUpDown'] = 357083987
	self.animations['Crouch'] = 357046927
	self.animations['ForceSetTrajectory'] = 357073448
	self.animations['InAir'] = 357041647
	self.animations['Skydive'] = 357045245
	self.animations['Parachute'] = 357059959
	self.animations['Swim'] = 357040993
	self.animations['InputBackward'] = 357044387
	self.animations['InputForward'] = 357051999
	self.animations['InputLeft'] = 357092042
	self.animations['InputRight'] = 357075447
	self.animations['IsEnemy'] = 357079627
	self.animations['Jump'] = 357045613
	self.animations['LeanLeftRight'] = 357053698
	self.animations['Prone'] = 357069682
	self.animations['Sprint'] = 357060599
	self.animations['GroundSupported'] = 357089319
	self.animations['GroundNormal'] = 357081890
	self.animations['GroundDistance'] = 357089995
	self.animations['GroundAngleZ'] = 357097607
	self.animations['GroundAngleX'] = 357045180
	self.animations['GroundAngleFromNormal'] = 357053696
	self.animations['IsClientAnimatable'] = 357082739
	self.animations['CustomizationScreen'] = 357054394
	self.animations['Minimal3pServer'] = 357049534
	self.animations['VerticalImpact'] = 357066923
	self.animations['FalseSignal'] = 357083129
	self.animations['LockArmsToCameraWeight'] = 357071202

	self.animations['Deploy'] = 357077793
	self.animations['AltDeploy'] = 357076754
	self.animations['Undeploy'] = 357083403
	self.animations['QuickSwitch'] = 357096925
	self.animations['Reload'] = 357092327
	self.animations['ReloadShotgun'] = 357066644
	self.animations['Fire'] = 357051578
	self.animations['FireSingle'] = 357044710
	self.animations['FireHoldAndRelease'] = 357064983
	self.animations['FireSimple'] = 357056218
	self.animations['FirstShotSpawned'] = 357097442
	self.animations['BoltAction'] = 357047668
	self.animations['PumpAction'] = 357095324
	self.animations['MeleeAttack'] = 357086433
	self.animations['QuickThrow'] = 357043717
	self.animations['QuickThrowType'] = -1
	self.animations['AimBody'] = 357072729
	self.animations['AlwaysAimHead'] = -1
	self.animations['OneHanded'] = 357083832
	self.animations['OneHandedAiming'] = 357048639
	self.animations['AimingEnabled'] = -1
	self.animations['LowerGun'] = 357043803
	self.animations['BreathControl'] = -1
	self.animations['RflType'] = 357081278
	self.animations['PstlType'] = 357078541
	self.animations['HgrType'] = 357100269
	self.animations['ATType'] = 357064029
	self.animations['ShgType'] = 357051302
	self.animations['LMGType'] = 357053602
	self.animations['BagType'] = 357049810
	self.animations['SnpType'] = 357074139
	self.animations['Zoom'] = 357100466
	self.animations['AimBodyWeight'] = -1
	self.animations['DisableZoomToggleWeight'] = -1
	self.animations['ZoomParameter'] = 357088475
	self.animations['ZoomScaleFactor'] = 357047133
	self.animations['Dispersion'] = 357061088
	self.animations['AimTargetPosBody'] = 357096124
	self.animations['ZoomOutSpeed'] = 357097529
	self.animations['ZoomInSpeed'] = 357047133
	self.animations['UnDeploySpeed'] = 357056732
	self.animations['DeploySpeed'] = 357070717
	self.animations['LightEnabled'] = -1
	self.animations['FireModeChanged'] = 357072482
	self.animations['AnimType'] = 357056105
	self.animations['GunDown'] = -1
	self.animations['NumberOfBulletsLeftInGun'] = 357056085
	self.animations['BulletsLeftInGun'] = 357078238
	self.animations['AbortVehicleDeploy'] = 357056401
	self.animations['CurrentProjectileIndex'] = 357103016
	self.animations['WeaponActionESIG'] = 357054436
	self.animations['IsSprinting'] = -1
	self.animations['PreparingToBash'] = -1
	self.animations['JustStartedSprinting'] = -1
	self.animations['KickBackInIronSight'] = 357042233
	self.animations['ZoomingTime'] = -1
	self.animations['TriggerZoomGunTwitch'] = -1
	self.animations['WeaponChooserSignal'] = 357091049
	self.animations['WeaponClassSignal'] = 357079929
	self.animations['OffsetX'] = 357091029
	self.animations['OffsetY'] = 357045622
	self.animations['OffsetZ'] = 357062644
	self.animations['AIAllowFire'] = -1
	self.animations['AIAltFireFromAnt'] = -1

end


function skeletontestClient:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
    self.m_Whatever = Hooks:Install('ClientEntityFactory:Create',999, self, self.OnEntityCreate)
    self.m_Hook = Hooks:Install("Input:PreUpdate", 999, self, self.OnUpdateInput)
end

function skeletontestClient:OnUpdateInput()
	local p_VisualTerrain = ResourceManager:GetSettings("EntitySettings")
	if(p_VisualTerrain ~= nil) then
		local s_VisualTerrain = EntitySettings (p_VisualTerrain)
		s_VisualTerrain.spawnSubLevelsFromLogic = true


	else 
		print("Failed to get VisualTerrainSettings")
	end
	local p_TerrainSettings = ResourceManager:GetSettings("TerrainSettings")
	if(p_TerrainSettings ~= nil) then
		local s_TerrainSettings = TerrainSettings(p_TerrainSettings)
	s_TerrainSettings.modifierDepthFactor = 20
	s_TerrainSettings.modifierSlopeMax = 20


	else 
		print("Failed to get VisualTerrainSettings")
	end



	if(self.skeleton ~= nil) then
		
		local boneCollision = BoneCollisionComponentData(self.skeleton.data)
		local skeleton = boneCollision.skeletonCollisionData
		local asset = SkeletonAsset(skeleton.skeletonAsset)
		print(tostring(asset.localPose:get(8)))
		print(tostring(asset.modelPose:get(8))) 


		--[[
		local localPose = skeleton.localPose:get(10)
		local modelPose = skeleton.modelPose:get(10)
		if(self.lastTransform[0] ~= tostring(localPose)) then
			print("localPose transform changed")
			print("last: " .. self.lastTransform[0])
			self.lastTransform[0] = tostring(localPose);
			print("new : " .. self.lastTransform[0])
		end
		if(self.lastTransform[1] ~= tostring(modelPose)) then
			print("modelPose transform changed")
			print("last: " .. self.lastTransform[1])
			self.lastTransform[1] = tostring(modelPose);
			print("new : " .. self.lastTransform[1])
		end
		]]
		
	end
end
function skeletontestClient:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
	end
end

function IsSpatial(type) 
	if(type == nil or type.name == nil) then
		return false
	end
	if(type.name == "SpatialEntityData" or type.name == "ComponentData") then
		return true
	elseif type.name == "DataContainer" then
		return false
	else
		return(IsSpatial(type.super))
	end
	
end

function skeletontestClient:OnEntityCreate(p_Hook, p_Data, p_Transform)
	if p_Data == nil then
		print("Didnt get no data")
	else
		--print(p_Data.typeInfo.name)
		--[[if(p_Data.typeInfo.name == "BoneCollisionComponentData") then
			

			print("data :  " .. p_Data.typeInfo.name)
			local x = p_Hook:Call()
			print("entity: " ..x.typeName)
			p_Hook:Return(x)
			self.skeleton = x
		end
			--]]
	end	 
end


g_skeletontestClient = skeletontestClient()

