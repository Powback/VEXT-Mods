class 'ghostsoldierServer'


function ghostsoldierServer:__init()
	print("Initializing ghostsoldierServer")
	self:RegisterVars()
	self:RegisterEvents()
end


function ghostsoldierServer:RegisterVars()
	self.m_Soldier = nil
	self.m_Customization = nil
	self.m_Entities = nil
end


function ghostsoldierServer:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
	self.m_PlayerChat = Events:Subscribe('Player:Chat', self, self.PlayerChat)
end
function ghostsoldierServer:PlayerChat(p_Player, p_RecipientMask, p_Message)
	if p_Message == '' then
		return
	end
	print(tostring(p_Player))
	print(p_Player)

	if p_Message == 'a' then
		local transform = p_Player.soldier.transform

		local params = EntityCreationParams()
		params.transform = transform
		params.networked = true

		print("lego")
		local entities = EntityManager:CreateEntitiesFromBlueprint(self.m_Soldier, params)
		for i, entity in ipairs(entities) do
			print(i)
			entity:Init(Realm.Realm_ClientAndServer, true)
		end

		self.m_Entities = entities 
	end
	if p_Message == 'b' then
		local s_Soldier = SoldierEntity(entities[1])
		s_Soldier:ApplyCustomization(self.m_Customization)
	end
	if p_Message == 'c' then
		p_Player.soldier:ApplyCustomization(self.m_Customization)
	end
end

function ghostsoldierServer:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeInfo.name == "SoldierBlueprint") then
			print("Soldier")
			self.m_Soldier = SoldierBlueprint(l_Instance)
		end
		if(l_Instance.typeInfo.name == "CustomizeSoldierData") then
			print("SoldierCustomization")
			self.m_Customization = CustomizeSoldierData(l_Instance)
			print(l_Instance.instanceGuid)
			print(self.m_Customization.name)

		end

	end
end


g_ghostsoldierServer = ghostsoldierServer()

