class 'PluginTemplateServer'

local SharedPatches = require '__shared/patches'

function PluginTemplateServer:__init()
	self:InitComponents()
	self:RegisterEvents()
end

function PluginTemplateServer:RegisterEvents()
	self.m_LoadedEvent = Events:Subscribe('ExtensionLoaded', self, self.Loaded)
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end

function PluginTemplateServer:InitComponents()
	self.m_SharedPatches = SharedPatches()
end

function PluginTemplateServer:Loaded()
--	self.m_SharedPatches:OnLoaded()
end

function PluginTemplateServer:ReadInstance(p_Instance, p_Guid)

end

g_PluginTemplateServer = PluginTemplateServer()

return PluginTemplateServer