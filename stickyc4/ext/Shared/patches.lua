class 'ShadedPatches'

function ShadedPatches:__init()
	self:InitComponents()
	self:RegisterEvents()
end

function ShadedPatches:RegisterEvents()
	self.m_LoadedEvent = Events:Subscribe('ExtensionLoaded', self, self.Loaded)
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end

function ShadedPatches:InitComponents()

end

function ShadedPatches:Loaded()

end

function ShadedPatches:ReadInstance(p_Instance, p_Guid)
	if p_Instance == nil then
		return
	end

	if p_Instance.type == "VehicleEntityData" then
		local s_Instance = VehicleEntityData(p_Instance)
		s_Instance.explosionPacksAttachable = true
	end
	if p_Instance.type == "SupplySphereEntityData" then
		local s_Instance = SupplySphereEntityData(p_Instance)
		s_Instance.maxAttachableInclination = -1
	end



	

		

end

g_ShadedPatches = ShadedPatches()

return ShadedPatches