class 'DataCrashClient'


function DataCrashClient:__init()
	print("Initializing DataCrashClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function DataCrashClient:RegisterVars()
	self.m_Ents = {}
	self.int = 0
end


function DataCrashClient:RegisterEvents()
	self.m_StateAddedEvent = Events:Subscribe('VE:StateAdded', self, self.CallCrash)
	self.m_StateRemovedEvent = Events:Subscribe('VE:StateRemoved', self, self.CallCrash)
	self.m_OnLoadedEvent = Events:Subscribe('ExtensionLoaded', self, self.CallCrash)
end


function DataCrashClient:CallCrash()
	local s_States = VisualEnvironmentManager:GetStates()

	for i, s_State in ipairs(s_States) do
		if s_State.entityName ~= 'EffectEntity' then
			if(s_State.entityName == "Levels/Web_Loading/Lighting/Web_Loading_VE") then
			    return
			end
			self:Crash()
		end
	end
end
function DataCrashClient:Crash()

	self.int = self.int +1 
	print("Creating LensScopeData")
	self.m_Ents[self.int] = LensScopeData()
	print("Created LensScopeData")

	self.int = self.int +1 
	print("Creating CameraParamsData")
	self.m_Ents[self.int] = CameraParamsData()
	print("Created CameraParamsData")

	self.int = self.int +1 
	print("Creating FilmGrainData")
	self.m_Ents[self.int] = FilmGrainData()
	print("Created FilmGrainData")

end

g_DataCrashClient = DataCrashClient()

