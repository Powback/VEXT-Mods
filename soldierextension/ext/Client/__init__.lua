class 'soldierextensionClient'


function soldierextensionClient:__init()
	print("Initializing soldierextensionClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function soldierextensionClient:RegisterVars()
	self.m_WeaponUp = false
end


function soldierextensionClient:RegisterEvents()
	Events:Subscribe('Client:UpdateInput', self, self.OnUpdateInput)
end
function soldierextensionClient:OnUpdateInput(p_Delta, p_SimulationDelta)

	if InputManager:WentKeyDown(InputDeviceKeys.IDK_V) then
		self:ToggleWeapon()
	end
end

function soldierextensionClient:ToggleWeapon()
	local s_LocalPlayer = PlayerManager:GetLocalPlayer()
	if(s_LocalPlayer == nil) then
		return
	end
	if(self.m_WeaponUp == true) then
		self.m_WeaponUp = false
	else
		self.m_WeaponUp = true
	end
	s_LocalPlayer:EnableInput(EntryInputActionEnum.EIAFire, self.m_WeaponUp)

end
g_soldierextensionClient = soldierextensionClient()

