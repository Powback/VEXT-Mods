class 'CreateFieldCacheShared'


function CreateFieldCacheShared:__init()
	print("Initializing CreateFieldCacheShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function CreateFieldCacheShared:RegisterVars()
	--self.m_this = that
end


function CreateFieldCacheShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('DataBus:CreateFieldCache', self, self.OnCreateFieldCache)
end


function CreateFieldCacheShared:OnCreateFieldCache(arena, cacheDatas, parent, parentRepresentative, connector)
	local s_Connector = PropertyConnector(connector)
	print(tostring(s_Connector.data))
	print(tostring(s_Connector.data.typeInfo.name))
	print(tostring(s_Connector.data.instanceGuid))
end



g_CreateFieldCacheShared = CreateFieldCacheShared()

