class 'DoAFlipServer'


function DoAFlipServer:__init()
	print("Initializing DoAFlipServer")
	self:RegisterVars()
	self:RegisterEvents()
end


function DoAFlipServer:RegisterVars()
	--local m_this = that
end


function DoAFlipServer:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end


function DoAFlipServer:ReadInstance(p_Instance, p_Guid)
	if p_Instance == nil then
		return
	end
	
	if p_Instance.typeName == "SomeType" then
		local s_Instance = SomeType(p_Instance)
		s_Instance.someThing = true
	end
	
	
	if p_Guid == Guid("WHATEVERJUSTSOMEGUIDTHATISDASHED", "D") then
		local s_Instance = SomeClass(p_Instance)
		s_Instance.someThing = true
	end
end


g_DoAFlipServer = DoAFlipServer()

return DoAFlipServer
