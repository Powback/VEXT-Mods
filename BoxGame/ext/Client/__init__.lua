class 'BoxGameClient'

local m_BoxInstanceGuid = Guid("071572A1-FC46-308E-A495-6EE575154438")
local MAX_CAST_DISTANCE = 1

function BoxGameClient:__init()
	print("Initializing BoxGameClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function BoxGameClient:RegisterVars()
	self.m_PendingRaycast = false
	self.m_LevelLoaded = false
end


function BoxGameClient:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
	-- Hooks:Install('ClientEntityFactory:CreateFromBlueprint', 500, self, self.OnEntityCreateFromBlueprint)
	-- Events:Subscribe('Client:LevelLoaded', self, self.OnLevelLoaded)
end



function BoxGameClient:OnLevelLoaded(p_Map, p_GameMode, p_Round)
	self.m_LevelLoaded = true
end


function BoxGameClient:OnEntityCreateFromBlueprint(p_Hook, p_Blueprint, p_Transform, p_Variation, p_Parent)
	if p_Blueprint.instanceGuid ~= m_BoxInstanceGuid or not self.m_LevelLoaded then
		p_Hook:Call()
		return
	end

	local entities = p_Hook:Call()

	if entities == nil then
		return
	end


	for k,entity in pairs(entities) do

		if entity:Is('ServerPhysicsEntity') then
			entity = PhysicsEntity(entity)

			-- print(entity.internalHealth)

			-- entity:SetActiveHealthState(HealthStateAction.OnHealthy)
			-- entity.internalHealth = 100
			-- print(entity.internalHealth)

			entity:RegisterDamageCallback(function(entity, damageInfo, damageGiverInfo)
				print("ouch")
			end)
			entity:RegisterCollisionCallback(function(entity, info)
				print("collisioned")
			end)
		end
	end
	p_Hook:Return(entity)
end

local function onEntityDamage(entity, damageInfo, damageGiverInfo)
	print("ouchhhh")
end

function BoxGameClient:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?') goto continue
		end
	end
	::continue::
end


g_BoxGameClient = BoxGameClient()

