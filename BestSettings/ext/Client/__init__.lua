class 'BestSettingsClient'


function BestSettingsClient:__init()
	print("Initializing BestSettingsClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function BestSettingsClient:RegisterVars()
	--local m_this = that
end


function BestSettingsClient:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end


function BestSettingsClient:ReadInstance(p_Instance, p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	
	if p_Instance.typeName == "CameraData" then
		local s_Instance = CameraData(p_Instance)
		--s_Instance.shadowViewDistanceScale = 1000000
	end

	if p_Instance.typeName == "MeshLodGroup" then
		local s_Instance = MeshLodGroup(p_Instance)
		s_Instance.lod1Distance = 100000
	    s_Instance.lod2Distance = 200000.0
	    s_Instance.lod3Distance = 400000.0
	    s_Instance.lod4Distance = 70000.0
	    s_Instance.lod5Distance = 100000.0
	    s_Instance.shadowDistance = -1
	 
	end
	
	if string.match(p_Instance.typeName, "CameraData") then
		local s_Instance = CameraData(p_Instance)
	end 

	if p_Instance.typeName == "CameraParamsComponentData" then
		local s_Instance = CameraParamsComponentData(p_Instance)

	end

	if p_Instance.typeName == "MaterialRelationTerrainDestructionData" then
		local s_Instance = MaterialRelationTerrainDestructionData(p_Instance)
		s_Instance.depth = 10000000
	end
end


g_BestSettingsClient = BestSettingsClient()

return BestSettingsClient
