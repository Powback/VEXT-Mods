class 'CommonPatch'

function CommonPatch:__init()
	self:RegisterEvents()
	self:RegisterVariables()
end

function CommonPatch:RegisterVariables()
  self.m_SkinnedMeshAssets = {}
  self.m_ObjectVariation = {}
  self.m_UnlockAssets = {}
  
  self.m_SocketDataList = {}
  self.m_SPRegistryContainer = nil
  self.m_sockets = 0
  self.m_loadHandle = 0
  self.m_RegistryContainer = nil
end

function CommonPatch:RegisterEvents()
	self.m_LevelLoadEvent = Events:Subscribe("Level:LoadResources", self, self.OnLoadResources)
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	self.m_EngineMessageEvent = Events:Subscribe('Engine:Message', self, self.OnEngineMessage)
	self.m_UpdateEvent = Events:Subscribe("Engine:Update", self, self.OnEngineUpdate)
end

function CommonPatch:OnLoadResources(p_Dedicated)
	print("OnLoadResources")
	self:RegisterVariables()
  
	ResourceManager:MountSuperBundle("Levels/COOP_003/COOP_003")
	self.m_loadHandle = ResourceManager:BeginLoadData(3, 
						{ "Levels/COOP_003/COOP_003", 
						"Levels/COOP_003/AB00_Parent" })
end

function CommonPatch:OnEngineUpdate(p_Delta, p_SimDelta)
	if self.m_loadHandle == nil then 
		return 
	end
	
	if not ResourceManager:PollBundleOperation(self.m_loadHandle) then 
		return 
	end
	
	self.m_loadHandle = nil
	
	if not ResourceManager:EndLoadData(self.m_loadHandle) then
		print("Bundles failed to load")
		return
	end
end

function CommonPatch:ReadInstance(p_Instance, p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end

	if p_Instance.typeName == "SkinnedMeshAsset" then
		local s_Instance = SkinnedMeshAsset(p_Instance)
		self.m_SkinnedMeshAssets[s_Instance.name] = s_Instance
	end

	if p_Instance.typeName == "ObjectVariation" then
		local s_Instance = ObjectVariation(p_Instance)
		self.m_ObjectVariation[s_Instance.name] = s_Instance
	end

	if p_Instance.typeName == "UnlockAsset" then
		local s_Instance = UnlockAsset(p_Instance)
		self.m_UnlockAssets[s_Instance.name] = s_Instance
	end
  
    if p_Instance.typeName == "SocketData" then
		local socket = SocketData(p_Instance)
		
		if p_Guid == Guid("CF40A87A-EE7B-4361-B8E7-565D9A2F8367", 'D') then
		  self.m_SocketDataList["SPHead"] = socket
		elseif p_Guid == Guid("C7215881-BB46-4CFC-9FB1-25A2A29CF046", 'D') then
		  self.m_SocketDataList["SPUpperbody"] = socket
		elseif p_Guid == Guid("A5E05A6A-0E90-4A1D-B2CE-B8E3C02AF349", 'D') then
		  self.m_SocketDataList["SPLowerbody"] = socket
		elseif p_Guid == Guid("A3F7A3CF-17B1-4EEF-BA38-40AF51531A14", 'D') then
		  self.m_SocketDataList["SPHeadGear"] = socket
		end
		
		-- Headgear SocketData
		if p_Guid == Guid("AD30FF47-0DF6-41E0-84B7-19BEFD3EC97E", 'D') then
		  if self.m_SocketDataList["SPHeadGear"] == nil then return end
		  local spSocket = SocketData(self.m_SocketDataList["SPHeadGear"])
		
		  local count = 0
		  for i = 0, spSocket:GetAvailableObjectsCount() - 1, 1
		  do
			socket:AddAvailableObjects(spSocket:GetAvailableObjectsAt(i))
			count = count + 1
		  end
		  print("Merged HeadGear SocketData count " .. tostring(count))
		end

		-- Head SocketData
		if p_Guid == Guid("1FDD59DD-A162-4771-9FF3-79E895D80514", 'D') then
		  if self.m_SocketDataList["SPHead"] == nil then return end
		  local spSocket = SocketData(self.m_SocketDataList["SPHead"])
		
		  local count = 0
		  for i = 0, spSocket:GetAvailableObjectsCount() - 1, 1
		  do
			socket:AddAvailableObjects(spSocket:GetAvailableObjectsAt(i))
			count = count + 1
		  end
		  print("Merged Head SocketData count " .. tostring(count))
		end
		-- Upperbody SocketData
		if p_Guid:ToString('D') == "23CC2C99-5116-4553-844C-6424062D2CF9" then
		  print("Found upperbody socketdata")
		  if self.m_SocketDataList["SPUpperbody"] == nil then return end
		  local spSocket = SocketData(self.m_SocketDataList["SPUpperbody"])
		
		  local count = 0
		  for i = 0, spSocket:GetAvailableObjectsCount() - 1, 1
		  do
			socket:AddAvailableObjects(spSocket:GetAvailableObjectsAt(i))
			count = count + 1
		  end
		  print("Merged Upperbody SocketData count " .. tostring(count))
		end
		-- Lowerbody SocketData
		if p_Guid == Guid("B0C6E016-A10E-4B8F-A3AE-2DA8580F6263", 'D') then
		  if self.m_SocketDataList["SPLowerbody"] == nil then return end
		  local spSocket = SocketData(self.m_SocketDataList["SPLowerbody"])
		
		  local count = 0
		  for i = 0, spSocket:GetAvailableObjectsCount() - 1, 1
		  do
			socket:AddAvailableObjects(spSocket:GetAvailableObjectsAt(i))
			count = count + 1
		  end
		  print("Merged Lowerbody SocketData count " .. tostring(count))
		end
	end
	
	if p_Guid == Guid("D8CCDDDF-03EF-8341-95AB-B6458DEEE07D", 'D') then
		local s_Instance = RegistryContainer(p_Instance)
		if self.m_SPRegistryContainer == nil then return end
		print("SP Registry has been registered. Merging...")
		for i = 0, self.m_SPRegistryContainer:GetAssetRegistryCount() - 1, 1
		do
		  s_Instance:AddAssetRegistry(self.m_SPRegistryContainer:GetAssetRegistryAt(i))
		end
		print("Merged AssetRegistry")	
	end

	if p_Instance.typeName == "SubWorldData" then
		local s_Instance = SubWorldData(p_Instance)
		if self.m_RegistryContainer == nil then
			if s_Instance.name ~= "Levels/COOP_003/AB00_Parent" then
				--self.m_RegistryContainer = RegistryContainer(s_Instance.registryContainer)
				--print("RegistryContainer registered from " .. s_Instance.name)
			else
				self.m_SPRegistryContainer = RegistryContainer(s_Instance.registryContainer)
				print("SP RegistryContainer registered from " .. s_Instance.name)
			end
		end
	end
end

function CommonPatch:OnEngineMessage(p_Message)
	if p_Message == nil then
		return
	end

	if p_Message.type == MessageType.ClientLevelLoadedMessage
	or p_Message.type == MessageType.ServerLevelLoadedMessage
	then
		print("Level loaded")
		
		local MP_RU_Assault_Appearance01 = self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/MP/RU/MP_RU_Assault_Appearance01"]
		local MP_RU_Engi_Appearance01 = self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/MP/RU/MP_RU_Engi_Appearance01"]
		local MP_RU_Recon_Appearance01 = self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/MP/RU/MP_RU_Recon_Appearance01"]
		local MP_RU_Support_Appearance01 = self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/MP/RU/MP_RU_Support_Appearance01"]

		local MP_RU_Engi_UpperBody01 = self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/MP/RU/UpperBody/MP_RU_Engi_UpperBody01"]
		local Arms = BlueprintAndVariationPair(MP_RU_Engi_UpperBody01:GetLinkedToAt(1))

		MP_RU_Assault_Appearance01:ClearLinkedTo()
		MP_RU_Engi_Appearance01:ClearLinkedTo()
		MP_RU_Recon_Appearance01:ClearLinkedTo()
		MP_RU_Support_Appearance01:ClearLinkedTo()

		local PLR_Terror_Upperbody02 = self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/PLR/UpperBody/PLR_Terror_Upperbody02"]
		local PLR_Terror_Upperbody01_V1 = self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/PLR/UpperBody/PLR_Terror_Upperbody01_V1"]
		local PLR_Terror_Upperbody03_V1 = self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/PLR/UpperBody/PLR_Terror_Upperbody03_V1"]
		PLR_Terror_Upperbody02:AddLinkedTo(Arms)
		PLR_Terror_Upperbody01_V1:AddLinkedTo(Arms)
		PLR_Terror_Upperbody03_V1:AddLinkedTo(Arms)
		
		MP_RU_Assault_Appearance01:AddLinkedTo(self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/PLR/Headgear/PLR_Terror_Scarf01_V1"])
		MP_RU_Assault_Appearance01:AddLinkedTo(self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/PLR/Headgear/PLR_Terror_Balaclava01"])
		MP_RU_Assault_Appearance01:AddLinkedTo(PLR_Terror_Upperbody02)
		MP_RU_Assault_Appearance01:AddLinkedTo(self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/PLR/LowerBody/PLR_Terror_Lowerbody02"])

		MP_RU_Engi_Appearance01:AddLinkedTo(self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/PLR/Headgear/PLR_Terror_Scarf01_V1"])
		MP_RU_Engi_Appearance01:AddLinkedTo(self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/PLR/Headgear/PLR_Terror_Balaclava01_V1"])
		MP_RU_Engi_Appearance01:AddLinkedTo(PLR_Terror_Upperbody01_V1)
		MP_RU_Engi_Appearance01:AddLinkedTo(self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/PLR/LowerBody/PLR_Terror_Lowerbody02_V1"])

		MP_RU_Recon_Appearance01:AddLinkedTo(self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/PLR/Headgear/PLR_Terror_Scarf01_V1"])
		MP_RU_Recon_Appearance01:AddLinkedTo(self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/PLR/Headgear/PLR_Terror_Balaclava01"])
		MP_RU_Recon_Appearance01:AddLinkedTo(PLR_Terror_Upperbody02)
		MP_RU_Recon_Appearance01:AddLinkedTo(self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/PLR/LowerBody/PLR_Terror_Lowerbody01"])

		MP_RU_Support_Appearance01:AddLinkedTo(self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/PLR/Headgear/PLR_Terror_Scarf01_V1"])
		MP_RU_Support_Appearance01:AddLinkedTo(self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/Heads/Head01_Enemy"])
		MP_RU_Support_Appearance01:AddLinkedTo(PLR_Terror_Upperbody03_V1)
		MP_RU_Support_Appearance01:AddLinkedTo(self.m_UnlockAssets["Persistence/Unlocks/Soldiers/Visual/PLR/LowerBody/PLR_Terror_Lowerbody02_V1"])
	end
end


g_CommonPatch = CommonPatch()

return CommonPatch