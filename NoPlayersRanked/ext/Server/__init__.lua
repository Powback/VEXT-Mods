class 'NoPlayersRankedServer'


function NoPlayersRankedServer:__init()
	print("Initializing NoPlayersRankedServer")
	self:RegisterVars()
	self:RegisterEvents()
end


function NoPlayersRankedServer:RegisterVars()
	--self.m_this = that
end


function NoPlayersRankedServer:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	self.m_EngineMessageEvent = Events:Subscribe('Engine:Message', self, self.OnEngineMessage)
end

function NoPlayersRankedServer:OnEngineMessage(p_Message) 

	local s_Settings = ResourceManager:GetSettings("ServerSettings")

	if s_Settings ~= nil then
		print("Got ServerSettings")
		local s_ServerSettings = ServerSettings(s_Settings)

		print(s_ServerSettings.drawActivePhysicsObjects)
		s_ServerSettings.respawnOnDeathPosition = true
		s_ServerSettings.isRenderDamageEvents = true
		s_ServerSettings.showTriggerDebugText = true
		s_ServerSettings.isStatsEnabled = true
		
	else
		print("Could not get OnlineSettings")
	end
end

function NoPlayersRankedServer:ReadInstance(p_Instance,p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	
	if p_Instance.typeName == "PreRoundEntityData" then
		local s_Instance = PreRoundEntityData(p_Instance)
		s_Instance.roundMinPlayerCount  = 1
	end
end


g_NoPlayersRankedServer = NoPlayersRankedServer()

