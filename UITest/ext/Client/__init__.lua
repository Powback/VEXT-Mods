class('TweakUI')


function TweakUI:__init()
    self:RegisterEvents()
    self.m_CursorMode = false
end

function TweakUI:RegisterEvents()
    Events:Subscribe('ExtensionLoaded', self, self.OnLoaded)
    Events:Subscribe('Engine:Update', self, self.OnUpdate)
end



function TweakUI:OnLoaded()
    WebUI:Init()
    WebUI:Show()
end
function round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end


function TweakUI:OnUpdate(p_Delta)
    local s_Player = PlayerManager:GetLocalPlayer()
    if s_Player ~= nil and s_Player.soldier ~= nil then
        local s_Transform = s_Player.soldier.transform

    WebUI:ExecuteJS(string.format('UpdateAngles(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);', 
       round(s_Transform.left.x * -1, 3), 
        round(s_Transform.left.y * -1, 3), 
        round(s_Transform.left.z * -1, 3), 

        round(s_Transform.up.x, 3), 
        round(s_Transform.up.y, 3), 
        round(s_Transform.up.z, 3), 

        round(s_Transform.forward.x, 3), 
        round(s_Transform.forward.y, 3), 
        round(s_Transform.forward.z, 3), 

        s_Transform.trans.x, 
        s_Transform.trans.y, 
        s_Transform.trans.z


        ))
end

end

g_Client = TweakUI()