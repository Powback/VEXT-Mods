class "RoyaleServer"

local GameStates =
{
	-- Peoples weapons will be disabled, but you will be able to run around and move and spawn/respawn
	-- The center radius will be selected
	PreRound = {},

	-- We will set everyones kits to knives and spawn them in a grid in the sky
	InGame = {},

	-- This is the in-between state of transitioning, where neither the PreRound will update nor the InGame
	-- This is just used for setting up game states in between and what not before being handed off to either PreRoundUpdate or InGameUpdate
	Transition = {},
}

function RoyaleServer:__init()
	print("TestServer Initializing")

	-- Subscribe to the update event to update game state
	self.UpdateEvent = Events:Subscribe("UpdateManager:Update", self, self.OnServerUpdate)
	self.ReadInstanceEvent = Events:Subscribe("Partition:ReadInstance", self, self.OnReadInstance)
	self.LevelLoadedEvent = Events:Subscribe("Server:LevelLoaded", self, self.OnLevelLoaded)
	self.LevelDestroyEvent = Events:Subscribe("Level:Destroy", self, self.OnLevelDestroy)

	Hooks:Install("Soldier:Damage", 999, self.FilterHook)

	-- Pre-round settings
	self.PreRoundTimer = 0.0

	-- Gameplay information
	self.GameState = GameStates.Transition

	-- Center location of the circle, this can be changed at any point during pre-round
	self.CenterLocation = Vec3(-347.0, 1000.0, 364.0) -- -347.904449 77.4996872 364.992493


	-- Radius from the CenterLocation, this will shrink slowly over time
	self.CenterRadius = 0.0

	-- The default round time in seconds, currently set to 300 (5 minutes)
	self.DefaultRoundTime = 30

	-- The current round time that will be ticking down
	self.CurrentRoundTime = 10

	-- We only want to update every 16.66ms
	self.UpdateRate = 16.66
	-- This holds our current time, to be updated in each of the OnUpdate functions
	self.UpdateTime = 0.0

	-- Since we can only kill 
	self.PlayersToKill = { }
end

function RoyaleServer:OnLevelDestroy()
	print("Destroying level")
	NetEvents:Broadcast("Roayle:LevelDestroy")
end

function RoyaleServer:FilterHook(Hook, Soldier, DamageInfo, Inflictor, Assistant)
	print("Soldier: " .. tostring(Soldier))
	print("DamageInfo: " .. tostring(DamageInfo))
	print("Inflictor: " .. tostring(Inflictor))
	print("Assistant: " .. tostring(Assistant))

	--print("Solider: " .. Soldier .. " DamageInfo " .. DamageInfo .. " Inflictor: " .. Inflictor .. " Assistant: " .. Assistant)
end

function RoyaleServer:OnLevelLoaded(LevelName, GameMode, Round, RoundsPerMap)
	print("Loaded level: " .. LevelName)

	-- Set up the initial state
	self:TransitionFromStartToPreRound()
end

function RoyaleServer:OnReadInstance(Instance, PartitionGuid, InstanceGuid)
	-- We want to remove all invisible walls
	if Instance.typeName == "ReferenceObjectData" then
		local objectData = ReferenceObjectData(Instance)

		-- Get the object blueprint
		local objectBlueprint = objectData.blueprint

		-- Verify that we have a valid blueprint
		if (objectBlueprint == nil) then
			goto referenceObjectData_break
		end

		-- We want to exclude everything that has "invisible" for walls and what not
		if string.find(objectBlueprint.name, "invis") ~= nil then
			objectData.excluded = true
		end

		::referenceObjectData_break::
	end
end
---------------------------------------------------------------------------------------------------
--	Player Input
---------------------------------------------------------------------------------------------------
function RoyaleServer:ToggleAllPlayerInput(Enabled)
	-- Iterate through each of the players and disable their input
	local players = PlayerManager:GetPlayers()

	-- Iterate through all players and disable their input
	for i, player in ipairs(players) do
		-- Verify that we have a valid player
		if player == nil then
			goto cont_player_input
		end

		-- These are the rules we want to enforce so people can't hurt each other
		player:EnableInput(EntryInputActionEnum.EIAFire, Enabled)
		player:EnableInput(EntryInputActionEnum.EIAFireCountermeasure, Enabled)
		player:EnableInput(EntryInputActionEnum.EIAReload, Enabled)
		player:EnableInput(EntryInputActionEnum.EIAMeleeAttack, Enabled)
		player:EnableInput(EntryInputActionEnum.EIAThrowGrenade, Enabled)
		player:EnableInput(EntryInputActionEnum.EIAInteract, Enabled)
		player:EnableInput(EntryInputActionEnum.EIAGiveOrder, Enabled)
		player:EnableInput(EntryInputActionEnum.EIAShowCommoRose, Enabled)
		player:EnableInput(EntryInputActionEnum.EIAShowLeaderCommoRose, Enabled)
		player:EnableInput(EntryInputActionEnum.EIAQuicktimeInteractDrag, Enabled)
		player:EnableInput(EntryInputActionEnum.EIAQuicktimeFire, Enabled)
		::cont_player_input::
	end
end

function RoyaleServer:OnServerUpdate(DeltaTime, CurrentPass)
	-- Only do updates in the PostFrame
	if CurrentPass == UpdatePass.UpdatePass_PostFrame then

		-- Always update the current round time
		self.CurrentRoundTime = self.CurrentRoundTime - DeltaTime

		-- If we are not at an even tick then just increment the timer
		if self.UpdateTime < self.UpdateRate then
			self.UpdateTime = self.UpdateTime + DeltaTime
			--print("ret")
			return
		end

		print("tick")
		-- Reset the update time
		self.UpdateTime = 0.0

		-- Update the Pre-Round
		if self.GameState == GameStates.PreRound then
			self:OnPreRoundUpdate(DeltaTime)
		end

		-- Update the in-game round
		if self.GameState == GameStates.InGame then
			self:OnRoundUpdate(DeltaTime)
		end

		-- Otherwise we must be in a transition state and we do nothing
	elseif CurrentPass == UpdatePass.UpdatePass_PreSim then
		-- Iterate through all of the players to kill and kill them, because
		-- there is a bug in Venice Unleashed where if you don't kill them
		-- on PreSim, a DICE BUG causes heap corruption and
		-- will cause the server to crash =[, fixing it is too complicated w/o source =[
		-- damnit dice y u do dis
		for i, playerId in ipairs(self.PlayersToKill) do
			local player = PlayerManager:GetPlayerByID(playerId)
			if player == nil then
				goto cont_player_kill_sim
			end

			if player.alive == false then
				goto cont_player_kill_sim
			end

			local soldier = player.soldier
			if soldier == nil then
				goto cont_player_kill_sim
			end

			soldier:Kill()

			::cont_player_kill_sim::
		end

		self.PlayersToKill = { }
	end
end

function RoyaleServer:OnPreRoundUpdate(DeltaTime)
	-- Always update the current round time in decreasing manner
	-- We start with how long the round should be then count down
	-- If transitioning from a new game/startup the round time in pre round is 2m by default


	-- If the pre-round timer has ran out
	if self.CurrentRoundTime <= 0.0 then
		-- This function will change the round state so OnPreRoundUpdate should stop being called
		self:TransitionFromPreRoundToInGame()

		-- We don't want to finish executing any more code, because TransitionFromPreRoundToInGame should have taken care of everything
		return
	end

	-- While we are in the pre-round you are able to respawn if you killed yourself somehow
	self:ToggleSpawning(true)

	-- Disable all players input that could be considered harmful
	self:ToggleAllPlayerInput(false);
end

function RoyaleServer:OnRoundUpdate(DeltaTime)
	if self.CurrentRoundTime <= 0.0 then
		-- This function will change the round state so OnPreRoundUpdate should stop being called
		self:TransitionFromStartToPreRound()

		-- We don't want to finish executing any more code, because TransitionFromPreRoundToInGame should have taken care of everything
		return
	end

	-- Don't allow respawns
	self:ToggleSpawning(false)

	-- Let them kill each other
	self:ToggleAllPlayerInput(true);
end

--
-- Helper function to disable spawning for all players
--
function RoyaleServer:ToggleSpawning(SpawningEnabled)
	-- Get the list of players
	local players = PlayerManager:GetPlayers()

	-- Iterate through each player and enable the ability to spawn
	for i, player in ipairs(players) do
		-- Check if we have a valid player
		if (player == nil) then
			goto cont_player_iteration
		end

		-- Set if the player is allowed to be spawned
		player.isAllowedToSpawn = SpawningEnabled

		-- This is used because lua does not have built in "continue"
		::cont_player_iteration::
	end
end

function RoyaleServer:TransitionFromStartToPreRound()
	-- This function will set up everything we need from the init stage to put us into pre-round
	print("Transitioning from Start/End to Pre-Round")
	ChatManager:SendMessage("Transitioning from Start/End to Pre-Round")

	-- Iterate through all players and kill them
	local players = PlayerManager:GetPlayers()
	for i, player in ipairs(players) do
		-- Validate our player
		if player == nil then
			goto cont_player_kill
		end

		-- Ensure that our player is already alive
		if player.alive == false then
			goto cont_player_kill
		end

		-- Validate our soldier
		if player.hasSoldier == false then
			goto cont_player_kill
		end

		local soldier = player.soldier
		if soldier == nil then
			goto cont_player_kill
		end

		table.insert(self.PlayersToKill, player.id)
		--soldier:Kill()

		::cont_player_kill::
	end

	-- Disable spawning
	self:ToggleSpawning(true)

	-- Disable players input again
	self:ToggleAllPlayerInput(false)

	-- Update the current round time
	self.CurrentRoundTime = self.DefaultRoundTime

	-- Set the game state to preround
	self.GameState = GameStates.PreRound
end

function RoyaleServer:TransitionFromPreRoundToInGame()
	ChatManager:SendMessage("Transitioning from Pre-Round to In-Game")
	print("Transitioning from Pre-Round to In-Game")
	-- Update the current game state
	self.GameState = GameStates.Transition

	-- Disable spawning
	self:ToggleSpawning(false)

	-- Enable players input again
	self:ToggleAllPlayerInput(true)

	-- Generate a new center point
	self.CenterRadius = 850.0
	self.CenterLocation = Vec3(-76.9179459, 650.2824326, -146.653702) -- TODO: Make this random

	-- Teleport everyone into the sky above the center point
	local players = PlayerManager:GetPlayers()
	for i, player in ipairs(players) do
		-- Validate our player
		if player == nil then
			goto cont_player_teleport
		end

		-- Ensure that our player is already alive
		if player.alive == false then
			goto cont_player_teleport
		end

		-- Validate our soldier
		if player.hasSoldier == false then
			goto cont_player_teleport
		end

		local soldier = player.soldier
		if soldier == nil then
			goto cont_player_teleport
		end

		-- Calculate our positions so everyone does not spawn right on top of everyone else
		local pos = Vec3(self.CenterLocation.x, self.CenterLocation.y, self.CenterLocation.z)
		
		-- Set the soldier position
		soldier:SetPosition(pos)

		::cont_player_teleport::
	end

	-- Update the current round time
	self.CurrentRoundTime = self.DefaultRoundTime

	-- Set the game state to in-game
	self.GameState = GameStates.InGame

	-- TODO: Update the time dynamically
	ChatManager:SendMessage("Game ending in " .. tostring(self.CurrentRoundTime) .. " seconds")
	print("Game ending in " .. tostring(self.CurrentRoundTime) .. " seconds")

end

local g_RoyaleServer = RoyaleServer()
return RoyaleServer