class "RoyaleClient"

function RoyaleClient:__init()
    -- Subscribe for getting the fire blueprint
    self.ReadInstanceEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)

    -- Subscribe to the server level unload event
    self.LevelDestroyedEvent = NetEvents:Subscribe("Royale:LevelDestroy", self, self.OnLevelDestroyed)

    -- Subscribe to update events
    self.UpdateManagerEvent = Events:Subscribe("UpdateManager:Update", self, self.OnUpdate)

    -- Hold the fire blueprint
    self.FireBlueprint = nil
    self.FireEmitterTime = 0.0
    self.FireEmitterResetTime = 3.0
    self.EffectsTable = {}
    self.Emitter = nil
    self.Effect = nil

    self.CurrentTime = 0.0
    
    print("RoyaleClient init")
end

function RoyaleClient:OnLevelDestroyed()
    print("Level Destroyed")
    self.FireBlueprint = nil
    self.EffectsTable = {}
end

function RoyaleClient:OnUpdate(DeltaTime, Pass)
    if self.FireBlueprint == nil then
        return
    end

    if Pass == UpdatePass.UpdatePass_PreInput then
        self.CurrentTime = self.CurrentTime + DeltaTime

        local s_Player = PlayerManager:GetLocalPlayer()
        if s_Player == nil then
            return false
        end
        
        local s_Soldier = s_Player.soldier
        if s_Soldier == nil then
            return false
        end
        
        local s_Transform = s_Soldier.transform
        if(self.EffectsTable[self.Effect] == nil) then
            print("Effect does not exist: " .. self.Effect)
            return
        end
        if(self.Emitter == nil) then
            local Effect = self.EffectsTable[self.Effect]
            print("Playing Effect " .. self.Effect)
            print(Effect)


            if p_Transform == nil then
                print("spawning with player transform")
                self.emitterTransform = s_Transform
            end

            local params = EffectParams()
            self.Emitter = EffectManager:PlayEffect(Effect, self.emitterTransform, params, false)
            print("spawned")
            print(self.Emitter)
        end


        if self.CurrentTime > self.FireEmitterResetTime then
            self.CurrentTime = 0.0
            
            EffectManager:ResetEffect(self.Emitter)
            print("Reset")
        end
    end
end

function RoyaleClient:OnPartitionLoaded(p_Partition)
  if p_Partition == nil then
        return
    end
    
    local s_Instances = p_Partition.instances


    for _, Instance in ipairs(s_Instances) do

        -- Validate instance
        if Instance == nil then
            return
        end

        -- Skip everything that is not Effectblueprint
        if Instance.typeInfo.name ~= "EffectBlueprint" then
            return
        end

        -- Cast to Effectblueprint
        local EffectBlueprint = EffectBlueprint(Instance)
        local EffectName = EffectBlueprint.name
        if (EffectName == nil) then
            return
        end

        self.EffectsTable[EffectName] = EffectBlueprint

        print("Effect: " .. EffectName)
        
        -- We only want the fire Effect
        if string.match(EffectName, "Fire") then
            print("Got Effect blueprint")
            self.FireBlueprint = EffectBlueprint
            self.Effect = EffectName
        end
    end
end

local g_RoyaleClient = RoyaleClient()
return RoyaleClient