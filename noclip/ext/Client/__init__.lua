class 'noclipClient'


function noclipClient:__init()
	print("Initializing noclipClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function noclipClient:RegisterVars()
	--self.m_this = that
	self.m_MeleeTime = 0.0
	self.m_Noclip = false
	self.m_TimeSinceTP = 0
end


function noclipClient:RegisterEvents()
	Events:Subscribe('Client:UpdateInput', self, self.OnUpdate)


	--self.m_EntityCreateEvent = Hooks:Install('ClientEntityFactory:Create', 500, self, self.OnEntityCreate)

end

function noclipClient:OnUpdate(p_Delta, p_SimulationDelta)
	
	self.m_TimeSinceTP = self.m_TimeSinceTP + p_Delta

	local s_NoclipKey = InputManager:WentKeyDown(InputDeviceKeys.IDK_V)
	local s_GoUp = InputManager:IsKeyDown(InputDeviceKeys.IDK_Space) 
	local s_GoDown = InputManager:IsKeyDown(InputDeviceKeys.IDK_LeftCtrl)
	local s_GoForward = InputManager:IsKeyDown(InputDeviceKeys.IDK_LeftAlt)


	-- If we had raycast here, we could activate noclip manually. But since it's broken. Fuck it..
	local s_Player = PlayerManager:GetLocalPlayer()
	if s_Player == nil then
		return
	end

	local s_Soldier = s_Player.soldier
	if s_Soldier == nil then
		return
	end
	if s_NoclipKey then
		if(self.m_Noclip == false) then
			self.m_Noclip = true
			print("Noclip: " .. tostring(self.m_Noclip))
			NetEvents:SendLocal('noclip:on', newTransform)
		else
			self.m_Noclip = false
			print("Noclip: " .. tostring(self.m_Noclip))
			NetEvents:SendLocal('noclip:off', newTransform)
		end
	end

	if(s_GoForward) then
		self:SetPosition("f")
	elseif(s_GoDown) then
		self:SetPosition("d")
	elseif(s_GoUp) then
		self:SetPosition("u")
	end

end



function noclipClient:SetPosition(p_Direction)
	if(self.m_Noclip == false or self.m_TimeSinceTP < 0.1 ) then
		return
	end
	local s_Player = PlayerManager:GetLocalPlayer()
	if s_Player == nil then
		return
	end

	local s_Soldier = s_Player.soldier
	if s_Soldier == nil then
		return
	end
	print(self.m_TimeSinceTP)
	self.m_TimeSinceTP = 0


	print(p_Direction)
	local s_Weapon = s_Player.soldier.weaponsComponent
	if s_Weapon ~= nil then
		local s_Transform = ClientUtils:GetCameraTransform()

		if(p_Direction == "f") then
			s_Transform = Vec3(s_Transform.forward.x * -1, s_Transform.forward.y * -1, s_Transform.forward.z * -1)
		elseif(p_Direction == "u") then
			s_Transform = s_Transform.up
		elseif(p_Direction == "d") then
			s_Transform = s_Transform.up 

		else 
			return
		end
		print("Almost there")
		local newTransform = s_Player.soldier.transform
		print(s_Transform)
		newTransform.forward = s_Transform
		newTransform.up = ClientUtils:GetCameraTransform().up
		newTransform.left = ClientUtils:GetCameraTransform().left

		local newX = (s_Transform.x * 10) + s_Player.soldier.transform.trans.x
		local newY = (s_Transform.y * 10) + s_Player.soldier.transform.trans.y
		local newZ = (s_Transform.z * 10) + s_Player.soldier.transform.trans.z
		newTransform.trans = Vec3(newX,newY,newZ)
		NetEvents:SendLocal('noclip:setposition', newTransform)
		print("YES!")

 	end
end

--[[
function noclipClient:OnEntityCreate(p_Hook, p_Data, p_Transform)
	--print(string.format('Creating entity type "%s" (%f, %f, %f)', p_Data.typeName, p_Transform.trans.x, p_Transform.trans.y, p_Transform.trans.z))
	if(p_Data.typeName == "SoldierEntityData") then
		print("Found soldier")
		self.soldier = p_Hook:Call()
		print("Called hook")
	end
	p_Hook:Next()
end
]]
g_noclipClient = noclipClient()

