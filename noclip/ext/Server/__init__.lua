class 'noclipServer'

function noclipServer:__init()
	NetEvents:Subscribe('noclip:setposition', self, self.OnPosition)
	NetEvents:Subscribe('noclip:off', self, self.TurnOff)
	NetEvents:Subscribe('noclip:on', self, self.TurnOn)


	Hooks:Install('ServerEntityFactory:Create', 9, self, self.OnEntityCreate)
	Events:Subscribe('Player:Respawn', self, self.OnPlayer)
	Events:Subscribe('Player:SpawnOnPlayer', self, self.OnPlayer)
	Events:Subscribe('Player:SpawnAtVehicle', self, self.OnPlayer)




	Events:Subscribe('Player:Deleted', self, self.OnPlayerDeleted)
	Events:Subscribe('Player:Destroyed', self, self.OnPlayerDeleted)

	self.m_Soldiers = {}
	self.m_PlayerSpawning = false
	self.m_TempSoldier = nil
end

function noclipServer:HasSoldier(p_Player)
	if p_Player == nil then
		return false
	elseif p_Player.soldier == nil then
		return false
	else
		return true
	end
end

function noclipServer:TurnOff(p_Player)
	
	self:SetGravity(p_Player, "EnableCollision")
end	

function noclipServer:TurnOn(p_Player)
	self:SetGravity(p_Player, "DisableCollision")
end	





function noclipServer:SetGravity(p_Player, collision)
	if(self:HasSoldier(p_Player)) then
		p_Player.soldier:FireEvent(collision)
	else 
		print("Tried to enable noclip for a player that\'s not alive: ' " .. p_Player)
	end
end

function noclipServer:OnPosition(p_Player, p_Transform)
	print("noclip")
	if(self:HasSoldier(p_Player)) then
			p_Player.soldier:SetPosition(p_Transform.trans)
	else 
		print("Tried to teleport a player that\'s not alive: " .. p_Player)
	end
end

function noclipServer:OnEntityCreate(p_Hook, p_Data, p_Transform)
	if (p_Data.typeName == "SoldierEntityData" ) then
		self.m_TempSoldier = SoldierEntityData(p_Data)
	end
	p_Hook:Next()
end

function noclipServer:OnPlayerDeleted(p_Player)
	self.m_Soldiers[p_Player.name] = nil
end

function noclipServer:OnPlayer(p_Player)
	self.m_Soldiers[p_Player.name] = self.m_TempSoldier
end
g_noclipServer = noclipServer()

-- If external class use "return MyModName"