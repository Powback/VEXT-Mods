class 'tryallvaluesShared'


function tryallvaluesShared:__init()
	print("Initializing tryallvaluesShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function tryallvaluesShared:RegisterVars()
	--self.m_this = that
end


function tryallvaluesShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end


function tryallvaluesShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		local s_Instance = _G[l_Instance.typeInfo.name](l_Instance)
		PrintFields(s_Instance)
	end
end

function PrintFields( p_Instance )
	local s_Fields = getFields(p_Instance.typeInfo)
	for k,l_Field in pairs(s_Fields) do
		local s_FieldName = FirstToLower(l_Field.name)
		print(l_Field.type)
		local s_Field = p_Instance[l_Field]

	end

end
function isPrintable( typ )
	if typ == "CString" or
	typ == "Float8" or
	typ == "Float16" or
	typ == "Float32" or
	typ == "Float64" or
	typ == "Int8" or
	typ == "Int16" or
	typ == "Int32" or
	typ == "Int64" or
	typ == "Uint8" or
	typ == "Uint16" or
	typ == "Uint32" or
	typ == "Uint64" or
	typ == "LinearTransform" or
	typ == "Vec2" or
	typ == "Vec3" or
	typ == "Vec4" or
	typ == "Boolean" or 
	typ == "Guid" then
		print(typ)
		return true
	end
	return false
end

function getFields( typeInfo )
	local s_Super = {}
	if typeInfo.super ~= nil then
		if typeInfo.super.name ~= "DataContainer" then
			for k,superv in pairs(getFields(typeInfo.super)) do
				table.insert(s_Super, superv)
			end
		end
	end
	for k,v in pairs(typeInfo.fields) do
		table.insert(s_Super, v)
	end
	return s_Super
end
function FirstToLower(str)
	return (str:gsub("^%L", string.lower))
end

g_tryallvaluesShared = tryallvaluesShared()

