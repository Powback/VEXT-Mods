class 'DefaultSettingsShared'
local m_cloner = require "__shared/InstanceCloner"
function DefaultSettingsShared:__init()
	print("Initializing DefaultSettingsShared")
	self:RegisterVars()
	self:RegisterEvents()
end

function DefaultSettingsShared:RegisterVars()
	self.m_this = that
end

function DefaultSettingsShared:RegisterEvents()
	self.m_EngineMessageEvent = Events:Subscribe('Engine:Message', self, self.OnEngineMessage)
	Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)

end

function DefaultSettingsShared:OnEngineMessage(p_Message) 

	if p_Message.type == MessageType.ClientConnectedMessage or
		p_Message.type == MessageType.ServerLevelLoadedMessage then 
		--self:GetSettings()
	end
end


function DefaultSettingsShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		error('p_Partition was nil')
		return
	end
 
	local instances = p_Partition.instances
   
	for _, instance in ipairs(instances) do
		if instance == nil then
			print("Instance is null wtf")
			break
		end

		if instance.typeName == "SoldierBlueprint" then
			local s_Instance = SoldierEntityData(instance)
			m_cloner:PrintFields(s_Instance, _G['SoldierBlueprint'].typeInfo)
		end
	end
end

function DefaultSettingsShared:GetSettings()
	local p_BFServerSettings = ResourceManager:GetSettings("BFServerSettings")
	if(p_BFServerSettings ~= nil) then
		local s_BFServerSettings = BFServerSettings(p_BFServerSettings)
		m_cloner:PrintFields(s_BFServerSettings, _G['BFServerSettings'].typeInfo)
	end
	local p_ClientSettings = ResourceManager:GetSettings("ClientSettings")
	if(p_ClientSettings ~= nil) then
		local s_ClientSettings = ClientSettings(p_ClientSettings)
		m_cloner:PrintFields(s_ClientSettings, _G['ClientSettings'].typeInfo)
	end
	local p_DecalSettings = ResourceManager:GetSettings("DecalSettings")
	if(p_DecalSettings ~= nil) then
		local s_DecalSettings = DecalSettings(p_DecalSettings)
		m_cloner:PrintFields(s_DecalSettings, _G['DecalSettings'].typeInfo)
	end
	local p_DxDisplaySettings = ResourceManager:GetSettings("DxDisplaySettings")
	if(p_DxDisplaySettings ~= nil) then
		local s_DxDisplaySettings = DxDisplaySettings(p_DxDisplaySettings)
		m_cloner:PrintFields(s_DxDisplaySettings, _G['DxDisplaySettings'].typeInfo)
	end
	local p_EffectManagerSettings = ResourceManager:GetSettings("EffectManagerSettings")
	if(p_EffectManagerSettings ~= nil) then
		local s_EffectManagerSettings = EffectManagerSettings(p_EffectManagerSettings)
		m_cloner:PrintFields(s_EffectManagerSettings, _G['EffectManagerSettings'].typeInfo)
	end
	local p_EnlightenRuntimeSettings = ResourceManager:GetSettings("EnlightenRuntimeSettings")
	if(p_EnlightenRuntimeSettings ~= nil) then
		local s_EnlightenRuntimeSettings = EnlightenRuntimeSettings(p_EnlightenRuntimeSettings)
		m_cloner:PrintFields(s_EnlightenRuntimeSettings, _G['EnlightenRuntimeSettings'].typeInfo)
	end
	local p_EntitySettings = ResourceManager:GetSettings("EntitySettings")
	if(p_EntitySettings ~= nil) then
		local s_EntitySettings = EntitySettings(p_EntitySettings)
		m_cloner:PrintFields(s_EntitySettings, _G['EntitySettings'].typeInfo)
	end
	local p_GameAnimationSettings = ResourceManager:GetSettings("GameAnimationSettings")
	if(p_GameAnimationSettings ~= nil) then
		local s_GameAnimationSettings = GameAnimationSettings(p_GameAnimationSettings)
		m_cloner:PrintFields(s_GameAnimationSettings, _G['GameAnimationSettings'].typeInfo)
	end
	local p_GameModeSettings = ResourceManager:GetSettings("GameModeSettings")
	if(p_GameModeSettings ~= nil) then
		local s_GameModeSettings = GameModeSettings(p_GameModeSettings)
		m_cloner:PrintFields(s_GameModeSettings, _G['GameModeSettings'].typeInfo)
	end
	local p_GameSettings = ResourceManager:GetSettings("GameSettings")
	if(p_GameSettings ~= nil) then
		local s_GameSettings = GameSettings(p_GameSettings)
		m_cloner:PrintFields(s_GameSettings, _G['GameSettings'].typeInfo)
	end
	local p_GameTimeSettings = ResourceManager:GetSettings("GameTimeSettings")
	if(p_GameTimeSettings ~= nil) then
		local s_GameTimeSettings = GameTimeSettings(p_GameTimeSettings)
		m_cloner:PrintFields(s_GameTimeSettings, _G['GameTimeSettings'].typeInfo)
	end
	local p_OnlineSettings = ResourceManager:GetSettings("OnlineSettings")
	if(p_OnlineSettings ~= nil) then
		local s_OnlineSettings = OnlineSettings(p_OnlineSettings)
		m_cloner:PrintFields(s_OnlineSettings, _G['OnlineSettings'].typeInfo)
	end
	local p_PerformanceTrackerSettings = ResourceManager:GetSettings("PerformanceTrackerSettings")
	if(p_PerformanceTrackerSettings ~= nil) then
		local s_PerformanceTrackerSettings = PerformanceTrackerSettings(p_PerformanceTrackerSettings)
		m_cloner:PrintFields(s_PerformanceTrackerSettings, _G['PerformanceTrackerSettings'].typeInfo)
	end
	local p_PersistenceSettings = ResourceManager:GetSettings("PersistenceSettings")
	if(p_PersistenceSettings ~= nil) then
		local s_PersistenceSettings = PersistenceSettings(p_PersistenceSettings)
		m_cloner:PrintFields(s_PersistenceSettings, _G['PersistenceSettings'].typeInfo)
	end
	local p_ResourceManagerSettings = ResourceManager:GetSettings("ResourceManagerSettings")
	if(p_ResourceManagerSettings ~= nil) then
		local s_ResourceManagerSettings = ResourceManagerSettings(p_ResourceManagerSettings)
		m_cloner:PrintFields(s_ResourceManagerSettings, _G['ResourceManagerSettings'].typeInfo)
	end
	local p_ServerSettings = ResourceManager:GetSettings("ServerSettings")
	if(p_ServerSettings ~= nil) then
		local s_ServerSettings = ServerSettings(p_ServerSettings)
		m_cloner:PrintFields(s_ServerSettings, _G['ServerSettings'].typeInfo)
	end
	local p_SoundSettings = ResourceManager:GetSettings("SoundSettings")
	if(p_SoundSettings ~= nil) then
		local s_SoundSettings = SoundSettings(p_SoundSettings)
		m_cloner:PrintFields(s_SoundSettings, _G['SoundSettings'].typeInfo)
	end
	local p_SyncedBFSettings = ResourceManager:GetSettings("SyncedBFSettings")
	if(p_SyncedBFSettings ~= nil) then
		local s_SyncedBFSettings = SyncedBFSettings(p_SyncedBFSettings)
		m_cloner:PrintFields(s_SyncedBFSettings, _G['SyncedBFSettings'].typeInfo)
	end
	local p_SyncedGameSettings = ResourceManager:GetSettings("SyncedGameSettings")
	if(p_SyncedGameSettings ~= nil) then
		local s_SyncedGameSettings = SyncedGameSettings(p_SyncedGameSettings)
		m_cloner:PrintFields(s_SyncedGameSettings, _G['SyncedGameSettings'].typeInfo)
	end
	local p_UISettings = ResourceManager:GetSettings("UISettings")
	if(p_UISettings ~= nil) then
		local s_UISettings = UISettings(p_UISettings)
		m_cloner:PrintFields(s_UISettings, _G['UISettings'].typeInfo)
	end
	local p_VeniceOnlineSettings = ResourceManager:GetSettings("VeniceOnlineSettings")
	if(p_VeniceOnlineSettings ~= nil) then
		local s_VeniceOnlineSettings = VeniceOnlineSettings(p_VeniceOnlineSettings)
		m_cloner:PrintFields(s_VeniceOnlineSettings, _G['VeniceOnlineSettings'].typeInfo)
	end
	local p_VeniceUISettings = ResourceManager:GetSettings("VeniceUISettings")
	if(p_VeniceUISettings ~= nil) then
		local s_VeniceUISettings = VeniceUISettings(p_VeniceUISettings)
		m_cloner:PrintFields(s_VeniceUISettings, _G['VeniceUISettings'].typeInfo)
	end
	local p_WindowSettings = ResourceManager:GetSettings("WindowSettings")
	if(p_WindowSettings ~= nil) then
		local s_WindowSettings = WindowSettings(p_WindowSettings)
		m_cloner:PrintFields(s_WindowSettings, _G['WindowSettings'].typeInfo)
	end
end

g_DefaultSettingsShared = DefaultSettingsShared()

