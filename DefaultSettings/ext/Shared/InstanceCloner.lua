class "InstanceCloner"

local savedInstances = {}

-- This doesn't work yet!!!!, you can't create a DataContainer() 
-- Clones every child of an instance, returns the new instance
function InstanceCloner:CloneInstance(instance)

	local newInstance = _G[instance.typeName]()
	print("-------------Atempting to copy "..instance.typeName)
	self:CopyFields(newInstance, instance, instance.typeInfo)
	print("-------------Copied succesfully")
	savedInstances = {}

	return newInstance
end

-- Copies an instance, all members are references to the original intance member except those in the path specified (if there is one).
-- Returns a new instance.
-- The path must be a table with strings of the object member names in order. If its a member of an array it has to be prefixed
-- with the position in the array. Example: A path to copy SoldierBlueprint and all its childs until CharacterStatePoseInfo:
-- path = { "Object", "CharacterPhysics", "0States", "0PoseInfo"}
function InstanceCloner:PartialCopy(instance, path)

	local newInstance = _G[instance.typeName]()

	if path == nil then
		path = {}
	end

	print("-------------Atempting to partially copy "..instance.typeName)
	self:PartialInstanceCopy(newInstance, instance, instance.typeInfo, path)
	print("-------------Copied succesfully")

	return newInstance
end


-- Prints all members and child members of a given instance. Useful for debugging.
function InstanceCloner:PrintFields( instance, typeInfo, padding, currentDepth, maxDepth)
	if(currentDepth == nil) then
		currentDepth = 0
	end

	if(maxDepth == nil) then
		maxDepth = -1
	else
		if(maxDepth ~= -1 and currentDepth > maxDepth) then
			return
		end
	end

	currentDepth = currentDepth + 1

	if(padding == nil) then
		padding = ""
	end 
	print(padding .. typeInfo.name .. ' {')
	padding = padding .. "	"

	for _, field in ipairs(typeInfo.fields) do

		if field.typeInfo ~= nil then

			local s_Name = FirstToLower(field.name)

			--Value that can be printed 
			--NOTE: these arent all possible types
			if field.typeInfo.name == 'CString' or

				 field.typeInfo.name == 'Float8' or
				 field.typeInfo.name == 'Float16' or
				 field.typeInfo.name == 'Float32' or
				 field.typeInfo.name == 'Float64' or

				 field.typeInfo.name == 'Int8' or
				 field.typeInfo.name == 'Int16' or
				 field.typeInfo.name == 'Int32' or
				 field.typeInfo.name == 'Int64' or

				 field.typeInfo.name == 'Uint8' or
				 field.typeInfo.name == 'Uint16' or
				 field.typeInfo.name == 'Uint32' or
				 field.typeInfo.name == 'Uint64' or

				 field.typeInfo.name == 'Boolean' then

				local s_Value = instance[s_Name]

				print(padding ..field.name..' ('..field.typeInfo.name..') : '.. tostring(s_Value))

			--Array
			elseif field.typeInfo.array then
				-- So UIBundlesAsset[uIBundleAssetStateList] returns nil even though it's not??
				if(instance[FirstToLower(field.name)] ~= nil) then
					local s_Count = #instance[FirstToLower(field.name)]
					print(padding ..field.name..' (Array), '..tostring(s_Count)..' Members {')

					if s_Count ~= 0 then
						s_Count = s_Count

						for i=1,s_Count,1 do 
							local instanceType = type(instance[FirstToLower(field.name)]:get(i))
							if(instanceType == "number" or instanceType == "string" ) then
								print(padding .. "[" .. i .. "] " ..instance[FirstToLower(field.name)]:get(i))
							else
								local s_MemberInstance = instance[FirstToLower(field.name)]:get(i)
								local s_Member = _G[s_MemberInstance.typeInfo.name](s_MemberInstance)

								if s_Member ~= nil then
									self:PrintFields(s_Member, s_Member.typeInfo, padding .. "	", currentDepth, maxDepth)
								end
							end
						end
					end
					print(padding .. "}")
				end
			--Enum
			elseif field.typeInfo.enum then
				local s_Value = instance[s_Name]
				print(padding..field.name..' (Enum) : ' .. tostring(s_Value))
			elseif field.typeInfo.name == "Guid" then
				local s_Value = instance[s_Name]
				print(padding..field.name..' (Guid) : ' .. tostring(s_Value))
			--Object
			else
				if instance[s_Name] ~= nil then 
					-- local s_Value = instance[s_Name]
					local i = _G[field.typeInfo.name](instance[s_Name])
					if i ~= nil then
						padding = padding .. "	"
						self:PrintFields( i, i.typeInfo, padding, currentDepth, maxDepth) 
					end
				else
					print(padding ..field.name..' (Object - '..field.typeInfo.name..') ' .."nil")
				end
			end
		end
	end
	print (padding:sub(1, -2) .. "}")
end

------------ "Private" Functions-------------------

function InstanceCloner:PartialInstanceCopy( instance, instance_copy, typeInfo, path)

	for _, field in ipairs(typeInfo.fields) do
		
		if field.typeInfo ~= nil then
			local s_Name = FirstToLower(field.name)

			--Array
			if field.typeInfo.array then
				
				local s_Count = instance["Get".. field.name .."Count"](instance)

				-- print('--'..field.name..' (Array), '..tostring(s_Count)..' Members:')

				if s_Count ~= 0 then
					s_Count = s_Count - 1

					for i=0,s_Count,1 do 
						local s_Member = instance["Get".. field.name .."At"](instance, i)

						-- local s_Member = _G[s_Member.typeName](s_Member)--might not work?

						--Get array index and member name
						if path[1] ~= nil then
							local index = tonumber(path[1]:match('%d+'))
							local varName = path[1]:gsub('%d+','') --removes the number
						end

						if index == i and tostring(field.name) == varName then
							print("Found member of an array in path. ".. index .. ", ".. varName)

							local pathcopy = shallowcopy(path)
							table.remove(pathcopy, 1)

							local s_Member_copy = _G[s_Member.typeName]()

							self:PartialInstanceCopy(s_Member, s_Member_copy, s_Member.typeInfo, pathcopy, member, value)

							instance_copy["Add".. field.name](instance_copy, s_Member_copy)
						else
						
							if s_Member ~= nil then
								instance_copy["Add".. field.name](instance_copy, s_Member)
							end
						end
					end

				end


			--Value
			elseif field.typeInfo.name == 'CString' or
						 field.typeInfo.name == 'Float8' or
						 field.typeInfo.name == 'Float16' or
						 field.typeInfo.name == 'Float32' or
						 field.typeInfo.name == 'Float64' or
						 field.typeInfo.name == 'Int8' or
						 field.typeInfo.name == 'Int16' or
						 field.typeInfo.name == 'Int32' or
						 field.typeInfo.name == 'Int64' or
						 field.typeInfo.name == 'Uint8' or
						 field.typeInfo.name == 'Uint16' or
						 field.typeInfo.name == 'Uint32' or
						 field.typeInfo.name == 'Uint64' or
						 field.typeInfo.name == 'Boolean' then
				-- print('--'..field.name..' ('..field.typeInfo.name..') : '..tostring(s_Value))

				instance_copy[s_Name]= instance[s_Name]

			--Enum
			elseif field.typeInfo.enum then
				-- print('--'..field.name..' (Enum) : ' .. tostring(s_Value))
				instance_copy[s_Name] = instance[s_Name]

			--Object
			else
				if instance[s_Name] ~= nil then 
					-- print('--'..field.name..' (Object - '..tostring(field.typeInfo.name)..')')

					if path[1] == field.name then
						print("Found object in path, creating new one")

						local s_NewInstance = _G[field.typeInfo.name](instance[s_Name])
						local s_NewInstance = _G[s_NewInstance.typeName](instance[s_Name])

						local s_NewInstance_copy = _G[s_NewInstance.typeName]()

						local pathcopy = shallowcopy(path)
						table.remove(pathcopy, 1)

						self:PartialInstanceCopy(s_NewInstance, s_NewInstance_copy, s_NewInstance.typeInfo, pathcopy)

						instance_copy[s_Name] = s_NewInstance_copy

					else
						instance_copy[s_Name] = instance[s_Name]
					end
				else
					-- print('--'..field.name..' (Object - '..tostring(field.typeInfo.name)..')'..' is null')
				end
			end
		end
	end

	if typeInfo.super ~= nil then
		-- print('Super class: '.. typeInfo.super.name)

		if typeInfo.super.name ~= "DataContainer" then
			local i = _G[typeInfo.super.name]()

			self:PartialInstanceCopy(instance, instance_copy, i.typeInfo, path)
		end
	end
end


--Doesn't work yet, you can't create a DataContainer()
function InstanceCloner:CopyFields( instance_copy, instance, typeInfo )

	for _, field in ipairs(typeInfo.fields) do

		if field.typeInfo ~= nil then

			local s_Name = FirstToLower(field.name)

			--Value that can be copied (not references)
			--NOTE: these arent all possible types
			if field.typeInfo.name == 'CString' or

				 field.typeInfo.name == 'Float8' or
				 field.typeInfo.name == 'Float16' or
				 field.typeInfo.name == 'Float32' or
				 field.typeInfo.name == 'Float64' or

				 field.typeInfo.name == 'Int8' or
				 field.typeInfo.name == 'Int16' or
				 field.typeInfo.name == 'Int32' or
				 field.typeInfo.name == 'Int64' or

				 field.typeInfo.name == 'Uint8' or
				 field.typeInfo.name == 'Uint16' or
				 field.typeInfo.name == 'Uint32' or
				 field.typeInfo.name == 'Uint64' or

					field.typeInfo.name == 'Boolean' then

				 instance_copy[s_Name] = instance[s_Name]
				
				local s_Value = instance[s_Name]

				-- print('--'..field.name..' ('..field.typeInfo.name..') : '..tostring(s_Value))

			--Array
			elseif field.typeInfo.array then
				
				local s_Count = instance["Get".. field.name .."Count"](instance)

				-- print('--'..field.name..' (Array), '..tostring(s_Count)..' Members:')

				if s_Count ~= 0 then
					s_Count = s_Count - 1


					for i=0,s_Count,1 do 
						local s_Member = instance["Get".. field.name .."At"](instance, i)


						if s_Member == nil then
							goto done1
						end
						 
						--Initialize table if its nil
						if savedInstances[s_Member.typeInfo.name] == nil then
							savedInstances[s_Member.typeInfo.name] = {}
						end

						for k, v in ipairs(savedInstances[s_Member.typeInfo.name]) do
							if s_Member == v.originalInstance then
								print("---------------found instance----------")

								instance_copy["Add".. field.name](instance_copy, v.copyInstance)

								goto done1
							end
						end

						print("---------------adding instance----------")

						local s_Member_copy = _G[s_Member.typeInfo.name]()
						table.insert(savedInstances[s_Member.typeInfo.name], {originalInstance = s_Member, copyInstance = s_Member_copy})

						
						self:CopyFields(s_Member_copy, s_Member, s_Member.typeInfo)
						instance_copy["Add".. field.name](instance_copy, s_Member_copy)
						
						::done1::

					end

				end

			--Enum
			elseif field.typeInfo.enum then
				instance_copy[s_Name] = instance[s_Name]
				-- print('--'..field.name..' (Enum) : ' .. tostring(s_Value))

			--Object
			else
				if instance[s_Name] ~= nil then 
					print('--'..field.name..' (Object - '..tostring(field.typeInfo.name)..')')
					if field.typeInfo.name ~= "DataContainer" then

						local i = _G[field.typeInfo.name](instance[s_Name])

						if i == nil then
							goto done
						end
						 
						--Initialize table if its nil
						if savedInstances[field.typeInfo.name] == nil then
							savedInstances[field.typeInfo.name] = {}
						end

						for k, v in ipairs(savedInstances[field.typeInfo.name]) do
							if i == v.originalInstance then
								print("---------------found instance----------")

								instance_copy[s_Name] = v.copyInstance

								goto done
							end
						end

						print("---------------adding instance----------")

						local i_copy = _G[field.typeInfo.name]()
						table.insert(savedInstances[field.typeInfo.name], {originalInstance = i, copyInstance = i_copy})

						
						self:CopyFields(i_copy, i, i.typeInfo)
						instance_copy[s_Name] = i_copy
						
						::done::

					end

				end
			end
		end
	end

	if typeInfo.super ~= nil then
		-- print('-------Found superclass--------')
		-- print('Super class: '.. typeInfo.super.name)
		if typeInfo.super.name ~= "DataContainer" then
			local i = _G[typeInfo.super.name]()
			-- local i_copy = _G[typeInfo.super.name]()
			self:CopyFields(instance_copy, instance, i.typeInfo)
		end

	end
end




function FirstToLower(str)
	return str:sub(1,1):lower()..str:sub(2)
end

function shallowcopy(orig)
		local orig_type = type(orig)
		local copy
		if orig_type == 'table' then
				copy = {}
				for orig_key, orig_value in pairs(orig) do
						copy[orig_key] = orig_value
				end
		else -- number, string, boolean, etc
				copy = orig
		end
		return copy
end

return InstanceCloner