class 'MVDIssueShared'


function MVDIssueShared:__init()
	print("Initializing MVDIssueShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function MVDIssueShared:RegisterVars()
	--self.m_this = that
end


function MVDIssueShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end


function MVDIssueShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeInfo.name == "MeshVariationDatabase") then
			local s_Instance = MeshVariationDatabase(l_Instance)
			if(s_Instance.name ~= "Levels/MP_018/MP_018/MeshVariationDb_Win32") then
				return
			end
			print("Found the entry")
			print("Entry count: " .. #s_Instance.entries)
		end

	end
end


g_MVDIssueShared = MVDIssueShared()

