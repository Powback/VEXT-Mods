class 'bundledebugShared'


function bundledebugShared:__init()
	print("Initializing bundledebugShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function bundledebugShared:RegisterVars()
	--self.m_this = that
end


function bundledebugShared:RegisterEvents()
	Hooks:Install('ResourceManager:LoadBundle',999, self, self.OnLoadBundle)
end

function bundledebugShared:OnLoadBundle(p_Hook, p_Bundle)
	print(string.format("Loading bundle '%s'", p_Bundle))
end

g_bundledebugShared = bundledebugShared()

