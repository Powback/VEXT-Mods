/**
 * Created by Powback on 22.08.2017.
 */
var effectList = document.getElementById("effects");

function SetEffect(effect) {
    console.log(effect)
    WebUI.Call('DispatchEventLocal', 'TweakUI:SetEffect', effect)
}

function AddEffect(p_Name) {
    var option = document.createElement("li");
    option.innerHTML = p_Name;
    effectList.appendChild(option);
    (function( $ ) {
        $(function() {
            $( "li", "#effects" ).sort(function( a, b ) {
                return $( a ).text() > $( b ).text();

            }).appendTo( "#effects" );

        });

    })( jQuery );

}

function SpawnEmitter() {
    WebUI.Call('DispatchEventLocal', 'TweakUI:SpawnEmitter')
}
$("#effects").on("click", "li", function(){
    SetEffect($(this).text());
});

$("#search").keyup(function () {
    var filter = jQuery(this).val();
    jQuery("#effects li").each(function () {
        if (jQuery(this).text().search(new RegExp(filter, "i")) < 0) {
            jQuery(this).hide();
        } else {
            jQuery(this).show()
        }
    });
});