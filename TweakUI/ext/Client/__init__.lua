class 'TweakUIClient'


function TweakUIClient:__init()
	print("Initializing TweakUIClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function TweakUIClient:RegisterVars()
	self.m_EffectTable = {}
	self.emitter = nil
	self.defaultEmitter = nil
	self.effect = nil

	self.emitterTime = 3

	self.currentTime = 0
	self.lastTime = 0

	self.emitterTransform = nil
end


function TweakUIClient:RegisterEvents()
	self.m_ExtensionLoadedEvent = Events:Subscribe('ExtensionLoaded', self, self.OnLoaded)
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	self.m_SetEffectEvent = Events:Subscribe('TweakUI:SetEffect', self, self.OnSetEffect)
	self.m_SetEffectEvent = Events:Subscribe('TweakUI:SpawnEmitter', self, self.OnSpawnEmitter)
	self.m_UpdateEvent = Events:Subscribe('Engine:Update', self, self.OnUpdate)
	self.m_ClientUpdateInputEvent = Events:Subscribe('Client:UpdateInput', self, self.OnUpdateInput)
end


function TweakUIClient:ReadInstance(p_Instance, p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	
	if p_Instance.typeName == "EffectBlueprint" then
		local s_Instance = EffectBlueprint(p_Instance)
		if(s_Instance.name == nil) then
			return
		end
		local s_Name = s_Instance.name
		self.m_EffectTable[s_Name] = s_Instance
		print(s_Name)
		self:SendBlueprint(s_Name)
	end
end

function TweakUIClient:OnSpawnEmitter(p_Transform)
	if self.effect == nil then
		print("No effect set")
		return
	end
	if(self.emitter ~= nil) then
		EffectManager:DisableEffects(self.emitter)
--		EffectManager:Clear()
	end

	local s_Player = PlayerManager:GetLocalPlayer()
	if s_Player == nil then
		return false
	end
	
	local s_Soldier = s_Player.soldier
	if s_Soldier == nil then
		return false
	end
	
	local s_Transform = s_Soldier.transform
	if(self.m_EffectTable[self.effect] == nil) then
		print("Effect does not exist")
		return
	end
	local effect = self.m_EffectTable[self.effect]
	print("Playing effect " .. self.effect)


	if p_Transform == nil then
		print("spawning with player transform")
		self.emitterTransform = s_Transform
	end

	local params = EffectParams()
	self.emitter = EffectManager:PlayEffect(effect, self.emitterTransform, params, false)
end
function TweakUIClient:OnUpdateInput(p_DeltaTime)
    if InputManager:WentKeyDown(InputDeviceKeys.IDK_F1) then
       -- WebUI:ExecuteJS('document.location.reload()')
    end

    if InputManager:WentKeyDown(InputDeviceKeys.IDK_F2) then
        WebUI:BringToFront()
        WebUI:EnableMouse()
        WebUI:Show()
    end

    if InputManager:WentKeyDown(InputDeviceKeys.IDK_F3) then
        --WebUI:BringToFront()
        WebUI:DisableMouse()
        WebUI:Hide()
    end
end
function TweakUIClient:OnUpdate(p_DeltaTime, p_SimulationDelta)
	
	if(self.emitter == nil) then
		return
	end
	

	self.currentTime = self.currentTime + p_DeltaTime

	if(self.currentTime > self.emitterTime) then
		print(tostring(self.currentTime))
		self.currentTime = 0
		print("Resetting effect")
		EffectManager:ResetEffect(self.emitter)		
	end
	
end

function TweakUIClient:SendBlueprint(p_Name) 
	WebUI:ExecuteJS(string.format('AddEffect(\"%s\")', p_Name))
end

function TweakUIClient:OnSetEffect(p_Name)
	print("Setting effect")
	print(p_Name)
	self.effect = p_Name
	self:OnSpawnEmitter(self.emitterTransform)
	print(tostring(self.emitterTransform))
end

function TweakUIClient:OnLoaded()
	-- Initialize our custom WebUI package.
	WebUI:Init()

	-- Show our custom WebUI package.
	WebUI:Show()
end


g_TweakUIClient = TweakUIClient()

return TweakUIClient
