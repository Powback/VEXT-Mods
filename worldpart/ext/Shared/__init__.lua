class 'worldpartShared'


function worldpartShared:__init()
	print("Initializing worldpartShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function worldpartShared:RegisterVars()
	self.referenceObject = nil
	self.worldPart = nil
	self.worldPartReference = nil
	self.m_Patched = 0
	self.m_Done = false
end


function worldpartShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
   	Events:Subscribe('Engine:Message', self, self.OnEngineMessage)
end

function worldpartShared:OnEngineMessage(p_Message)
	if p_Message.type == MessageType.ClientLevelFinalizedMessage then
		--self:OnModify(Realm.Realm_Client)
	end
	if p_Message.type == MessageType.ServerLevelLoadedMessage then
		--self:OnModify(Realm.Realm_ClientAndServer)
	end
	if p_Message.type == MessageType.ServerStaticModelSpawnMessage then
--		self.m_SpawnedStatic = self.m_SpawnedStatic + 1
	end
	if p_Message.type == MessageType.ServerGameplayCapturePointResetMessage then
		--print(self.m_SpawnedStatic)
	end
	
	if (p_Message.type == MessageType.CoreGameTimerMessage) then
		self.m_Patched = self.m_Patched + 1
	end
	if (self.m_Done == false and self.m_Patched == 2) or (p_Message.type == MessageType.ClientLevelLoadProgressMessage and self.m_Done == false) then
		--local s_WorldPartReference = self:CreateData("DAWGLEGO", self.m_ToSpawn)
		self:PatchLevel()--s_WorldPartReference)
		self.m_Done = true
	end
end

function worldpartShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	local s_Instances = p_Partition.instances
	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if l_Instance.typeInfo == LevelData.typeInfo then
            self.m_LevelData = LevelData(l_Instance)
            self.m_LevelData:MakeWritable()
        end
	end
end


function worldpartShared:GetWPDHighestIndex(object)
    if object.isLazyLoaded then
        return 0
    end
 
    local data = PrefabBlueprint(object)
    local highestIndex = 0
 
    for _, object in pairs(data.objects) do
        if object:Is('GameObjectData') then
            local gameObject = GameObjectData(object)
 
            if gameObject.indexInBlueprint > highestIndex then
                highestIndex = gameObject.indexInBlueprint
            end
        else
            print('Found unknown object in prefab: ' .. object.typeInfo.name)
        end
    end
 
    return highestIndex
end

function worldpartShared:CalculateIndexInBlueprint(data)
    local finalIndex = 0
 
    for _, object in pairs(data.objects) do
        if object.typeInfo == WorldPartReferenceObjectData.typeInfo then
            local referenceObjectData = WorldPartReferenceObjectData(object)
           
            if referenceObjectData.blueprint ~= nil then
                local index = self:GetWPDHighestIndex(referenceObjectData.blueprint)
       
                if index > finalIndex then
                    finalIndex = index
                end
            else
                print('Encountered WPROD with null blueprint: ' .. referenceObjectData.name)
            end
        else
            print('Encountered unknown object in data: ' .. object.typeInfo.name)
        end
    end
 
    return finalIndex
end

function worldpartShared:CreateData()
    if referenceObject ~= nil then
        return
    end
 
    -- Find data of the thing we need to spawn.
    local bp = ResourceManager:LookupDataContainer(
        ResourceCompartment.ResourceCompartment_Game,
        --'Objects/Vegetation/TreeParkChestnut_M_01/TreeParkChestnut_M_01'
        'Objects/ConcreteWall_01/ConcreteWall_01Short'
    )
 
    local variation = ResourceManager:LookupDataContainer(
        ResourceCompartment.ResourceCompartment_Game,
        'Levels/XP3_LeGrandVal/ObjectVariations/XP3_Shield_TreeParkChestnut_M_01'
    )
 
    variation = nil
 
    -- Create our reference object data.
    self.referenceObject = ReferenceObjectData()
    self.referenceObject.isEventConnectionTarget = 3
    self.referenceObject.isPropertyConnectionTarget = 3
    self.referenceObject.indexInBlueprint = -1
    self.referenceObject.blueprintTransform = LinearTransform()
    self.referenceObject.blueprintTransform.trans = Vec3(548.726563, 111.660820, 304.606903)
    self.referenceObject.blueprint = Blueprint(bp)
    self.referenceObject.objectVariation = variation
    self.referenceObject.streamRealm = StreamRealm.StreamRealm_None
    self.referenceObject.castSunShadowEnable = true
    self.referenceObject.excluded = false
 
    -- Create our world part data.
    self.worldPart = WorldPartData()
    self.worldPart.name = 'MyCustomData'
    self.worldPart.enabled = true
    self.worldPart.objects:add(self.referenceObject)
 
    -- Create our world part reference object data.
    self.worldPartReference = WorldPartReferenceObjectData()
    self.worldPartReference.isEventConnectionTarget = 3
    self.worldPartReference.isPropertyConnectionTarget = 3
    self.worldPartReference.indexInBlueprint = -1
    self.worldPartReference.blueprintTransform = LinearTransform()
    self.worldPartReference.blueprint = self.worldPart
    self.worldPartReference.streamRealm = StreamRealm.StreamRealm_None
    self.worldPartReference.castSunShadowEnable = true
    self.worldPartReference.excluded = false
end

function worldpartShared:PatchLevel()
    print('Patching level with custom spawn data.')
 
    local data = self.m_LevelData
    data:MakeWritable()
 
    -- Create our data if it doesn't exist.
    self:CreateData()
 
    -- Grab the registry container and make it writable.
    local registry = data.registryContainer
    registry:MakeWritable()
 
    -- Calculate the highest blueprint index.
    -- This is just to do things "properly". You can probably
    -- get away with just using a very high number.
    local highestIndex = self:CalculateIndexInBlueprint(data)
    print('Highest blueprint index: ' .. tostring(highestIndex))
 
    -- Patch our data.
    self.worldPartReference.indexInBlueprint = highestIndex + 1
    self.referenceObject.indexInBlueprint = highestIndex + 2
 
    -- Add necessary instance to the registry.
    registry.blueprintRegistry:add(self.worldPart)
    registry.blueprintRegistry:add(self.referenceObject.blueprint)
    registry.referenceObjectRegistry:add(self.worldPartReference)
    registry.referenceObjectRegistry:add(self.referenceObject)
 
    -- Add WPROD to the level.
    data.objects:add(self.worldPartReference)
 
    print('Finished patching level!')
end

g_worldpartShared = worldpartShared()

