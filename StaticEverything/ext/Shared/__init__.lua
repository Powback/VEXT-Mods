class 'StaticEverythingShared'


function StaticEverythingShared:__init()
	print("Initializing StaticEverythingShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function StaticEverythingShared:RegisterVars()
	--self.m_this = that
end


function StaticEverythingShared:RegisterEvents()
	--self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
	Hooks:Install('ServerEntityFactory:Create',999, self, self.OnEntityCreate)
	Hooks:Install('ClientEntityFactory:Create',999, self, self.OnEntityCreate)

end

function StaticEverythingShared:OnEntityCreate(p_Hook, p_Data, p_Transform)
	if p_Data == nil then
		print("Didnt get no data")
	else
		if(p_Data.typeInfo.name == "DynamicModelEntityData") then
			local s_OriginalObject = DynamicModelEntityData(p_Data)

			local s_Object = StaticModelEntityData()
			s_Object.isEventConnectionTarget = 3
			s_Object.isPropertyConnectionTarget = 3 
			s_Object.indexInBlueprint = 0

			s_Object.enabled = true
			s_Object.visible = true
			s_Object.mesh =  s_OriginalObject.mesh
			
			if(s_OriginalObject.mesh ~= nil) then
				print("The original object is NOT nil")
			end
			if(s_Object.mesh == nil) then
				print("The new object is nil??!")
			end

			local s_PartInfo = PhysicsPartInfo()
			s_Object.physicsPartInfos:add(s_PartInfo)

			local s_IndexRange = IndexRange()
			s_IndexRange.first = 4294967295
			s_IndexRange.last = 4294967295

			s_Object.networkInfo.partNetworkIdRanges:add(s_IndexRange)

			s_Object.components:add(s_OriginalObject.part)
			s_Object.runtimeComponentCount = 1


			for k,v in ipairs(s_Object.typeInfo.fields) do
				print(v.name .. ": " ..tostring(s_Object[firstToLower(v.name)]))
			end

			p_Hook:Pass(s_Object, p_Transform)

		end
	end	 
end

function StaticEverythingShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeInfo.name == "StaticModelEntityData") then
			self.m_Instance = StaticModelEntityData(l_Instance)
		end

		if(l_Instance.typeInfo.name == "ObjectBlueprint") then
			local s_Instance = ObjectBlueprint(l_Instance:Clone(l_Instance.instanceGuid))
			if(s_Instance.object.typeInfo.name == "DynamicModelEntityData") then
				print("________")


				local s_OriginalObject = DynamicModelEntityData(s_Instance.object)

				local s_Object = StaticModelEntityData()
				s_Object.isEventConnectionTarget = 3
				s_Object.isPropertyConnectionTarget = 3 
				s_Object.indexInBlueprint = 0

				s_Object.enabled = true
				s_Object.visible = true
				s_Object.mesh =  s_OriginalObject.mesh
				
				if(s_OriginalObject.mesh ~= nil) then
					print("The original object is NOT nil")
				end
				if(s_Object.mesh == nil) then
					print("The new object is nil??!")
				end

				local s_PartInfo = PhysicsPartInfo()
				s_Object.physicsPartInfos:add(s_PartInfo)

				local s_IndexRange = IndexRange()
				s_IndexRange.first = 4294967295
				s_IndexRange.last = 4294967295

				s_Object.networkInfo.partNetworkIdRanges:add(s_IndexRange)

				s_Object.components:add(s_OriginalObject.part)
				s_Object.runtimeComponentCount = 1


				s_Instance.object = s_Object
				print(l_Instance.instanceGuid)
				for k,v in ipairs(s_Object.typeInfo.fields) do
					print(v.name .. ": " ..tostring(s_Object[firstToLower(v.name)]))
				end
				p_Partition:ReplaceInstance(l_Instance, s_Instance, true)
			end
		end

	end
end

function firstToLower(str)
	return (str:gsub("^%L", string.lower))
end
g_StaticEverythingShared = StaticEverythingShared()

