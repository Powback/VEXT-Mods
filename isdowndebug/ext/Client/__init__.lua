class 'isdowndebugClient'


function isdowndebugClient:__init()
	print("Initializing isdowndebugClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function isdowndebugClient:RegisterVars()
	--self.m_this = that
end


function isdowndebugClient:RegisterEvents()
		Events:Subscribe('Client:UpdateInput', self, self.OnUpdate)
		

end


function isdowndebugClient:OnUpdate(p_Delta, p_SimulationDelta)
		if InputManager:IsDown(InputConceptIdentifiers.ConceptMoveFB) then
		print("IsDown: ConceptMoveFB")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptMoveLR) then
		print("IsDown: ConceptMoveLR")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptMoveForward) then
		print("IsDown: ConceptMoveForward")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptMoveBackward) then
		print("IsDown: ConceptMoveBackward")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptMoveLeft) then
		print("IsDown: ConceptMoveLeft")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptMoveRight) then
		print("IsDown: ConceptMoveRight")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptYaw) then
		print("IsDown: ConceptYaw")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptPitch) then
		print("IsDown: ConceptPitch")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptRoll) then
		print("IsDown: ConceptRoll")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptRecenterCamera) then
		print("IsDown: ConceptRecenterCamera")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptFire) then
		print("IsDown: ConceptFire")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptAltFire) then
		print("IsDown: ConceptAltFire")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptFireCountermeasure) then
		print("IsDown: ConceptFireCountermeasure")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptReload) then
		print("IsDown: ConceptReload")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptZoom) then
		print("IsDown: ConceptZoom")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptToggleCamera) then
		print("IsDown: ConceptToggleCamera")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSprint) then
		print("IsDown: ConceptSprint")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptCrawl) then
		print("IsDown: ConceptCrawl")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptToggleWeaponLight) then
		print("IsDown: ConceptToggleWeaponLight")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptJump) then
		print("IsDown: ConceptJump")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptCrouch) then
		print("IsDown: ConceptCrouch")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptCrouchOnHold) then
		print("IsDown: ConceptCrouchOnHold")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptProne) then
		print("IsDown: ConceptProne")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptInteract) then
		print("IsDown: ConceptInteract")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptPickUp) then
		print("IsDown: ConceptPickUp")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptDrop) then
		print("IsDown: ConceptDrop")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptBreathControl) then
		print("IsDown: ConceptBreathControl")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptParachute) then
		print("IsDown: ConceptParachute")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSwitchInventoryItem) then
		print("IsDown: ConceptSwitchInventoryItem")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSelectInventoryItem1) then
		print("IsDown: ConceptSelectInventoryItem1")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSelectInventoryItem2) then
		print("IsDown: ConceptSelectInventoryItem2")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSelectInventoryItem3) then
		print("IsDown: ConceptSelectInventoryItem3")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSelectInventoryItem4) then
		print("IsDown: ConceptSelectInventoryItem4")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSelectInventoryItem5) then
		print("IsDown: ConceptSelectInventoryItem5")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSelectInventoryItem6) then
		print("IsDown: ConceptSelectInventoryItem6")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSelectInventoryItem7) then
		print("IsDown: ConceptSelectInventoryItem7")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSelectInventoryItem8) then
		print("IsDown: ConceptSelectInventoryItem8")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSelectInventoryItem9) then
		print("IsDown: ConceptSelectInventoryItem9")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSwitchToPrimaryWeapon) then
		print("IsDown: ConceptSwitchToPrimaryWeapon")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSwitchToGrenadeLauncher) then
		print("IsDown: ConceptSwitchToGrenadeLauncher")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSwitchToStaticGadget) then
		print("IsDown: ConceptSwitchToStaticGadget")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSwitchToDynamicGadget1) then
		print("IsDown: ConceptSwitchToDynamicGadget1")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSwitchToDynamicGadget2) then
		print("IsDown: ConceptSwitchToDynamicGadget2")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptMeleeAttack) then
		print("IsDown: ConceptMeleeAttack")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptThrowGrenade) then
		print("IsDown: ConceptThrowGrenade")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptCycleFireMode) then
		print("IsDown: ConceptCycleFireMode")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptChangeVehicle) then
		print("IsDown: ConceptChangeVehicle")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptBrake) then
		print("IsDown: ConceptBrake")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptHandBrake) then
		print("IsDown: ConceptHandBrake")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptClutch) then
		print("IsDown: ConceptClutch")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptGearUp) then
		print("IsDown: ConceptGearUp")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptGearDown) then
		print("IsDown: ConceptGearDown")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptGearSwitch) then
		print("IsDown: ConceptGearSwitch")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptNextPosition) then
		print("IsDown: ConceptNextPosition")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSelectPosition1) then
		print("IsDown: ConceptSelectPosition1")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSelectPosition2) then
		print("IsDown: ConceptSelectPosition2")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSelectPosition3) then
		print("IsDown: ConceptSelectPosition3")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSelectPosition4) then
		print("IsDown: ConceptSelectPosition4")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSelectPosition5) then
		print("IsDown: ConceptSelectPosition5")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSelectPosition6) then
		print("IsDown: ConceptSelectPosition6")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSelectPosition7) then
		print("IsDown: ConceptSelectPosition7")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSelectPosition8) then
		print("IsDown: ConceptSelectPosition8")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptCameraPitch) then
		print("IsDown: ConceptCameraPitch")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptCameraYaw) then
		print("IsDown: ConceptCameraYaw")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptMapZoom) then
		print("IsDown: ConceptMapZoom")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptMapInnerZoom) then
		print("IsDown: ConceptMapInnerZoom")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptMapSize) then
		print("IsDown: ConceptMapSize")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptMapThreeDimensional) then
		print("IsDown: ConceptMapThreeDimensional")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptScoreboard) then
		print("IsDown: ConceptScoreboard")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptMenu) then
		print("IsDown: ConceptMenu")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSpawnMenu) then
		print("IsDown: ConceptSpawnMenu")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptCancel) then
		print("IsDown: ConceptCancel")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptCommMenu1) then
		print("IsDown: ConceptCommMenu1")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptCommMenu2) then
		print("IsDown: ConceptCommMenu2")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptCommMenu3) then
		print("IsDown: ConceptCommMenu3")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptAccept) then
		print("IsDown: ConceptAccept")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptDecline) then
		print("IsDown: ConceptDecline")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSelect) then
		print("IsDown: ConceptSelect")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptBack) then
		print("IsDown: ConceptBack")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptActivate) then
		print("IsDown: ConceptActivate")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptDeactivate) then
		print("IsDown: ConceptDeactivate")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptEdit) then
		print("IsDown: ConceptEdit")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptView) then
		print("IsDown: ConceptView")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptParentNavigateLeft) then
		print("IsDown: ConceptParentNavigateLeft")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptParentNavigateRight) then
		print("IsDown: ConceptParentNavigateRight")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptMenuZoomIn) then
		print("IsDown: ConceptMenuZoomIn")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptMenuZoomOut) then
		print("IsDown: ConceptMenuZoomOut")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptPanX) then
		print("IsDown: ConceptPanX")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptPanY) then
		print("IsDown: ConceptPanY")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptVoiceFunction1) then
		print("IsDown: ConceptVoiceFunction1")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSayAllChat) then
		print("IsDown: ConceptSayAllChat")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptTeamChat) then
		print("IsDown: ConceptTeamChat")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSquadChat) then
		print("IsDown: ConceptSquadChat")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSquadLeaderChat) then
		print("IsDown: ConceptSquadLeaderChat")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptToggleChat) then
		print("IsDown: ConceptToggleChat")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptQuicktimeInteractDrag) then
		print("IsDown: ConceptQuicktimeInteractDrag")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptQuicktimeFire) then
		print("IsDown: ConceptQuicktimeFire")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptQuicktimeBlock) then
		print("IsDown: ConceptQuicktimeBlock")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptQuicktimeFastMelee) then
		print("IsDown: ConceptQuicktimeFastMelee")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptQuicktimeJumpClimb) then
		print("IsDown: ConceptQuicktimeJumpClimb")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptQuicktimeCrouchDuck) then
		print("IsDown: ConceptQuicktimeCrouchDuck")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptFreeCameraMoveUp) then
		print("IsDown: ConceptFreeCameraMoveUp")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptFreeCameraMoveDown) then
		print("IsDown: ConceptFreeCameraMoveDown")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptFreeCameraMoveLR) then
		print("IsDown: ConceptFreeCameraMoveLR")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptFreeCameraMoveFB) then
		print("IsDown: ConceptFreeCameraMoveFB")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptFreeCameraRotateX) then
		print("IsDown: ConceptFreeCameraRotateX")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptFreeCameraRotateY) then
		print("IsDown: ConceptFreeCameraRotateY")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptFreeCameraIncreaseSpeed) then
		print("IsDown: ConceptFreeCameraIncreaseSpeed")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptFreeCameraDecreaseSpeed) then
		print("IsDown: ConceptFreeCameraDecreaseSpeed")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptFreeCameraFOVModifier) then
		print("IsDown: ConceptFreeCameraFOVModifier")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptFreeCameraChangeFOV) then
		print("IsDown: ConceptFreeCameraChangeFOV")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptFreeCameraSwitchSpeed) then
		print("IsDown: ConceptFreeCameraSwitchSpeed")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptFreeCameraTurboSpeed) then
		print("IsDown: ConceptFreeCameraTurboSpeed")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptFreeCameraActivator1) then
		print("IsDown: ConceptFreeCameraActivator1")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptFreeCameraActivator2) then
		print("IsDown: ConceptFreeCameraActivator2")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptFreeCameraActivator3) then
		print("IsDown: ConceptFreeCameraActivator3")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptFreeCameraMayaInputActivator) then
		print("IsDown: ConceptFreeCameraMayaInputActivator")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptTargetedCameraDistance) then
		print("IsDown: ConceptTargetedCameraDistance")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptTargetedCameraRotateX) then
		print("IsDown: ConceptTargetedCameraRotateX")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptTargetedCameraRotateY) then
		print("IsDown: ConceptTargetedCameraRotateY")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptTargetedCameraChangeSpeed) then
		print("IsDown: ConceptTargetedCameraChangeSpeed")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptLThumb) then
		print("IsDown: ConceptLThumb")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptRThumb) then
		print("IsDown: ConceptRThumb")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptToggleMinimapType) then
		print("IsDown: ConceptToggleMinimapType")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptUndefined) then
		print("IsDown: ConceptUndefined")
	end
	if InputManager:IsDown(InputConceptIdentifiers.ConceptSize) then
		print("IsDown: ConceptSize")
	end
end


g_isdowndebugClient = isdowndebugClient()

