class 'rmtweaksShared'


function rmtweaksShared:__init()
	print("Initializing rmtweaksShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function rmtweaksShared:RegisterVars()
	--self.m_this = that
end


function rmtweaksShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end


function rmtweaksShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeInfo.Name == "SoldierWeaponData") then
			local s_Instance = SoldierWeaponData(l_Instance:Clone(l_Instance.instanceGuid))
			for _, l_weaponState in ipairs(s_Instance.weaponStates) do
				local l_State = WeaponStateData(l_weaponState)
				l_State.alwaysAimHead = false
				l_State.animationConfiguration.shootModuleData.zoomedKickbackFactor = 10
				l_State.allowSwitchingToWeaponReloading = false
				l_State.redeployWhenSwitchingWeaponStates = false
				l_State.useQuickThrowOnAutomaticSwitchback = false
				l_State.canBeInSupportedShooting = true      
			end
			p_Partition:ReplaceInstance(l_Instance, s_Instance, true)
		end
		if(l_Instance.typeInfo.Name == "FiringFunctionData") then
			local s_Instance = FiringFunctionData(l_Instance:Clone(l_Instance.instanceGuid))
			s_Instance.shot.relativeTargetAiming = true
			s_Instance.shot.forceSpawnToCamera = false
			s_Instance.shot.spawnVisualAtWeaponBone = true
			s_Instance.shot.activeForceSpawnToCamera = true    
			p_Partition:ReplaceInstance(l_Instance, s_Instance, true)
		end

	end
end


g_rmtweaksShared = rmtweaksShared()

