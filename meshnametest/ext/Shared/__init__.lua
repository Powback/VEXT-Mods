class 'meshnametestShared'


function meshnametestShared:__init()
	print("Initializing meshnametestShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function meshnametestShared:RegisterVars()
	self.m_Blueprints = {}
	self.m_Meshes = {}
	self.m_Variations = {}
end


function meshnametestShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
	self.m_EngineMessageEvent = Events:Subscribe('Engine:Message', self, self.OnEngineMessage)

end


function meshnametestShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeInfo.super.name == "Blueprint") then
			local s_Instance = Blueprint(l_Instance)
			self.m_Blueprints[#self.m_Blueprints + 1] = s_Instance.name:lower()
		end
		if(l_Instance.typeInfo.super.name == "MeshAsset") then
			local s_Instance = MeshAsset(l_Instance)
			self.m_Meshes[s_Instance.name:lower()] = tostring(l_Instance.instanceGuid)
		end
		if(l_Instance.typeInfo.name == "MeshVariationDatabase") then
			local s_Instance = MeshVariationDatabase(l_Instance)
			for k, v in ipairs(s_Instance.entries) do

				local l_mvdEntry = MeshVariationDatabaseEntry(v)
				if(l_mvdEntry.mesh == nil) then
					return
				end
				local l_MeshGuid = tostring(l_mvdEntry.mesh.instanceGuid)

				if(self.m_Variations[l_MeshGuid] == nil) then
					self.m_Variations[l_MeshGuid] = {}
				end
				table.insert(self.m_Variations[l_MeshGuid], l_mvdEntry.variationAssetNameHash)
			end
		end  
	end
end



function meshnametestShared:OnEngineMessage(p_Message) 
	if p_Message.type == MessageType.ClientLevelLoadedMessage or
		p_Message.type == MessageType.ServerLevelLoadedMessage then 
		self:CheckTheory()
	end
end

function meshnametestShared:CheckTheory()
	for k, v in ipairs(self.m_Blueprints) do
		if(self.m_Meshes[v .. "_mesh"] == nil) then
			print("Missing: " .. v .. "_mesh")
		else
			local l_MeshGuid = self.m_Meshes[v .. "_mesh"]

			if(self.m_Variations[l_MeshGuid] ~= nil ) then

				print(v .. ": " .. dump(self.m_Variations[l_MeshGuid]))
			else 
				print("No variation for " .. v)
			end
		end
	end
end

function dump(o)
	if(o == nil) then
		print("tried to load jack shit")
	end
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end
g_meshnametestShared = meshnametestShared()

