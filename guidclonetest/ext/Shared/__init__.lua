class 'guidclonetestShared'


function guidclonetestShared:__init()
	print("Initializing guidclonetestShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function guidclonetestShared:RegisterVars()
	self.m_Instances = {}
end


function guidclonetestShared:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end


function guidclonetestShared:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeInfo == StaticModelEntityData.typeInfo) then
			local s_Instance = StaticModelEntityData(l_Instance:Clone(l_Instance.instanceGuid))
			if(self.m_Instances[tostring(s_Instance.instanceGuid)] ~= nil) then
				print("DUPLICATE GUID YOU STUPID FUCK: " .. tostring(s_Instance.instanceGuid))
			else
				self.m_Instances[tostring(s_Instance.instanceGuid)] = true
			end
		end
	end
end


local HAVOK_GUID_PREFIX = "ED170123"
local GUIDIndex = 0

function GenerateStaticGuid()
    GUIDIndex = GUIDIndex + 1
    return Guid(HAVOK_GUID_PREFIX.."-0000-0000-0000-"..GetFilledNumberAsString(GUIDIndex, 12), "D")
end

function GetFilledNumberAsString(n, stringLength)
    local n_string = tostring(n)
    local prefix = ""

    if string.len(n_string) < stringLength then
        for i=1,stringLength - string.len(n_string) do
            prefix = prefix .."0"
        end
    end

    return (prefix..n_string)
end


g_guidclonetestShared = guidclonetestShared()

