class 'EntityDebugClient'


function EntityDebugClient:__init()
	print("Initializing EntityDebugClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function EntityDebugClient:RegisterVars()
	self.entity = nil
	self.m_SkinnedMeshAssets = {}
	self.m_dyn = {}
	self.m_spawned = {}
	self.m_Head = nil
end


function EntityDebugClient:RegisterEvents()
	--self.m_EntityCreationEvent = Hooks:Install('ClientEntityFactory:Create',1, self, self.OnEntityCreate)
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	Hooks:Install('Partition:ReadInstance', 100, self, self.ReadInstance)
	self.m_EngineUpdateEvent = Events:Subscribe('Engine:Update', self, self.OnEngineUpdate)
end
function EntityDebugClient:ReadInstance(p_Instance, p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end

	if p_Instance.typeName == "DynamicModelEntityData" then
		print(tostring(p_Guid))
		table.insert(self.m_dyn, DynamicModelEntityData(p_Instance))
	end

	if p_Guid == Guid('FD6EBF18-6B04-41DD-A2D4-387B69A62857', 'D') then
		self.m_Head = SkinnedMeshAsset(p_Instance)
	end

end
function EntityDebugClient:OnEngineUpdate(p_Delta, p_SimDelta)
	local s_LocalPlayer = PlayerManager:GetLocalPlayer()
    if(s_LocalPlayer == nil) then
    	return
    end
	if s_LocalPlayer.soldier == nil then
		return
	end
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F1) then
		-- WebUI:ExecuteJS('document.location.reload()')
		self:SpawnDyn(s_LocalPlayer.soldier.transform)
	end

	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F2) then
		-- WebUI:ExecuteJS('document.location.reload()')
		self:MoveDyn(s_LocalPlayer.soldier.transform)
	end

end
function EntityDebugClient:SpawnDyn( p_Transform )
	p_Transform.trans.y = p_Transform.trans.y + 10
	self.m_dyn[1].mesh = self.m_Head
	self.m_spawned[1] = EntityManager:CreateClientEntity(self.m_dyn[1], p_Transform)
	--print("so far so good")

	if self.m_spawned[1] == nil then
		--print("Could not spawn explosion")
		return
	end
	self.m_spawned[1]:Init(Realm.Realm_Client, true)

	--print("spawned explosion")
end
function EntityDebugClient:MoveDyn( p_Transform )
	print(tostring(self.m_spawned[1]))
	local s_Spatial = SpatialEntity.new(self.m_spawned[1])
	s_Spatial.transform = p_Transform
end


function EntityDebugClient:OnEntityCreate(p_Hook, p_Data, p_Transform)

	if(p_Data.typeName == "EmitterEntityData" or p_Data.typeName == "SoundEffectEntityData" or p_Data.typeName == "EffectEntityData") then
		p_Hook:Next()
		return
	end

	print(string.format('Creating entity type "%s" (%f, %f, %f)', p_Data.typeName, p_Transform.trans.x, p_Transform.trans.y, p_Transform.trans.z))

	if (p_Data.typeName == "AntEventEntityData") then
		local s_Mesh = AntEventEntityData(p_Data)
		print(s_Mesh.transform)
		self.m_dyn[2] = s_Mesh
	    
	    p_Hook:Pass(p_Data, tempTransform)
	end
	p_Hook:Next()
end




g_EntityDebugClient = EntityDebugClient()

