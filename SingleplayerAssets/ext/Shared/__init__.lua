class 'SingleplayerAssetsShared'


function SingleplayerAssetsShared:__init()
	print("Initializing SingleplayerAssetsShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function SingleplayerAssetsShared:RegisterVars()
	self.reg = nil
	self.vlad = nil
	self.loadingMP = false
	self.SkinnedMeshAsset = {}
	self.UnlockAsset = {}
	self.MeshVariationDatabaseEntry = {}
	self.MeshVariationDatabase = {}
	self.vladUA = nil
	self.TextureAsset = {}
	self.assaultHeadUnlock = nil
	self.vladUnlockAsset = nil
	self.usAsssaultUnlockAsset = nil
	self.defcamo = nil
	self.spcole = nil
	self.vladhead = nil
end


function SingleplayerAssetsShared:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
	Hooks:Install('ResourceManager:LoadBundle', self, self.OnLoadBundle)
	Events:Subscribe('Level:LoadResources', self, self.OnLoadResources)
	self.m_EngineMessageEvent = Events:Subscribe('Engine:Message', self, self.OnEngineMessage)
end
function SingleplayerAssetsShared:OnEngineMessage(p_Message)
	if p_Message == nil then
		return
	end

	if p_Message.type == MessageType.ServerLevelLoadedMessage then
			self:ReplaceHead()
		return
	end
	return
end

function SingleplayerAssetsShared:LevelLoaded()

end
function SingleplayerAssetsShared:ReadInstance(p_Instance, p_Guid)
	if p_Instance == nil then
		return
	end

	if p_Instance.typeName == "SkinnedMeshAsset" then
		table.insert(self.SkinnedMeshAsset, SkinnedMeshAsset(p_Instance))
		local s_Instance = SkinnedMeshAsset(p_Instance)
		s_Instance.streamingEnable = false
	end
	if p_Instance.typeName == "UnlockAsset" then
		table.insert(self.UnlockAsset, SkinnedMeshAsset(p_Instance))
	end
	if p_Instance.typeName == "TextureAsset" then
		table.insert(self.TextureAsset, TextureAsset(p_Instance))
	end
	if p_Instance.typeName == "MeshVariationDatabaseEntry" then
		table.insert(self.MeshVariationDatabaseEntry, MeshVariationDatabaseEntry(p_Instance))
	end
	if p_Instance.typeName == "MeshVariationDatabase" then
		local s_Instance = MeshVariationDatabase(p_Instance)
		--print(string.format("MeshV DB: Count: %s | Guid: %s", s_Instance:GetEntriesCount(), p_Guid:ToString('D')))
		table.insert(self.MeshVariationDatabase, s_Instance)
	end
	

	if p_Instance.typeName == "RegistryContainer" then
	--	local s_Instance = RegistryContainer(p_Instance)
--		table.insert(self.reg, s_Instance)--
--		print(p_Guid:ToString('D'))

	end

	if(p_Guid == Guid('B5C8DFEC-7503-4771-8BCB-5F75DE529D89', 'D')) then
		self.assaultHeadUnlock = UnlockAsset(p_Instance)
	end
	
	if p_Guid == Guid('686C52F9-6A3A-E106-23FA-3D8095FAB61E', 'D') then
		self.vlad = SkinnedMeshAsset(p_Instance)
		--self.vlad.nameHash = 3856345854
		print("vlad")
	end

	if p_Guid == Guid('78C7EC53-1000-44C2-86FF-555445266F64', 'D') then
		self.vladUA = UnlockAsset(p_Instance)
	end
	if p_Guid == Guid("CD6E1919-1F87-A81D-67DC-4C3D3DE3409F", 'D') then
		print("Found vlad meshvariation")
	end
	if p_Guid == Guid("E24CBE7D-2933-4C11-D672-8E33E661E097", 'D') then
		print("Found vlad textures")
	end

	if p_Guid == Guid('EEBC7D5F-B8BA-A2C8-7C14-CE53212537EE', 'D') then
		self.reg = RegistryContainer(p_Instance)
		self:AddRegistry()
		self:AddMeshVar()
		print("RC Found")
	end

	if p_Guid == Guid('78C7EC53-1000-44C2-86FF-555445266F64', 'D') then
		print("Found vlad unlockasset")
		self.vladUnlockAsset = p_Instance
	end
	if p_Guid == Guid('F2ECBAB2-F00A-47CA-66DC-0F89C6A138D4','D') then
		print("Found US Assault unlockasset")
		self.usAsssaultUnlockAsset = p_Instance
	end

	if p_Guid == Guid('4BA49FBC-4E15-4669-8A3E-0A3F46F346BE','D') then
		self.defcamo = UnlockAsset(p_Instance)
	end
	if p_Guid == Guid('AA8968AA-9051-970E-2149-D78FEFAA937B','D') then
		self.spcole = SkinnedMeshAsset(p_Instance)
	end
	if p_Guid == Guid('686C52F9-6A3A-E106-23FA-3D8095FAB61E','D') then
		self.vladhead = SkinnedMeshAsset(p_Instance)
	end

end
function SingleplayerAssetsShared:AddRegistry() 
	print("Adding reg")
	local reg = self.reg
		for index, SkinnedMeshAsset in pairs(self.SkinnedMeshAsset) do
			reg:AddAssetRegistry(SkinnedMeshAsset)
			--print("Added Mesh")
		end
		for index, UnlockAsset in pairs(self.UnlockAsset) do
			reg:AddAssetRegistry(UnlockAsset)
			--print("Added unlockasset")
		end
		for index, TextureAsset in pairs(self.TextureAsset) do
			reg:AddAssetRegistry(TextureAsset)
			--print("Added TextureAsset")
		end
	print("Done adding reg")
	
end
function SingleplayerAssetsShared:AddMeshVar() 
	print("Adding MeshVariationDatabase")
	for index, reg in ipairs(self.MeshVariationDatabase) do
		--print(string.format("Initial count: %s", reg:GetEntriesCount()))
		for index, MeshVariationDatabaseEntry in pairs(self.MeshVariationDatabaseEntry) do
			reg:AddEntries(MeshVariationDatabaseEntry)
			--print("Added meshvariation")
		end
		--print(string.format("Final count: %s", reg:GetEntriesCount()))
	end
	print("Done adding MeshVariationDatabase")
end

function SingleplayerAssetsShared:ReplaceHead()
--[[	if(self.spcole == nil) then
		print("No cole")
	end
	if(self.vladhead == nil) then
		print("No vlad head")
	end
	local cole = self.spcole
	local vlad = self.vladhead
	print("Setting name")
	print(tostring(cole.name))
	print(tostring(vlad.name))
	print("Set name")
	cole.lodGroup = vlad.lodGroup
	cole:ClearMaterials()
	 local s_NodeCount = vlad:GetMaterialsCount()
 
    for i = s_NodeCount - 1, 0, -1 do
        local s_Node = vlad:GetMaterialsAt(i)
        cole:AddMaterialsAt(i, MeshMaterial(s_Node))	       
    end
	--cole.materials = vlad.materials
	cole.nameHash = vlad.nameHash
	]]
	self.usAsssaultUnlockAsset = self.vladUnlockAsset
	local assault  = UnlockAsset(self.usAsssaultUnlockAsset)
	local vlad = UnlockAsset(self.vladUnlockAsset)
	assault:ClearLinkedTo()
    assault:AddLinkedTo(UnlockAsset(vlad:GetLinkedToAt(0)))
    assault:AddLinkedTo(UnlockAsset(vlad:GetLinkedToAt(1)))
	print("Replaced??")
	--[[
	self.assaultHeadUnlock.debugUnlockId = "SP_RUS_Alexey_Fullbody01"
	self.assaultHeadUnlock.identifier = 2325852630
	self.assaultHeadUnlock.autoAvailable = true
	self.assaultHeadUnlock:ClearLinkedTo()
		if(self.vlad == nil) then
			print("NO VLAD!")
			return
		end
	self.assaultHeadUnlock:AddLinkedTo(self.vlad)
	print("Replaced vlad")
	print(tostring(self.assaultHeadUnlock:GetLinkedToCount()))
	]]
end

function SingleplayerAssetsShared:OnLoadResources(p_Dedicated)
	print("Loading level resources!")
	SharedUtils:MountSuperBundle('SpChunks')
	SharedUtils:MountSuperBundle('levels/sp_paris/sp_paris')
	SharedUtils:PrecacheBundle("levels/sp_paris/sp_paris")
	SharedUtils:PrecacheBundle("levels/sp_paris/corridorsluice")

	SharedUtils:MountSuperBundle('levels/sp_villa/sp_villa')
	SharedUtils:PrecacheBundle("levels/sp_villa/sp_villa")
	SharedUtils:PrecacheBundle("levels/sp_villa/basement")
	
end

function SingleplayerAssetsShared:OnLoadBundle(p_Hook, p_Bundle)
	print(string.format("Loading bundle '%s'", p_Bundle))


	local s_Name = p_Bundle:lower()
		if(string.match(s_Name, "mp_")) then
		self.loadingMP = true
	end
	return p_Hook:CallOriginal(p_Bundle)
end


g_SingleplayerAssetsShared = SingleplayerAssetsShared()

return SingleplayerAssetsShared
