class 'patches'

local m_Changed = false

function patches:__init()
	self:RegisterEvents()
  self.m_SPSolomon = nil
  self.m_SPSolomonVariation = nil
end

function patches:RegisterEvents()
	self.m_LevelLoadEvent = Events:Subscribe("Level:LoadResources", self, self.OnLoadResources)
  self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end

function patches:OnLoadResources(p_Dedicated)
  print("Loading level resources!")
  m_SPSolomon = nil
  m_SPSolomonVariation = nil
	SharedUtils:MountSuperBundle("Levels/SP_Finale/SP_Finale")
	SharedUtils:PrecacheBundle("Levels/SP_Finale/SP_Finale")
  SharedUtils:PrecacheBundle("Levels/SP_Finale/Fight_SUB")
end

function patches:ReadInstance(p_Instance, p_Guid)
	if p_Instance == nil then
		return
	end
  
  if p_Guid == Guid('1D023894-57C9-F7EF-9F00-695C9D47A767', 'D') then
    self.m_SPSolomon = SkinnedMeshAsset(p_Instance)
    --self.m_SPSolomon.name = "characters/heads/head06/head06_mp_Mesh"
    --self.m_SPSolomon.nameHash = 2069213429
  end
  
  if p_Guid == Guid('1B42E26B-ECF3-9013-05E3-7F1B4A4B82CA', 'D') then
    local s_Instance = RegistryContainer(p_Instance)
    local count = s_Instance:GetAssetRegistryCount()
    
    if self.m_SPSolomon == nil then return end
    
    s_Instance:SetAssetRegistryAt(838, self.m_SPSolomon)
    print("Registry changed")
    
  end
  
  
end


g_patches = patches()

return patches
