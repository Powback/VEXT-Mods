class 'TerrainTestClient'


function TerrainTestClient:__init()
	print("Initializing TerrainTestClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function TerrainTestClient:RegisterVars()
	
end


function TerrainTestClient:RegisterEvents()
	self.m_ClientUpdateInputEvent = Events:Subscribe('Client:UpdateInput', self, self.OnUpdateInput)
end

function TerrainTestClient:OnUpdateInput(p_Delta)
		local s_Setting = VisualTerrainSettings(ResourceManager:GetSettings("VisualTerrainSettings"))
		s_Setting.debugOverlayBrushCoverageMin = Vec2(1,1)
		s_Setting.renderMode = TerrainRenderMode.TerrainRenderMode_LayerCount3d
		s_Setting.debugOverlayGridSize = 10
		s_Setting.debugOverlayIsolineSpacing = 10
		s_Setting.debugOverlayBrushSize = 10
		s_Setting.drawDecal3dEnable = true
		s_Setting.drawTextureTileBoxesEnable = true
		s_Setting.drawEnable = true
		s_Setting.drawPatchBoxesEnable = true
		s_Setting.drawDebugTextEnable = true
		s_Setting.drawWaterEnable = true
		s_Setting.drawDebugTexturesEnable = true
		s_Setting.debugOverlayIsolinesEnable = true
		s_Setting.debugOverlayWireframeEnable = true
		s_Setting.debugOverlaySketchTextureEnable = true
		s_Setting.decalEnable = true
		s_Setting.drawPatchesEnable = true
		s_Setting.drawQuadtreeStatsEnable = true
		s_Setting.regenerateTexturesEnable = true
		s_Setting.dynamicMaskEnable = true
		s_Setting.gpuTextureCompressionEnable = false
		s_Setting.drawTextureDebugDepthComplexity = true
		s_Setting.drawMeshScatteringStatsEnable = true
		s_Setting.textureDrawTerrainLayersEnable = true
		s_Setting.textureForceUpdateEnable = true
		s_Setting.textureCompressFastAlgorithmEnable = false
		s_Setting.drawTextureDebugColors = true
		s_Setting.forcePatchRebuildEnable = true
		s_Setting.debugOverlayBrushEnable = true
		s_Setting.wireframeEnable = true
		s_Setting.drawVertexYTextureEnable = true
		s_Setting.editServiceEnable = true
		s_Setting.debugOverlayGridEnable = true
		s_Setting.drawMeshScatteringInstanceBoxesEnable = true



	
	local s_Reload = InputManager:IsDown(InputConceptIdentifiers.ConceptReload)
	if s_Reload == true then

	end
end

g_TerrainTestClient = TerrainTestClient()

