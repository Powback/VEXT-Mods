class 'SpectatorUI'

function SpectatorUI:__init()
	-- Install our hooks.
	--self.m_UIPushScreenHook = Hooks:Install('UI:PushScreen', self, self.OnPushScreen)
end

function SpectatorUI:Update(p_Delta, p_SimDelta)
	
end

function SpectatorUI:OnPushScreen(p_Hook, p_Screen, p_GraphPriority, p_ParentGraph)
	print(p_Screen.name)
	
	-- Check if this is one of the screens we override.
	if p_Screen.name ~= "UI/Flow/Screen/HudConquestScreen" and
	   p_Screen.name ~= "UI/Flow/Screen/HudMPScreen" and
	   p_Screen.name ~= "UI/Flow/Screen/HudGMScreen" and
	   p_Screen.name ~= "UI/Flow/Screen/HudRushScreen" and
	   p_Screen.name ~= "UI/Flow/Screen/HudSDMScreen" and
	   p_Screen.name ~= "UI/Flow/Screen/HudScreen" and
	   p_Screen.name ~= "UI/Flow/Screen/HudTDMScreen" then
		return p_Hook:CallOriginal(p_Screen, p_GraphPriority, p_ParentGraph)
   	end

	local s_NodeCount = p_Screen:GetNodesCount()

	-- Iterate through all the nodes.
	for i = s_NodeCount - 1, 0, -1 do
		local s_Node = p_Screen:GetNodesAt(i)

		-- Remove the ones we don't need.
		if s_Node ~= nil then
			if s_Node.name == "HudOutOfBoundsAlertMessage" or
			   s_Node.name == "Ammo" or
			   s_Node.name == "VehicleHealth" or
			   s_Node.name == "PassangerList" or
			   s_Node.name == "Health" or
			   s_Node.name == "SquadList" or
			   s_Node.name == "Compass" or
			   s_Node.name == "Minimap" or
			   s_Node.name == "MapmarkerManager" or
			   s_Node.name == "HudBackgroundWidget" or
			   s_Node.name == "ObjectiveBar" or
			   s_Node.name == "TicketCounter" then
			   	p_Screen:RemoveNodesAt(i)
			end
		end
	end

	-- Call the original function.
	return p_Hook:CallOriginal(p_Screen, p_GraphPriority, p_ParentGraph)
end

return SpectatorUI