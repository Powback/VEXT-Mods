local AdvancedChatHooks = require 'advanced-chat/hooks'
local AdvancedChatMessages = require 'advanced-chat/messages'

class 'AdvancedChat'

function AdvancedChat:__init()
	print("Initializing Advanced Chat!")

	-- Subscribe to events.
	self.m_ChatMessageEvent = Events:Subscribe('AC:SendChatMessage', self, self.OnSendChatMessage)

	-- Initialize the other components.
	self.m_Hooks = AdvancedChatHooks()
	self.m_Messages = AdvancedChatMessages()
end

function AdvancedChat:OnSendChatMessage(p_Contents)
	-- Get the target of the message and the message itself.
	local s_Target = p_Contents:match("^([a-z]+):.*$")
	local s_Message = p_Contents:match("^[a-z]+:(.*)$")

	-- Trim the message.
	local s_From = s_Message:match"^%s*()"
 	s_Message = s_From > #s_Message and "" or s_Message:match(".*%S", s_From)

	-- Ignore if the message is empty.
	if s_Message:len() == 0 then
		return
	end

	-- Get the local player.
	local s_LocalPlayer = PlayerManager:GetLocalPlayer()

	-- We can't send a message if we don't have an active player.
	if s_LocalPlayer == nil then
		return
	end
	
	-- Spectator Crash Fix, spectators can't use use squad or team
	if SpectatorManager:GetCameraMode() ~= SpectatorCameraMode.Disabled then
		ChatManager:SendMessage(s_Message)
		return
	end

	-- Dispatch message based on the specified target.
	if s_Target == 'all' then
		ChatManager:SendMessage(s_Message)
		return
	end

	if s_Target == 'team' then
		ChatManager:SendMessage(s_Message, s_LocalPlayer.teamID)
		return
	end

	if s_Target == 'sqd' then
		ChatManager:SendMessage(s_Message, s_LocalPlayer.teamID, s_LocalPlayer.squadID)
		return
	end

	return
end

function AdvancedChat:Update(p_Delta)
	-- We do nothing here.
end

return AdvancedChat