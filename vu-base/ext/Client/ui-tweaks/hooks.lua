class 'UITweaksHooks'

function UITweaksHooks:__init()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.OnReadInstance)
	
	self.m_PrivacyButtonGuid = Guid("E47CD95C-EAF7-4AC5-8129-0B379BE3462C", "D")
	self.m_ChassisGuid = Guid("D959DF11-EA30-D238-A0D5-5687735963E4", "D")
	
	
end

function UITweaksHooks:OnReadInstance(p_Instance, p_Guid)
	if p_Instance == nil then
		return
	end
	
	if p_Guid == self.m_PrivacyButtonGuid then
		print("Found Privacy WidgetNode")
		local s_Node = WidgetNode(p_Instance)
		s_Node.verticalAlign = WidgetVerticalAlignment.WVA_Top
	end
	
	if p_Guid == self.m_ChassisGuid then
		local s_Chassis = ChassisComponentData(p_Instance)
		local s_Transform = s_Chassis.transform
		
		print("Transform: %f %f %f", s_Transform.trans.x, s_Transform.trans.y, s_Transform.trans.z)
	end
end

return UITweaksHooks