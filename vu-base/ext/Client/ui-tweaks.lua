local UITweaksHooks = require 'ui-tweaks/hooks'

class 'UITweaks'

function UITweaks:__init()
	print("Initializing UI Tweaks!")
	
	self.m_Hooks = UITweaksHooks()
end

return UITweaks