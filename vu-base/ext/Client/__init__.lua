class 'VeniceUnleashed'

local AdvancedChat = require 'advanced-chat'
local VeniceSpectator = require 'venice-spectator'
local UITweaks = require 'ui-tweaks'

function VeniceUnleashed:__init()
	print("Initializing the Venice Unleashed Mod Base!")

	-- Initialize all our components.
	self.m_AdvancedChat = AdvancedChat()
	self.m_Spectator = VeniceSpectator()
	self.m_UITweaks = UITweaks()

	-- Subscribe to events.
	self.m_ExtensionLoadedEvent = Events:Subscribe('ExtensionLoaded', self, self.OnLoaded)
	self.m_EngineUpdateEvent = Events:Subscribe('Engine:Update', self, self.OnUpdate)
end

function VeniceUnleashed:OnLoaded()
	-- Initialize our custom WebUI package.
	WebUI:Init()

	-- Show our custom WebUI package.
	WebUI:Show()
end

function VeniceUnleashed:OnUpdate(p_Delta, p_SimDelta)
	self.m_Spectator:Update(p_Delta, p_SimDelta)
end

-- Initialize the Base.
g_VUBase = VeniceUnleashed()