class 'NadeModShared'


function NadeModShared:__init()
	print("Initializing NadeModShared")
	self:RegisterVars()
	self:RegisterEvents()
	self.smokenade = nil
	self.nade = nil
	self.nadeSet = false
end



function NadeModShared:RegisterVars()
	--local m_this = that
end


function NadeModShared:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end


function NadeModShared:ReadInstance(p_Instance, p_Guid)
	if p_Instance == nil then
		return
	end
	

	if p_Guid == Guid("48BBE181-231E-4E7F-A959-10ECA1BCAF57", 'D') then --smoke
		print("Got smoke")
		local s_Instance = VeniceExplosionEntityData(p_Instance)
		self.smoke = s_Instance.detonationEffect
	end
	if p_Guid == Guid("0E0932A0-E8EF-4037-983B-F35A6F7FEEE5", 'D') then --nade
		print("Got nade")
		local s_Instance = VeniceExplosionEntityData(p_Instance)
		s_Instance.detonationEffect = EffectBlueprint(self.smoke)
		print("Set smoke")
		self.nade = s_Instance
	end

	if(self.nade ~= nil and self.smokenade ~= nil and self.nadeSet == false) then
		print("Setting smoke")
		self.nade.detonationEffect = EffectBlueprint(self.smokenade)
		self.nadeSet = true
		
	end
	
	
end


g_NadeModShared = NadeModShared()

return NadeModShared
