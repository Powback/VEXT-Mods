class 'lookaroundClient'


function lookaroundClient:__init()
	print("Initializing lookaroundClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function lookaroundClient:RegisterVars()
	self.instance = nil
	self.issueShown = false
	self.partitionCount = 0
end


function lookaroundClient:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end

function lookaroundClient:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end


		if(l_Instance.typeInfo.name == "LevelReportingAsset") then
			self.instance = LevelReportingAsset(l_Instance:Clone())
			print("Is the cloned instance readonly? " .. tostring(self.instance.isReadOnly))
			print("Instance partition: " .. self.partitionCount)
			print(self.instance)
			p_Partition:ReplaceInstance(l_Instance, self.instance, true)
			print("Is the cloned instance readonly? " .. tostring(self.instance.isReadOnly))
			print("Instance partition: " .. self.partitionCount)
		end
		if(self.instance ~= nil and self.instance.isReadOnly == true and self.issueShown == false) then
			print("The issue is readonly...")
			print("Instance partition: " .. self.partitionCount)
			print(self.instance)
			self.issueShown = true
		end
	end

	self.partitionCount = self.partitionCount + 1
end

g_lookaroundClient = lookaroundClient()

