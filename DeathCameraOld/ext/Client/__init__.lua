class 'DeathCameraClient'

function DeathCameraClient:__init()
	self:InitComponents()
end

function DeathCameraClient:InitComponents()
	self.types = {}
	self.m_EngineUpdateEvent = Events:Subscribe('Engine:Update', self, self.OnEngineUpdate)
	Hooks:Install('ClientEntityFactory:Create', 9, self, self.OnEntityCreate)
	self.cameras = {}
	self.entities = {}
	self.current = 1
end

function DeathCameraClient:OnEngineUpdate(p_Delta, p_SimDelta)
local s_LocalPlayer = PlayerManager:GetLocalPlayer()
    if(s_LocalPlayer == nil) then
    	return
    end
	if s_LocalPlayer.soldier == nil then
		return
	end
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F1) then
		-- WebUI:ExecuteJS('document.location.reload()')
		self:StartSequence()
	end
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F2) then
		self:StopSequence()
	end
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F3) then
		self:ResetSequence()
	end
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F4) then
		self:Increment()
	end
	if InputManager:WentKeyDown(InputDeviceKeys.IDK_F5) then
		self:Decrement()
	end
end

function DeathCameraClient:OnEntityCreate(p_Hook, p_Data, p_Transform)
	if p_Data == nil then
		print("Didnt get no data")
	else
		if(self.types[p_Data.typeName] == nil) then
			print(p_Data.typeName)
			self.types[p_Data.typeName] = true
		end
		if p_Data.typeName == "SequenceEntityData" then
			local s_Guid = p_Data.instanceGuid

			if s_Guid == Guid('1FFE39E6-5A0D-4819-9535-B1200964C5CE') or
			s_Guid == Guid('D193C7DB-CFE9-41FA-8112-6FE0BD08D59C') or
			s_Guid == Guid('88B82033-4E34-46B3-AE14-2E50CF1C8300') or
			s_Guid == Guid('3D06D784-744C-4405-B271-E1544E248F4D') or
			s_Guid == Guid('5C57D80A-DC4F-485B-9EF8-E71CCFE71494') or
			s_Guid == Guid('4F8FB7D8-8ED9-4D00-8E97-B17D66DF497B') or
			s_Guid == Guid('2B77D0BE-3E8C-498B-910F-0769BBDEAF45') or

			s_Guid == Guid('E134EA13-7312-4ACC-B535-93C8A41BCE99') or
			s_Guid == Guid('745363B8-5AE4-4CEC-AE39-76C6311E7AD0')



			then
				return
			end
			local x = p_Hook:Call()
			print(p_Data.instanceGuid)
			p_Hook:Return(x)
			table.insert(self.entities, x)
		end
	end 
end

function DeathCameraClient:StartSequence()
	for k, v in ipairs(self.entities) do
		v:FireEvent("Start")
	end

end

function DeathCameraClient:ResetSequence()
	for k, v in ipairs(self.entities) do
		v:FireEvent("Reset")
	end
end

function DeathCameraClient:StopSequence()
	for k, v in ipairs(self.entities) do
		v:FireEvent("Stop")
	end
end
function DeathCameraClient:Increment()
	print(#self.entities)
	self.current = self.current + 1
	print(self.current)

end
function DeathCameraClient:Decrement()
	self.current = self.current - 1
	print(self.current)

end

function DeathCameraClient:Enable()
	local s_Iterator = EntityManager:GetClientIterator('SequenceEntity')

	if s_Iterator == nil then
		print('Failed to get camera iterator')
	else
		local s_Entity = s_Iterator:Next()

		while s_Entity ~= nil do
			s_Entity:FireEvent("Enable") -- Enable
			s_Entity = s_Iterator:Next()
		end
	end
end

function DeathCameraClient:Disable()
	local s_Iterator = EntityManager:GetClientIterator('SequenceEntity')

	if s_Iterator == nil then
		print('Failed to get camera iterator')
	else
		local s_Entity = s_Iterator:Next()

		while s_Entity ~= nil do
			s_Entity:FireEvent("Disable") -- Disable
			s_Entity = s_Iterator:Next()
		end
	end
end


local DeathCameraClient = DeathCameraClient()