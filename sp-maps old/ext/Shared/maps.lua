class('Maps')

function Maps:__init()
	self.m_Maps = {}

	self:RegisterMaps()
	self:RegisterEvents()
end

function Maps:RegisterMaps()
	table.insert(self.m_Maps, require('__shared/maps/sp_villa')())
end

function Maps:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.OnReadInstance)
	self.m_LoadResourcesEvent = Events:Subscribe('Level:LoadResources', self, self.OnLoadResources)
end

function Maps:OnLoadResources(p_Dedicated)
	for i, map in ipairs(self.m_Maps) do
		map:OnLoadResources(p_Dedicated)
	end
end

function Maps:OnReadInstance(p_Instance, p_Guid)
	for i, map in ipairs(self.m_Maps) do
		map:OnReadInstance(p_Instance, p_Guid)
	end	
end

return Maps