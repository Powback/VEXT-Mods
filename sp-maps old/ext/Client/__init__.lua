local Maps = require('__shared/maps')
g_Maps = Maps()

function OnUpdate(p_Delta)
	local s_Player = PlayerManager:GetLocalPlayer()

	if s_Player ~= nil and s_Player.soldier ~= nil then
		local s_Transform = s_Player.soldier.transform
		
		if InputManager:WentKeyDown(InputDeviceKeys.IDK_V) then
			print(string.format("(%f, %f, %f) (%f, %f, %f) (%f, %f, %f) (%f, %f, %f)",
				s_Transform.left.x, s_Transform.left.y, s_Transform.left.z,
				s_Transform.up.x, s_Transform.up.y, s_Transform.up.z,
				s_Transform.forward.x, s_Transform.forward.y, s_Transform.forward.z,
				s_Transform.trans.x, s_Transform.trans.y, s_Transform.trans.z
			))
		end
	end
end

Events:Subscribe('Engine:Update', OnUpdate)