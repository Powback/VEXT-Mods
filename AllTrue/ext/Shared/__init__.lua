class 'AllTrueShared'


function AllTrueShared:__init()
	print("Initializing AllTrueShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function AllTrueShared:RegisterVars()
	--self.m_this = that
end


function AllTrueShared:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end


function AllTrueShared:ReadInstance(p_Instance,p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	
	if p_Instance.typeName == "BoolEntityData" then
		local s_Instance = BoolEntityData(p_Instance)
		s_Instance.defaultValue = true
	end
	if p_Instance.typeName == "BoolToEventEntityData" then
		local s_Instance = BoolToEventEntityData(p_Instance)
		s_Instance.value = true
	end


end


g_AllTrueShared = AllTrueShared()

