class 'alphatestClient'


function alphatestClient:__init()
	print("Initializing alphatestClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function alphatestClient:RegisterVars()
	self.m_SomeTexture = nil
end


function alphatestClient:RegisterEvents()
	self.m_PartitionLoadedEvent = Events:Subscribe('Partition:Loaded', self, self.OnPartitionLoaded)
end


function alphatestClient:OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end
		if(l_Instance.typeInfo.name == "PhysicsEntityData") then
			local s_Instance = PhysicsEntityData(l_Instance:Clone(l_Instance.instanceGuid))
			s_Instance.movableParts = true
			p_Partition:ReplaceInstance(l_Instance, s_Instance, true)
		end
	end
end


g_alphatestClient = alphatestClient()

