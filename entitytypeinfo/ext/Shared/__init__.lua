class 'entitytypeinfoShared'


function entitytypeinfoShared:__init()
	print("Initializing entitytypeinfoShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function entitytypeinfoShared:RegisterVars()
	--self.m_this = that
end


function entitytypeinfoShared:RegisterEvents()
	Hooks:Install('ClientEntityFactory:Create',999, self, self.OnEntityCreate)
	Hooks:Install('ServerEntityFactory:Create',999, self, self.OnEntityCreate)
end


function entitytypeinfoShared:OnEntityCreate(p_Hook, p_Data, p_Transform)
    if p_Data == nil then
        print("Didnt get no data")
    else
        local s_Entity = p_Hook:Call(p_Data, p_Transformz)
        print(p_Data.typeInfo.name)
        if(s_Entity == nil) then
        	print("skip")
        	return
        end
        print(s_Entity.typeInfo.name)
    end
end

g_entitytypeinfoShared = entitytypeinfoShared()

