class 'TweakUIClient'
local Type = require 'type'
function TweakUIClient:__init()

	print("Initializing TweakUIClient")
	self:RegisterVars()
	self:RegisterEvents()
	self.Type = Type()
end
function TweakUIClient:RegisterVars()
	self.m_CursorMode = false
	self.m_Variables = {}
	self.m_instances = {}
end

function TweakUIClient:RegisterEvents()
	self.m_EnableCursorModeHook = Hooks:Install('UI:EnableCursorMode', self, self.OnEnableCursorMode)
	
	--self.m_StateAddedEvent = Events:Subscribe('VE:StateAdded', self, self.OnStateAdded)
	--self.m_StateRemovedEvent = Events:Subscribe('VE:StateRemoved', self, self.OnStateRemoved)
    --self.m_InputConceptEventHook = Hooks:Install('UI:InputConceptEvent', self, self.OnInputConceptEvent)
    self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
    self.m_OnLoadedEvent = Events:Subscribe('ExtensionLoaded', self, self.OnLoaded)
    self.m_OnUpdateInputEvent = Events:Subscribe('Client:UpdateInput', self, self.OnUpdateInput)
    --self.m_OnUpdateEvent = Events:Subscribe('Engine:Update', self, self.OnUpdate)


    self.m_OnWebUIUpdateEvent = Events:Subscribe('CT:UpdateValue', self, self.OnUpdateValue)


end
function TweakUIClient:ReadInstance(p_Instance, p_Guid)
	if p_Guid == Guid('1CAE0E91-1EC1-48B9-8FFE-90EDA105FFB4', 'D') then
		self.m_instances[p_Guid:ToString("D")] = FiringFunctionData(p_Instance)
		Type:FiringFunctionData("M16A4", p_Instance, p_Guid:ToString('D'))
	end
end

function TweakUIClient:OnUpdateValue(p_Contents)
	
	WebUI:ExecuteJS(string.format('Debug("Server got data: %s")', p_Contents))
	local s_Content = split(p_Contents, ":")
	local s_guid = s_Content[1]
	local s_class = s_Content[2] -- ColorCorrection
	local s_key = s_Content[3] -- Contrast
	local s_type = s_Content[4] -- Vec3
	local s_val = tostring(s_Content[5])
	WebUI:ExecuteJS(string.format('Debug("Server: %s")', p_Contents))



	
	local m_class = self.m_instances[s_guid] --colorCorrection
	if m_class ~= nil then
		
		
		local s_object = m_class[s_key] --colorCorrection Contrast
		
		if(s_type == "float") then 
			m_class[s_key] = tonumber(s_Content[5])
		end
		if(s_type == "bool") then
			if(s_val == "true") then
				m_class[s_key] = true
				WebUI:ExecuteJS(string.format('Debug("True: %s")', p_val))
			else
				m_class[s_key] = false
				WebUI:ExecuteJS(string.format('Debug("False: %s")', p_val))
			end
		end
		if(s_type == "Vec2") then -- Vec2
			local s_var1 = s_Content[4] --x
			local s_val1 = tonumber(s_Content[5]) --1
			local s_var2 = s_Content[6] --y
			local s_val2 = tonumber(s_Content[7]) --1
			m_class[s_key] = Vec2(s_val1,s_val2)
			WebUI:ExecuteJS(string.format('Debug("Debug! %s %s %s %s ")', s_var1, s_val1, s_var2, s_val2))
		end		
		if(s_type == "Vec3") then -- Vec3

			local s_var1 = s_Content[4] --x
			local s_val1 = tonumber(s_Content[5]) --1
			local s_var2 = s_Content[6] --y
			local s_val2 = tonumber(s_Content[7]) --1
			local s_var3 = s_Content[8] --z
			local s_val3 = tonumber(s_Content[9]) --1
			m_class[s_key] = Vec3(s_val1,s_val2,s_val3)
			WebUI:ExecuteJS(string.format('Debug("Debug! %s %s %s %s %s %s")', s_var1, s_val1, s_var2, s_val2, s_var3, s_val3))
		end		
		if(s_type == "Vec4") then -- Vec4

			local s_var1 = s_Content[4] --x
			local s_val1 = tonumber(s_Content[5]) --1
			local s_var2 = s_Content[6] --y
			local s_val2 = tonumber(s_Content[7]) --1
			local s_var3 = s_Content[8] --z
			local s_val3 = tonumber(s_Content[9]) --1
			local s_var4 = s_Content[10] --w
			local s_val4 = tonumber(s_Content[11]) --1
			m_class[s_key] = Vec4(s_val1,s_val2,s_val3,s_val4)
			WebUI:ExecuteJS(string.format('Debug("Debug! %s %s %s %s %s %s %s %s")', s_var1, s_val1, s_var2, s_val2, s_var3, s_val3, s_var4, s_val4))
		end			
		
		WebUI:ExecuteJS(string.format('Debug("Serv3232er: %s")', p_Contents))
	end
	
end

function TweakUIClient:OnLoaded()
    WebUI:Init()
end

function TweakUIClient:OnUpdateInput(p_Delta)
    
    if InputManager:WentKeyDown(InputDeviceKeys.IDK_F1) then
        WebUI:ExecuteJS('document.location.reload()')
    end

    if InputManager:WentKeyDown(InputDeviceKeys.IDK_F2) then
        WebUI:BringToFront()
        WebUI:EnableMouse()
        WebUI:Show()
    end

    if InputManager:WentKeyDown(InputDeviceKeys.IDK_F3) then
        --WebUI:BringToFront()
        WebUI:DisableMouse()
        WebUI:Hide()
    end
end

function TweakUIClient:OnEnableCursorMode(p_Hook, p_Enable, p_Cursor)
    -- Here we store the current cursor mode as requested by the
    -- engine in order to restore it later on.
    self.m_CursorMode = p_Enable
    p_Hook:CallOriginal(p_Enable, p_Cursor)
end



-- Copy pasta'd from http://www.computercraft.info/forums2/index.php?/topic/930-lua-string-split/page__p__93664#entry93664
function split(pString, pPattern)
   local Table = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pPattern
   local last_end = 1
   local s, e, cap = pString:find(fpat, 1)
   while s do
	  if s ~= 1 or cap ~= "" then
	 table.insert(Table,cap)
	  end
	  last_end = e+1
	  s, e, cap = pString:find(fpat, last_end)
   end
   if last_end <= #pString then
	  cap = pString:sub(last_end)
	  table.insert(Table, cap)
   end
   return Table
end

g_TweakUIClient = TweakUIClient()

return TweakUIClient
