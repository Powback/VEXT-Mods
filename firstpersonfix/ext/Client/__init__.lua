class 'firstpersonfixClient'


function firstpersonfixClient:__init()
	print("Initializing firstpersonfixClient")
	self:RegisterVars()
	self:RegisterEvents()
end


function firstpersonfixClient:RegisterVars()
	--self.m_this = that
end


function firstpersonfixClient:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end


function firstpersonfixClient:ReadInstance(p_Instance,p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	
	if p_Guid == Guid("83CB9E70-3D9A-4C34-B8D6-787CEADBD8A3", "D") then
		local s_Instance = AntAnimatableComponentData(p_Instance)
		s_Instance.someThing = true
	end
end


g_firstpersonfixClient = firstpersonfixClient()

