class 'camerarotationClient'


function camerarotationClient:__init()
    print("Initializing camerarotationClient")
    self:RegisterVars()
    self:RegisterEvents()
end


function camerarotationClient:RegisterVars()
    self.m_TempSoldier = nil
    self.m_MySoldier = nil
end


function camerarotationClient:RegisterEvents()
   -- Hooks:Install('UI:PushScreen', 999, self, self.OnPushScreen)
   -- Hooks:Install('ClientEntityFactory:Create', 9, self, self.OnEntityCreate)
    Events:Subscribe('Engine:Update', self, self.OnUpdate)
end

function camerarotationClient:OnUpdate(p_Delta, p_SimulationDelta)
    local s_Player = PlayerManager:GetLocalPlayer()
    if s_Player == nil then
        return
    end
    if s_Player.soldier == nil then
        return
    end
    local s_Soldier = s_Player.soldier
    print(tostring(s_Soldier.authorativePitch .. " | " .. s_Soldier.authorativeYaw))
 --[[
    if self.m_MySoldier ~= nil then
    print(tostring(self.trans))
        print("Found soldier")
        local s_Body = SoldierBodyComponentData(self.m_MySoldier:GetComponentsAt(0))
        print("got bbody")

        
       -- local s_WCP = SoldierWeaponsComponentData(s_Body:GetComponentsAt(5))
       -- print(tostring(s_WCP.transform))
    end
    ]]
end


function camerarotationClient:OnPushScreen(p_Hook, p_Screen, p_GraphPriority, p_ParentGraph)
    if p_Screen == nil then
        p_Hook:Next()
    end
     --[[

    local s_Screen = UIScreenAsset(p_Screen)
    print(tostring(s_Screen.name:lower()))
    if (s_Screen.name == "UI/Flow/Screen/HudScreen") then
        print("spawning")
        if (self.m_TempSoldier ~= nil) then
            print("Adding tempsoldier as soldier")
            self.m_MySoldier = self.m_TempSoldier
        end
    end
    if (s_Screen.name == "UI/Flow/Screen/KillScreen") then
        print("removing my soldier")
        self.m_MySoldier = nil
    end
    --]]
    p_Hook:Next()
end

function camerarotationClient:OnEntityCreate(p_Hook, p_Data, p_Transform)

    if (p_Data.typeName == "SoldierEntityData") then
        print("Adding tempsoldier")
        self.m_TempSoldier = SoldierEntityData(p_Data)
    end

end


g_camerarotationClient = camerarotationClient()

