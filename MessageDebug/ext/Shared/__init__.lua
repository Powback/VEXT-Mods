class 'MessageDebugShared'


function MessageDebugShared:__init()
	print("Initializing MessageDebugShared")
	self:RegisterEvents()
end


function MessageDebugShared:RegisterEvents()
	self.m_EngineMessageEvent = Events:Subscribe('Engine:Message', self, self.OnEngineMessage)
end

function MessageDebugShared:OnEngineMessage(p_Message) 
	if p_Message.type == MessageType.ClientAbortCutsceneMessage then 
		print("ClientAbortCutsceneMessage")
	end
	if p_Message.type == MessageType.ClientActiveLicensesMessage then 
		print("ClientActiveLicensesMessage")
	end
	if p_Message.type == MessageType.ClientConnectedMessage then 
		print("ClientConnectedMessage")
	end
	if p_Message.type == MessageType.ClientContinueSingleplayerMessage then 
		print("ClientContinueSingleplayerMessage")
	end
	if p_Message.type == MessageType.ClientDisconnectedMessage then 
		print("ClientDisconnectedMessage")
	end
	if p_Message.type == MessageType.ClientDrawFpsMessage then 
		print("ClientDrawFpsMessage")
	end
	if p_Message.type == MessageType.ClientDrawNetworkStatsMessage then 
		print("ClientDrawNetworkStatsMessage")
	end
	if p_Message.type == MessageType.ClientDrawObjectLimitsMessage then 
		print("ClientDrawObjectLimitsMessage")
	end
	if p_Message.type == MessageType.ClientDrawStatsMessage then 
		print("ClientDrawStatsMessage")
	end
	if p_Message.type == MessageType.ClientEnterHudIngameMessage then 
		print("ClientEnterHudIngameMessage")
	end
	if p_Message.type == MessageType.ClientEnteredIngameMessage then 
		print("ClientEnteredIngameMessage")
	end
	if p_Message.type == MessageType.ClientExitToMenuMessage then 
		print("ClientExitToMenuMessage")
	end
	if p_Message.type == MessageType.ClientJoinMultiplayerMessageBase then 
		print("ClientJoinMultiplayerMessageBase")
	end
	if p_Message.type == MessageType.ClientJoinServerJobMessage then 
		print("ClientJoinServerJobMessage")
	end
	if p_Message.type == MessageType.ClientLeftRemoteServerMessage then 
		print("ClientLeftRemoteServerMessage")
	end
	if p_Message.type == MessageType.ClientLevelDescriptionLoadedMessage then 
		print("ClientLevelDescriptionLoadedMessage")
	end
	if p_Message.type == MessageType.ClientLevelLoadProgressMessage then 
		print("ClientLevelLoadProgressMessage")
	end
	if p_Message.type == MessageType.ClientLevelLoadedMessage then 
		print("ClientLevelLoadedMessage")
	end
	if p_Message.type == MessageType.ClientLevelUnloadedMessage then 
		print("ClientLevelUnloadedMessage")
	end
	if p_Message.type == MessageType.ClientLoadLevelMessage then 
		print("ClientLoadLevelMessage")
	end
	if p_Message.type == MessageType.ClientLoadLevelRequestedMessage then 
		print("ClientLoadLevelRequestedMessage")
	end
	if p_Message.type == MessageType.ClientLoadingDebugInfoEnabledMessage then 
		print("ClientLoadingDebugInfoEnabledMessage")
	end
	if p_Message.type == MessageType.ClientPeerNetworkRemovedMessage then 
		print("ClientPeerNetworkRemovedMessage")
	end
	if p_Message.type == MessageType.ClientRenderTagsMessage then 
		print("ClientRenderTagsMessage")
	end
	if p_Message.type == MessageType.ClientRestartSingleplayerMessage then 
		print("ClientRestartSingleplayerMessage")
	end
	if p_Message.type == MessageType.ClientRestartTimerMessage then 
		print("ClientRestartTimerMessage")
	end
	if p_Message.type == MessageType.ClientReturnToMenuMessage then 
		print("ClientReturnToMenuMessage")
	end
	if p_Message.type == MessageType.ClientSetServerPasswordMessage then 
		print("ClientSetServerPasswordMessage")
	end
	if p_Message.type == MessageType.ClientStartMultiplayerMessage then 
		print("ClientStartMultiplayerMessage")
	end
	if p_Message.type == MessageType.ClientStartSingleplayerMessageBase then 
		print("ClientStartSingleplayerMessageBase")
	end
	if p_Message.type == MessageType.ClientStartedMessage then 
		print("ClientStartedMessage")
	end
	if p_Message.type == MessageType.ClientWantFullscreenMessage then 
		print("ClientWantFullscreenMessage")
	end
	if p_Message.type == MessageType.ClientCharacterLocalPlayerDeletedMessage then 
		print("ClientCharacterLocalPlayerDeletedMessage")
	end
	if p_Message.type == MessageType.ClientCharacterLocalPlayerSetMessage then 
		print("ClientCharacterLocalPlayerSetMessage")
	end
	if p_Message.type == MessageType.ClientCharacterSpawnDoneMessage then 
		print("ClientCharacterSpawnDoneMessage")
	end
	if p_Message.type == MessageType.ClientCollisionExplosionPackDetonatedMessage then 
		print("ClientCollisionExplosionPackDetonatedMessage")
	end
	if p_Message.type == MessageType.ClientCollisionExplosionPackPlacedMessage then 
		print("ClientCollisionExplosionPackPlacedMessage")
	end
	if p_Message.type == MessageType.ClientCollisionGrenadeCollisionMessage then 
		print("ClientCollisionGrenadeCollisionMessage")
	end
	if p_Message.type == MessageType.ClientConnectionInitializedMessage then 
		print("ClientConnectionInitializedMessage")
	end
	if p_Message.type == MessageType.ClientConnectionLinkLevelMessage then 
		print("ClientConnectionLinkLevelMessage")
	end
	if p_Message.type == MessageType.ClientConnectionLoadLevelMessage then 
		print("ClientConnectionLoadLevelMessage")
	end
	if p_Message.type == MessageType.ClientConnectionUnloadLevelMessage then 
		print("ClientConnectionUnloadLevelMessage")
	end
	if p_Message.type == MessageType.ClientControllableSpawnDoneMessage then 
		print("ClientControllableSpawnDoneMessage")
	end
	if p_Message.type == MessageType.ClientControllableUnspawnDoneMessage then 
		print("ClientControllableUnspawnDoneMessage")
	end
	if p_Message.type == MessageType.ClientControllableToComponentsPlayerEnteredMessage then 
		print("ClientControllableToComponentsPlayerEnteredMessage")
	end
	if p_Message.type == MessageType.ClientControllableToComponentsPlayerExitMessage then 
		print("ClientControllableToComponentsPlayerExitMessage")
	end
	if p_Message.type == MessageType.ClientDynamicModelRenderStatsMessage then 
		print("ClientDynamicModelRenderStatsMessage")
	end
	if p_Message.type == MessageType.ClientGameplayControllableLowHealthMessage then 
		print("ClientGameplayControllableLowHealthMessage")
	end
	if p_Message.type == MessageType.ClientGameplayPlayerBaseDestroyedMessage then 
		print("ClientGameplayPlayerBaseDestroyedMessage")
	end
	if p_Message.type == MessageType.ClientGameplaySoldierHitMessage then 
		print("ClientGameplaySoldierHitMessage")
	end
	if p_Message.type == MessageType.ClientLevelFinalizedMessage then 
		print("ClientLevelFinalizedMessage")
	end
	if p_Message.type == MessageType.ClientLevelSpawnDebugEntitiesMessage then 
		print("ClientLevelSpawnDebugEntitiesMessage")
	end
	if p_Message.type == MessageType.ClientLevelSpawnEntitiesEndMessage then 
		print("ClientLevelSpawnEntitiesEndMessage")
	end
	if p_Message.type == MessageType.ClientLevelenterLevelDisableDestructionEffectsTimeMessage then 
		print("ClientLevelenterLevelDisableDestructionEffectsTimeMessage")
	end
	if p_Message.type == MessageType.ClientLevelenterLevelDisableEffectsTimeMessage then 
		print("ClientLevelenterLevelDisableEffectsTimeMessage")
	end
	if p_Message.type == MessageType.ClientMetricsPauseGameMessage then 
		print("ClientMetricsPauseGameMessage")
	end
	if p_Message.type == MessageType.ClientMetricsPopUIScreenMessage then 
		print("ClientMetricsPopUIScreenMessage")
	end
	if p_Message.type == MessageType.ClientMetricsPushUIScreenMessage then 
		print("ClientMetricsPushUIScreenMessage")
	end
	if p_Message.type == MessageType.ClientMetricsUIActionMessage then 
		print("ClientMetricsUIActionMessage")
	end
	if p_Message.type == MessageType.ClientOverlayBlockOverlayAvailabilityMessage then 
		print("ClientOverlayBlockOverlayAvailabilityMessage")
	end
	if p_Message.type == MessageType.ClientPlayerAmmoPickupMessage then 
		print("ClientPlayerAmmoPickupMessage")
	end
	if p_Message.type == MessageType.ClientPlayerChangedAllowedToSpawnOnMessage then 
		print("ClientPlayerChangedAllowedToSpawnOnMessage")
	end
	if p_Message.type == MessageType.ClientPlayerChangedMatchReadyStatusMessage then 
		print("ClientPlayerChangedMatchReadyStatusMessage")
	end
	if p_Message.type == MessageType.ClientPlayerChangedPlayerViewMessage then 
		print("ClientPlayerChangedPlayerViewMessage")
	end
	if p_Message.type == MessageType.ClientPlayerChangedSquadLeaderStatusMessage then 
		print("ClientPlayerChangedSquadLeaderStatusMessage")
	end
	if p_Message.type == MessageType.ClientPlayerChangedVoiceChannelMessage then 
		print("ClientPlayerChangedVoiceChannelMessage")
	end
	if p_Message.type == MessageType.ClientPlayerConnectMessage then 
		print("ClientPlayerConnectMessage")
	end
	if p_Message.type == MessageType.ClientPlayerDeletedMessage then 
		print("ClientPlayerDeletedMessage")
	end
	if p_Message.type == MessageType.ClientPlayerEnterEntryMessage then 
		print("ClientPlayerEnterEntryMessage")
	end
	if p_Message.type == MessageType.ClientPlayerEnterExitVehicleMessage then 
		print("ClientPlayerEnterExitVehicleMessage")
	end
	if p_Message.type == MessageType.ClientPlayerKilledMessage then 
		print("ClientPlayerKilledMessage")
	end
	if p_Message.type == MessageType.ClientPlayerLocalSetMessage then 
		print("ClientPlayerLocalSetMessage")
	end
	if p_Message.type == MessageType.ClientPlayerManDownMessage then 
		print("ClientPlayerManDownMessage")
	end
	if p_Message.type == MessageType.ClientPlayerNewSquadOrderMessage then 
		print("ClientPlayerNewSquadOrderMessage")
	end
	if p_Message.type == MessageType.ClientPlayerRequestCameraChangeMessage then 
		print("ClientPlayerRequestCameraChangeMessage")
	end
	if p_Message.type == MessageType.ClientPlayerSwitchSquadMessage then 
		print("ClientPlayerSwitchSquadMessage")
	end
	if p_Message.type == MessageType.ClientPlayerSwitchTeamMessage then 
		print("ClientPlayerSwitchTeamMessage")
	end
	if p_Message.type == MessageType.ClientPlayerUpdateCameraComponentMessage then 
		print("ClientPlayerUpdateCameraComponentMessage")
	end
	if p_Message.type == MessageType.ClientPlayerWeaponPickupMessage then 
		print("ClientPlayerWeaponPickupMessage")
	end
	if p_Message.type == MessageType.ClientSoldierChangeCoverStateMessage then 
		print("ClientSoldierChangeCoverStateMessage")
	end
	if p_Message.type == MessageType.ClientSoldierOnJumpMessage then 
		print("ClientSoldierOnJumpMessage")
	end
	if p_Message.type == MessageType.ClientSoldierOnLandMessage then 
		print("ClientSoldierOnLandMessage")
	end
	if p_Message.type == MessageType.ClientSoundVoiceOverFinishedMessage then 
		print("ClientSoundVoiceOverFinishedMessage")
	end
	if p_Message.type == MessageType.ClientSpawnSpawnedOrUnSpawnedMessage then 
		print("ClientSpawnSpawnedOrUnSpawnedMessage")
	end
	if p_Message.type == MessageType.ClientStateEnteredFrontendMessage then 
		print("ClientStateEnteredFrontendMessage")
	end
	if p_Message.type == MessageType.ClientStateEnteredIngameMessage then 
		print("ClientStateEnteredIngameMessage")
	end
	if p_Message.type == MessageType.ClientStateEnteredLoadingLevelMessage then 
		print("ClientStateEnteredLoadingLevelMessage")
	end
	if p_Message.type == MessageType.ClientStateEnteredStaticMessage then 
		print("ClientStateEnteredStaticMessage")
	end
	if p_Message.type == MessageType.ClientStateEnteredUnloadMessage then 
		print("ClientStateEnteredUnloadMessage")
	end
	if p_Message.type == MessageType.ClientStateRequestFrontendMessage then 
		print("ClientStateRequestFrontendMessage")
	end
	if p_Message.type == MessageType.ClientStateRequestIngameMessage then 
		print("ClientStateRequestIngameMessage")
	end
	if p_Message.type == MessageType.ClientStateRequestLoadingIngameBundlesMessage then 
		print("ClientStateRequestLoadingIngameBundlesMessage")
	end
	if p_Message.type == MessageType.ClientStateRequestLoadingLevelMessage then 
		print("ClientStateRequestLoadingLevelMessage")
	end
	if p_Message.type == MessageType.ClientStateRequestStaticMessage then 
		print("ClientStateRequestStaticMessage")
	end
	if p_Message.type == MessageType.ClientStateRequestUnloadMessage then 
		print("ClientStateRequestUnloadMessage")
	end
	if p_Message.type == MessageType.ClientStateResponseLoadingIngameBundlesMessage then 
		print("ClientStateResponseLoadingIngameBundlesMessage")
	end
	if p_Message.type == MessageType.ClientStaticModelDrawBoundingBoxMessage then 
		print("ClientStaticModelDrawBoundingBoxMessage")
	end
	if p_Message.type == MessageType.ClientStaticModelDrawBoundingBoxNonPhysicsMessage then 
		print("ClientStaticModelDrawBoundingBoxNonPhysicsMessage")
	end
	if p_Message.type == MessageType.ClientStaticModelDrawBoundingBoxPhysicsMessage then 
		print("ClientStaticModelDrawBoundingBoxPhysicsMessage")
	end
	if p_Message.type == MessageType.ClientStaticModelRenderKeyFrameTrailMessage then 
		print("ClientStaticModelRenderKeyFrameTrailMessage")
	end
	if p_Message.type == MessageType.ClientVehicleCriticalDamageMessage then 
		print("ClientVehicleCriticalDamageMessage")
	end
	if p_Message.type == MessageType.ClientWeaponDispersionUpdatedMessage then 
		--print("ClientWeaponDispersionUpdatedMessage")
	end
	if p_Message.type == MessageType.ClientWeaponPlayerPrimaryOutOfAmmoMessage then 
		print("ClientWeaponPlayerPrimaryOutOfAmmoMessage")
	end
	if p_Message.type == MessageType.ClientWeaponPlayerPrimaryWeaponFireMessage then 
		print("ClientWeaponPlayerPrimaryWeaponFireMessage")
	end
	if p_Message.type == MessageType.ClientWeaponPlayerWeaponChangeMessage then 
		print("ClientWeaponPlayerWeaponChangeMessage")
	end
	if p_Message.type == MessageType.ClientWeaponPlayerWeaponReloadBeginMessage then 
		print("ClientWeaponPlayerWeaponReloadBeginMessage")
	end
	if p_Message.type == MessageType.ClientWeaponPlayerWeaponReloadEndMessage then 
		print("ClientWeaponPlayerWeaponReloadEndMessage")
	end
	if p_Message.type == MessageType.CoreBudgetManagerMessage then 
		print("CoreBudgetManagerMessage")
	end
	if p_Message.type == MessageType.CoreCleanupMessage then 
		print("CoreCleanupMessage")
	end
	if p_Message.type == MessageType.CoreDebugReadSaveGameDataMessage then 
		print("CoreDebugReadSaveGameDataMessage")
	end
	if p_Message.type == MessageType.CoreDemoStatusMessage then 
		print("CoreDemoStatusMessage")
	end
	if p_Message.type == MessageType.CoreDetailedMemoryStatsEnabledMessage then 
		print("CoreDetailedMemoryStatsEnabledMessage")
	end
	if p_Message.type == MessageType.CoreDialogLevelMessage then 
		print("CoreDialogLevelMessage")
	end
	if p_Message.type == MessageType.CoreDisplayAssertsMessage then 
		print("CoreDisplayAssertsMessage")
	end
	if p_Message.type == MessageType.CoreEndMemoryFileStreamMessage then 
		print("CoreEndMemoryFileStreamMessage")
	end
	if p_Message.type == MessageType.CoreEndMemoryTrackingMessage then 
		print("CoreEndMemoryTrackingMessage")
	end
	if p_Message.type == MessageType.CoreEnteredIngameMessage then 
		print("CoreEnteredIngameMessage")
	end
	if p_Message.type == MessageType.CoreExitIngameMessage then 
		print("CoreExitIngameMessage")
	end
	if p_Message.type == MessageType.CoreGameTimerMessage then 
		print("CoreGameTimerMessage")
	end
	if p_Message.type == MessageType.CoreHibernateMessage then 
		print("CoreHibernateMessage")
	end
	if p_Message.type == MessageType.CoreJobProcessorCountMessage then 
		print("CoreJobProcessorCountMessage")
	end
	if p_Message.type == MessageType.CoreLiveEditingEnableMessage then 
		print("CoreLiveEditingEnableMessage")
	end
	if p_Message.type == MessageType.CoreLogLevelMessage then 
		print("CoreLogLevelMessage")
	end
	if p_Message.type == MessageType.CoreMainThreadInitMessage then 
		print("CoreMainThreadInitMessage")
	end
	if p_Message.type == MessageType.CoreMemoryStatsEnabledMessage then 
		print("CoreMemoryStatsEnabledMessage")
	end
	if p_Message.type == MessageType.CorePanicMessage then 
		print("CorePanicMessage")
	end
	if p_Message.type == MessageType.CorePropertyTweakedMessage then 
		print("CorePropertyTweakedMessage")
	end
	if p_Message.type == MessageType.CoreQuitMessage then 
		print("CoreQuitMessage")
	end
	if p_Message.type == MessageType.CoreQuittingInitiatedMessage then 
		print("CoreQuittingInitiatedMessage")
	end
	if p_Message.type == MessageType.CoreResourceLoadingFailureMessage then 
		print("CoreResourceLoadingFailureMessage")
	end
	if p_Message.type == MessageType.CoreTweakBlueprintBundleLoadedMessage then 
		print("CoreTweakBlueprintBundleLoadedMessage")
	end
	if p_Message.type == MessageType.CoreTweakBlueprintBundleUnloadedMessage then 
		print("CoreTweakBlueprintBundleUnloadedMessage")
	end
	if p_Message.type == MessageType.CoreTweakSubLevelBundleLoadedMessage then 
		print("CoreTweakSubLevelBundleLoadedMessage")
	end
	if p_Message.type == MessageType.CoreTweakSubLevelBundleUnloadedMessage then 
		print("CoreTweakSubLevelBundleUnloadedMessage")
	end
	if p_Message.type == MessageType.CoreUpdateClipboardMessage then 
		print("CoreUpdateClipboardMessage")
	end
	if p_Message.type == MessageType.CoreWriteSaveGameDoneMessage then 
		print("CoreWriteSaveGameDoneMessage")
	end
	if p_Message.type == MessageType.CoreWriteSaveGameMessage then 
		print("CoreWriteSaveGameMessage")
	end
	if p_Message.type == MessageType.NetworkCameraPositionMessage then 
		print("NetworkCameraPositionMessage")
	end
	if p_Message.type == MessageType.NetworkChangeGameSettingMessage then 
		print("NetworkChangeGameSettingMessage")
	end
	if p_Message.type == MessageType.NetworkClientCameraControlMessage then 
		print("NetworkClientCameraControlMessage")
	end
	if p_Message.type == MessageType.NetworkConnectedMessage then 
		print("NetworkConnectedMessage")
	end
	if p_Message.type == MessageType.NetworkCreatePlayerMessage then 
		print("NetworkCreatePlayerMessage")
	end
	if p_Message.type == MessageType.NetworkDebugServerInputMessage then 
		print("NetworkDebugServerInputMessage")
	end
	if p_Message.type == MessageType.NetworkDifficultyChangedMessage then 
		print("NetworkDifficultyChangedMessage")
	end
	if p_Message.type == MessageType.NetworkDisconnectedMessage then 
		print("NetworkDisconnectedMessage")
	end
	if p_Message.type == MessageType.NetworkDrawServerGhostsMessage then 
		print("NetworkDrawServerGhostsMessage")
	end
	if p_Message.type == MessageType.NetworkDrawServerGhostsSkipStaticMessage then 
		print("NetworkDrawServerGhostsSkipStaticMessage")
	end
	if p_Message.type == MessageType.NetworkDumpMessageCreatorsMessage then 
		print("NetworkDumpMessageCreatorsMessage")
	end
	if p_Message.type == MessageType.NetworkFirstPlayerEnteredMessage then 
		print("NetworkFirstPlayerEnteredMessage")
	end
	if p_Message.type == MessageType.NetworkGhostCountWarningDrawXMessage then 
		print("NetworkGhostCountWarningDrawXMessage")
	end
	if p_Message.type == MessageType.NetworkGhostCountWarningDrawYMessage then 
		print("NetworkGhostCountWarningDrawYMessage")
	end
	if p_Message.type == MessageType.NetworkGhostCountWarningInfoMessage then 
		print("NetworkGhostCountWarningInfoMessage")
	end
	if p_Message.type == MessageType.NetworkGhostCountWarningInstanceThresholdMessage then 
		print("NetworkGhostCountWarningInstanceThresholdMessage")
	end
	if p_Message.type == MessageType.NetworkGhostCountWarningMessage then 
		print("NetworkGhostCountWarningMessage")
	end
	if p_Message.type == MessageType.NetworkGhostCountWarningThresholdMessage then 
		print("NetworkGhostCountWarningThresholdMessage")
	end
	if p_Message.type == MessageType.NetworkGhostCountWarningTimeMessage then 
		print("NetworkGhostCountWarningTimeMessage")
	end
	if p_Message.type == MessageType.NetworkJuiceSessionMessage then 
		print("NetworkJuiceSessionMessage")
	end
	if p_Message.type == MessageType.NetworkLevelLoadedAckMessage then 
		print("NetworkLevelLoadedAckMessage")
	end
	if p_Message.type == MessageType.NetworkLoadLevelMessage then 
		print("NetworkLoadLevelMessage")
	end
	if p_Message.type == MessageType.NetworkMatchReadyStatusChangedMessage then 
		print("NetworkMatchReadyStatusChangedMessage")
	end
	if p_Message.type == MessageType.NetworkMaxClientCountMessage then 
		print("NetworkMaxClientCountMessage")
	end
	if p_Message.type == MessageType.NetworkMetricsSaveGameSavedMessage then 
		print("NetworkMetricsSaveGameSavedMessage")
	end
	if p_Message.type == MessageType.NetworkMovePlayerMessage then 
		print("NetworkMovePlayerMessage")
	end
	if p_Message.type == MessageType.NetworkOnPlayerSpawnedMessage then 
		print("NetworkOnPlayerSpawnedMessage")
	end
	if p_Message.type == MessageType.NetworkPerformanceProfileMessage then 
		print("NetworkPerformanceProfileMessage")
	end
	if p_Message.type == MessageType.NetworkPlayerSelectedUnlockAssetsMessage then 
		print("NetworkPlayerSelectedUnlockAssetsMessage")
	end
	if p_Message.type == MessageType.NetworkPlayerSelectedWeaponMessage then 
		print("NetworkPlayerSelectedWeaponMessage")
	end
	if p_Message.type == MessageType.NetworkProtocolVersionMessage then 
		print("NetworkProtocolVersionMessage")
	end
	if p_Message.type == MessageType.NetworkRequestLoadLevelMessage then 
		print("NetworkRequestLoadLevelMessage")
	end
	if p_Message.type == MessageType.NetworkScreenFadeMessage then 
		print("NetworkScreenFadeMessage")
	end
	if p_Message.type == MessageType.NetworkSelectSpawnGroupMessage then 
		print("NetworkSelectSpawnGroupMessage")
	end
	if p_Message.type == MessageType.NetworkSelectTeamMessage then 
		print("NetworkSelectTeamMessage")
	end
	if p_Message.type == MessageType.NetworkServerAddressMessage then 
		print("NetworkServerAddressMessage")
	end
	if p_Message.type == MessageType.NetworkServerPortMessage then 
		print("NetworkServerPortMessage")
	end
	if p_Message.type == MessageType.NetworkSetActiveWeaponSlotMessage then 
		print("NetworkSetActiveWeaponSlotMessage")
	end
	if p_Message.type == MessageType.NetworkSetPlayerViewMessage then 
		print("NetworkSetPlayerViewMessage")
	end
	if p_Message.type == MessageType.NetworkSettingsMessage then 
		print("NetworkSettingsMessage")
	end
	if p_Message.type == MessageType.NetworkSpawnCustomizationMessage then 
		print("NetworkSpawnCustomizationMessage")
	end
	if p_Message.type == MessageType.NetworkSpawnHereMessage then 
		print("NetworkSpawnHereMessage")
	end
	if p_Message.type == MessageType.NetworkSpawnMessage then 
		print("NetworkSpawnMessage")
	end
	if p_Message.type == MessageType.NetworkSpawnOnSelectedMessage then 
		print("NetworkSpawnOnSelectedMessage")
	end
	if p_Message.type == MessageType.NetworkSpawnVehicleCustomizationMessage then 
		print("NetworkSpawnVehicleCustomizationMessage")
	end
	if p_Message.type == MessageType.NetworkSuicideMessage then 
		print("NetworkSuicideMessage")
	end
	if p_Message.type == MessageType.NetworkTimeSyncMessage then 
		print("NetworkTimeSyncMessage")
	end
	if p_Message.type == MessageType.NetworkUnSpawnCustomizationMessage then 
		print("NetworkUnSpawnCustomizationMessage")
	end
	if p_Message.type == MessageType.PresenceAchievementRequestMessageBase then 
		print("PresenceAchievementRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceBlobMessageBase then 
		print("PresenceBlobMessageBase")
	end
	if p_Message.type == MessageType.PresenceBlobRequestMessageBase then 
		print("PresenceBlobRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceBlockListMessageBase then 
		print("PresenceBlockListMessageBase")
	end
	if p_Message.type == MessageType.PresenceBrowserMessageBase then 
		print("PresenceBrowserMessageBase")
	end
	if p_Message.type == MessageType.PresenceBrowserRequestMessageBase then 
		print("PresenceBrowserRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceCalendarMessageBase then 
		print("PresenceCalendarMessageBase")
	end
	if p_Message.type == MessageType.PresenceCalendarRequestMessageBase then 
		print("PresenceCalendarRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceCommMessageBase then 
		print("PresenceCommMessageBase")
	end
	if p_Message.type == MessageType.PresenceCommRequestMessageBase then 
		print("PresenceCommRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceCommerceMessageBase then 
		print("PresenceCommerceMessageBase")
	end
	if p_Message.type == MessageType.PresenceCommerceRequestMessageBase then 
		print("PresenceCommerceRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceConnectionMessageBase then 
		print("PresenceConnectionMessageBase")
	end
	if p_Message.type == MessageType.PresenceConnectionRequestMessageBase then 
		print("PresenceConnectionRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceCouchMessageBase then 
		print("PresenceCouchMessageBase")
	end
	if p_Message.type == MessageType.PresenceFriendMessageBase then 
		print("PresenceFriendMessageBase")
	end
	if p_Message.type == MessageType.PresenceFriendRequestMessageBase then 
		print("PresenceFriendRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceGameConfigurationMessageBase then 
		print("PresenceGameConfigurationMessageBase")
	end
	if p_Message.type == MessageType.PresenceGameConfigurationRequestMessageBase then 
		print("PresenceGameConfigurationRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceGameMessageBase then 
		print("PresenceGameMessageBase")
	end
	if p_Message.type == MessageType.PresenceGameQueueMessageBase then 
		print("PresenceGameQueueMessageBase")
	end
	if p_Message.type == MessageType.PresenceGameRequestMessageBase then 
		print("PresenceGameRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceHistoryMessageBase then 
		print("PresenceHistoryMessageBase")
	end
	if p_Message.type == MessageType.PresenceInitialControllerIndexMessageBase then 
		print("PresenceInitialControllerIndexMessageBase")
	end
	if p_Message.type == MessageType.PresenceJoinResultMessageBase then 
		print("PresenceJoinResultMessageBase")
	end
	if p_Message.type == MessageType.PresenceLicenseMessageBase then 
		print("PresenceLicenseMessageBase")
	end
	if p_Message.type == MessageType.PresenceLicenseRequestMessageBase then 
		print("PresenceLicenseRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceLivePartyMessageBase then 
		print("PresenceLivePartyMessageBase")
	end
	if p_Message.type == MessageType.PresenceMatchFeedMessageBase then 
		print("PresenceMatchFeedMessageBase")
	end
	if p_Message.type == MessageType.PresenceMatchFeedRequestMessageBase then 
		print("PresenceMatchFeedRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceMatchImagesMessageBase then 
		print("PresenceMatchImagesMessageBase")
	end
	if p_Message.type == MessageType.PresenceMatchmakerMessageBase then 
		print("PresenceMatchmakerMessageBase")
	end
	if p_Message.type == MessageType.PresenceNewsTickerMessageBase then 
		print("PresenceNewsTickerMessageBase")
	end
	if p_Message.type == MessageType.PresenceNewsTickerRequestMessageBase then 
		print("PresenceNewsTickerRequestMessageBase")
	end
	if p_Message.type == MessageType.PresencePeerGameMessageBase then 
		print("PresencePeerGameMessageBase")
	end
	if p_Message.type == MessageType.PresencePeerGameRequestMessageBase then 
		print("PresencePeerGameRequestMessageBase")
	end
	if p_Message.type == MessageType.PresencePingUpdatedMessageBase then 
		print("PresencePingUpdatedMessageBase")
	end
	if p_Message.type == MessageType.PresencePlaygroupAttributesMessageBase then 
		print("PresencePlaygroupAttributesMessageBase")
	end
	if p_Message.type == MessageType.PresencePlaygroupMessageBase then 
		print("PresencePlaygroupMessageBase")
	end
	if p_Message.type == MessageType.PresencePlaygroupRequestMessageBase then 
		print("PresencePlaygroupRequestMessageBase")
	end
	if p_Message.type == MessageType.PresencePopupMessageBase then 
		print("PresencePopupMessageBase")
	end
	if p_Message.type == MessageType.PresencePopupRequestMessageBase then 
		print("PresencePopupRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceRspMessageBase then 
		print("PresenceRspMessageBase")
	end
	if p_Message.type == MessageType.PresenceRspRequestMessageBase then 
		print("PresenceRspRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceServerBannerMessageBase then 
		print("PresenceServerBannerMessageBase")
	end
	if p_Message.type == MessageType.PresenceServerBannerRequestMessageBase then 
		print("PresenceServerBannerRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceServerBrowserMessageBase then 
		print("PresenceServerBrowserMessageBase")
	end
	if p_Message.type == MessageType.PresenceServerBrowserRequestMessageBase then 
		print("PresenceServerBrowserRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceStorageMessageBase then 
		print("PresenceStorageMessageBase")
	end
	if p_Message.type == MessageType.PresenceStorageRequestMessageBase then 
		print("PresenceStorageRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceSystemUIMessageBase then 
		print("PresenceSystemUIMessageBase")
	end
	if p_Message.type == MessageType.PresenceUserIdMessageBase then 
		print("PresenceUserIdMessageBase")
	end
	if p_Message.type == MessageType.PresenceUserIdRequestMessageBase then 
		print("PresenceUserIdRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceUserSettingsMessageBase then 
		print("PresenceUserSettingsMessageBase")
	end
	if p_Message.type == MessageType.PresenceUserSettingsRequestMessageBase then 
		print("PresenceUserSettingsRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceUserTitleInfoMessageBase then 
		print("PresenceUserTitleInfoMessageBase")
	end
	if p_Message.type == MessageType.PresenceUserTitleInfoRequestMessageBase then 
		print("PresenceUserTitleInfoRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceWebFeedMessageBase then 
		print("PresenceWebFeedMessageBase")
	end
	if p_Message.type == MessageType.PresenceWebFeedRequestMessageBase then 
		print("PresenceWebFeedRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceWebMessageBase then 
		print("PresenceWebMessageBase")
	end
	if p_Message.type == MessageType.PresenceWebRequestMessageBase then 
		print("PresenceWebRequestMessageBase")
	end
	if p_Message.type == MessageType.PresenceXPromoMessageBase then 
		print("PresenceXPromoMessageBase")
	end
	if p_Message.type == MessageType.PresenceXPromoRequestMessageBase then 
		print("PresenceXPromoRequestMessageBase")
	end
	if p_Message.type == MessageType.ServerActiveLicensesMessage then 
		print("ServerActiveLicensesMessage")
	end
	if p_Message.type == MessageType.ServerApplyConfigurationMessage then 
		print("ServerApplyConfigurationMessage")
	end
	if p_Message.type == MessageType.ServerClientConnectionConnectedMessage then 
		print("ServerClientConnectionConnectedMessage")
	end
	if p_Message.type == MessageType.ServerClientConnectionRemovedMessage then 
		print("ServerClientConnectionRemovedMessage")
	end
	if p_Message.type == MessageType.ServerDrawCombatAreaGeometryMessage then 
		print("ServerDrawCombatAreaGeometryMessage")
	end
	if p_Message.type == MessageType.ServerInternalLoadLevelMessage then 
		print("ServerInternalLoadLevelMessage")
	end
	if p_Message.type == MessageType.ServerIsDesertingAllowedMessage then 
		print("ServerIsDesertingAllowedMessage")
	end
	if p_Message.type == MessageType.ServerIsRenderDamageEventsMessage then 
		print("ServerIsRenderDamageEventsMessage")
	end
	if p_Message.type == MessageType.ServerIsStatsEnabledMessage then 
		print("ServerIsStatsEnabledMessage")
	end
	if p_Message.type == MessageType.ServerJobEnableForceEnabledHackMessage then 
		print("ServerJobEnableForceEnabledHackMessage")
	end
	if p_Message.type == MessageType.ServerLevelLoadedMessage then 
		print("ServerLevelLoadedMessage")
	end
	if p_Message.type == MessageType.ServerLevelUnloadedMessage then 
		print("ServerLevelUnloadedMessage")
	end
	if p_Message.type == MessageType.ServerLoadGameMessage then 
		print("ServerLoadGameMessage")
	end
	if p_Message.type == MessageType.ServerLoadLevelMessage then 
		print("ServerLoadLevelMessage")
	end
	if p_Message.type == MessageType.ServerLogGameAttributesMessage then 
		print("ServerLogGameAttributesMessage")
	end
	if p_Message.type == MessageType.ServerPausedMessage then 
		print("ServerPausedMessage")
	end
	if p_Message.type == MessageType.ServerQueryProviderEnabledMessage then 
		print("ServerQueryProviderEnabledMessage")
	end
	if p_Message.type == MessageType.ServerRemoteAdministrationPortMessage then 
		print("ServerRemoteAdministrationPortMessage")
	end
	if p_Message.type == MessageType.ServerResendSyncedSettingsMessage then 
		print("ServerResendSyncedSettingsMessage")
	end
	if p_Message.type == MessageType.ServerResetConfigurationMessage then 
		print("ServerResetConfigurationMessage")
	end
	if p_Message.type == MessageType.ServerRespawnOnDeathPositionMessage then 
		print("ServerRespawnOnDeathPositionMessage")
	end
	if p_Message.type == MessageType.ServerRestartForESportsMatchMessage then 
		print("ServerRestartForESportsMatchMessage")
	end
	if p_Message.type == MessageType.ServerRestartTimerMessage then 
		print("ServerRestartTimerMessage")
	end
	if p_Message.type == MessageType.ServerRotateLevelMessage then 
		print("ServerRotateLevelMessage")
	end
	if p_Message.type == MessageType.ServerStartedMessage then 
		print("ServerStartedMessage")
	end
	if p_Message.type == MessageType.ServerStopMessage then 
		print("ServerStopMessage")
	end
	if p_Message.type == MessageType.ServerStoppedMessage then 
		print("ServerStoppedMessage")
	end
	if p_Message.type == MessageType.ServerThreadingEnableMessage then 
		print("ServerThreadingEnableMessage")
	end
	if p_Message.type == MessageType.ServerUnloadLevelMessage then 
		print("ServerUnloadLevelMessage")
	end
	if p_Message.type == MessageType.ServerAdminBanPlayerMessage then 
		print("ServerAdminBanPlayerMessage")
	end
	if p_Message.type == MessageType.ServerAdminSetServerNameMessage then 
		print("ServerAdminSetServerNameMessage")
	end
	if p_Message.type == MessageType.ServerAdministrationEventsEnabledMessage then 
		print("ServerAdministrationEventsEnabledMessage")
	end
	if p_Message.type == MessageType.ServerAdministrationLoginMessage then 
		print("ServerAdministrationLoginMessage")
	end
	if p_Message.type == MessageType.ServerAdministrationPacketMessage then 
		print("ServerAdministrationPacketMessage")
	end
	if p_Message.type == MessageType.ServerAdministrationPasswordMessage then 
		print("ServerAdministrationPasswordMessage")
	end
	if p_Message.type == MessageType.ServerAdministrationQuitMessage then 
		print("ServerAdministrationQuitMessage")
	end
	if p_Message.type == MessageType.ServerBanListEventMessage then 
		print("ServerBanListEventMessage")
	end
	if p_Message.type == MessageType.ServerClubMemberCreatedMessage then 
		print("ServerClubMemberCreatedMessage")
	end
	if p_Message.type == MessageType.ServerClubMemberDeletedMessage then 
		print("ServerClubMemberDeletedMessage")
	end
	if p_Message.type == MessageType.ServerCollisionExplosionDamageMessage then 
		print("ServerCollisionExplosionDamageMessage")
	end
	if p_Message.type == MessageType.ServerCollisionExplosionPackDestroyedMessage then 
		print("ServerCollisionExplosionPackDestroyedMessage")
	end
	if p_Message.type == MessageType.ServerCollisionExplosionPackPlacedMessage then 
		print("ServerCollisionExplosionPackPlacedMessage")
	end
	if p_Message.type == MessageType.ServerCollisionExplosionPreDamageMessage then 
		print("ServerCollisionExplosionPreDamageMessage")
	end
	if p_Message.type == MessageType.ServerCollisionExplosionSpawnMessage then 
		print("ServerCollisionExplosionSpawnMessage")
	end
	if p_Message.type == MessageType.ServerCollisionExplosionUnSpawnMessage then 
		print("ServerCollisionExplosionUnSpawnMessage")
	end
	if p_Message.type == MessageType.ServerCollisionGrenadeCollisionMessage then 
		print("ServerCollisionGrenadeCollisionMessage")
	end
	if p_Message.type == MessageType.ServerCollisionGrenadeThrowMessage then 
		print("ServerCollisionGrenadeThrowMessage")
	end
	if p_Message.type == MessageType.ServerCollisionProjectileFireMessage then 
		print("ServerCollisionProjectileFireMessage")
	end
	if p_Message.type == MessageType.ServerCollisionProjectileImpactMessage then 
		print("ServerCollisionProjectileImpactMessage")
	end
	if p_Message.type == MessageType.ServerCollisionProjectileTimeoutMessage then 
		print("ServerCollisionProjectileTimeoutMessage")
	end
	if p_Message.type == MessageType.ServerComponentEntryComponentProcessedInputMessage then 
		
		-- This is just spam

		--print("ServerComponentEntryComponentProcessedInputMessage") 


	end
	if p_Message.type == MessageType.ServerComponentEntryOnPlayerEntersMessage then 
		print("ServerComponentEntryOnPlayerEntersMessage")
	end
	if p_Message.type == MessageType.ServerComponentEntryOnPlayerExitsMessage then 
		print("ServerComponentEntryOnPlayerExitsMessage")
	end
	if p_Message.type == MessageType.ServerComponentEntryOnUnspawnMessage then 
		print("ServerComponentEntryOnUnspawnMessage")
	end
	if p_Message.type == MessageType.ServerComponentWeaponOnSpawnMessage then 
		print("ServerComponentWeaponOnSpawnMessage")
	end
	if p_Message.type == MessageType.ServerComponentWeaponOnUnspawnMessage then 
		print("ServerComponentWeaponOnUnspawnMessage")
	end
	if p_Message.type == MessageType.ServerControllableSpawnDoneMessage then 
		print("ServerControllableSpawnDoneMessage")
	end
	if p_Message.type == MessageType.ServerControllableUnspawnDoneMessage then 
		print("ServerControllableUnspawnDoneMessage")
	end
	if p_Message.type == MessageType.ServerControllableToComponentsPlayerEnteredMessage then 
		print("ServerControllableToComponentsPlayerEnteredMessage")
	end
	if p_Message.type == MessageType.ServerControllableToComponentsPlayerExitMessage then 
		print("ServerControllableToComponentsPlayerExitMessage")
	end
	if p_Message.type == MessageType.ServerEntityBangerEntityOnSpawnMessage then 
		print("ServerEntityBangerEntityOnSpawnMessage")
	end
	if p_Message.type == MessageType.ServerEntityBangerEntityOnUnspawnMessage then 
		print("ServerEntityBangerEntityOnUnspawnMessage")
	end
	if p_Message.type == MessageType.ServerEntityOnDamageMessage then 
		print("ServerEntityOnDamageMessage")
	end
	if p_Message.type == MessageType.ServerEntityPickupOnSpawnMessage then 
		print("ServerEntityPickupOnSpawnMessage")
	end
	if p_Message.type == MessageType.ServerEntityPickupOnUnspawnMessage then 
		print("ServerEntityPickupOnUnspawnMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerAddGameBanRequestMessage then 
		print("ServerGameManagerAddGameBanRequestMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerAddGameBanResponseMessage then 
		print("ServerGameManagerAddGameBanResponseMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerAddQueuedPlayerMessage then 
		print("ServerGameManagerAddQueuedPlayerMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerCheckAggressiveJoinStateMessage then 
		print("ServerGameManagerCheckAggressiveJoinStateMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerGameAttributesUpdateMessage then 
		print("ServerGameManagerGameAttributesUpdateMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerGameSettingsUpdateMessage then 
		print("ServerGameManagerGameSettingsUpdateMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerGetGameDataMessage then 
		print("ServerGameManagerGetGameDataMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerMatchmakingStateMessage then 
		print("ServerGameManagerMatchmakingStateMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerPlayerJoiningMessage then 
		print("ServerGameManagerPlayerJoiningMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerPlayerJoiningQueueMessage then 
		print("ServerGameManagerPlayerJoiningQueueMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerRefreshGameAttributesMessage then 
		print("ServerGameManagerRefreshGameAttributesMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerResetServerMessage then 
		print("ServerGameManagerResetServerMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerRestartLevelRequestMessage then 
		print("ServerGameManagerRestartLevelRequestMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerUpdateGameServerListsMessage then 
		print("ServerGameManagerUpdateGameServerListsMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerUpdateMatchmakingStateMessage then 
		print("ServerGameManagerUpdateMatchmakingStateMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerUpdatePlayerCapacityRequestMessage then 
		print("ServerGameManagerUpdatePlayerCapacityRequestMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerUpdatePlayerCapacityResponseMessage then 
		print("ServerGameManagerUpdatePlayerCapacityResponseMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerValidateContentRequestMessage then 
		print("ServerGameManagerValidateContentRequestMessage")
	end
	if p_Message.type == MessageType.ServerGameManagerValidateContentResponseMessage then 
		print("ServerGameManagerValidateContentResponseMessage")
	end
	if p_Message.type == MessageType.ServerGameplayCTFFlagCapturedMessage then 
		print("ServerGameplayCTFFlagCapturedMessage")
	end
	if p_Message.type == MessageType.ServerGameplayCTFFlagDroppedMessage then 
		print("ServerGameplayCTFFlagDroppedMessage")
	end
	if p_Message.type == MessageType.ServerGameplayCTFFlagPickedUpMessage then 
		print("ServerGameplayCTFFlagPickedUpMessage")
	end
	if p_Message.type == MessageType.ServerGameplayCTFFlagReturnedMessage then 
		print("ServerGameplayCTFFlagReturnedMessage")
	end
	if p_Message.type == MessageType.ServerGameplayCapturePointCapturedMessage then 
		print("ServerGameplayCapturePointCapturedMessage")
	end
	if p_Message.type == MessageType.ServerGameplayCapturePointChangedMessage then 
		print("ServerGameplayCapturePointChangedMessage")
	end
	if p_Message.type == MessageType.ServerGameplayCapturePointLostMessage then 
		print("ServerGameplayCapturePointLostMessage")
	end
	if p_Message.type == MessageType.ServerGameplayCapturePointPlayerEnteredMessage then 
		print("ServerGameplayCapturePointPlayerEnteredMessage")
	end
	if p_Message.type == MessageType.ServerGameplayCapturePointResetMessage then 
		print("ServerGameplayCapturePointResetMessage")
	end
	if p_Message.type == MessageType.ServerGameplayDeserterMessage then 
		print("ServerGameplayDeserterMessage")
	end
	if p_Message.type == MessageType.ServerGameplayDeserterReturnMessage then 
		print("ServerGameplayDeserterReturnMessage")
	end
	if p_Message.type == MessageType.ServerGameplayFightHarderMessage then 
		print("ServerGameplayFightHarderMessage")
	end
	if p_Message.type == MessageType.ServerGameplayGameModeResetMessage then 
		print("ServerGameplayGameModeResetMessage")
	end
	if p_Message.type == MessageType.ServerGameplayMedkitMessage then 
		print("ServerGameplayMedkitMessage")
	end
	if p_Message.type == MessageType.ServerGameplayPlayerBaseDestroyedMessage then 
		print("ServerGameplayPlayerBaseDestroyedMessage")
	end
	if p_Message.type == MessageType.ServerGameplayPlayerMenuCancelMessage then 
		print("ServerGameplayPlayerMenuCancelMessage")
	end
	if p_Message.type == MessageType.ServerGameplayPlayerMenuOkMessage then 
		print("ServerGameplayPlayerMenuOkMessage")
	end
	if p_Message.type == MessageType.ServerGameplayPreviousWeatherStateMessage then 
		print("ServerGameplayPreviousWeatherStateMessage")
	end
	if p_Message.type == MessageType.ServerGameplayServerPlayerMenuCancelMessage then 
		print("ServerGameplayServerPlayerMenuCancelMessage")
	end
	if p_Message.type == MessageType.ServerGameplayServerPlayerMenuOkMessage then 
		print("ServerGameplayServerPlayerMenuOkMessage")
	end
	if p_Message.type == MessageType.ServerGameplayTeamLostLeadMessage then 
		print("ServerGameplayTeamLostLeadMessage")
	end
	if p_Message.type == MessageType.ServerGameplayTeamReinforceMessage then 
		print("ServerGameplayTeamReinforceMessage")
	end
	if p_Message.type == MessageType.ServerGameplayTeamTakeLeadMessage then 
		print("ServerGameplayTeamTakeLeadMessage")
	end
	if p_Message.type == MessageType.ServerGameplayVoiceOverFinishedMessage then 
		print("ServerGameplayVoiceOverFinishedMessage")
	end
	if p_Message.type == MessageType.ServerInputDeactivateInputRestrictionMessage then 
		print("ServerInputDeactivateInputRestrictionMessage")
	end
	if p_Message.type == MessageType.ServerInputReactivateInputRestrictionMessage then 
		print("ServerInputReactivateInputRestrictionMessage")
	end
	if p_Message.type == MessageType.ServerLevelCompletedMessage then 
		print("ServerLevelCompletedMessage")
	end
	if p_Message.type == MessageType.ServerLevelSpawnEntitiesBeginMessage then 
		print("ServerLevelSpawnEntitiesBeginMessage")
	end
	if p_Message.type == MessageType.ServerLevelSpawnEntitiesEndMessage then 
		print("ServerLevelSpawnEntitiesEndMessage")
	end
	if p_Message.type == MessageType.ServerLevelStartedMessage then 
		print("ServerLevelStartedMessage")
	end
	if p_Message.type == MessageType.ServerMapSequencerControlledInfoMessage then 
		print("ServerMapSequencerControlledInfoMessage")
	end
	if p_Message.type == MessageType.ServerMapSequencerControlledMessage then 
		print("ServerMapSequencerControlledMessage")
	end
	if p_Message.type == MessageType.ServerMapSequencerDebugOutputMessage then 
		print("ServerMapSequencerDebugOutputMessage")
	end
	if p_Message.type == MessageType.ServerMapSequencerEventMessageBase then 
		print("ServerMapSequencerEventMessageBase")
	end
	if p_Message.type == MessageType.ServerMapSequencerLevelLoadedMessage then 
		print("ServerMapSequencerLevelLoadedMessage")
	end
	if p_Message.type == MessageType.ServerMapSequencerMaxPlayerCountMessage then 
		print("ServerMapSequencerMaxPlayerCountMessage")
	end
	if p_Message.type == MessageType.ServerMapSequencerStatusMessage then 
		print("ServerMapSequencerStatusMessage")
	end
	if p_Message.type == MessageType.ServerMapSequencerSwitchingLevelsMessage then 
		print("ServerMapSequencerSwitchingLevelsMessage")
	end
	if p_Message.type == MessageType.ServerMetricsDetonateExplosionMessage then 
		print("ServerMetricsDetonateExplosionMessage")
	end
	if p_Message.type == MessageType.ServerMetricsObjectiveSuccessMessage then 
		print("ServerMetricsObjectiveSuccessMessage")
	end
	if p_Message.type == MessageType.ServerMetricsSaveGameLoadedMessage then 
		print("ServerMetricsSaveGameLoadedMessage")
	end
	if p_Message.type == MessageType.ServerMetricsSaveGameSavedMessage then 
		print("ServerMetricsSaveGameSavedMessage")
	end
	if p_Message.type == MessageType.ServerPeerInitializedMessage then 
		print("ServerPeerInitializedMessage")
	end
	if p_Message.type == MessageType.ServerPeerLoadLevelMessage then 
		print("ServerPeerLoadLevelMessage")
	end
	if p_Message.type == MessageType.ServerPlayerAboutToClearSoldierMessage then 
		print("ServerPlayerAboutToClearSoldierMessage")
	end
	if p_Message.type == MessageType.ServerPlayerAboutToCreateForConnectionMessage then 
		print("ServerPlayerAboutToCreateForConnectionMessage")
	end
	if p_Message.type == MessageType.ServerPlayerChangeChatChannelMessage then 
		print("ServerPlayerChangeChatChannelMessage")
	end
	if p_Message.type == MessageType.ServerPlayerChangedSoldierMessage then 
		print("ServerPlayerChangedSoldierMessage")
	end
	if p_Message.type == MessageType.ServerPlayerChangedSquadSpawnerStatusMessage then 
		print("ServerPlayerChangedSquadSpawnerStatusMessage")
	end
	if p_Message.type == MessageType.ServerPlayerChangedVoiceChannelMessage then 
		print("ServerPlayerChangedVoiceChannelMessage")
	end
	if p_Message.type == MessageType.ServerPlayerChatMessage then 
		print("ServerPlayerChatMessage")
	end
	if p_Message.type == MessageType.ServerPlayerCreateMessage then 
		print("ServerPlayerCreateMessage")
	end
	if p_Message.type == MessageType.ServerPlayerCreatedForConnectionMessage then 
		print("ServerPlayerCreatedForConnectionMessage")
	end
	if p_Message.type == MessageType.ServerPlayerCustomizationDoneMessage then 
		print("ServerPlayerCustomizationDoneMessage")
	end
	if p_Message.type == MessageType.ServerPlayerDebugFriendZoneSpawnMessage then 
		print("ServerPlayerDebugFriendZoneSpawnMessage")
	end
	if p_Message.type == MessageType.ServerPlayerDestroyMessage then 
		print("ServerPlayerDestroyMessage")
	end
	if p_Message.type == MessageType.ServerPlayerEnterEntryMessage then 
		print("ServerPlayerEnterEntryMessage")
	end
	if p_Message.type == MessageType.ServerPlayerEnteredLevelMessage then 
		print("ServerPlayerEnteredLevelMessage")
	end
	if p_Message.type == MessageType.ServerPlayerExitEntryMessage then 
		print("ServerPlayerExitEntryMessage")
	end
	if p_Message.type == MessageType.ServerPlayerGunMasterLevelChangedMessage then 
		print("ServerPlayerGunMasterLevelChangedMessage")
	end
	if p_Message.type == MessageType.ServerPlayerInstantSuicideMessage then 
		print("ServerPlayerInstantSuicideMessage")
	end
	if p_Message.type == MessageType.ServerPlayerKilledMessage then 
		print("ServerPlayerKilledMessage")
	end
	if p_Message.type == MessageType.ServerPlayerKitReplacedMessage then 
		print("ServerPlayerKitReplacedMessage")
	end
	if p_Message.type == MessageType.ServerPlayerLeftLevelMessage then 
		print("ServerPlayerLeftLevelMessage")
	end
	if p_Message.type == MessageType.ServerPlayerLevelLoadedMessage then 
		print("ServerPlayerLevelLoadedMessage")
	end
	if p_Message.type == MessageType.ServerPlayerLicensesAvailableMessage then 
		print("ServerPlayerLicensesAvailableMessage")
	end
	if p_Message.type == MessageType.ServerPlayerManDownRevivedMessage then 
		print("ServerPlayerManDownRevivedMessage")
	end
	if p_Message.type == MessageType.ServerPlayerManuallySelectedSpawnPointMessage then 
		print("ServerPlayerManuallySelectedSpawnPointMessage")
	end
	if p_Message.type == MessageType.ServerPlayerMeleeInteruptedMessage then 
		print("ServerPlayerMeleeInteruptedMessage")
	end
	if p_Message.type == MessageType.ServerPlayerNoInteractivityKickMessage then 
		print("ServerPlayerNoInteractivityKickMessage")
	end
	if p_Message.type == MessageType.ServerPlayerReleasingLevelMessage then 
		print("ServerPlayerReleasingLevelMessage")
	end
	if p_Message.type == MessageType.ServerPlayerRespawnMessage then 
		print("ServerPlayerRespawnMessage")
	end
	if p_Message.type == MessageType.ServerPlayerReviveAcceptedMessage then 
		print("ServerPlayerReviveAcceptedMessage")
	end
	if p_Message.type == MessageType.ServerPlayerReviveMessage then 
		print("ServerPlayerReviveMessage")
	end
	if p_Message.type == MessageType.ServerPlayerReviveRefusedMessage then 
		print("ServerPlayerReviveRefusedMessage")
	end
	if p_Message.type == MessageType.ServerPlayerSpawnAtVehicleMessage then 
		print("ServerPlayerSpawnAtVehicleMessage")
	end
	if p_Message.type == MessageType.ServerPlayerSpawnOnPlayerMessage then 
		print("ServerPlayerSpawnOnPlayerMessage")
	end
	if p_Message.type == MessageType.ServerPlayerSpawnOnSelectedSpawnPointMessage then 
		print("ServerPlayerSpawnOnSelectedSpawnPointMessage")
	end
	if p_Message.type == MessageType.ServerPlayerSquadLeaderStatusChangedMessage then 
		print("ServerPlayerSquadLeaderStatusChangedMessage")
	end
	if p_Message.type == MessageType.ServerPlayerSwitchSquadMessage then 
		print("ServerPlayerSwitchSquadMessage")
	end
	if p_Message.type == MessageType.ServerPlayerSwitchTeamMessage then 
		print("ServerPlayerSwitchTeamMessage")
	end
	if p_Message.type == MessageType.ServerPlayerTeamKillKickMessage then 
		print("ServerPlayerTeamKillKickMessage")
	end
	if p_Message.type == MessageType.ServerPlayerTickMessage then 
		print("ServerPlayerTickMessage")
	end
	if p_Message.type == MessageType.ServerRoundInterruptedMessage then 
		print("ServerRoundInterruptedMessage")
	end
	if p_Message.type == MessageType.ServerRoundOverMessage then 
		print("ServerRoundOverMessage")
	end
	if p_Message.type == MessageType.ServerRoundResetMessage then 
		print("ServerRoundResetMessage")
	end
	if p_Message.type == MessageType.ServerSoldierChangingWeaponMessage then 
		print("ServerSoldierChangingWeaponMessage")
	end
	if p_Message.type == MessageType.ServerSoldierDamagedMessage then 
		print("ServerSoldierDamagedMessage")
	end
	if p_Message.type == MessageType.ServerSoldierFiringMessage then 
		print("ServerSoldierFiringMessage")
	end
	if p_Message.type == MessageType.ServerSoldierKilledMessage then 
		print("ServerSoldierKilledMessage")
	end
	if p_Message.type == MessageType.ServerSoldierManDownMessage then 
		print("ServerSoldierManDownMessage")
	end
	if p_Message.type == MessageType.ServerSoldierOnInitMessage then 
		print("ServerSoldierOnInitMessage")
	end
	if p_Message.type == MessageType.ServerSoldierSelfHealMessage then 
		print("ServerSoldierSelfHealMessage")
	end
	if p_Message.type == MessageType.ServerSoldierSoldierDamageMessage then 
		print("ServerSoldierSoldierDamageMessage")
	end
	if p_Message.type == MessageType.ServerSoldierSpawnDoneMessage then 
		print("ServerSoldierSpawnDoneMessage")
	end
	if p_Message.type == MessageType.ServerSoldierUnspawnDoneMessage then 
		print("ServerSoldierUnspawnDoneMessage")
	end
	if p_Message.type == MessageType.ServerSoldierWeaponReplacedMessage then 
		print("ServerSoldierWeaponReplacedMessage")
	end
	if p_Message.type == MessageType.ServerSoundVoiceOverFinishedMessage then 
		print("ServerSoundVoiceOverFinishedMessage")
	end
	if p_Message.type == MessageType.ServerStaticModelDamagedPartByPlayerMessage then 
		print("ServerStaticModelDamagedPartByPlayerMessage")
	end
	if p_Message.type == MessageType.ServerStaticModelDamagedPartMessage then 
		print("ServerStaticModelDamagedPartMessage")
	end
	if p_Message.type == MessageType.ServerStaticModelDestroyedAllCollapsablePartsMessage then 
		print("ServerStaticModelDestroyedAllCollapsablePartsMessage")
	end
	if p_Message.type == MessageType.ServerStaticModelGroupDestroyedPartMessage then 
		print("ServerStaticModelGroupDestroyedPartMessage")
	end
	if p_Message.type == MessageType.ServerStaticModelSpawnMessage then 
		print("ServerStaticModelSpawnMessage")
	end
	if p_Message.type == MessageType.ServerSubLevelOnStreamedInMessage then 
		print("ServerSubLevelOnStreamedInMessage")
	end
	if p_Message.type == MessageType.ServerUnlockListEventMessage then 
		print("ServerUnlockListEventMessage")
	end
	if p_Message.type == MessageType.ServerUnlockListRefreshMessage then 
		print("ServerUnlockListRefreshMessage")
	end
	if p_Message.type == MessageType.ServerUnlockSystemSetupMessage then 
		print("ServerUnlockSystemSetupMessage")
	end
	if p_Message.type == MessageType.ServerVehicleDamageMessage then 
		print("ServerVehicleDamageMessage")
	end
	if p_Message.type == MessageType.ServerVehicleDestroyedMessage then 
		print("ServerVehicleDestroyedMessage")
	end
	if p_Message.type == MessageType.ServerVehicleDisabledMessage then 
		print("ServerVehicleDisabledMessage")
	end
	if p_Message.type == MessageType.ServerVehicleEnterMessage then 
		print("ServerVehicleEnterMessage")
	end
	if p_Message.type == MessageType.ServerVehicleEnterRestrictionMessage then 
		print("ServerVehicleEnterRestrictionMessage")
	end
	if p_Message.type == MessageType.ServerVehicleExitMessage then 
		print("ServerVehicleExitMessage")
	end
	if p_Message.type == MessageType.ServerVehicleForceArmamentReturnMessage then 
		print("ServerVehicleForceArmamentReturnMessage")
	end
	if p_Message.type == MessageType.ServerVehicleJumpMessage then 
		print("ServerVehicleJumpMessage")
	end
	if p_Message.type == MessageType.ServerVehicleLockableMessage then 
		print("ServerVehicleLockableMessage")
	end
	if p_Message.type == MessageType.ServerVehicleSpawnDoneMessage then 
		print("ServerVehicleSpawnDoneMessage")
	end
	if p_Message.type == MessageType.ServerVehicleSwitchTeamMessage then 
		print("ServerVehicleSwitchTeamMessage")
	end
	if p_Message.type == MessageType.ServerVehicleUnspawnMessage then 
		print("ServerVehicleUnspawnMessage")
	end
	if p_Message.type == MessageType.ServerWeaponArtilleryFiredMessage then 
		print("ServerWeaponArtilleryFiredMessage")
	end
	if p_Message.type == MessageType.ServerWeaponLaserDesignatorMessage then 
		print("ServerWeaponLaserDesignatorMessage")
	end
	if p_Message.type == MessageType.ServerWeaponMortarStrikeMessage then 
		print("ServerWeaponMortarStrikeMessage")
	end
	if p_Message.type == MessageType.ServerWeaponPlayerPrimaryFireLockedShotSpawnedMessage then 
		print("ServerWeaponPlayerPrimaryFireLockedShotSpawnedMessage")
	end
	if p_Message.type == MessageType.ServerWeaponPlayerPrimaryFireShotSpawnedMessage then 
		print("ServerWeaponPlayerPrimaryFireShotSpawnedMessage")
	end
	if p_Message.type == MessageType.ServerWeaponPlayerPrimaryOutOfAmmoMessage then 
		print("ServerWeaponPlayerPrimaryOutOfAmmoMessage")
	end
	if p_Message.type == MessageType.ServerWeaponPlayerReloadMessage then 
		print("ServerWeaponPlayerReloadMessage")
	end
	if p_Message.type == MessageType.ServerWeaponPlayerResupplyMessage then 
		print("ServerWeaponPlayerResupplyMessage")
	end
	if p_Message.type == MessageType.ServerWeaponPlayerWeaponReloadEndMessage then 
		print("ServerWeaponPlayerWeaponReloadEndMessage")
	end
	if p_Message.type == MessageType.ServerWeaponPlayerWeaponRemovedMessage then 
		print("ServerWeaponPlayerWeaponRemovedMessage")
	end
	if p_Message.type == MessageType.ServerWeaponSoldierWeaponActivateMessage then 
		print("ServerWeaponSoldierWeaponActivateMessage")
	end
	if p_Message.type == MessageType.ServerWeaponSoldierWeaponReloadMessage then 
		print("ServerWeaponSoldierWeaponReloadMessage")
	end
	if p_Message.type == MessageType.ServerWeaponWeaponComponentActivateMessage then 
		print("ServerWeaponWeaponComponentActivateMessage")
	end
	if p_Message.type == MessageType.ServerWeaponWeaponComponentReloadMessage then 
		print("ServerWeaponWeaponComponentReloadMessage")
	end
	if p_Message.type == MessageType.SessionPlayerAuthenticatedMessage then 
		print("SessionPlayerAuthenticatedMessage")
	end
	if p_Message.type == MessageType.SessionPlayerJoinedMessage then 
		print("SessionPlayerJoinedMessage")
	end
	if p_Message.type == MessageType.SessionPlayerLeftMessage then 
		print("SessionPlayerLeftMessage")
	end
	if p_Message.type == MessageType.StatisticsEventMessageBase then 
		print("StatisticsEventMessageBase")
	end
	if p_Message.type == MessageType.UIAmpPortMessage then 
		print("UIAmpPortMessage")
	end
	if p_Message.type == MessageType.UIAmpServerEnabledMessage then 
		print("UIAmpServerEnabledMessage")
	end
	if p_Message.type == MessageType.UIAmpWaitForServerConnectionMessage then 
		print("UIAmpWaitForServerConnectionMessage")
	end
	if p_Message.type == MessageType.UIBundleLoadedMessage then 
		print("UIBundleLoadedMessage")
	end
	if p_Message.type == MessageType.UIClearEnable2Message then 
		print("UIClearEnable2Message")
	end
	if p_Message.type == MessageType.UIClearEnableMessage then 
		print("UIClearEnableMessage")
	end
	if p_Message.type == MessageType.UIControllerConnectedMessage then 
		print("UIControllerConnectedMessage")
	end
	if p_Message.type == MessageType.UIControllerDisconnectedMessage then 
		print("UIControllerDisconnectedMessage")
	end
	if p_Message.type == MessageType.UIControlpointChangedTeamMessage then 
		print("UIControlpointChangedTeamMessage")
	end
	if p_Message.type == MessageType.UICycleRadioChannelMessage then 
		print("UICycleRadioChannelMessage")
	end
	if p_Message.type == MessageType.UIDamageGivenToEnemyMessage then 
		print("UIDamageGivenToEnemyMessage")
	end
	if p_Message.type == MessageType.UIDialogScreenPushedMessage then 
		print("UIDialogScreenPushedMessage")
	end
	if p_Message.type == MessageType.UIDisableMoviesMessage then 
		print("UIDisableMoviesMessage")
	end
	if p_Message.type == MessageType.UIEndOfRoundReadyMessage then 
		print("UIEndOfRoundReadyMessage")
	end
	if p_Message.type == MessageType.UIEnterFrontendMessage then 
		print("UIEnterFrontendMessage")
	end
	if p_Message.type == MessageType.UIExitToMenuReasonMessage then 
		print("UIExitToMenuReasonMessage")
	end
	if p_Message.type == MessageType.UIFakedLeaderboardNativeIDMessage then 
		print("UIFakedLeaderboardNativeIDMessage")
	end
	if p_Message.type == MessageType.UIFlowDialogNodeReachedMessage then 
		print("UIFlowDialogNodeReachedMessage")
	end
	if p_Message.type == MessageType.UIFontCacheTextureHeightMessage then 
		print("UIFontCacheTextureHeightMessage")
	end
	if p_Message.type == MessageType.UIFontCacheTextureWidthMessage then 
		print("UIFontCacheTextureWidthMessage")
	end
	if p_Message.type == MessageType.UIGraphExitedMessage then 
		print("UIGraphExitedMessage")
	end
	if p_Message.type == MessageType.UIHasSuppressedEnemyMessage then 
		print("UIHasSuppressedEnemyMessage")
	end
	if p_Message.type == MessageType.UIHudChangeInventoryWeaponMessage then 
		print("UIHudChangeInventoryWeaponMessage")
	end
	if p_Message.type == MessageType.UIHudChatMessage then 
		print("UIHudChatMessage")
	end
	if p_Message.type == MessageType.UIHudDebugPauseMessage then 
		print("UIHudDebugPauseMessage")
	end
	if p_Message.type == MessageType.UIHudDistortHudMessage then 
		print("UIHudDistortHudMessage")
	end
	if p_Message.type == MessageType.UIHudEnableMinimapMessage then 
		print("UIHudEnableMinimapMessage")
	end
	if p_Message.type == MessageType.UIHudMessageMessage then 
		print("UIHudMessageMessage")
	end
	if p_Message.type == MessageType.UIHudOutputStaticMessage then 
		print("UIHudOutputStaticMessage")
	end
	if p_Message.type == MessageType.UIHudResetWinningTeamMessage then 
		print("UIHudResetWinningTeamMessage")
	end
	if p_Message.type == MessageType.UIHudReviveWeaponEquippedChangedMessage then 
		print("UIHudReviveWeaponEquippedChangedMessage")
	end
	if p_Message.type == MessageType.UIHudShowCaptureProgressMessage then 
		print("UIHudShowCaptureProgressMessage")
	end
	if p_Message.type == MessageType.UIHudShowEnemiesMessage then 
		print("UIHudShowEnemiesMessage")
	end
	if p_Message.type == MessageType.UIHudShowVoteMenuMessage then 
		print("UIHudShowVoteMenuMessage")
	end
	if p_Message.type == MessageType.UIHudShowVoteResultsMessage then 
		print("UIHudShowVoteResultsMessage")
	end
	if p_Message.type == MessageType.UIHudTicketBleedMessage then 
		print("UIHudTicketBleedMessage")
	end
	if p_Message.type == MessageType.UIHudToggleMapZoomMessage then 
		print("UIHudToggleMapZoomMessage")
	end
	if p_Message.type == MessageType.UIHudUpdateCrosshairMessage then 
		print("UIHudUpdateCrosshairMessage")
	end
	if p_Message.type == MessageType.UIHudWarningMessage then 
		print("UIHudWarningMessage")
	end
	if p_Message.type == MessageType.UIInputPressedMessage then 
		print("UIInputPressedMessage")
	end
	if p_Message.type == MessageType.UIInputStatusChangedMessage then 
		print("UIInputStatusChangedMessage")
	end
	if p_Message.type == MessageType.UILanguageMessage then 
		print("UILanguageMessage")
	end
	if p_Message.type == MessageType.UILevelFontCacheTextureHeightMessage then 
		print("UILevelFontCacheTextureHeightMessage")
	end
	if p_Message.type == MessageType.UILevelFontCacheTextureWidthMessage then 
		print("UILevelFontCacheTextureWidthMessage")
	end
	if p_Message.type == MessageType.UIMatchImagesRetrievedMessage then 
		print("UIMatchImagesRetrievedMessage")
	end
	if p_Message.type == MessageType.UIMatchUGCDisabledMessage then 
		print("UIMatchUGCDisabledMessage")
	end
	if p_Message.type == MessageType.UIMaxMinimapIconsConsoleMessage then 
		print("UIMaxMinimapIconsConsoleMessage")
	end
	if p_Message.type == MessageType.UIMaxMinimapIconsPcMessage then 
		print("UIMaxMinimapIconsPcMessage")
	end
	if p_Message.type == MessageType.UIMaximumNumberOfAwardsInEORMessage then 
		print("UIMaximumNumberOfAwardsInEORMessage")
	end
	if p_Message.type == MessageType.UIMinPredictedSightSpeedMessage then 
		print("UIMinPredictedSightSpeedMessage")
	end
	if p_Message.type == MessageType.UIOnScreenTracesMessage then 
		print("UIOnScreenTracesMessage")
	end
	if p_Message.type == MessageType.UIOutputEnabledMessage then 
		print("UIOutputEnabledMessage")
	end
	if p_Message.type == MessageType.UIOverrideMatchMessage then 
		print("UIOverrideMatchMessage")
	end
	if p_Message.type == MessageType.UIOverrideRootedPlayersMessage then 
		print("UIOverrideRootedPlayersMessage")
	end
	if p_Message.type == MessageType.UIPlayerVehicleHealthChangeMessage then 
		print("UIPlayerVehicleHealthChangeMessage")
	end
	if p_Message.type == MessageType.UIPlayerVehicleListRefreshMessage then 
		print("UIPlayerVehicleListRefreshMessage")
	end
	if p_Message.type == MessageType.UIPopupHideMessage then 
		print("UIPopupHideMessage")
	end
	if p_Message.type == MessageType.UIPopupShowMessage then 
		print("UIPopupShowMessage")
	end
	if p_Message.type == MessageType.UIPreEndOfRoundReadyMessage then 
		print("UIPreEndOfRoundReadyMessage")
	end
	if p_Message.type == MessageType.UIReadyForUnloadMessage then 
		print("UIReadyForUnloadMessage")
	end
	if p_Message.type == MessageType.UIRenderCommandBufferSizeMessage then 
		print("UIRenderCommandBufferSizeMessage")
	end
	if p_Message.type == MessageType.UIRenderEnableMessage then 
		print("UIRenderEnableMessage")
	end
	if p_Message.type == MessageType.UIRenderIndexBufferSizeMessage then 
		print("UIRenderIndexBufferSizeMessage")
	end
	if p_Message.type == MessageType.UIRenderLevelCommandBufferSizeMessage then 
		print("UIRenderLevelCommandBufferSizeMessage")
	end
	if p_Message.type == MessageType.UIRenderLevelIndexBufferSizeMessage then 
		print("UIRenderLevelIndexBufferSizeMessage")
	end
	if p_Message.type == MessageType.UIRenderLevelVertexBufferSizeMessage then 
		print("UIRenderLevelVertexBufferSizeMessage")
	end
	if p_Message.type == MessageType.UIRenderPcBufferScaleMessage then 
		print("UIRenderPcBufferScaleMessage")
	end
	if p_Message.type == MessageType.UIRenderVertexBufferSizeMessage then 
		print("UIRenderVertexBufferSizeMessage")
	end
	if p_Message.type == MessageType.UIRequestEndOfRoundMessage then 
		print("UIRequestEndOfRoundMessage")
	end
	if p_Message.type == MessageType.UIRequestMatchImagesMessage then 
		print("UIRequestMatchImagesMessage")
	end
	if p_Message.type == MessageType.UIRequestPreEndOfRoundMessage then 
		print("UIRequestPreEndOfRoundMessage")
	end
	if p_Message.type == MessageType.UIReturnMouseToUIMessage then 
		print("UIReturnMouseToUIMessage")
	end
	if p_Message.type == MessageType.UIScreenCountChangeMessage then 
		print("UIScreenCountChangeMessage")
	end
	if p_Message.type == MessageType.UIScreenLoadedMessage then 
		print("UIScreenLoadedMessage")
	end
	if p_Message.type == MessageType.UISetValueMessage then 
		print("UISetValueMessage")
	end
	if p_Message.type == MessageType.UIShowDebugInfoMessage then 
		print("UIShowDebugInfoMessage")
	end
	if p_Message.type == MessageType.UIShowDebugStringTagMessage then 
		print("UIShowDebugStringTagMessage")
	end
	if p_Message.type == MessageType.UIShowMinimapMessage then 
		print("UIShowMinimapMessage")
	end
	if p_Message.type == MessageType.UIShowProgressObjectMessage then 
		print("UIShowProgressObjectMessage")
	end
	if p_Message.type == MessageType.UIShowScoreboardMessage then 
		print("UIShowScoreboardMessage")
	end
	if p_Message.type == MessageType.UISkipIntroVideoSequenceMessage then 
		print("UISkipIntroVideoSequenceMessage")
	end
	if p_Message.type == MessageType.UISoldierChangeMessage then 
		print("UISoldierChangeMessage")
	end
	if p_Message.type == MessageType.UISoldierHitUpdatedMessage then 
		print("UISoldierHitUpdatedMessage")
	end
	if p_Message.type == MessageType.UISquadStatusChangedMessage then 
		print("UISquadStatusChangedMessage")
	end
	if p_Message.type == MessageType.UIStartSinglePlayerMenuMessage then 
		print("UIStartSinglePlayerMenuMessage")
	end
	if p_Message.type == MessageType.UIStereoInteractionCompDepthMessage then 
		print("UIStereoInteractionCompDepthMessage")
	end
	if p_Message.type == MessageType.UITrackedAwardChangedMessage then 
		print("UITrackedAwardChangedMessage")
	end
	if p_Message.type == MessageType.UITrackedAwardUpdatedMessage then 
		print("UITrackedAwardUpdatedMessage")
	end
	if p_Message.type == MessageType.UITransitionEffectStartMessage then 
		print("UITransitionEffectStartMessage")
	end
	if p_Message.type == MessageType.UITransitionEffectStopMessage then 
		print("UITransitionEffectStopMessage")
	end
	if p_Message.type == MessageType.UITriggerPopupHideMessage then 
		print("UITriggerPopupHideMessage")
	end
	if p_Message.type == MessageType.UITriggerPopupShowMessage then 
		print("UITriggerPopupShowMessage")
	end
	if p_Message.type == MessageType.UIUpdateEnableMessage then 
		print("UIUpdateEnableMessage")
	end
	if p_Message.type == MessageType.UIUserConnectedMessage then 
		print("UIUserConnectedMessage")
	end
	if p_Message.type == MessageType.UIUserDisconnectedMessage then 
		print("UIUserDisconnectedMessage")
	end
	if p_Message.type == MessageType.UIUserNotificationMessage then 
		print("UIUserNotificationMessage")
	end
	if p_Message.type == MessageType.UIUserSkippedLoginMessage then 
		print("UIUserSkippedLoginMessage")
	end
	if p_Message.type == MessageType.UIVKBDInputDoneMessage then 
		print("UIVKBDInputDoneMessage")
	end
	if p_Message.type == MessageType.UIVehicleHitDirectionUpdatedMessage then 
		print("UIVehicleHitDirectionUpdatedMessage")
	end
	if p_Message.type == MessageType.UIVerboseOutputEnabledMessage then 
		print("UIVerboseOutputEnabledMessage")
	end
	if p_Message.type == MessageType.UINetworkAdminYellMessage then 
		print("UINetworkAdminYellMessage")
	end
	if p_Message.type == MessageType.UINetworkAllowSkipVideoMessage then 
		print("UINetworkAllowSkipVideoMessage")
	end
	if p_Message.type == MessageType.UINetworkCoopGameMessage then 
		print("UINetworkCoopGameMessage")
	end
	if p_Message.type == MessageType.UINetworkCoopPlayerMessage then 
		print("UINetworkCoopPlayerMessage")
	end
	if p_Message.type == MessageType.UINetworkEnableHudMessage then 
		print("UINetworkEnableHudMessage")
	end
	if p_Message.type == MessageType.UINetworkEndOfRoundBonusMessage then 
		print("UINetworkEndOfRoundBonusMessage")
	end
	if p_Message.type == MessageType.UINetworkGunMasterNotificationMessage then 
		print("UINetworkGunMasterNotificationMessage")
	end
	if p_Message.type == MessageType.UINetworkHudScoringMessage then 
		print("UINetworkHudScoringMessage")
	end
	if p_Message.type == MessageType.UINetworkHudTextMessage then 
		print("UINetworkHudTextMessage")
	end
	if p_Message.type == MessageType.UINetworkHudTooltipMessage then 
		print("UINetworkHudTooltipMessage")
	end
	if p_Message.type == MessageType.UINetworkKilledOtherPlayerMessage then 
		print("UINetworkKilledOtherPlayerMessage")
	end
	if p_Message.type == MessageType.UINetworkMenuResponseMessage then 
		print("UINetworkMenuResponseMessage")
	end
	if p_Message.type == MessageType.UINetworkOwnPlayerKilledMessage then 
		print("UINetworkOwnPlayerKilledMessage")
	end
	if p_Message.type == MessageType.UINetworkPlayVideoMessage then 
		print("UINetworkPlayVideoMessage")
	end
	if p_Message.type == MessageType.UINetworkPlayerCollectibleTextMessage then 
		print("UINetworkPlayerCollectibleTextMessage")
	end
	if p_Message.type == MessageType.UINetworkPlayerConnectMessage then 
		print("UINetworkPlayerConnectMessage")
	end
	if p_Message.type == MessageType.UINetworkPlayerDeserterTextMessage then 
		print("UINetworkPlayerDeserterTextMessage")
	end
	if p_Message.type == MessageType.UINetworkPlayerDisconnectMessage then 
		print("UINetworkPlayerDisconnectMessage")
	end
	if p_Message.type == MessageType.UINetworkPlayerKillsTextMessage then 
		print("UINetworkPlayerKillsTextMessage")
	end
	if p_Message.type == MessageType.UINetworkPlayerMissionObjectiveTextMessage then 
		print("UINetworkPlayerMissionObjectiveTextMessage")
	end
	if p_Message.type == MessageType.UINetworkPlayerRankUpMessage then 
		print("UINetworkPlayerRankUpMessage")
	end
	if p_Message.type == MessageType.UINetworkPlayerTextMessage then 
		print("UINetworkPlayerTextMessage")
	end
	if p_Message.type == MessageType.UINetworkPlayerTutorialInstructionsTextMessage then 
		print("UINetworkPlayerTutorialInstructionsTextMessage")
	end
	if p_Message.type == MessageType.UINetworkRollCreditsMessage then 
		print("UINetworkRollCreditsMessage")
	end
	if p_Message.type == MessageType.UINetworkRoundWarningMessage then 
		print("UINetworkRoundWarningMessage")
	end
	if p_Message.type == MessageType.UINetworkSetCoopServerLobbyGameTypeMessage then 
		print("UINetworkSetCoopServerLobbyGameTypeMessage")
	end
	if p_Message.type == MessageType.UINetworkSetPlayerTeamMessage then 
		print("UINetworkSetPlayerTeamMessage")
	end
	if p_Message.type == MessageType.UINetworkSkipVideoMessage then 
		print("UINetworkSkipVideoMessage")
	end
	if p_Message.type == MessageType.UINetworkSquadWipeInstigatorMessage then 
		print("UINetworkSquadWipeInstigatorMessage")
	end
	if p_Message.type == MessageType.UINetworkSquadWipeMessage then 
		print("UINetworkSquadWipeMessage")
	end
	if p_Message.type == MessageType.UINetworkStealBodyMessage then 
		print("UINetworkStealBodyMessage")
	end
	if p_Message.type == MessageType.UINetworkStopVideoMessage then 
		print("UINetworkStopVideoMessage")
	end
	if p_Message.type == MessageType.UINetworkVideoDoneMessage then 
		print("UINetworkVideoDoneMessage")
	end
	if p_Message.type == MessageType.UINetworkVoiceOverSubtitleTextMessage then 
		print("UINetworkVoiceOverSubtitleTextMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkAllWeaponKillsMessage then 
		print("VeniceNetworkAllWeaponKillsMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkAwardGainedMessage then 
		print("VeniceNetworkAwardGainedMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkCreateSquadMessage then 
		print("VeniceNetworkCreateSquadMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkDefibKillMessage then 
		print("VeniceNetworkDefibKillMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkDogTagMessage then 
		print("VeniceNetworkDogTagMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkEorStatsMessage then 
		print("VeniceNetworkEorStatsMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkFocusPointMessage then 
		print("VeniceNetworkFocusPointMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkFriendZoneDebugEnableMessage then 
		print("VeniceNetworkFriendZoneDebugEnableMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkFriendZoneMessage then 
		print("VeniceNetworkFriendZoneMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkJoinSquadByOnlineIdMessage then 
		print("VeniceNetworkJoinSquadByOnlineIdMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkJoinSquadMessage then 
		print("VeniceNetworkJoinSquadMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkKickFromSquadMessage then 
		print("VeniceNetworkKickFromSquadMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkLeaveSquadMessage then 
		print("VeniceNetworkLeaveSquadMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkLocalClientConnectionInfoUpdatedMessage then 
		print("VeniceNetworkLocalClientConnectionInfoUpdatedMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkMetricsAchievementMessage then 
		print("VeniceNetworkMetricsAchievementMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkMissionAwardsStatusMessage then 
		print("VeniceNetworkMissionAwardsStatusMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkNotifyMicStateMessage then 
		print("VeniceNetworkNotifyMicStateMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkOnPlayerSelectedTeamMessage then 
		print("VeniceNetworkOnPlayerSelectedTeamMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkOrderMessage then 
		print("VeniceNetworkOrderMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkRemoveOrderMessage then 
		print("VeniceNetworkRemoveOrderMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkRequestAllWeaponKillsMessage then 
		print("VeniceNetworkRequestAllWeaponKillsMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkRequestCoopEorStatsMessage then 
		print("VeniceNetworkRequestCoopEorStatsMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkRequestCoopPlayerInformationMessage then 
		print("VeniceNetworkRequestCoopPlayerInformationMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkRequestEorStatsMessage then 
		print("VeniceNetworkRequestEorStatsMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkReviveFinishedMessage then 
		print("VeniceNetworkReviveFinishedMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkReviveInitiatedMessage then 
		print("VeniceNetworkReviveInitiatedMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkReviveResponseMessage then 
		print("VeniceNetworkReviveResponseMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkSendTelemetryTokenMessage then 
		print("VeniceNetworkSendTelemetryTokenMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkSetPingMessage then 
		print("VeniceNetworkSetPingMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkSetSquadPrivacyMessage then 
		print("VeniceNetworkSetSquadPrivacyMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkSetVoiceChannelMessage then 
		print("VeniceNetworkSetVoiceChannelMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkSpotExplosionPackMessage then 
		print("VeniceNetworkSpotExplosionPackMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkSpottingMessage then 
		print("VeniceNetworkSpottingMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkUnlockAchievementsMessage then 
		print("VeniceNetworkUnlockAchievementsMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkUnlockGainedMessage then 
		print("VeniceNetworkUnlockGainedMessage")
	end
	if p_Message.type == MessageType.VeniceNetworkVirtualGameStateMessag then 
		print("VeniceNetworkVirtualGameStateMessag")
	end
end



g_MessageDebugShared = MessageDebugShared()

