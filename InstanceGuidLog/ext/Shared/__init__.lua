class 'InstanceGuidLogShared'


function InstanceGuidLogShared:__init()
	print("Initializing InstanceGuidLogShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function InstanceGuidLogShared:RegisterVars()
	--self.m_this = that
end


function InstanceGuidLogShared:RegisterEvents()
	self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
end


function InstanceGuidLogShared:ReadInstance(p_Instance,p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end
	
	print(string.format("%s | %s | %s", p_Instance.typeName, p_PartitionGuid:ToString('D'), p_Guid:ToString('D')))
end


g_InstanceGuidLogShared = InstanceGuidLogShared()

