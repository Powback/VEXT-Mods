class 'coopreviveShared'


function coopreviveShared:__init()
	print("Initializing coopreviveShared")
	self:RegisterVars()
	self:RegisterEvents()
end


function coopreviveShared:RegisterVars()
	self.m_TeamData = nil
	self.m_Coop = nil
	self.m_Changed = false
	self.m_Changed2 = false
    self.m_BundleHandle = nil
  
  	self.m_1 = nil
  	self.m_2 = nil
  	self.m_3 = nil
 	self.m_4 = nil
  	self.m_5 = nil
	self.m_6 = nil
 	self.m_7 = nil
end
 
function coopreviveShared:RegisterEvents()
    self.m_LevelLoadEvent = Events:Subscribe("Level:LoadResources", self, self.OnLoadResources)
    self.m_ReadInstanceEvent = Events:Subscribe('Partition:ReadInstance', self, self.ReadInstance)
    self.m_UpdateEvent = Events:Subscribe('Engine:Update', self, self.OnUpdate)
end
 
function coopreviveShared:OnLoadResources(p_Dedicated)
    print("Loading level resources!")
   -- ResourceManager:MountSuperBundle("Levels/COOP_002/COOP_002")

   -- self.m_BundleHandle = ResourceManager:BeginLoadData(3, {
   --     'levels/coop_002/coop_002'
   -- })
end
 
function coopreviveShared:OnUpdate(p_Delta, p_SimDelta)
    if self.m_BundleHandle == nil then
        return
    end
 
    if not ResourceManager:PollBundleOperation(self.m_BundleHandle) then
        return
    end
 
    self.m_BundleHandle = nil
 
    if not ResourceManager:EndLoadData(self.m_BundleHandle) then
        print('Bundles failed to load')
        return
    end
 
    print('Bundles successfully loaded')
end




function coopreviveShared:ReadInstance(p_Instance,p_PartitionGuid, p_Guid)
	if p_Instance == nil then
		return
	end

	if p_Instance.typeName == "SoldierEntityData" then
		local s_Instance = SoldierEntityData(p_Instance)
		--s_Instance.interactiveManDownAllowed = true
		s_Instance.lowerGunOnOwnTeam = true 
	end


	if p_Instance.typeName == "VeniceSoldierHealthModuleData" then
		local s_Instance = VeniceSoldierHealthModuleData(p_Instance)
		--s_Instance.interactiveManDown = true
		s_Instance.interactiveManDownPoseConstraints.standPose = false
		s_Instance.interactiveManDownPoseConstraints.crouchPose = false

		s_Instance.binding.interactiveManDown.assetId = 357042550

		s_Instance.manDownStateHealthPoints = 9
		s_Instance.postReviveResponseTime = 5
		s_Instance.postReviveHealth = 10 
		s_Instance.sprintDisabledWhenDamagedTime = 1
		s_Instance.sprintDisabledDamageThreshold = 10
		s_Instance.timeForCorpse = 99999 
		
		--s_Instance.interactiveManDownThreshold
	end

	if p_Instance.typeName == "EntityInteractionComponentData" then
		local s_Instance = EntityInteractionComponentData(p_Instance)
		s_Instance.allowInteractionWithSoldiers = true
		s_Instance.onlyAllowInteractionWithManDownSoldiers = true
	end

	

end


g_coopreviveShared = coopreviveShared()

