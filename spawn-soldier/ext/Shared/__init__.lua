local m_vuExtensions = require "__shared/VUExtensions"

local dimaUnlockAsset = nil
local camoDrPepperUnlockAsset = nil

function OnPartitionLoaded(p_Partition)
	if p_Partition == nil then
		return
	end
	
	local s_Instances = p_Partition.instances


	for _, l_Instance in ipairs(s_Instances) do
		if l_Instance == nil then
			print('Instance is null?')
			break
		end

		if l_Instance.instanceGuid == Guid('643375F5-8FA6-484F-B6BA-92DF19C0F023') then
			print("Found dima !!")
			dimaUnlockAsset = m_vuExtensions:PrepareInstanceForEdit(p_Partition, l_Instance)
			--dimaUnlockAsset.linkedTo:erase(3)
		end	


		if l_Instance.instanceGuid == Guid('E628556D-6CA5-4B9D-851A-CCE10BF6A59B') then --UI/Art/Persistence/Camo/U_CAMO_DrPepper
			print("Found U_CAMO_DrPepper")

			if dimaUnlockAsset == nil then
				print("but dima was nil")
				return
			end

			camoDrPepperUnlockAsset = m_vuExtensions:PrepareInstanceForEdit(p_Partition, l_Instance)
			--camoDrPepperUnlockAsset.linkedTo:add(dimaUnlockAsset)

			--for _, linkedTo in pairs(camoDrPepperUnlockAsset.linkedTo) do
				-- local unlockAsset = UnlockAsset(linkedTo)
				-- print(unlockAsset.name)
			--	print(_)
			--end
		end

		-- if l_Instance.instanceGuid == Guid('0A19B0DA-2EB1-DA48-8B7A-3D7B45D32BAC') then
		-- 	print("Found head sp_dima_head_mesh !!")
		-- end
		
		-- if l_Instance.instanceGuid == Guid('4D495953-B532-845C-232F-1CCC207F506') then
		-- 	print("Found upperbody sp_rus_alexey_mesh !! <<<<<<<<<<")
		-- end

		-- if l_Instance.instanceGuid == Guid('A6EDA21B-2817-6B88-D987-BB8A82CFE293') then
		-- 	print("Found base arms arms1p_rus_mesh !!")
		-- end
		
		-- if l_Instance.instanceGuid == Guid('2A971044-2DF9-D29A-C7C8-81E5521D3E21') then
		-- 	print("Found variation arms arms1p_rus_dima !!")
		-- end
	end
end


function OnLoadBundle(p_Hook, p_Bundle)
	print(p_Bundle)
	if(p_Bundle == "UI/Flow/Bundle/LoadingBundleMp" or p_Bundle == "gameconfigurations/game") then
		
		Events:Dispatch('BundleMounter:LoadBundle', 'levels/sp_villa/sp_villa', {
			-- "levels/sp_villa/background",
			-- "levels/sp_villa/rail",
			-- "levels/sp_villa/lightmap_01",
			-- "levels/sp_villa/drive_2",
			"levels/sp_villa/lightmap_cutscene01",
			-- "levels/sp_villa/sp_villa_uiplaying",
			"levels/sp_villa/sp_villa"
			-- "levels/sp_villa/sp_villa_uiendofround",
			-- "levels/sp_villa/lightmap_03",
			-- "levels/sp_villa/sp_villa_uiloadingsp",
			-- "levels/sp_villa/villa",
			-- "levels/sp_villa/sp_villa_loading_music",
			-- "levels/sp_villa/drive_pc",
			-- "levels/sp_villa/poolhouse",
			-- "levels/sp_villa/drive",
			-- "levels/sp_villa/lightmap_02",
			-- "levels/sp_villa/sp_villa_settings_win32",
			-- "levels/sp_villa/halo",
			-- "levels/sp_villa/garden_pc",
			-- "levels/sp_villa/sp_villa_gameconfiglight_win32",
			-- "levels/sp_villa/basement",
			-- "levels/sp_villa/blackburn",
			-- "levels/sp_villa/sp_villa_uipreendofround",
			-- "levels/sp_villa/halo_backdrop",
			-- "levels/sp_villa/landing",
			-- "levels/sp_villa/garden",
			-- "levels/sp_villa/chopper",
			-- "levels/sp_villa/villa_pc",
			-- "levels/sp_villa/villa_extra",
			-- "levels/sp_villa/gatehouse",
			-- "levels/sp_villa/poolhouse_extra",
			-- "levels/sp_villa/railsuv",
			-- "levels/sp_villa/lightmap_cutscene02",
			-- "levels/sp_villa/sp_villa_uiloadingmp",
		})
	end
end

Hooks:Install('ResourceManager:LoadBundle', 99, OnLoadBundle)
Events:Subscribe('Partition:Loaded', OnPartitionLoaded)