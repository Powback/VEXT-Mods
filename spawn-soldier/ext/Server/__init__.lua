function string:split(sep)
   local sep, fields = sep or ":", {}
   local pattern = string.format("([^%s]+)", sep)
   self:gsub(pattern, function(c) fields[#fields+1] = c end)
   return fields
end

--

local soldierAsset = nil
local soldierBlueprint = nil
local weapon = nil
local weaponAtt0 = nil
local weaponAtt1 = nil
local drPepper = nil

function OnChat(player, recipientMask, message)
	if message == '' then
		return
	end

	print('Chat: ' .. message)

	local parts = message:split(' ')

	if parts[1] == 'spawn' then
		SpawnPlayer(player)
	end
end

function OnPartitionLoaded(p_Partition)

	if p_Partition == nil then
		error('OnPartitionLoaded() - p_Partition was nil')
		return
	end

	for _, instance in ipairs(p_Partition.instances) do
		if instance == nil then
			print('Instance is null?')
			break
		end

		-- if instance.instanceGuid == Guid('643375F5-8FA6-484F-B6BA-92DF19C0F023') then
		-- 	print("Found dima !!")
		-- end	

		if instance.typeInfo.name == 'VeniceSoldierCustomizationAsset' then
			local asset = VeniceSoldierCustomizationAsset(instance)

			if asset.name == 'Gameplay/Kits/RURecon' then
				print('Found soldier customization asset ' .. asset.name)
				soldierAsset = asset
			end
		end

		if instance.typeInfo.name == 'SoldierBlueprint' then
			soldierBlueprint = SoldierBlueprint(instance)
			print('Found soldier blueprint ' .. soldierBlueprint.name)
		end

		if instance.typeInfo.name == 'SoldierWeaponUnlockAsset' then
			local asset = SoldierWeaponUnlockAsset(instance)

			if asset.name == 'Weapons/M416/U_M416' then
				print('Found soldier weapon unlock asset ' .. asset.name)
				weapon = asset
			end
		end
		if instance.typeInfo.name == 'UnlockAsset' then
			local asset = UnlockAsset(instance)

			if asset.name == 'Weapons/M416/U_M416_ACOG' then
				print('Found weapon unlock asset ' .. asset.name)
				weaponAtt0 = asset
			end

			if asset.name == 'Weapons/M416/U_M416_Silencer' then
				print('Found weapon unlock asset ' .. asset.name)
				weaponAtt1 = asset
			end

			-- if asset.name == 'Persistence/Unlocks/Soldiers/Visual/MP/RU/MP_RU_Recon_Appearance_DrPepper' then
			if asset.name == 'Persistence/Unlocks/Soldiers/Visual/SP/SP_RUS_Dima_Fullbody01' then
				print('Found DIMA !! : ' .. asset.name)
				drPepper = asset
			end
		end

	end
end

function SpawnPlayer(player)
	if player == nil or player.soldier ~= nil then
		print('Player must be dead to spawn')
		return
	end

	local transform = LinearTransform(
		Vec3(1, 0, 0),
		Vec3(0, 1, 0),
		Vec3(0, 0, 1),
		Vec3(0, 200, 0)
	)

	print('Setting soldier primary weapon')
	player:SelectWeapon(WeaponSlot.WeaponSlot_0, weapon, { weaponAtt0, weaponAtt1 })

	print('Setting soldier class and appearance')
	player:SelectUnlockAssets(soldierAsset, { drPepper })

	print('Creating soldier')
	local soldier = player:CreateSoldier(soldierBlueprint, transform)

	if soldier == nil then
		print('Failed to create player soldier')
		return
	end

	print('Spawning soldier')
	player:SpawnSoldierAt(soldier, transform, CharacterPoseType.CharacterPoseType_Stand)

	print('Soldier spawned')
	print(tostring(drPepper))
end

function OnLoadLevel()
	soldierAsset = nil
	soldierBlueprint = nil
	weapon = nil
	weaponAtt0 = nil
	weaponAtt1 = nil
	drPepper = nil
end

Events:Subscribe('Player:Chat', OnChat)
Events:Subscribe('Partition:Loaded', OnPartitionLoaded)
	-- Events:Subscribe('Partition:ReadInstance', OnInstance)
Events:Subscribe('Level:LoadResources', OnLoadLevel)